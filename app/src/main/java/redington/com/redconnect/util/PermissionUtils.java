package redington.com.redconnect.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;

public class PermissionUtils {
    private Activity currentActivity;
    private PermissionResultCallback permissionResultCallback;
    private List<String> permissionList = new ArrayList<>();
    private ArrayList<String> listPermissionsNeeded = new ArrayList<>();
    private String dialogContent = "";
    private int reqcode;

    public PermissionUtils(Context context) {
        this.currentActivity = (Activity) context;

        permissionResultCallback = (PermissionResultCallback) context;
    }

    public void checkPermission(List<String> permissions, String dialogcontent, int requestcode) {
        this.permissionList = permissions;
        this.dialogContent = dialogcontent;
        this.reqcode = requestcode;

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkAndRequestPermissions(permissions, requestcode)) {
                permissionResultCallback.permissionGranted(requestcode);
            }
        } else {
            permissionResultCallback.permissionGranted(requestcode);

        }

    }

    private boolean checkAndRequestPermissions(List<String> permissions, int reqcode) {

        if (!permissions.isEmpty()) {
            listPermissionsNeeded = new ArrayList<>();

            for (int i = 0; i < permissions.size(); i++) {
                int hasPermission = ContextCompat.checkSelfPermission(currentActivity, permissions.get(i));

                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissions.get(i));
                }

            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(currentActivity, listPermissionsNeeded
                        .toArray(new String[listPermissionsNeeded.size()]), reqcode);
                return false;
            }
        }

        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            BaseActivity.logd(String.valueOf(requestCode));
            if (grantResults.length > 0) {
                Map<String, Integer> perms = new HashMap<>();

                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                checkPermissionList(perms);


            }

        }
    }

    private void checkPermissionList(Map<String, Integer> perms) {
        final ArrayList<String> pendingPermissions = new ArrayList<>();

        for (int i = 0; i < listPermissionsNeeded.size(); i++) {
            if (perms.get(listPermissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(currentActivity, listPermissionsNeeded.get(i)))
                    pendingPermissions.add(listPermissionsNeeded.get(i));
                else {
                    permissionResultCallback.neverAskAgain(reqcode);
                    showSettingPage();
                    return;
                }
            }

        }

        if (!pendingPermissions.isEmpty()) {
            showMessageOKCancel(dialogContent,
                    (dialog, which) -> {

                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            checkPermission(permissionList, dialogContent, reqcode);

                        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                            if (permissionList.size() == pendingPermissions.size())
                                permissionResultCallback.permissionDenied(reqcode);
                            else
                                permissionResultCallback.partialPermissionGranted(reqcode, pendingPermissions);

                        }


                    });

        } else {
            permissionResultCallback.permissionGranted(reqcode);

        }


    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(currentActivity)
                .setMessage(message)
                .setPositiveButton(GlobalConstants.OK, okListener)
                .setNegativeButton(GlobalConstants.CANCEL, okListener)
                .create()
                .show();
    }

    private void showSettingPage() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(currentActivity);
        alertDialogBuilder
                .setMessage(GlobalConstants.PERMISSIONS)
                .setPositiveButton(GlobalConstants.Y, (dialog, which) -> {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", currentActivity.getPackageName(), null);
                    intent.setData(uri);
                    currentActivity.startActivity(intent);
                })
                .setNegativeButton(GlobalConstants.N, (dialog, which) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


}
