package redington.com.redconnect.util;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;

public class EmptyCartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_empty_cart);

        findView();
    }

    /**
     * <p>Initialize UI references and update content</p>
     */
    private void findView() {
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        toolbar(IntentConstants.CART);
    }

    /**
     * <p>
     *     This method in turn calls the  {@link redington.com.redconnect.common.activity.BaseActivity#setMenuBack(Context) setMenuBack} method,
     *     defined in the BaseActivity. This will finish the current Activity
     *     with a slide down animation.
     * </p>
     * @param view  the id of the view that was clicked
     */
    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    /**
     * <p>
     *     Closes the current activity with an animation and opens
     *     to the Dashboard Activity. 
     * </p>
     * @param view
     */
    public void mContinueShopping(View view) {
        LaunchIntentManager.routeToActivity(view.getContext(), DashboardActivity.class);
        overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.close_background);
    }


}
