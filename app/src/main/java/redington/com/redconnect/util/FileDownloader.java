package redington.com.redconnect.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import redington.com.redconnect.common.activity.BaseActivity;

public class FileDownloader
{

    private static final int MEGABYTE = 1024 * 1024;


    private FileDownloader() {
        throw new IllegalStateException();
    }

    /**
     * <p>
     *     Downloads the file from the provided Url and saves to
     *     to the specified local file path.
     * </p>
     * @param fileUrl       the url of the file that needs to be downloaded.
     * @param directory     the local path to the file to be saved.
     * @return
     * @throws  MalformedURLException when the provided url is invalid
     * @throws  FileNotFoundException if the specified local directory does not exist
     * @throws  IOException           when the connection to the File fails or could not be created.
     */
    public static boolean downloadFile(String fileUrl, File directory) {  /*Kamesnh*/
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            try (FileOutputStream fileOutputStream = new FileOutputStream(directory)) {

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.close();
                return true;  /*Kamesh*/
            }

        } catch (FileNotFoundException e) {
            BaseActivity.logd(e.getMessage());
        } catch (MalformedURLException e) {
            BaseActivity.logd(e.getMessage());
        } catch (IOException e) {
            BaseActivity.logd(e.getMessage());
        }

        return false;  /*Kamesh*/
    }


}
