package redington.com.redconnect.util;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;

import redington.com.redconnect.constants.GlobalConstants;

public class CommonUtils
{
    private Context mContext;
    private ProgressDialog mProgressDialog;

    /**
     * <p>This is a single parameter constructor that
     *      saves a copy of the supplied context in the class.
     * </p>
     * @param context   the context , usualy the Activity Context.
     */
    public CommonUtils(Context context) {
        this.mContext = context;
    }

    /**
     * <p>
     *     Checks if a Network connection is currently available or not
     *     by using the ConnectivityManager System Service
     * </p>
     * @param context   the current context , required to instanciate the ConnectivityManager service
     * @return          returns true if a Network cinnection exists , false otherwise
     */
    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected();
    }

    /**
     * <p>
     *     Create and display a ProgressDialog at  runtime with
     *     a single fixed message. This is a non cancelable dialog.<br/><br/>
     *     Note. The instance of the progressDialog is saved <b>globally</b> in the <b>CommonUtils</b> object.
     * </p>
     */
    public void showProgressDialog() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage(GlobalConstants.PLEASE_WAIT);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }

    /**
     * Dismiss the ProgressDialog whose reference is currently saved
     * in the CommonUtils instance.
     */
    public void dismissProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
            //progressDialogDismissValidation();
        }
    }

    /**
     * <p>
     *      Dismiss the ProgressDialog whose reference is currently saved
     *      in the CommonUtils instance. A validation is performed on the
     *      Activity to confirm if it is still active.<br/><br/>
     *
     *      Note. This method is currently not in use.
     * </p>
     */
    private void progressDialogDismissValidation() {
        if (mContext instanceof Activity) {
            if (!((Activity) mContext).isFinishing()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!(((Activity) mContext).isDestroyed())) {
                        dismissDialogWithException();
                    }
                } else {
                    dismissDialogWithException();
                }

            } else {
                dismissDialogWithException();
            }
        }
        mProgressDialog.dismiss();
        mProgressDialog = null;
    }

    /**
     * <p>
     *      Dismiss the ProgressDialog whose reference is currently saved
     *      in the CommonUtils instance. This method force dismisses the progressDialog
     *      without any validation.
     * </p>
     */
    private void dismissDialogWithException() {
        try {
            mProgressDialog.dismiss();
        } catch (final IllegalArgumentException e) {
            //Noithing.
        } catch (final Exception e) {
            //Nothing.
        } finally {
            mProgressDialog = null;
        }
    }


    /**
     * <p>
     *     Sets a Circular Gradient as the background to the provided view
     *     The gradient is created at runtime from the supplied  colorcode and
     *     corner radius.
     * </p>
     * @param idView        the view to which the gradient must be set.
     * @param colorCode     the start color of the gradient
     * @param radius        the x,y corner radii supplied as a array of 8 int values.
     */
    public void setGradientColor(View idView, int colorCode, float[] radius) {
        int color = ContextCompat.getColor(mContext, colorCode);
        int r = Color.red(color);
        int g = Color.green(color) + 10;
        int b = Color.blue(color) + 10;
        int endColorCode = Color.argb(255, r, g, b);

        int[] gradientColor = {colorCode, endColorCode};
        GradientDrawable drawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT, gradientColor);
        drawable.setCornerRadii(radius);
        idView.setBackground(drawable);
    }

    /**
     * <p>
     *     Sets a Linear Gradient as the background to the provided view
     *     It is a Vertical Gradient drawn Top to Bottom
     *     The gradient is created at runtime from the supplied  start and end colors
     *     and corner radius.
     * </p>
     *
     * @param idView        the view to which the gradient must be set.
     * @param startColor    the start color of the gradient
     * @param endColor      the end Color of the gradient
     * @param radius        the x,y corner radii supplied as a array of 8 int values.
     */
    public void setTwoGradientColor(View idView, int startColor, int endColor, float[] radius) {
        int colorSart = ContextCompat.getColor(mContext, startColor);
        int colorEnd = ContextCompat.getColor(mContext, endColor);
        int[] gradientColor = {colorSart, colorEnd};
        GradientDrawable drawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, gradientColor);
        drawable.setCornerRadii(radius);
        idView.setBackground(drawable);
    }

    /**
     * <p>
     *     Sets a Linear Gradient as the background to the provided view
     *     It is a Horizontal Gradient drawn Left to Right
     *     The gradient is created at runtime from the supplied start and end colors
     *     and corner radius.
     * </p>
     *
     * @param idView        the view to which the gradient must be set.
     * @param startColor    the start color of the gradient
     * @param endColor      the end Color of the gradient
     * @param radius        the x,y corner radii supplied as a array of 8 int values.
     */
    public void setTwoGradientLoginColor(View idView, int startColor, int endColor, float[] radius) {
        int colorSart = ContextCompat.getColor(mContext, startColor);
        int colorEnd = ContextCompat.getColor(mContext, endColor);
        int[] gradientColor = {colorSart, colorEnd};
        GradientDrawable drawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT, gradientColor);
        drawable.setCornerRadii(radius);
        idView.setBackground(drawable);

    }

}
