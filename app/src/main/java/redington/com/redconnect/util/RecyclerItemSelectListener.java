package redington.com.redconnect.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;


public class RecyclerItemSelectListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector mGestureDetector;
    private TouchPosition mGetPosition;

    public RecyclerItemSelectListener(Context mContext, final RecyclerView mRecyclerView, final TouchPosition mListener) {
        mGetPosition = mListener;
        mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View view = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                if (view != null && mListener != null) {
                    mGetPosition.onLongItemClick(view, mRecyclerView.getChildAdapterPosition(view));
                }
            }
        });

    }


    @Override
    public boolean onInterceptTouchEvent(final RecyclerView rv, MotionEvent e) {
        View view = rv.findChildViewUnder(e.getX(), e.getY());
        if (view != null && mGetPosition != null && mGestureDetector.onTouchEvent(e)) {
            mGetPosition.onItemClick(view, rv.getChildAdapterPosition(view));
            rv.setEnabled(false);
            return true;
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        //Not in use
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        //Not in use
    }

    public interface TouchPosition {
        void onItemClick(View view, int position);

        void onLongItemClick(View view, int position);

    }
}
