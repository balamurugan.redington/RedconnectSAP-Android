package redington.com.redconnect.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class LaunchIntentManager {

    private LaunchIntentManager() {
        throw new IllegalStateException("LaunchIntentManager");
    }

     public static void routeToActivity(final Context activityContext, final Class<? extends Activity> destinationActivity) {
            activityContext.startActivity(new Intent(activityContext, destinationActivity));
            ((Activity) activityContext).finish();
    }

    public static void routeToActivityStack(final Context activityContext, final Class<? extends Activity> destinationActivity) {
        activityContext.startActivity(new Intent(activityContext, destinationActivity));
    }

    public static void routeToActivityStackBundleStack(final Context activityContext, final Class<? extends Activity> destinationActivity, Bundle bundle) {
        activityContext.startActivity(new Intent(activityContext, destinationActivity).putExtras(bundle));
        ((Activity) activityContext).finish();
    }

    public static void routeToActivityStackBundle(final Context activityContext, final Class<? extends Activity> destinationActivity, Bundle bundle)
    {activityContext.startActivity(new Intent(activityContext, destinationActivity).putExtras(bundle)); }

}
