package redington.com.redconnect.util;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;

public class NoDataActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_no_data);

        Bundle bundle1 = getIntent().getExtras();
        if (bundle1 != null) {
            findValues(bundle1.getString(GlobalConstants.NODATA_ACTIVITY));
        }
    }


    private void findValues(String errorMessage) {
        ImageView mNoRecordsFound = findViewById(R.id.img_no_records_found);
        TextView mTextError = findViewById(R.id.text_error);
        LinearLayout totalLayout = findViewById(R.id.totallayout);
        Button mBackButton = findViewById(R.id.backButton);

        if (errorMessage.equals(GlobalConstants.NO_RECORDS)) {
            mNoRecordsFound.setImageResource(R.drawable.no_data);
            mTextError.setText(GlobalConstants.GO_BACK);
            totalLayout.setVisibility(View.GONE);
            mBackButton.setVisibility(View.VISIBLE);
        } else if (errorMessage.equals(GlobalConstants.SERVER_ERR)) {
            mNoRecordsFound.setImageResource(R.drawable.server_error);
            mTextError.setText(GlobalConstants.CONNECT_ERROR);
            totalLayout.setVisibility(View.VISIBLE);
            mBackButton.setVisibility(View.GONE);
        }
        mBackButton.setOnClickListener(v -> setMenuBack(v.getContext()));

    }

    public void mBackToRetry(View view) {
        if (CommonUtils.isNetworkAvailable(view.getContext())) {
            SharedPreferences.Editor editor = spInternet.edit();
            editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, true);
            editor.apply();
            setMenuBack(view.getContext());
        } else {
            shortToast(view.getContext(), GlobalConstants.INTERNET_CHECK);
        }

    }

    public void mBackToPreviousPage(View v) {
        SharedPreferences.Editor editor = spBack.edit();
        editor.putBoolean(GlobalConstants.SP_BACK_BUTTON, true);
        editor.apply();
        setMenuBack(v.getContext());
    }

    @Override
    public void onBackPressed() {
        SharedPreferences.Editor editor = spBack.edit();
        editor.putBoolean(GlobalConstants.SP_BACK_BUTTON, true);
        editor.apply();
        setMenuBack(NoDataActivity.this);
    }
}
