package redington.com.redconnect.util;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;

import java.lang.reflect.Field;

import redington.com.redconnect.common.activity.BaseActivity;

public class ApplicationModifier extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        overrideFont(getApplicationContext(), "SERIF", "font/Montserrat-SemiBold.ttf");
    }

    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            BaseActivity.logd("Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);

        }
    }
}