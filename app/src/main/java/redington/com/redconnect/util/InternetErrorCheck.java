package redington.com.redconnect.util;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;

public class InternetErrorCheck extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_screen);

        initViews();

    }


    private void initViews() {

        Button mRetry = findViewById(R.id.btn_retry);

        mRetry.setOnClickListener(v -> {
            if (CommonUtils.isNetworkAvailable(InternetErrorCheck.this)) {
                SharedPreferences.Editor editor = spInternet.edit();
                editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, true);
                editor.apply();
                finish();
            } else {
                callAnimation();
            }
        });
    }

    private void callAnimation() {
        Animation animation = AnimationUtils.loadAnimation(InternetErrorCheck.this, R.anim.shake);
        findViewById(R.id.errorTextContent).startAnimation(animation);
    }

    public void menuBack(View view) {
        if (CommonUtils.isNetworkAvailable(view.getContext())) {
            SharedPreferences.Editor editor = spInternet.edit();
            editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, true);
            editor.apply();
            finish();
        } else {
            callAnimation();
        }
    }

    @Override
    public void onBackPressed() {
        if (CommonUtils.isNetworkAvailable(InternetErrorCheck.this)) {
            SharedPreferences.Editor editor = spInternet.edit();
            editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, true);
            editor.apply();
            finish();
        } else {
            callAnimation();
        }
    }
}
