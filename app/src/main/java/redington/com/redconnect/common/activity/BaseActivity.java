package redington.com.redconnect.common.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.MenuBottomSheet;
import redington.com.redconnect.redconnect.notification.service.MyFirebaseMessagingService;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.NoDataActivity;

/**
 * Common Activity with some basic methods &
 * constants that is extended by most other activities
 */
public abstract class BaseActivity extends AppCompatActivity {

    public static final String SP_LOGGED_IN = GlobalConstants.LOGGED_IN;
    public static final String SP_FIRST_TIME = GlobalConstants.FURST_TIME;

    public static final String SP_USER_REMEMBER = GlobalConstants.REMEMBER;
    public static final String SP_EMAIL_ID = GlobalConstants.EMAIL;
    public static final String SP_PASSWORD = GlobalConstants.PASS_WORD;
    public static final String SP_CUSTOMER = GlobalConstants.CUSTOMER;
    public static final String SP_CUSTOMERNAME = GlobalConstants.CUSTOMER_NAME;
    public static final String SP_MAIL = GlobalConstants.MAIL;
    public static final String SP_PH_NO = GlobalConstants.PHONE_NUMBER;
    public static final String CARTDELSEQ = GlobalConstants.CART_DEL_SEQ;
    public static final String BILLINGDELSEQ = GlobalConstants.BILLING_SEQ;
    public static final String CASH_DISCOUNT = GlobalConstants.CASH_DISCOUNT;

    public static final String BLOCK_CHARACTER_SET = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
    public static final String BLOCK_USER_CHARACTER_SET = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ ";

    public static final InputFilter filter = new InputFilter() {
        String blockCharacterSet = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
    private static final String LOGIN_PREFS = GlobalConstants.LOGIN_PREFS;
    private static final String VIEW_PAGER = GlobalConstants.VIEW_PAGER;
    public static SharedPreferences sp;
    public static SharedPreferences sp1;
    public static SharedPreferences spInternet;
    public static SharedPreferences spBack;
    public static SharedPreferences selectCart;
    public static SharedPreferences cartData;
    public static SharedPreferences cartCount;
    public static SharedPreferences cartDeliverySequence;
    public static SharedPreferences billingaddressSeq;
    public static SharedPreferences cashDiscount;
    private static LogLevel sLoggingLevel = LogLevel.OFF;
    private static SecretKeySpec keySpec;

    public static InputFilter setValidInoutFilter(String blockCharacterSet) {
        return new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
    }

    /**
     * Display a short duration Snackbar / Toast Message with the
     * supplied text in Dialog.
     *
     * @param view CoordinateLayout must be in parent layout (Named as topLayout).
     * @param msg  The Message to be displayed
     */
    public static void shortToast(Context con, View view, String msg) {
        if (NotificationManagerCompat.from(con).areNotificationsEnabled()) {
            Toast toast = Toast.makeText(con, msg, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            if (view != null) {
                msg = (msg != null) ? msg : "";
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    /**
     * Display a short duration Snackbar / Toast Message with the
     * supplied text.
     *
     * @param con The Activity Context
     * @param msg The Message to be displayed
     */
    public static void shortToast(Context con, String msg) {
        if (NotificationManagerCompat.from(con).areNotificationsEnabled()) {
            Toast toast = Toast.makeText(con, msg, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            View view = ((Activity) con).getWindow().getDecorView().findViewById(android.R.id.content);
            if (view != null) {
                msg = (msg != null) ? msg : "";
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    public static void shortToastChanges(Context con, String msg) {
        if (NotificationManagerCompat.from(con).areNotificationsEnabled()) {
            Toast toast = Toast.makeText(con, msg, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            View view = ((Activity) con).getWindow().getDecorView().findViewById(android.R.id.content);
            if (view != null) {
                msg = (msg != null) ? msg : "";
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    /**
     * Display a long duration Snackbar / Toast Message with the
     * supplied text.
     *
     * @param con The Activity Context
     * @param msg The Message to be displayed
     */
    public static void longToast(Context con, String msg) {
        if (NotificationManagerCompat.from(con).areNotificationsEnabled()) {
            Toast toast = Toast.makeText(con, msg, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            View view = ((Activity) con).getWindow().getDecorView().findViewById(android.R.id.content);
            if (view != null) {
                msg = (msg != null) ? msg : "";
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }


    public static void longToast(Context con, View view, String msg) {
        if (NotificationManagerCompat.from(con).areNotificationsEnabled()) {
            Toast toast = Toast.makeText(con, msg, Toast.LENGTH_LONG);
            toast.show();
        } else {
            if (view != null) {
                msg = (msg != null) ? msg : "";
                Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }


    /**
     * Encrypts the provided text using AES/ECB Encryption
     * and a fixed encryption key.
     *
     * @param text The text to be encrypted
     * @return The encrypted text
     */
    public static String mEncrypt(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        final String key = "MAKV2SPBNI992121";
        keySpec = new SecretKeySpec(key.getBytes(), "AES");
        final javax.crypto.Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] encryptedValue = cipher.doFinal(text.getBytes("UTF-8"));
        return Base64.encodeToString(encryptedValue, Base64.NO_WRAP);

    }

    /**
     * Decrypts the provided data using AES/ECB Decryption
     * and a fixed encryption key.
     *
     * @param text The text to be decrypted
     * @return The decrypted text
     */
    public static String mDecypt(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        final javax.crypto.Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] decrptedValue = Base64.decode(text, Base64.NO_WRAP);
        return new String(cipher.doFinal(decrptedValue));
    }

    /*Log Functions*/

    /**
     * Writes the provided text to the LogCat
     * with the provided formating.
     *
     * @param message The text to be logged
     * @param params  The Format to be used
     */
    public static void logd(String message, Object... params) {
        if (logDebugEnabled()) {
            String mTag = "REDington Connect";
            Log.d(mTag, String.format(message, params));
        }
    }

    /**
     * Check if Logging is enabled<br/>
     * Logging status is defined in the enum
     *
     * @return true if the LogLevel is not OFF
     * @see BaseActivity.LogLevel
     */
    public static boolean logDebugEnabled() {
        return sLoggingLevel.ordinal() >= LogLevel.DEBUG.ordinal();
    }


    /**
     * Generate a NumberFormat Instance<br/>
     * Fixed format with Language 'en' and Country 'in'
     *
     * @return the NumberFormat Instance
     */
    public static NumberFormat isValidNumberFormat() {
        return NumberFormat.getInstance(new Locale("en", "in"));
    }


    /**
     * Calculates the difference between 2 date objects
     * in days , hours and minutes
     *
     * @param startDate The Start date as a Java date object
     * @param endDate   The end date
     * @return The duration beetween the dates as a String
     */
    public static String calculateDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;

        logd(String.valueOf(elapsedSeconds));

        String days = String.valueOf(elapsedDays) + "d ";
        String hours = String.valueOf(elapsedHours) + "h ";
        String minutes = String.valueOf(elapsedMinutes) + "m";

        return String.valueOf(days + hours + minutes);
    }

    /**
     * Subtract 2 hours from the supplied date and time
     * and return the result in milliseconds
     *
     * @param date The date as String (dd/MM/yy)
     * @param time The time as String  (hh:mm)
     * @return The subtracted value in millis
     */
    public static long dateFormatMinusTwo(String date, String time) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.getDefault());
        try {
            Date date1 = format.parse(date + " " + time);

            Time t = Time.valueOf("02:00:00");
            long l = t.getTime();

            return date1.getTime() - l;
        } catch (ParseException e) {
            logd(e.getMessage());
        }
        return 0L;
    }

    /**
     * Convert the supplied date and time into milli seconds
     *
     * @param date The date as String (dd/MM/yy)
     * @param time The time as String (hh:mm:ss)
     * @return the converted date-time in millis
     */
    public static long longConversion(String date, String time) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm:ss", Locale.getDefault());
        try {
            Date date1 = format.parse(date + " " + time);
            return date1.getTime();
        } catch (ParseException e) {
            logd(e.getMessage());
        }
        return 0L;
    }

    /**
     * Calculates the difference between 2 dates in milliseconds
     *
     * @param startDate start date as Date Object
     * @param endDate   end date as Date Object
     * @return Difference in millis
     */
    public static long longDiffDate(Date startDate, Date endDate) {
        return endDate.getTime() - startDate.getTime();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        spUpdate();
    }


    /**
     * Loads the required sharedpreference files into
     * memory.
     */
    private void spUpdate() {
        sp = getSharedPreferences(LOGIN_PREFS, Context.MODE_PRIVATE);
        sp1 = getSharedPreferences(VIEW_PAGER, Context.MODE_PRIVATE);
        spInternet = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, Context.MODE_PRIVATE);
        spBack = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, Context.MODE_PRIVATE);
        selectCart = getSharedPreferences(GlobalConstants.SP_SELECT_CART, Context.MODE_PRIVATE);
        cartData = getSharedPreferences(GlobalConstants.CART_DETAILS, Context.MODE_PRIVATE);
        cartCount = getSharedPreferences(GlobalConstants.SP_CART_COUNT, Context.MODE_PRIVATE);
        cartDeliverySequence = getSharedPreferences(GlobalConstants.CART_DEL_SEQ, Context.MODE_PRIVATE);
        billingaddressSeq = getSharedPreferences(GlobalConstants.BILLING_SEQ, Context.MODE_PRIVATE);
        cashDiscount = getSharedPreferences(GlobalConstants.CASH_DISCOUNT, Context.MODE_PRIVATE);
    }

    /*Internet Error Check Screen Check false*/

    /**
     * Set Internet Check SharedPreference Field to false<br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_INTERNET_CHECK Preference File}<br/><br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_INTERNET_CHECK_BOOLEAN Preference Key}
     */
    public void internetCheckFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        editor.apply();
    }

    /**
     * Set Back Button SharedPreference Field to false<br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_BACK_CHECK Preference File}<br/><br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_BACK_BUTTON Preference Key}
     */
    public void backButtonFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        editor.apply();
    }

    /**
     * Set Select Cart SharedPreference Field to false<br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_SELECT_CART Preference File}<br/><br/>
     * {@link redington.com.redconnect.constants.GlobalConstants#SP_SELECT_CART_BOOLEAN Preference Key}
     */
    public void selectCartFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_SELECT_CART, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_SELECT_CART_BOOLEAN, false);
        editor.apply();
    }

    /**
     * Get the resource id for the specified
     * resource name.
     *
     * @param aString The resource name
     * @return The resource id
     */
    public int getStringResourceByName(String aString) {
        String packageName = getPackageName();
        return getResources().getIdentifier(aString, "id", packageName);
    }

    /**
     * <p>
     * Sets the toolbar background based on the supplied activity
     * For CustomerVoice and UserProfile the toolbar text color
     * is also updated.
     * </p>
     *
     * @param toolBarText the current activity name
     */
    public void toolbar(String toolBarText) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = findViewById(R.id.toolbar_title);

        if (toolBarText.equals(IntentConstants.CUSTOMERVOICE) || toolBarText.equals(IntentConstants.USER_PROFILE)) {
            toolbar.setBackgroundResource(android.R.color.transparent);
            AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
            appBarLayout.setBackgroundResource(android.R.color.transparent);
            toolbarTitle.setTextColor(Color.WHITE);
        } else {
            toolbar.setBackgroundResource(R.color.white_100);
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(toolBarText.toUpperCase());

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.slide_down);
    }

    /**
     * Opens the NoDataActivity activity and attaches the
     * supplied string in a bundle.
     * In case of server error it additionally closes the current activity .
     *
     * @param context   the activity context
     * @param errorData The error status
     */
    public void noRecords(Context context, String errorData) {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.NODATA_ACTIVITY, errorData);

        if (GlobalConstants.SERVER_ERR.equals(errorData)) {
            LaunchIntentManager.routeToActivityStackBundle(context, NoDataActivity.class, bundle);
        } else {
            LaunchIntentManager.routeToActivityStackBundleStack(context, NoDataActivity.class, bundle);
        }
    }

    /**
     * Show or Hide the cart item count
     * on the cart icon in toolbar.
     */
    @SuppressLint({"SetTextI18n", "NewApi"})
    public void getCartUpdate() {
        spUpdate();
        int cartValue = cartCount.getInt(GlobalConstants.SP_CART_COUNT, 0);
        LinearLayout mLayout = findViewById(R.id.LL_cartText);
        if (cartValue != 0) {
            mLayout.setVisibility(View.VISIBLE);
            TextView txtCount = findViewById(R.id.txtCount);
            txtCount.setText(Integer.toString(cartValue));

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setShape(GradientDrawable.OVAL);
                gradientDrawable.setColor(Color.rgb(82, 78, 126));
                txtCount.setBackground(gradientDrawable);
            }
        } else {
            mLayout.setVisibility(View.GONE);
        }

    }

    /**
     * Set the cart item count SharedPreference Field<br/>
     *
     * @param count cart item count
     *              {@link redington.com.redconnect.constants.GlobalConstants#SP_CART_COUNT Preference File}<br/>
     *              {@link redington.com.redconnect.constants.GlobalConstants#SP_CART_COUNT Preference Key}
     */
    public void setCartUpdate(int count) {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_CART_COUNT, Context.MODE_PRIVATE).edit();
        editor.putInt(GlobalConstants.SP_CART_COUNT, count);
        editor.apply();
    }

    /**
     * Set the cart delete sequence SharedPreference Field<br/>
     *
     * @param delSeq the value to be set
     *               {@link redington.com.redconnect.constants.GlobalConstants#CART_DEL_SEQ Preference File}<br/>
     *               {@link redington.com.redconnect.constants.GlobalConstants#CART_DEL_SEQ Preference Key}
     */
    public void setCartDelSeqUpdate(String delSeq) {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.CART_DEL_SEQ, Context.MODE_PRIVATE).edit();
        editor.putString(CARTDELSEQ, delSeq);
        editor.apply();
    }

    /**
     * Set the billing sequence SharedPreference Field<br/>
     *
     * @param delSeq the value to be set
     *               {@link redington.com.redconnect.constants.GlobalConstants#BILLING_SEQ Preference File}<br/>
     *               {@link redington.com.redconnect.constants.GlobalConstants#BILLING_SEQ Preference Key}
     */
    public void setBillingSeq(String delSeq) {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.BILLING_SEQ, Context.MODE_PRIVATE).edit();
        editor.putString(BILLINGDELSEQ, delSeq);
        editor.apply();
    }

    /**
     * Set the cash discount SharedPreference Field<br/>
     *
     * @param cashDiscount the value to be set
     *                     {@link redington.com.redconnect.constants.GlobalConstants#CASH_DISCOUNT Preference File}<br/>
     *                     {@link redington.com.redconnect.constants.GlobalConstants#CASH_DISCOUNT Preference Key}
     */
    public void setCashDiscount(String cashDiscount) {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.CASH_DISCOUNT, Context.MODE_PRIVATE).edit();
        editor.putString(CASH_DISCOUNT, cashDiscount);
        editor.apply();
    }

    /**
     * Instaciate and display a fixed bottom sheet dialog
     * menu in the calling activity.
     *
     * @param mMenuContent the activity context
     */
    public void setRightNavigation(Context mMenuContent) {
        MenuBottomSheet menuBottomSheet = MenuBottomSheet.getInstance();
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.MENU_ACTIVITY, mMenuContent.getClass().getSimpleName());
        menuBottomSheet.setArguments(bundle);
        menuBottomSheet.show(getSupportFragmentManager(), " Sheet");
    }

    /**
     * Closes the current activity with a
     * slide  down animation.
     * <p>
     * The animation is currently hardcoded into the
     * overridePendingTransition method call
     *
     * @param context the activity context
     */
    public void setMenuBack(Context context) {
        String mClassName = context.getClass().getSimpleName();
        logd(mClassName);
        finish();
        overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.slide_down);
    }

    /*Validate Decimal Values in Cheque Pending and Payment Due*/
    public String mValidateDecimal(String str, int maxBeforePoint) {
        if ((int) str.charAt(0) == (int) '.') str = "0" + str;
        int max = str.length();

        StringBuilder rFinal = new StringBuilder();
        boolean after = false;
        int i = 0;
        int up = 0;
        int decimal = 0;
        char character;
        while (i < max) {
            character = str.charAt(i);
            if ((int) character != (int) '.' && !after) {
                up++;

                if (up > maxBeforePoint) {
                    rFinal.append(".");
                    return rFinal.toString();
                }

            } else if ((int) character == (int) '.') {
                after = true;
            } else {
                decimal++;
                if (decimal > 2)
                    return rFinal.toString();
            }
            rFinal.append(character);
            i++;
        }
        return rFinal.toString();
    }

    /*Double Value Validation*/
    public double doubleValidate(String result) {
        String val = result.trim();
        if (val.equals("")) {
            return 0.0d;
        } else {
            return Double.parseDouble(val);
        }
    }

    /*Hide Keyboard*/
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /*Date Format*/
    public String dateFormat(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(date);
    }

    /**
     * Enum defining various log levels
     * that is refered in the logging method
     * <p>
     * OFF      - disable Logging
     * DEBUG    - Logging in debug level
     * VERBOSE  - Logging in verbose level
     *
     * @see #logd(String, Object...)
     */
    public enum LogLevel {
        OFF, DEBUG, VERBOSE
    }


    public void sendNotification(String hashText) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                                            MyFirebaseMessagingService.NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.leaf_image)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setContentText(hashText)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(hashText));

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }
}
