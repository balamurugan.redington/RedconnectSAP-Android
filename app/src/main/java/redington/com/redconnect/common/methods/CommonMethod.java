package redington.com.redconnect.common.methods;

import android.content.Context;
import android.os.CountDownTimer;

import java.text.NumberFormat;
import java.util.Locale;

import redington.com.redconnect.common.activity.BaseActivity;

/**
 * Utility Class containing non ui methods that are
 * accessed from different classes
 */
public class CommonMethod {

    private static final int INTERVAL_COUNTDOWN = 1000;
    private Boolean intervalBoolean = true;

    public CommonMethod(Context mContext) {
        BaseActivity.logd(mContext.getClass().getSimpleName());
    }


    public Boolean listAccess() {
        new CountDownTimer(INTERVAL_COUNTDOWN, 1) {
            @Override
            public void onTick(long l) {
                intervalBoolean = false;
            }

            @Override
            public void onFinish() {
                intervalBoolean = true;
            }
        }.start();
        return intervalBoolean;
    }

    /**
     * <p>
     *     Convert the provided String into a double if
     *     possible or return 0.
     * </p>
     * @param doubleString  the double number as a string.
     * @return  String as double value
     */
    public static Double getValidDouble(String doubleString) {
        String value = (doubleString!=null && doubleString.trim().length()>0) ? doubleString:"0";
        return Double.parseDouble(value);
    }

    public static Integer getValidInt(String intString) {
        String value = (intString!=null && intString.trim().length()>0) ? intString:"0";
        return Integer.parseInt(value);
    }

    /**
     * Validate the provided String and return 0 if
     * it is null or empty.
     *
     * @param doubleString  the string to be validated
     * @return              the string as is if valid or zero other wise
     */
    public static String getValidDoubleString(String doubleString) {
        return (doubleString!=null && doubleString.trim().length()>0) ? doubleString:"0";

    }

    /**
     * Validate the provided String and return 0 if
     * it is null or empty.
     *
     * @param intString  the string to be validated
     * @return              the string as is if valid or zero other wise
     */
    public static String getValidIntString(String intString) {
        return (intString!=null && intString.trim().length()>0) ? intString:"0";
    }


    /**
     * Generate a NumberFormat Instance<br/>
     * And format the supplied String in the
     * provided language and country format
     *
     * @return  a formatted Double as String
     */
    public static String formatLargeDouble(String doubleString,String language,String country) {
        NumberFormat numberformat = NumberFormat.getInstance(new Locale(language, country));
        Double data = Double.parseDouble(doubleString);
        return numberformat.format(data);
    }

    /**
     * @see #formatLargeDouble(String, String, String)
     */
    public static String formatLargeDouble(String doubleString)
    {return formatLargeDouble(doubleString,"en","in");    }

    /**
     * <p>
     *     Verifies if the provided String contains only numbers.
     * </p>
     * @param text  the String to be checked
     * @return      true if the string contains numbers only
     */
    public static boolean isNumber(String text) {
        char[] chars = text.toCharArray();
        for(char ch: chars) {
            if(!Character.isDigit(ch))
                return false;
        }
        return true;
    }



}

