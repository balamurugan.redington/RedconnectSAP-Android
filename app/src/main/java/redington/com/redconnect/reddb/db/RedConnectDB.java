package redington.com.redconnect.reddb.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import redington.com.redconnect.reddb.constant.DbConstants;
import redington.com.redconnect.reddb.constant.DbTable;


public class RedConnectDB {

    /*>>>  DB Details <<<<*/
    private static final int DBVERSION = 3;
    private static final String DBNAME = "RedConnect.db";

    /*>>> Local Data <<<*/
    private SQLiteDatabase sqlDB = null;
    private DBHelper dbHelper;

    public RedConnectDB(Context context) {
        dbHelper = new DBHelper(context);
    }

    /*>>>> DB Open <<<<*/
    public SQLiteDatabase open() {
        if (sqlDB != null && sqlDB.isOpen()) {
            sqlDB.close();
        }
        sqlDB = dbHelper.getWritableDatabase();
        return sqlDB;
    }

    public void close() {
        dbHelper.close();
    }

    /*>>>> Helper Class  <<<<*/
    public static class DBHelper extends SQLiteOpenHelper {
        private DBHelper(Context context) {
            super(context, DBNAME, null, DBVERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.CARTORDERTABLE);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.CARTORDERPRODUCTTABLE);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.SPOTPRODUCTLIST);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.CHEQUEPENDINGTABLE);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.PAYMENTDUETABLE);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.GETCATEGORYTABLE);
            db.execSQL(DbConstants.CREATE_TABLE + DbTable.DASHBOARDTABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int preVersion, int postVersion) {

            db.execSQL(DbConstants.DROP_TABLE + DbConstants.TABLE_CART_ORDER);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.TABLE_CART_PRODUCT_LIST);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.TABLE_SPOT_LIST);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.CHEQUE_PENDING_TABLE);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.PAYMENT_COMMITMENT_TABLE);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.GET_ITEM_CATEGORY_TABLE);
            db.execSQL(DbConstants.DROP_TABLE + DbConstants.DASHBOARD_TABLE);
            onCreate(db);

        }
    }

}
