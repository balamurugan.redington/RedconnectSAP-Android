package redington.com.redconnect.reddb.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.dashboard.model.DashboardBean;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryData;
import redington.com.redconnect.redconnect.preorder.model.SpotPojoList;
import redington.com.redconnect.reddb.constant.DbConstants;
import redington.com.redconnect.reddb.db.RedConnectDB;
import redington.com.redconnect.reddb.model.CartOrderGeneration;
import redington.com.redconnect.reddb.model.CartProductsList;
import redington.com.redconnect.reddb.model.ChequesPendingModel;
import redington.com.redconnect.reddb.model.DashboardMenuPojo;
import redington.com.redconnect.reddb.model.PaymentDueModel;

public class RedDAO {


    private RedConnectDB redConnectDB;
    private SQLiteDatabase sql = null;
    private String selectOrderQ;
    private String selectOrderProductQ;
    private String chequesPendingQ;
    private String paymentDueQ;
    private String categoryQuery;
    private String selectSpotList;
    private String getDashboardData;

    public RedDAO(Context context) {
        redConnectDB = new RedConnectDB(context);
        sql = redConnectDB.open();

        selectOrderQ = DbConstants.SELECT_TABLE + DbConstants.TABLE_CART_ORDER +
                DbConstants.DB_WHERE + DbConstants.DB_SPACE + DbConstants.CO_CUSTOMER_CODE +
                DbConstants.DB_EQUAL + DbConstants.DB_QT +
                GlobalConstants.GetSharedValues.getSpUserId() + DbConstants.DB_QT;

        selectOrderProductQ = DbConstants.SELECT_TABLE + DbConstants.TABLE_CART_PRODUCT_LIST +
                DbConstants.DB_SPACE;

        selectSpotList = DbConstants.SELECT_TABLE + DbConstants.TABLE_SPOT_LIST + DbConstants.DB_SPACE;

        getDashboardData = DbConstants.SELECT_TABLE + DbConstants.DASHBOARD_TABLE +
                DbConstants.DB_WHERE + DbConstants.DB_SPACE + DbConstants.USER_ID +
                DbConstants.DB_EQUAL + DbConstants.DB_QT +
                GlobalConstants.GetSharedValues.getSpUserId() + DbConstants.DB_QT;

        chequesPendingQ = DbConstants.SELECT_TABLE + DbConstants.CHEQUE_PENDING_TABLE +
                DbConstants.DB_WHERE + DbConstants.DB_SPACE + DbConstants.CP_USER_ID +
                DbConstants.DB_EQUAL + DbConstants.DB_QT +
                GlobalConstants.GetSharedValues.getSpUserId() + DbConstants.DB_QT;

        paymentDueQ = DbConstants.SELECT_TABLE + DbConstants.PAYMENT_COMMITMENT_TABLE +
                DbConstants.DB_WHERE + DbConstants.DB_SPACE + DbConstants.PD_USER_ID +
                DbConstants.DB_EQUAL + DbConstants.DB_QT +
                GlobalConstants.GetSharedValues.getSpUserId() + DbConstants.DB_QT;

        categoryQuery = DbConstants.SELECT_TABLE + DbConstants.GET_ITEM_CATEGORY_TABLE +
                DbConstants.DB_WHERE + DbConstants.DB_SPACE + DbConstants.USER_ID +
                DbConstants.DB_EQUAL + DbConstants.DB_QT +
                GlobalConstants.GetSharedValues.getSpUserId() + DbConstants.DB_QT;
    }


    /*>>>>>> Cart Order Generation <<<<<<<<*/
    public void insertCartOrder(CartOrderGeneration cartOrder) {
        ContentValues setValues = new ContentValues();
        setValues.put(DbConstants.CO_CUSTOMER_CODE, cartOrder.getCoCustomerCode());
        setValues.put(DbConstants.CO_MODE, cartOrder.getCoMode());
        setValues.put(DbConstants.CO_STOCK_LOCATION, cartOrder.getCoStockLocation());
        setValues.put(DbConstants.CO_PAYMENT_DAYS, cartOrder.getCoPaymentDays());
        setValues.put(DbConstants.CO_DEL_SEQ, cartOrder.getCoDelSeq());
        setValues.put(DbConstants.CO_INVOICE_SEQ, cartOrder.getCoInvoiceSeq());
        setValues.put(DbConstants.CO_CDC, cartOrder.getCoCDC());
        setValues.put(DbConstants.CO_REF_NUM, cartOrder.getCoReferenceNumber());
        setValues.put(DbConstants.CO_PAYMENT_TYPE, cartOrder.getCoPaymentType());

        setValues.put(DbConstants.CO_DATE, cartOrder.getCoDate());
        setValues.put(DbConstants.CO_AMOUNT, cartOrder.getCoAmount());
        setValues.put(DbConstants.CO_TRANSACTION_ID, cartOrder.getCoTransactionID());
        setValues.put(DbConstants.CO_STATUS, cartOrder.getCoStatus());
        setValues.put(DbConstants.CO_REMARKS, cartOrder.getCoRemarks());
        setValues.put(DbConstants.CO_AMOUNT_RECEIVED, cartOrder.getCoAmountReceived());
        setValues.put(DbConstants.CO_TRANSACTION_RECEIVED, cartOrder.getCoTransactionReceived());
        setValues.put(DbConstants.CO_REFERENCE_NUM, cartOrder.getCoRefernceNum());
        setValues.put(DbConstants.CO_USER_ID, cartOrder.getCoUserID());
        setValues.put(DbConstants.CO_DEVICE_ID, cartOrder.getCoDeviceID());
        setValues.put(DbConstants.CO_UUID, cartOrder.getCoUuid());

        sql.insert(DbConstants.TABLE_CART_ORDER, null, setValues);
        redConnectDB.close();
    }

    public void insertCartProductList(CartProductsList productsList) {
        ContentValues setValues = new ContentValues();
        setValues.put(DbConstants.CO_CUSTOMER_CODE, productsList.getCoUserid());
        setValues.put(DbConstants.CO_ITEM_CODE, productsList.getCoItemcode());
        setValues.put(DbConstants.COLINE_QTY, productsList.getCoLineQty());
        setValues.put(DbConstants.CO_UNIT_PRICE, productsList.getCoUnitPrice());
        setValues.put(DbConstants.CO_CASH_DISCOUNT, productsList.getCoCashDiscount());
        sql.insert(DbConstants.TABLE_CART_PRODUCT_LIST, null, setValues);
        redConnectDB.close();
    }

    /*Insert Spot Data*/
    public void insertSpotList(SpotPojoList spotPojoList) {
        ContentValues setValues = new ContentValues();
        setValues.put(DbConstants.SPOT_CUSTOMER_CODE, spotPojoList.getmUserID());
        setValues.put(DbConstants.SPOT_ITEM_CODE, spotPojoList.getmItemCode());
        setValues.put(DbConstants.SPOTLINE_QTY, spotPojoList.getmQuantity());
        setValues.put(DbConstants.SPOT_UNIT_PRICE, spotPojoList.getmUnitPrice());
        setValues.put(DbConstants.SPOT_CASH_DISCOUNT, spotPojoList.getmCashdiscountPercent());
        setValues.put(DbConstants.SPOT_PROMO_CODE, spotPojoList.getmPromoCode());
        setValues.put(DbConstants.SPOT_ORC, spotPojoList.getOrcAmount());
        sql.insert(DbConstants.TABLE_SPOT_LIST, null, setValues);
        redConnectDB.close();
    }


    /*Insert Cheques Pending*/
    public void insertChequePending(ChequesPendingModel chequesPendingModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.CP_USER_ID, chequesPendingModel.getCpUserId());
        contentValues.put(DbConstants.CP_DATE, chequesPendingModel.getCpDate());
        contentValues.put(DbConstants.CP_AMOUNT, chequesPendingModel.getCpAmount());
        contentValues.put(DbConstants.CP_TRANSACTION_ID, chequesPendingModel.getCpTxnID());
        contentValues.put(DbConstants.CP_STATUS, chequesPendingModel.getCpStatus());
        contentValues.put(DbConstants.CP_REMARKS, chequesPendingModel.getCpReamrks());
        contentValues.put(DbConstants.CP_AMOUNT_RECEIVED, chequesPendingModel.getCpAmountReceived());
        contentValues.put(DbConstants.CP_TRANSACTION_RECEIVED, chequesPendingModel.getCpTransactionReceived());
        contentValues.put(DbConstants.CP_REFERENCE_NO, chequesPendingModel.getCpRefernceNum());
        contentValues.put(DbConstants.CP_DEVICE_ID, chequesPendingModel.getCpDeviceID());
        contentValues.put(DbConstants.CP_UID, chequesPendingModel.getCpUidNum());
        sql.insert(DbConstants.CHEQUE_PENDING_TABLE, null, contentValues);
        redConnectDB.close();
    }

    /*Insert Cheques Pending*/
    public void insertPaymentDue(PaymentDueModel paymentDueModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.PD_USER_ID, paymentDueModel.getPdUserId());
        contentValues.put(DbConstants.PD_DATE, paymentDueModel.getPdDate());
        contentValues.put(DbConstants.PD_AMOUNT, paymentDueModel.getPdAmount());
        contentValues.put(DbConstants.PD_TRANSACTION_ID, paymentDueModel.getPdTxnID());
        contentValues.put(DbConstants.PD_STATUS, paymentDueModel.getPdStatus());
        contentValues.put(DbConstants.PD_REMARKS, paymentDueModel.getPdReamrks());
        contentValues.put(DbConstants.PD_AMOUNT_RECEIVED, paymentDueModel.getPdAmountReceived());
        contentValues.put(DbConstants.PD_TRANSATION_RECEIVED, paymentDueModel.getPdTransactionReceived());
        contentValues.put(DbConstants.PD_REFERENCE_NO, paymentDueModel.getPdmRefNum());
        contentValues.put(DbConstants.PD_DEVICE_ID, paymentDueModel.getPdDeviceId());
        contentValues.put(DbConstants.PDP_UID, paymentDueModel.getPdUuidNum());
        sql.insert(DbConstants.PAYMENT_COMMITMENT_TABLE, null, contentValues);
        redConnectDB.close();
    }

    /*Dashboard Data*/
    public void insertDashboardData(DashboardBean dashboardBean) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.USER_ID, dashboardBean.getUserId());
        values.put(DbConstants.DB_DATA, dashboardBean.getDashboardData());
        sql.insert(DbConstants.DASHBOARD_TABLE, null, values);
        redConnectDB.close();
    }


    /*Get Count's*/
    public int getCartOrderCount() {
        Cursor cursor = sql.rawQuery(selectOrderQ, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }


    public int getChequesPendingCount() {
        Cursor cursor = sql.rawQuery(chequesPendingQ, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int getPaymentDueCount() {
        Cursor cursor = sql.rawQuery(paymentDueQ, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int getCategoryCount() {
        Cursor cursor = sql.rawQuery(categoryQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int getDashCount() {
        Cursor cursor = sql.rawQuery(getDashboardData, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


    /*Delete Data From DB's*/
    public void deleteAddressList(String userID) {
        sql.delete(DbConstants.TABLE_CART_ORDER,
                DbConstants.CO_CUSTOMER_CODE + " = ?", new String[]{userID});
        sql.close();
    }

    public void deleteProductList(String userId) {
        sql.delete(DbConstants.TABLE_CART_PRODUCT_LIST,
                DbConstants.CO_CUSTOMER_CODE + " = ?", new String[]{userId});
        sql.close();
    }


    public void deleteSpoList(String userID) {
        sql.delete(DbConstants.TABLE_SPOT_LIST,
                DbConstants.SPOT_CUSTOMER_CODE + " = ?", new String[]{userID});
        sql.close();
    }

    public void deleteChequePending(String userID) {
        sql.delete(DbConstants.CHEQUE_PENDING_TABLE,
                DbConstants.CP_USER_ID + " = ?", new String[]{userID});
        sql.close();
    }

    public void deletePaymentDue(String userID) {
        sql.delete(DbConstants.PAYMENT_COMMITMENT_TABLE, DbConstants.PD_USER_ID + "= ?", new String[]{userID});
        sql.close();
    }

    public void deleteCategory(String userId) {
        sql.delete(DbConstants.GET_ITEM_CATEGORY_TABLE, DbConstants.USER_ID + "= ?", new String[]{userId});
        sql.close();
    }

    public void deleteDahboard(String userId) {
        sql.delete(DbConstants.DASHBOARD_TABLE, DbConstants.USER_ID + "= ?", new String[]{userId});
        sql.close();
    }

    /*Get Cart product Array List*/
    public List<CartProductsList> getCartProductList() {
        ArrayList<CartProductsList> cartProductsLists = new ArrayList<>();
        Cursor cursor = sql.rawQuery(selectOrderProductQ, null);
        if (cursor.moveToFirst()) {
            do {
                CartProductsList productsList = new CartProductsList();
                productsList.setCoId(cursor.getInt(0));
                productsList.setCoUserid(cursor.getString(1));
                productsList.setCoItemcode(cursor.getString(2));
                productsList.setCoLineQty(cursor.getString(3));
                productsList.setCoUnitPrice(cursor.getString(4));
                productsList.setCoCashDiscount(cursor.getString(5));
                cartProductsLists.add(productsList);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cartProductsLists;
    }

    /*get Spot Array*/
    public List<SpotPojoList> getSpotList() {
        ArrayList<SpotPojoList> spotPojoLists = new ArrayList<>();
        Cursor cursor = sql.rawQuery(selectSpotList, null);
        if (cursor.moveToFirst()) {
            do {
                SpotPojoList spotPojoList = new SpotPojoList();
                spotPojoList.setmID(cursor.getInt(0));
                spotPojoList.setmUserID(cursor.getString(1));
                spotPojoList.setmItemCode(cursor.getString(2));
                spotPojoList.setmQuantity(cursor.getString(3));
                spotPojoList.setmUnitPrice(cursor.getString(4));
                spotPojoList.setmCashdiscountPercent(cursor.getString(5));
                spotPojoList.setmPromoCode(cursor.getString(6));
                spotPojoList.setOrcAmount(cursor.getString(7));
                spotPojoLists.add(spotPojoList);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return spotPojoLists;
    }

    /*Get Cart Address List*/
    public List<CartOrderGeneration> getCartAddressList() {
        ArrayList<CartOrderGeneration> cartOrderGenerations = new ArrayList<>();
        Cursor cursor = sql.rawQuery(selectOrderQ, null);
        if (cursor.moveToFirst()) {
            do {
                CartOrderGeneration orderGenerations = new CartOrderGeneration();
                orderGenerations.setCoId(cursor.getInt(0));
                orderGenerations.setCoCustomerCode(cursor.getString(1));
                orderGenerations.setCoMode(cursor.getString(2));
                orderGenerations.setCoStockLocation(cursor.getString(3));
                orderGenerations.setCoPaymentDays(cursor.getString(4));
                orderGenerations.setCoDelSeq(cursor.getString(5));
                orderGenerations.setCoInvoiceSeq(cursor.getString(6));
                orderGenerations.setCoCDC(cursor.getString(7));
                orderGenerations.setCoReferenceNumber(cursor.getString(8));
                orderGenerations.setCoPaymentType(cursor.getString(9));

                orderGenerations.setCoDate(cursor.getString(10));
                orderGenerations.setCoAmount(cursor.getString(11));
                orderGenerations.setCoTransactionID(cursor.getString(12));
                orderGenerations.setCoStatus(cursor.getString(13));
                orderGenerations.setCoRemarks(cursor.getString(14));
                orderGenerations.setCoAmountReceived(cursor.getString(15));
                orderGenerations.setCoTransactionReceived(cursor.getString(16));
                orderGenerations.setCoRefernceNum(cursor.getString(17));
                orderGenerations.setCoUserID(cursor.getString(18));
                orderGenerations.setCoDeviceID(cursor.getString(19));
                orderGenerations.setCoUuid(cursor.getString(20));

                cartOrderGenerations.add(orderGenerations);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return cartOrderGenerations;
    }

    /*Get Cheques Pending Data*/
    public List<ChequesPendingModel> getChequePendingData() {
        ArrayList<ChequesPendingModel> chequesPendingModels = new ArrayList<>();
        Cursor cursor = sql.rawQuery(chequesPendingQ, null);
        if (cursor.moveToFirst()) {
            do {
                ChequesPendingModel pendingModel = new ChequesPendingModel();
                pendingModel.setCpId(cursor.getInt(0));
                pendingModel.setCpUserId(cursor.getString(1));
                pendingModel.setCpDate(cursor.getString(2));
                pendingModel.setCpAmount(cursor.getString(3));
                pendingModel.setCpTxnID(cursor.getString(4));
                pendingModel.setCpStatus(cursor.getString(5));
                pendingModel.setCpReamrks(cursor.getString(6));
                pendingModel.setCpAmountReceived(cursor.getString(7));
                pendingModel.setCpTransactionReceived(cursor.getString(8));
                pendingModel.setCpRefernceNum(cursor.getString(9));
                pendingModel.setCpDeviceID(cursor.getString(10));
                pendingModel.setCpUidNum(cursor.getString(11));
                chequesPendingModels.add(pendingModel);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return chequesPendingModels;
    }

    /*Get Payment Due Data*/
    public List<PaymentDueModel> getPaymentDue() {
        ArrayList<PaymentDueModel> dueModels = new ArrayList<>();
        Cursor cursor = sql.rawQuery(paymentDueQ, null);
        if (cursor.moveToFirst()) {
            do {
                PaymentDueModel model = new PaymentDueModel();
                model.setPdId(cursor.getInt(0));
                model.setPdUserId(cursor.getString(1));
                model.setPdDate(cursor.getString(2));
                model.setPdAmount(cursor.getString(3));
                model.setPdTxnID(cursor.getString(4));
                model.setPdStatus(cursor.getString(5));
                model.setPdReamrks(cursor.getString(6));
                model.setPdAmountReceived(cursor.getString(7));
                model.setPdTransactionReceived(cursor.getString(8));
                model.setPdmRefNum(cursor.getString(9));
                model.setPdDeviceId(cursor.getString(10));
                model.setPdUuidNum(cursor.getString(11));
                dueModels.add(model);

            } while (cursor.moveToNext());

        }
        cursor.close();
        return dueModels;

    }


    public List<DashboardBean> getDashboard() {
        ArrayList<DashboardBean> beansArraylist = new ArrayList<>();
        Cursor cursor = sql.rawQuery(getDashboardData, null);
        if (cursor.moveToFirst()) {
            do {
                DashboardBean dashboardBean = new DashboardBean();
                dashboardBean.setId(cursor.getString(0));
                dashboardBean.setUserId(cursor.getString(1));
                dashboardBean.setDashboardData(cursor.getString(2));
                beansArraylist.add(dashboardBean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return beansArraylist;
    }

    /*Dashboard menu's*/
    public void insertDashboardMenu(DashboardMenuPojo menuPojo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.USER_ID, menuPojo.getUserId());
        contentValues.put(DbConstants.CATEGORY, menuPojo.getCategory());
        contentValues.put(DbConstants.DESCRIPTION, menuPojo.getDescription());
        contentValues.put(DbConstants.BRAND, menuPojo.getBrand());
        contentValues.put(DbConstants.IMAGE_1, menuPojo.getImage1());
        contentValues.put(DbConstants.FLAG, menuPojo.getFlag());
        sql.insert(DbConstants.GET_ITEM_CATEGORY_TABLE, null, contentValues);
        redConnectDB.close();

    }


    public List<PreorderCategoryData> getDashboardCategory() {
        ArrayList<DashboardMenuPojo> dbDatas = new ArrayList<>();
        ArrayList<PreorderCategoryData> menuDatas = new ArrayList<>();
        Cursor cursor = sql.rawQuery(categoryQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DashboardMenuPojo dbDatas1 = new DashboardMenuPojo();
                dbDatas1.setDbId(cursor.getInt(0));
                dbDatas1.setUserId(cursor.getString(1));
                dbDatas1.setCategory(cursor.getString(2));
                dbDatas1.setDescription(cursor.getString(3));
                dbDatas1.setBrand(cursor.getString(4));
                dbDatas1.setImage1(cursor.getString(5));
                dbDatas1.setFlag(cursor.getString(6));
                dbDatas.add(dbDatas1);
            } while (cursor.moveToNext());
        }
        cursor.close();
        for (DashboardMenuPojo list : dbDatas) {
            PreorderCategoryData data = new PreorderCategoryData();
            data.setCategory(list.getCategory());
            data.setDescription(list.getDescription());
            data.setBRND68(list.getBrand());
            data.setmImage1(list.getImage1());
            data.setFlag(list.getFlag());
            menuDatas.add(data);
        }

        return menuDatas;
    }


}
