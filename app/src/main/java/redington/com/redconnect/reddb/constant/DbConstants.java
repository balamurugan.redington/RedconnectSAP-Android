package redington.com.redconnect.reddb.constant;


public class DbConstants {

    public static final String CO_INVOICE_SEQ = "invoice_seq";


    private DbConstants() {
        throw new IllegalStateException("DB Constants");
    }

    /*>>>> DB Data Types <<<<*/
    static final String PRIMARY_KEY_AUTO = " PRIMARY KEY AUTOINCREMENT";
    public static final String DB_NOT_NULL = " NOT NULL";
    public static final String DB_REAL = " REAL";
    static final String DB_TEXT = " TEXT";
    static final String DB_INT = " INTEGER";
    static final String DB_BLOB = " BLOB";
    public static final String DB_WHERE = " WHERE";
    public static final String DB_FROM = " FROM ";
    public static final String DB_SELECT = " SELECT ";
    public static final String DB_ASTERIK = " *";
    public static final String DB_AND = " AND";
    public static final String DB_EQUAL = " = ";
    public static final String DB_QT = "\"";
    public static final String DB_SPACE = " ";


    /*>>> Table Name <<<<*/
    public static final String TABLE_CART_ORDER = "CartOrderGenerate";
    public static final String TABLE_CART_PRODUCT_LIST = "CartOrderProductList";
    public static final String TABLE_SPOT_LIST = "SpotProductList";
    public static final String CHEQUE_PENDING_TABLE = "ChequePending";
    public static final String PAYMENT_COMMITMENT_TABLE = "paymentCommitment";
    public static final String GET_ITEM_CATEGORY_TABLE = "getItemCategory";
    public static final String DASHBOARD_TABLE = "dashboardTable";


    /*>>>> DB Query's <<<<*/
    public static final String CREATE_TABLE = "create table ";
    public static final String DROP_TABLE = "drop table if exists ";
    public static final String SELECT_TABLE = "select * FROM ";


    /*>>> Cart Order Field Creation <<<<*/
    static final String CO_ID = "id";
    public static final String CO_CUSTOMER_CODE = "CustomerCode";
    public static final String CO_MODE = "Mode";
    public static final String CO_STOCK_LOCATION = "StockLocation";
    public static final String CO_PAYMENT_DAYS = "PaymentDays";
    public static final String CO_DEL_SEQ = "Del_Seq";
    public static final String CO_ITEM_CODE = "itemCode";
    public static final String COLINE_QTY = "lineQty";
    public static final String CO_UNIT_PRICE = "unitPrice";
    public static final String CO_CASH_DISCOUNT = "cashDiscount";
    public static final String CO_CDC = "cdc";
    public static final String CO_DATE = "Date";
    public static final String CO_AMOUNT = "Amount";
    public static final String CO_AMOUNT_RECEIVED = "AmountReceived";
    public static final String CO_TRANSACTION_RECEIVED = "TransactionReceived";
    public static final String CO_TRANSACTION_ID = "Transaction_ID";
    public static final String CO_STATUS = "Status";
    public static final String CO_REMARKS = "Remarks";

    /*SPOT*/
    static final String SPOT_ID = "ID";
    public static final String SPOT_CUSTOMER_CODE = "CustomerCode";
    public static final String SPOT_ITEM_CODE = "ItemCode";
    public static final String SPOTLINE_QTY = "lineQty";
    public static final String SPOT_UNIT_PRICE = "UnitPrice";
    public static final String SPOT_CASH_DISCOUNT = "CashDiscount";
    public static final String SPOT_PROMO_CODE = "PromoCode";
    public static final String SPOT_ORC = "ORC";


    public static final String CO_REFERENCE_NUM = "ReferenceNo";
    public static final String CO_REF_NUM = "ReferenceNumber";
    public static final String CO_PAYMENT_TYPE = "PaymentType";
    public static final String CO_USER_ID = "UserID";
    public static final String CO_DEVICE_ID = "DeviceID";
    public static final String CO_UUID = "UID";


    /*Cheques Pending Table*/
    static final String CP_ID = "id";
    public static final String CP_USER_ID = "UserID";
    public static final String CP_DATE = "Date";
    public static final String CP_AMOUNT = "Amount";
    public static final String CP_TRANSACTION_ID = "Transaction_ID";
    public static final String CP_STATUS = "Status";
    public static final String CP_REMARKS = "Remarks";
    public static final String CP_AMOUNT_RECEIVED = "AmountReceived";
    public static final String CP_TRANSACTION_RECEIVED = "TransactionReceived";

    public static final String CP_REFERENCE_NO = "ReferenceNo";
    public static final String CP_DEVICE_ID = "DeviceID";
    public static final String CP_UID = "UID";


    /*Payment Due Table*/
    static final String PD_ID = "id";
    public static final String PD_USER_ID = "UserID";
    public static final String PD_DATE = "Date";
    public static final String PD_AMOUNT = "Amount";
    public static final String PD_TRANSACTION_ID = "Transaction_ID";
    public static final String PD_STATUS = "Status";
    public static final String PD_REMARKS = "Remarks";
    public static final String PD_AMOUNT_RECEIVED = "AmountReceived";
    public static final String PD_TRANSATION_RECEIVED = "TransationReceived";
    public static final String PD_REFERENCE_NO = "ReferenceNo";
    public static final String PD_DEVICE_ID = "DeviceID";
    public static final String PDP_UID = "UID";

    /*Get Item Code Details*/
    public static final String ID = "id";
    public static final String USER_ID = "User_id";
    public static final String CATEGORY = "category";
    public static final String DESCRIPTION = "description";
    public static final String BRAND = "brand";
    public static final String IMAGE_1 = "image_1";
    public static final String FLAG = "flag";

    /*Dashboard API Call*/
    public static final String DB_DATA = "data";


}
