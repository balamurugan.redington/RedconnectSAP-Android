package redington.com.redconnect.reddb.model;



public class ChequesPendingModel {
    private int cpId;
    private String cpUserId;
    private String cpDate;
    private String cpAmount;
    private String cpTxnID;
    private String cpStatus;
    private String cpReamrks;
    private String cpAmountReceived;
    private String cpTransactionReceived;

    private String cpRefernceNum;
    private String cpDeviceID;
    private String cpUidNum;

    public int getCpId() {
        return cpId;
    }

    public void setCpId(int cpId) {
        this.cpId = cpId;
    }

    public String getCpUserId() {
        return cpUserId;
    }

    public void setCpUserId(String cpUserId) {
        this.cpUserId = cpUserId;
    }

    public String getCpDate() {
        return cpDate;
    }

    public void setCpDate(String cpDate) {
        this.cpDate = cpDate;
    }

    public String getCpAmount() {
        return cpAmount;
    }

    public void setCpAmount(String cpAmount) {
        this.cpAmount = cpAmount;
    }

    public String getCpTxnID() {
        return cpTxnID;
    }

    public void setCpTxnID(String cpTxnID) {
        this.cpTxnID = cpTxnID;
    }

    public String getCpStatus() {
        return cpStatus;
    }

    public void setCpStatus(String cpStatus) {
        this.cpStatus = cpStatus;
    }

    public String getCpReamrks() {
        return cpReamrks;
    }

    public void setCpReamrks(String cpReamrks) {
        this.cpReamrks = cpReamrks;
    }

    public String getCpAmountReceived() {
        return cpAmountReceived;
    }

    public void setCpAmountReceived(String cpAmountReceived) {
        this.cpAmountReceived = cpAmountReceived;
    }

    public String getCpTransactionReceived() {
        return cpTransactionReceived;
    }

    public void setCpTransactionReceived(String cpTransactionReceived) {
        this.cpTransactionReceived = cpTransactionReceived;
    }

    public String getCpRefernceNum() {
        return cpRefernceNum;
    }

    public void setCpRefernceNum(String cpRefernceNum) {
        this.cpRefernceNum = cpRefernceNum;
    }

    public String getCpDeviceID() {
        return cpDeviceID;
    }

    public void setCpDeviceID(String cpDeviceID) {
        this.cpDeviceID = cpDeviceID;
    }

    public String getCpUidNum() {
        return cpUidNum;
    }

    public void setCpUidNum(String cpUidNum) {
        this.cpUidNum = cpUidNum;
    }
}
