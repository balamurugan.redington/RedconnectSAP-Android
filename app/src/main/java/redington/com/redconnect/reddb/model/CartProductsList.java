package redington.com.redconnect.reddb.model;


public class CartProductsList {
    private int coId;
    private String coItemcode;
    private String coLineQty;
    private String coUnitPrice;
    private String coCashDiscount;
    private String coUserid;
    private String coCDC;

    public int getCoId() {
        return coId;
    }

    public void setCoId(int coId) {
        this.coId = coId;
    }

    public String getCoItemcode() {
        return coItemcode;
    }

    public void setCoItemcode(String coItemcode) {
        this.coItemcode = coItemcode;
    }

    public String getCoLineQty() {
        return coLineQty;
    }

    public void setCoLineQty(String coLineQty) {
        this.coLineQty = coLineQty;
    }

    public String getCoUnitPrice() {
        return coUnitPrice;
    }

    public void setCoUnitPrice(String coUnitPrice) {
        this.coUnitPrice = coUnitPrice;
    }

    public String getCoCashDiscount() {
        return coCashDiscount;
    }

    public void setCoCashDiscount(String coCashDiscount) {
        this.coCashDiscount = coCashDiscount;
    }

    public String getCoUserid() {
        return coUserid;
    }

    public void setCoUserid(String coUserid) {
        this.coUserid = coUserid;
    }

    public String getCoCDC() {
        return coCDC;
    }

    public void setCoCDC(String coCDC) {
        this.coCDC = coCDC;
    }
}
