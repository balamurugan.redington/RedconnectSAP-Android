package redington.com.redconnect.reddb.constant;


public class DbTable {

    private DbTable() {
        throw new IllegalStateException("Db Table");
    }

    /*Pre order */
    public static final String CARTORDERTABLE = DbConstants.TABLE_CART_ORDER + "(" +
            DbConstants.CO_ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.CO_CUSTOMER_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_MODE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_STOCK_LOCATION + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_PAYMENT_DAYS + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_DEL_SEQ + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_INVOICE_SEQ + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_CDC + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_REF_NUM + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_PAYMENT_TYPE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_DATE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_AMOUNT + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_TRANSACTION_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_STATUS + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_REMARKS + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_AMOUNT_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_TRANSACTION_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_REFERENCE_NUM + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_USER_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_DEVICE_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_UUID + DbConstants.DB_TEXT + ")";


    /*Cart*/
    public static final String CARTORDERPRODUCTTABLE = DbConstants.TABLE_CART_PRODUCT_LIST + "(" +
            DbConstants.CO_ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.CO_CUSTOMER_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_ITEM_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.COLINE_QTY + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_UNIT_PRICE + DbConstants.DB_TEXT + ", " +
            DbConstants.CO_CASH_DISCOUNT + DbConstants.DB_TEXT + ")";

    /*SPOT*/
    public static final String SPOTPRODUCTLIST = DbConstants.TABLE_SPOT_LIST + "(" +
            DbConstants.SPOT_ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.SPOT_CUSTOMER_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOT_ITEM_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOTLINE_QTY + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOT_UNIT_PRICE + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOT_CASH_DISCOUNT + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOT_PROMO_CODE + DbConstants.DB_TEXT + ", " +
            DbConstants.SPOT_ORC + DbConstants.DB_TEXT + ")";

    /*Cheque Pending*/
    public static final String CHEQUEPENDINGTABLE = DbConstants.CHEQUE_PENDING_TABLE + "(" +
            DbConstants.CP_ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.CP_USER_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_DATE + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_AMOUNT + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_TRANSACTION_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_STATUS + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_REMARKS + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_AMOUNT_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_TRANSACTION_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_REFERENCE_NO + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_DEVICE_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CP_UID + DbConstants.DB_TEXT + " )";

    /*Payment Due*/
    public static final String PAYMENTDUETABLE = DbConstants.PAYMENT_COMMITMENT_TABLE + "(" +
            DbConstants.PD_ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.PD_USER_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_DATE + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_AMOUNT + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_TRANSACTION_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_STATUS + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_REMARKS + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_AMOUNT_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_TRANSATION_RECEIVED + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_REFERENCE_NO + DbConstants.DB_TEXT + ", " +
            DbConstants.PD_DEVICE_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.PDP_UID + DbConstants.DB_TEXT + ")";

    /*Dashboard Category*/
    public static final String GETCATEGORYTABLE = DbConstants.GET_ITEM_CATEGORY_TABLE + "(" +
            DbConstants.ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.USER_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.CATEGORY + DbConstants.DB_TEXT + ", " +
            DbConstants.DESCRIPTION + DbConstants.DB_TEXT + ", " +
            DbConstants.BRAND + DbConstants.DB_TEXT + ", " +
            DbConstants.IMAGE_1 + DbConstants.DB_TEXT + ", " +
            DbConstants.FLAG + DbConstants.DB_TEXT + ")";


    /*Dashboard */
    public static final String DASHBOARDTABLE = DbConstants.DASHBOARD_TABLE + "(" +
            DbConstants.ID + DbConstants.DB_INT + DbConstants.PRIMARY_KEY_AUTO + ", " +
            DbConstants.USER_ID + DbConstants.DB_TEXT + ", " +
            DbConstants.DB_DATA + DbConstants.DB_TEXT + ")";


}
