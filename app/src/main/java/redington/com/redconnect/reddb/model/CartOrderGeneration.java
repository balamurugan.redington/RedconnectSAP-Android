package redington.com.redconnect.reddb.model;


public class CartOrderGeneration {

    private int coId;
    private String coCustomerCode;
    private String coStockLocation;
    private String coPaymentDays;
    private String coDelSeq;
    private String coDate;
    private String coAmount;
    private String coTransactionID;
    private String coStatus;
    private String coRemarks;
    private String coMode;
    private String coAmountReceived;
    private String coTransactionReceived;
    private String coRefernceNum;
    private String coUserID;
    private String coDeviceID;
    private String coUuid;
    private String coInvoiceSeq;
    private String coCDC;
    private String coReferenceNumber;
    private String coPaymentType;

    public String getCoPaymentType() {
        return coPaymentType;
    }

    public void setCoPaymentType(String coPaymentType) {
        this.coPaymentType = coPaymentType;
    }

    public String getCoReferenceNumber() {
        return coReferenceNumber;
    }

    public void setCoReferenceNumber(String coReferenceNumber) {
        this.coReferenceNumber = coReferenceNumber;
    }

    public String getCoCDC() {
        return coCDC;
    }

    public void setCoCDC(String coCDC) {
        this.coCDC = coCDC;
    }

    public String getCoInvoiceSeq() {
        return coInvoiceSeq;
    }

    public void setCoInvoiceSeq(String coInvoiceSeq) {
        this.coInvoiceSeq = coInvoiceSeq;
    }

    public int getCoId() {
        return coId;
    }

    public void setCoId(int coId) {
        this.coId = coId;
    }

    public String getCoCustomerCode() {
        return coCustomerCode;
    }

    public void setCoCustomerCode(String coCustomerCode) {
        this.coCustomerCode = coCustomerCode;
    }

    public String getCoStockLocation() {
        return coStockLocation;
    }

    public void setCoStockLocation(String coStockLocation) {
        this.coStockLocation = coStockLocation;
    }

    public String getCoPaymentDays() {
        return coPaymentDays;
    }

    public void setCoPaymentDays(String coPaymentDays) {
        this.coPaymentDays = coPaymentDays;
    }

    public String getCoDelSeq() {
        return coDelSeq;
    }

    public void setCoDelSeq(String coDelSeq) {
        this.coDelSeq = coDelSeq;
    }


    public String getCoDate() {
        return coDate;
    }

    public void setCoDate(String coDate) {
        this.coDate = coDate;
    }

    public String getCoAmount() {
        return coAmount;
    }

    public void setCoAmount(String coAmount) {
        this.coAmount = coAmount;
    }

    public String getCoTransactionID() {
        return coTransactionID;
    }

    public void setCoTransactionID(String coTransactionID) {
        this.coTransactionID = coTransactionID;
    }

    public String getCoStatus() {
        return coStatus;
    }

    public void setCoStatus(String coStatus) {
        this.coStatus = coStatus;
    }

    public String getCoRemarks() {
        return coRemarks;
    }

    public void setCoRemarks(String coRemarks) {
        this.coRemarks = coRemarks;
    }

    public String getCoMode() {
        return coMode;
    }

    public void setCoMode(String coMode) {
        this.coMode = coMode;
    }

    public String getCoAmountReceived() {
        return coAmountReceived;
    }

    public void setCoAmountReceived(String coAmountReceived) {
        this.coAmountReceived = coAmountReceived;
    }

    public String getCoTransactionReceived() {
        return coTransactionReceived;
    }

    public void setCoTransactionReceived(String coTransactionReceived) {
        this.coTransactionReceived = coTransactionReceived;
    }

    public String getCoRefernceNum() {
        return coRefernceNum;
    }

    public void setCoRefernceNum(String coRefernceNum) {
        this.coRefernceNum = coRefernceNum;
    }

    public String getCoUserID() {
        return coUserID;
    }

    public void setCoUserID(String coUserID) {
        this.coUserID = coUserID;
    }

    public String getCoDeviceID() {
        return coDeviceID;
    }

    public void setCoDeviceID(String coDeviceID) {
        this.coDeviceID = coDeviceID;
    }

    public String getCoUuid() {
        return coUuid;
    }

    public void setCoUuid(String coUuid) {
        this.coUuid = coUuid;
    }
}
