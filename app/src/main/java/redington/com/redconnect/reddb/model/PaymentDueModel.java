package redington.com.redconnect.reddb.model;


public class PaymentDueModel {
    private int pdId;
    private String pdUserId;
    private String pdDate;
    private String pdAmount;
    private String pdTxnID;
    private String pdStatus;
    private String pdReamrks;
    private String pdAmountReceived;
    private String pdTransactionReceived;

    private String pdmRefNum;
    private String pdDeviceId;
    private String pdUuidNum;

    public int getPdId() {
        return pdId;
    }

    public void setPdId(int pdId) {
        this.pdId = pdId;
    }

    public String getPdUserId() {
        return pdUserId;
    }

    public void setPdUserId(String pdUserId) {
        this.pdUserId = pdUserId;
    }

    public String getPdDate() {
        return pdDate;
    }

    public void setPdDate(String pdDate) {
        this.pdDate = pdDate;
    }

    public String getPdAmount() {
        return pdAmount;
    }

    public void setPdAmount(String pdAmount) {
        this.pdAmount = pdAmount;
    }

    public String getPdTxnID() {
        return pdTxnID;
    }

    public void setPdTxnID(String pdTxnID) {
        this.pdTxnID = pdTxnID;
    }

    public String getPdStatus() {
        return pdStatus;
    }

    public void setPdStatus(String pdStatus) {
        this.pdStatus = pdStatus;
    }

    public String getPdReamrks() {
        return pdReamrks;
    }

    public void setPdReamrks(String pdReamrks) {
        this.pdReamrks = pdReamrks;
    }

    public String getPdAmountReceived() {
        return pdAmountReceived;
    }

    public void setPdAmountReceived(String pdAmountReceived) {
        this.pdAmountReceived = pdAmountReceived;
    }

    public String getPdTransactionReceived() {
        return pdTransactionReceived;
    }

    public void setPdTransactionReceived(String pdTransactionReceived) {
        this.pdTransactionReceived = pdTransactionReceived;
    }

    public String getPdmRefNum() {
        return pdmRefNum;
    }

    public void setPdmRefNum(String pdmRefNum) {
        this.pdmRefNum = pdmRefNum;
    }

    public String getPdDeviceId() {
        return pdDeviceId;
    }

    public void setPdDeviceId(String pdDeviceId) {
        this.pdDeviceId = pdDeviceId;
    }

    public String getPdUuidNum() {
        return pdUuidNum;
    }

    public void setPdUuidNum(String pdUuidNum) {
        this.pdUuidNum = pdUuidNum;
    }
}
