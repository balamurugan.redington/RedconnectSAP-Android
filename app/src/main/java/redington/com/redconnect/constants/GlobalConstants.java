package redington.com.redconnect.constants;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import redington.com.redconnect.common.activity.BaseActivity;

import static redington.com.redconnect.common.activity.BaseActivity.BILLINGDELSEQ;
import static redington.com.redconnect.common.activity.BaseActivity.CARTDELSEQ;
import static redington.com.redconnect.common.activity.BaseActivity.SP_CUSTOMERNAME;
import static redington.com.redconnect.common.activity.BaseActivity.SP_EMAIL_ID;
import static redington.com.redconnect.common.activity.BaseActivity.SP_MAIL;
import static redington.com.redconnect.common.activity.BaseActivity.SP_PH_NO;
import static redington.com.redconnect.common.activity.BaseActivity.billingaddressSeq;
import static redington.com.redconnect.common.activity.BaseActivity.cartCount;
import static redington.com.redconnect.common.activity.BaseActivity.cartDeliverySequence;
import static redington.com.redconnect.common.activity.BaseActivity.cashDiscount;
import static redington.com.redconnect.common.activity.BaseActivity.sp;


public class GlobalConstants {


    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String IMAGE_URL = "url";
    public static final String IMAGE = "image";
    /*Kamesh*/
    /*public static final String LOAD_PDF = "https://docs.google.com/viewer?url=";*/
    /*public static final String LOAD_PDF = "http://drive.google.com/viewerng/viewer?embedded=true&url=";*/
    public static final String LOAD_PDF = "https://docs.google.com/gview?embedded=true&url=";
    /*Kamesh*/
    public static final String CART_NOT_ADDED = "The cart submission failed. Please try again later.";
    public static final String SPOT_CODE = "spot_code";
    public static final String UPDATED = "Updated successfully.";
    public static final String TOOLBAR_TITLE = "title";
    public static final String CASH_DISCOUNT_ALERT = "You are entitled to avail cash discount for yor transaction, Click yes to proceed.";
    public static final String WEEK_VALUE = "week";
    public static final String CAPITAL_FLOAT_INSERT = "Your ID created Successfully.";
    public static final String OTP_NOT_MATCH = "OTP not matching";
    public static final String HDFC = "HDFC";
    public static final String EXWAREHOUSE = "AAA";
    public static final String VIEW_MORE = "View More";
    public static final String VIEW_LESS = "View Less";
    public static final String MARKET_APP = "Unable to find the app.";
    public static final String ADDRESS = "Address";
    public static final String CASH_DISCOUNT = "cash";
    public static final String PERCENT = " % ";
    public static final String DISCOUNT = "discount";
    public static final String CASH_TOAST = "Cash discount not available for the products.";
    public static final String LOCATION = "location";
    public static final String CART_ARRAY = "Cart";
    public static final String POJO_ARRAY = "Pojo Class";
    public static final String PROMOCODE_ARRAY = "Promo code";
    public static final String ORDER_FAILED = "Order Failed";
    public static final String TXN_ID = "id";
    public static final String LOGIN = "login";
    public static final String ADDRESS_ERROR = "Could not select Ex-Warehouse. Please select other Billing address.";
    /*For Shared Preference Values*/
    public static final String EMAIL = "email_Id";
    public static final String CUSTOMER = "CUSTOMER";
    public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    public static final String MAIL = "MAIL";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String RECORDING_FILE_SOUCRE = "voices/Recording.mp3";
    public static final String VOICE_FOLDER = "voices";
    public static final String VOICE_FORMAT = ".mp3";
    public static final String SPACE = " ";
    public static final String NEW_LINE = "\n \n ";
    /*Text's*/
    public static final String RETRY = "Retry";
    public static final String RE_ORDER = "re_order";
    public static final String BACK = "Press again to exit";
    public static final String INTERNET_CHECK = "Please check your Internet Connection..!";
    public static final String PAY_NOW_AGAIN = "Please click Pay Now again.";
    public static final String NO_DATA = "No Data Found";
    public static final String NO_BRANCH = "No Branch Found";
    public static final String GO_BACK = "No Info Found \n \n Please try again later.";
    public static final String FEEDBACK = "Thank You for your feedback";
    public static final String NO_FEEDBACK = "Feedback Not Saved. Please try again...!";
    public static final String ENTER_FEEDBACK = "Please enter your feedback";
    public static final String TICKET_GEN = "Your ticket has been successfully generated";
    public static final String TICKET_NOT_GENERATED = "Ticket Not Generated..!";
    public static final String TICKET_NO = "Ticket No : ";
    public static final String ORDER_NUMBER = "Order Number";
    public static final String ORDER_PLACED = "Your order has been placed successfully.";
    public static final String INVOICE_PLACED = "Invoice commitment is saved successfully.";
    public static final String ORDER_NOT_PLACED_DATA = "Payment failure. Please check your data.";
    public static final String TRANSACTION_CANCELLED = "Your transaction has been cancelled.";
    public static final String CAM_ERROR = "Please allow permissions to access pictures.";
    public static final String SOUND_ERROR = "Please allow permissions to record your voice..";
    public static final String SOUND_PLAY_ERROR = "Please allow permissions to play your voice..";
    public static final String SOUND_ENABLE_ERROR = "Please allow permissions to access Voice of Customer.";
    public static final String DEVICE_ERROR = "Please allow permissions to read device ID.";
    public static final String SERVER_ERROR = "Could not connect to server. Please try after again";
    public static final String TRY_AGAIN = "Please Try Again.";
    public static final String RECORD_AGAIN = "No voice record file found. Please record again.";
    public static final String REQUEST_SENT = "Your feedback has been successfully sent.";
    public static final String ALLOW_PERMIT = "Please allow permissions...!";
    /*Shared Preference Details*/
    public static final String LOGGED_IN = "LOGGED_IN";
    /*public static final String FURST_TIME = "FURST_TIME";*/
    public static final String FURST_TIME = "FIRST_TIME";
    public static final String LOGIN_PREFS = "REDington Connect";
    public static final String VIEW_PAGER = "VIEW_PAGER";
    public static final String REMEMBER = "REMEMBER";
    public static final String PASS_WORD = "PASS_WORD";
    public static final String SP_INTERNET_CHECK = "INTERNET_CHECK";
    public static final String SP_BACK_CHECK = "BACK_CHECK";
    public static final String SP_SELECT_CART = "SELECT_CART";
    public static final String SP_INTERNET_CHECK_BOOLEAN = "Check";
    public static final String SP_BACK_BUTTON = "Back";
    public static final String SP_SELECT_CART_BOOLEAN = "CART";
    public static final String SP_CART_COUNT = "SP_CART_COUNT";
    public static final String CART_DETAILS = "CART_DETAILS";
    public static final String IMAGE_UPLOAD = "Image uploaded successfully.";
    /*Login Activity*/
    public static final String EMAILVALI = "Please enter User Name";
    public static final String PASS_VALID = "Please enter your Password";
    public static final String YES = "Y";
    public static final String Y = "Yes";
    public static final String N = "No";
    public static final String NO = "N";
    public static final String CANCEL = "Cancel";
    public static final String CHANGE_PASS_RESPONSE = "Password Changed Successfully";
    public static final String OLD_PASS = "Please Enter the Old Password.";
    public static final String NEW_PASS = "Please Enter the New Password.";
    public static final String SERVER_ERROR_BAR = "Server Error.";
    public static final String NO_DESCRIPTION = "No Description...";
    public static final String LOGOUT = "Logout";
    public static final String WANTLOGOUT = "Do you want to Logout?";
    public static final String NOT_VALID_CUST = "Customer not Valid";
    public static final String INTERNET_ERROR = "No Internet Connection.";
    /*Pre Order*/
    public static final String ADD_TO_CART = "ADD TO CART";
    public static final String GO_TO_CART = "GO TO CART";
    public static final String ZERO_QTY = "You cannot have 0 quantity.";
    public static final String EMPTY_QTY = "Please enter the quantity required";
    public static final String CART_SUCCESS = "Item added to cart.";
    public static final String STOCK_AVAILABLE = "Stock Available";
    public static final String LOW_STOCK = "Low Stock";
    public static final String STOCK_EXHAUST = "Stock Exhausted";
    public static final String COMING_SOON = "Coming Soon ";
    /*For CART API*/
    public static final String MODE_SELECT = "Select";
    public static final String MODE_INSERT = "Insert";
    public static final String MODE_CANCEL = "Cancel";
    public static final String CART_STATUS = "X";
    public static final String CART_PRESENT = "Previous order is pending.Please click " +
            "the retry button to proceed the order.";
    public static final String CART_NOT_DELETE = "Not deleted in Cart";
    public static final String CART_CHECK = "Check cart";
    public static final String DIALOG_DISMISS = "Dialog Dismiss";
    public static final String MENU_ACTIVITY = "MenuActivity";
    public static final String PRODUCT_NAME = "PRODUCT_NAME";
    public static final String PRODUCT_PRICE = "PRODUCT_PRICE";
    public static final String STOCK_CHECK = "STOCK_CHECK";
    public static final String SORT_NO = "Sort Number";
    public static final String ITEM_CODE = "ItemCode";
    public static final String VENDOR_CODE = "VendorCode";
    /*Help Desk Activity*/
    public static final String CLOSED = "Closed";
    public static final String PENDING = "Pending";
    public static final String COMPLETE = "Completed";
    public static final String STATUS = "STATUS";
    public static final String CATEGORY = "Category";
    public static final String MOBILE_NUM = "MOBILE_NUM";
    public static final String TICKET_NUM = "TICKET_NUM";
    public static final String HP_DESC = "HP_DESC";
    public static final String HP_COMMNETS = "HP_COMMNETS";
    public static final String HP_FEED_BACK = "HP_FEED_BACK";
    public static final String HP_RATING_BAR = "HP_RATING_BAR";
    public static final String CAPTURE_PHOTO = "Capture Photo";
    public static final String CHOOSE_GALLERY = "Choose From Gallery";
    public static final String ADD_PHOTO = "Add Photo!";
    public static final String DESC_TOAST = "Please enter description.";
    public static final String ORDERNUM = "order";

    /*Order Status*/
    public static final String BIZ_DESC = "DESC";
    public static final String INV_NO = "number";
    public static final String INV_VALUE = "value";
    public static final String INV_DATE = "date";
    public static final String DUE_DATE = "duedate";
    public static final String PENDING_DAYS = "PENDING";
    public static final String OLD_DAYS = "old";
    public static final String BALANCE_DUEAMOUNT = "BALANCE_DUEAMOUNT";
    /*Customer ELigibility */
    public static final String STATUS_R = "R";
    public static final String STATUS_A = "A";
    public static final String SPOT_ARRAY = "SPOT_ARRAY";
    /*Menu*/
    public static final String CHECK = "CHECK";
    public static final String LOCKED = "LOCKED";
    /*--------- Kamesh ---------*/
    public static final String PRODUCT_SUB_CATEGORY_ACTIVITY_LIST = "ProductSubCategoryActivityList";
    public static final String SEARCH_ACTIVITY_LIST = "SearchActivityList";
    public static final String MOBILE_TYPE = "ANDROID";
    public static final String NULL_DATA = "";
    public static final String PRODUCT_ACTIVITY = "ProductActivity";
    public static final String PRODUCT_CATEGORY = "ProductCategory";
    public static final String PRODUCT_BRAND = "ProductBrand";
    public static final String PRODUCT_ITEM_CODE = "ProductItemCode";
    public static final String PRODUCT_VENDOR_CODE = "ProductVendorCode";
    public static final String SEARCH_DATA = "SearchDataInput";
    public static final String PREORDER_SPEC_BUNDLE = "PreorderSpecification";
    public static final String PRODUCT_NAME_DETAILS = "Product Name";
    public static final String CUST_CODE_VAL = "Please enter valid Customer Code";
    public static final String API_DASHBOARD_MENULIST = "REDC";
    public static final String CUSTOM_BOTTOM_SHEET = "Custom Bottom Sheet";
    public static final String PLEASE_ENTER_THE = "Please enter the ";
    public static final String EXP_VALUE = "Expected Value";
    public static final String DIALOG_DESCRIPTION = "Enter Description";
    public static final String FEED_BACK = "Please enter your feedback";
    public static final String EDT_EXPECTED_VALUE = "Enter the Expected Value ";
    public static final String EXPECTED_VALUE_VAL = "Expected Value cannot be greater than Total Overdue.";
    public static final String EXPECTED_VALUE_VA_2 = "Expected Value cannot be greater than Overdue Amount.";
    public static final String EXPECTED_ZERO_VALUE = "You cannot have 0 Value.";
    public static final String ZERO_RATING_VALUE = "Please rate our service. ";
    /*Payment Strings*/
    public static final String ORD_STATUS_PROCESSING = "PROCESSING";
    public static final String ORD_STATUS_PICKED = "PICKED";
    public static final String ORD_STATUS_INVOICE = "INVOICED";
    public static final String ORD_STATUS_DISPATCH = "DISPATCHED";
    public static final String ORD_STATUS_DELIVER = "DELIVERED";
    public static final String ORDER_STATUS_IMAGE = "statusImage";
    public static final String ORDERSTATUS_TEXT = "statusText";
    public static final String ORDER_FRAME_LAYOUT = "frameLayout";
    public static final String ORDER_FRAME_LAYOUT_WHITE = "frameLayout_";
    public static final String ORDER_STATUS_VIEW = "statusView";
    public static final String NO_RECORDS = "NO_RECORDS";
    public static final String SERVER_ERR = "SERVER_ERR";
    public static final String NODATA_ACTIVITY = "NodataActivity";
    public static final String CONNECT_ERROR = "There was a problem connecting to the server. Please try again.";
    public static final String RECORD_NO = "20";
    public static final String PRODUCT_IMAGE_URL = "ProductImageURL";
    public static final String SEARCH_TEXT = "SearchText";
    public static final String SUCCESS = "Success";
    public static final String SUCCESS_VALIDATION = "success";
    public static final String FAILURE_VALIDATION = "failure";
    public static final String CANCEL_VALIDATION = "Cancel";
    public static final String FAILURE = "Failure";
    public static final String BACKGROUND_PROCESS = "Processing your order.";
    public static final String VALID_AMOUNT = "Please enter the valid expected value.";
    public static final String RECORD_TEXT = "Do you want to cancel this recording?";
    public static final String RECORDING_TOAST = "Please stop the recording and send.";
    public static final String PLEASE_WAIT = "Please Wait...";
    public static final String ENTER_KEYWORDS = "Enter Search Keywords";
    public static final String OK = "OK";
    public static final String ALREADY_IN_CART = "Item already in cart. Go to cart to change your order qty.";
    public static final String FORGOT_PASS = "Password has been successfully sent to your registered mail id.";
    public static final String VOICE_SAVED_NAME = "Confirm File Save ?";
    /*API Validation in Payment Screen*/
    public static final String PAYMENT_SCREEN_API = "Payment Screen";
    public static final String CHEQUE_API = "Cheque Pending";
    public static final String PAYMENTDUE_API = "Payment Due";
    public static final String REFERENCE_NUM = "Reference Number";
    public static final String ENTER_TEXT = "Please enter the ";
    public static final String M_RES_CITY1 = "M_RES_CITY1";
    public static final String M_RES_CITY2 = "M_RES_CITY2";
    public static final String M_STATUS_FLAG = "A";
    public static final String M_REQ_UESR = "App";
    /*Partner Registration*/
    public static final String SELECT_CITY = "Select City";
    public static final String ENTER_CITY = "Enter City";
    public static final String VALID_CITY = "Please enter the valid city";
    public static final String PARTNER_SUCCESS = "Your details are submitted successfully. \n We will update you soon.";
    public static final String RECEIVE_OTP = "Please allow permission to receive OTP";
    public static final String PLACE_SKIP_ORDER = " Do you want to place the order?";
    public static final String CANCEL_ORDER = "Do you want to cancel this transaction ?";
    public static final String TERMS_DATA = "TERMS_DATA";
    public static final String SAVE = "save";
    public static final String SPOT_BOOELAN = " SPOT_BOOELAN";
    public static final String FROM = "FROM";
    public static final String COMING_FROM = "COMING_FROM";
    public static final String RECENT_DEALS = "RECENT_DEALS";
    public static final String RECENTFESTIVALS = "RECENTFESTIVALS";
    public static final String RECENT_TOP_SELLINGS = "RECENT_TOP_SELLINGS";
    public static final String RECENTEDM = "RECENTEDM";
    public static final String TEMP_DOWN = " is temporarily down";
    /*DashBoard*/
    public static final String M_CART = "Cart";
    public static final String M_PREORDER = "Preorder";
    public static final String M_EDM = "EDM";
    public static final String M_DASHBOARD = "Dashboard";
    public static final String M_PROFILE = "Profile";
    public static final String M_RETRY = "Retry";
    public static final String MRECENTVIEWS = "Recent Views";
    public static final String MDEALS = "Deals";
    public static final String MFESTIVALS = "Offers";
    public static final String MTOPSELLING = "Top Selling Products";
    /*Payment Gateway*/
    public static final String SURL = "https://payu.herokuapp.com/success";
    public static final String FURL = "https://payu.herokuapp.com/failure";
    public static final String URL = "http://edi.redingtonb2b.in/RedConnect-Staging/Payment.aspx";
    public static final String UDF1 = "udf1";
    public static final String UDF2 = "udf2";
    public static final String UDF3 = "udf3";
    public static final String UDF4 = "udf4";
    public static final String UDF5 = "udf5";
    /*For Testing*/
    public static final String MERCHANT_KEY = "gtKFFx";
    public static final String SALTKEY = "eCwWELxi";
    /*For Live URl*/
    //public static final String MERCHANT_KEY = "Gcizej";
    //public static final String SALTKEY = "MzKQUE1e";

    public static final String SEARCH = "Search Activity";
    public static final String PRICE = "Price ";
    public static final String OTP = "Please enter your OTP.";
    public static final String APP_NAME = "REDington Connect - Android";
    public static final String OTP_RE_GEN = "OTP sent to your registered Mobile Number.";
    public static final String OTP_SENT = "OTP sent successfully";
    public static final String OTP_DATE = "Date Expired.Please click Resent OTP.";
    public static final String OTP_TIME = "Time Expired.Please click Resent OTP.";
    public static final String OTP_FAILURE = "Invalid OTP.Please click Resent OTP.";
    public static final String PROCDUCT_ACTUAL_PRICE = "Actual Price";
    public static final String PROCDUCT_DIS_PERCENT = "Discount Percent";
    public static final String CART_DEL_SEQ = "delSeq";
    public static final String BILLING_SEQ = "billingSeq";
    public static final String PERMISSIONS = "Go to settings and enable permissions.";
    public static final String HEADERS_REQ = "Header Request";
    public static final String AMOUNT = "Payable Amount";
    public static final String CLEAR_CART = "Please clear the Cart before processing the SPOT orders";
    public static final String ORDER_GENERATION = "ORDGEN";
    public static final String INVOICE_COMMITMENT = "INVCOM";
    public static final String DASHBOARD_RETRY = "Your previous order is still in process.Please click ok to proceed.";
    public static final String CUST_CARE = "Due to some technical reasons we cannot proceed your order now. " +
            "Please contact customer care.";
    private static final String REMARKS = "remarks";
    public static final String DIALOG_REMARKS = PLEASE_ENTER_THE + REMARKS;
    private static final String ACT_PAY_COMM_DUE = "PaymentCommitmentDueActivity";
    public static final String ADD_TYP_INV_DUE = ACT_PAY_COMM_DUE + "InoviceDue";
    private static final String PLS_SELECT = "Please select ";
    public static final String PRO_ADDRESS = PLS_SELECT + "any one Address";
    /*For Payment gateway*/
    public static final String FROM_DATE = PLS_SELECT + "from date";
    public static final String TO_DATE = PLS_SELECT + "to date";
    /*JSON Formation*/
    private static final String HEADER_TYPE = "Content-Type";
    private static final String HEADER_VALUE = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String CAPITAL_FLOAT_USERNAME = "Capitalfloat";
    private static final String CAPITAL_FLOAT_PASS = "R3dC@pit@l";
    private static final String CAPITAL_FLOAT_BASIC = "Basic ";

    private static String selectText = "Please select the ";
    /*PArtner Values*/
    public static final String PARTNER_DEALER_REG = selectText + "  Dealer Registration";
    public static final String PARTNER_PAN_PROOF = selectText + " PAN Proof";
    public static final String PARTNER_TIN_PROOF = selectText + " TIN Proof";
    public static final String PARTNER_RES_PRO_PART_DIRECTOR = selectText + " Residence / Proprietor / partners / Director";
    public static final String PARTNER_PARTNERSHIP_DEED = selectText + " Partnership Deed";
    public static final String PARTNER_CETIFY_INCORPORATION = selectText + " Certification of Incorporation";
    public static final String PARTNER_MDA = selectText + " MDA";
    public static final String PARTNER_AOA = selectText + " AOA";
    public static final String PARTNER_BANK_STATE = selectText + " Bank Statement";
    public static final String PARTNER_FINANCIAL = selectText + " Financials";

    /*Kamesh*/
    /* E-Magazine */
    public static final String SUCCESS_E_MAGAZINE = "E-Magazine downloaded Successfully \n";
    public static final String FAILED_E_MAGAZINE = "E-Magazine download Failed";
    public static final String PRODUCT_PRICE_NA = "Product price is not available";
    /*Kamesh*/

    /*Order Generation*/
    public static final String ORDER_BANK_INFO = "OrderServiceGSTCharges";


    private GlobalConstants() {
        throw new IllegalStateException();
    }

    /*Headers Formation*/
    public static JSONObject getJsonHeaders() {
        JSONObject headers = new JSONObject();
        try {
            headers.put(HEADER_TYPE, HEADER_VALUE);
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }

    public static JSONObject getCapitalFloatHeaders() {
        JSONObject headers = new JSONObject();
        try {
            String credentials = CAPITAL_FLOAT_USERNAME + ":" + CAPITAL_FLOAT_PASS;
            headers.put(AUTHORIZATION, CAPITAL_FLOAT_BASIC + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP));
            headers.put(HEADER_TYPE, HEADER_VALUE);
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }


    /*Shared Preference Values*/
    public static class GetSharedValues {

        private GetSharedValues() {
            throw new IllegalStateException("Global Constants");
        }

        public static String getSpUserId() {
            return sp.getString(SP_EMAIL_ID, GlobalConstants.EMAIL);
        }


        public static String getSpUserName() {
            return sp.getString(SP_CUSTOMERNAME, GlobalConstants.CUSTOMER_NAME);
        }

        public static String getSpMail() {
            return sp.getString(SP_MAIL, GlobalConstants.MAIL);
        }

        public static String getSpMobileNo() {
            return sp.getString(SP_PH_NO, GlobalConstants.PHONE_NUMBER);
        }

        public static String getCartDelSeq() {
            return cartDeliverySequence.getString(CARTDELSEQ, GlobalConstants.CART_DEL_SEQ);
        }

        public static String getBillingSeq() {
            return billingaddressSeq.getString(BILLINGDELSEQ, GlobalConstants.BILLING_SEQ);
        }

        public static String getCashDiscount() {
            return cashDiscount.getString(CASH_DISCOUNT, GlobalConstants.CASH_DISCOUNT);
        }
    }


    public static class GetCartCount {

        private GetCartCount() {
            throw new IllegalStateException();
        }

        public static int getcartCou() {
            return cartCount.getInt(GlobalConstants.SP_CART_COUNT, 0);
        }


    }


}
