package redington.com.redconnect.constants;


public class IntentConstants {


    public static final String OTP = "OTP";
    public static final String OTP_VALIDATE = "OTP Successfully Validated.";
    public static final String TXN_SUCCESS = "Transaction Success";
    public static final String TXN_FAILUE = "Transaction Failure";
    public static final String TXN_CANCEL = "Transaction Cancelled";
    public static final String PAYOPTION = "pay";
    public static final String USER_NOT_AVL = "User ID not available. Please use valid User ID.";
    public static final String NOT_ACTIVE = "The ID is created but it's not active. Please contact capital float.";
    public static final String RESENT_OTP = "resent_otp";
    public static final String NEW_USER = "User id is not registered with capital float. please sign up now!";
    public static final String CAPITAL_FLOAT_REGISTERED = "Capital Float account is already created for this JBA ID.";


    private IntentConstants() {
        throw new IllegalStateException("Intent Constants");
    }

    /*>>>> Address Activity<<<<*/
    public static final String DELIVERY_SEQUENCE = "DeliverySequence";
    public static final String ADDRESS_1 = "Address1";
    public static final String ADDRESS_2 = "Address2";
    public static final String ADDRESS_3 = "Address3";
    public static final String ADDRESS_4 = "Address4";
    public static final String ADDRESS_5 = "Address5";
    public static final String ADDRESS_6 = "Address6";
    public static final String LOCATION_CODE = "LocationCode";
    public static final String PAYMENT_DAYS = "PaymentDays";

    /*Float Values*/
    public static final float CORNER_RADIUS_3 = 6.0f;

    /*Partner Registration*/
    public static final String FIRSTARRAY = "first";
    public static final String SECONDARRAY = "second";
    public static final String THIRDARRAY = "third";


    public static final String POLICY = "POLICIES";
    public static final String SETTINGS = "SETTINGS";

    /*OTP*/
    public static final String INVALIDOTP = "INVALID OTP";
    public static final String VALIDOTP = "VALID OTP";
    public static final String OTPREG = "OTP Regenerated";
    public static final String OTPDATE = "DATE EXPIRED";
    public static final String OTPTIME = "TIME EXPIRED";

    /*Payment Due*/
    public static final String WEEK = "week";
    public static final String WEEKVALUE = "weekValue";
    public static final String WEEKNAME = "weekName";

    /*ToolBar Titles*/
    public static final String TICKETHISTORY = "TICKET HISTORY";
    public static final String HELPDESKSTATUS = "HELP DESK STATUS";
    public static final String DASHBOARD = "DASHBOARD";
    public static final String PREORDER = "PRE ORDER";
    public static final String PRODUCTDETAIL = "PRODUCT DETAIL";
    public static final String TOTALREVIEW = "ALL REVIEWS";
    public static final String BRANDS_QUICK_GUIDE = "BRANDS QUICK GUIDE";
    public static final String ORDERDETAIL = "ORDER DETAILS";
    public static final String CART = "MY CART";
    public static final String SHIPMENTRACK = "SHIPMENT TRACKING";
    public static final String ORDERHISTORY = "ORDER HISTORY";
    public static final String TRACKDETAILS = "TRACKING DETAILS";
    public static final String CHEQUEPENDING = "CHEQUE PENDING";

    public static final String PAYMENTCOMMIT = "PAYMENT COMMITMENT";
    public static final String PAYMENTDUE = "PAYMENT DUE";
    public static final String INVOICELIST = "OVERDUE INVOICE";
    public static final String CUSTOMERVOICE = "CUSTOMER VOICE";

    public static final String PLACEORDER = "ORDER CONFIRMATION";
    public static final String PAYMENTSCREEN = "NOTIFICATIONS";
    public static final String CUSTOMERPROFILE = "REGISTRATION";
    public static final String SPOT = "SPECIAL PRICE ORDER TRANSACTION";
    public static final String MYSPOT = "MY SPOT";
    public static final String SPOT_NOTIFY = "SPOT";
    public static final String PAYMENTOPTIONS = "PAYMENT OPTIONS";
    public static final String PURCHASED = "PURCHASED ITEMS";
    public static final String SHOWBREAKUP = "BREAKUP DETAILS";
    public static final String NEWS_FEED = "News Feed";

    public static final String RETRY = "retry";
    public static final String LOGIN = "LOGIN";
    public static final String FORGOT_PASS = "Forgot Password";
    public static final String CART_UPDATE = "Cart Update";
    public static final String USER_PROFILE = "USER PROFILE";
    public static final String CHANGE_PASS = "Change password";
    public static final String IMAGE_UPLOAD = "Image Upload";

    public static final String DATE = "Due to some system shutdown process, App will be remain closed from 12AM to 7AM";

    public static final String REMARKS = "REMARKS";
    public static final String AMOUNT = "amount";
    public static final String TAG = "Red Connect";
    public static final String STOCKLOCATION = "stock location";
    public static final String PAYMENTDAYS_CARD = "payment days";
    public static final String DELIVERYSEQUENCE_CARD = "del seq";
    public static final String SPOTBOOLEAN = "spot";
    public static final String APP_ID = "app_id";
    public static final String SPOTARRAY = "spot Array";
    public static final String CARTARRAY = "cart Array";
    public static final String PAYMENTAMOUNT = "Payable Amount ";

    public static final String CREDITCARD = "CC";
    public static final String DEBITCARD = "DC";
    public static final String NETBANKING = "NB";

    public static final String EDM = "EDM";
    public static final String PRE_ORDER = "ORDER";
    public static final String AMOUNTMISMATCH = "The cart is changed. Please confirm your cart values.";
    public static final String AMOUNTMISMATCHSPOT = "The spot is changed. Please confirm your SPOT values.";

    public static final String CREDITCARD_PAYMNENT = " Credit card payment.";
    public static final String DEBITCARD_PAYMNENT = " Debit card payment.";
    public static final String NETBANKING_PAYMNENT = " Netbanking payment.";

    public static final String SERVICE_CHARGE = "Service Charge  ";
    public static final String GST = "GST 18%  ";
    public static final String PERCENT = "%";

    public static final String STATUS = "Status";
    public static final String CUST_NAME = "Name";
    public static final String ORDER_NUM = "Order Num";

    public static final String TIME = "time";

    public static final String POPULAR = "No popular products found..!";
    public static final String FIREBASE = "Couldn't reach Firebase Server. Please login again..!";

    public static final String SALEORDER = "Sales Order not yet generated";
    public static final String WEB_ORDER_NUM = "ECOM";
    public static final String WEB_ORDER_STATUS = "WEB ORDER PLACED";

    public static final String PRODUCT_NAME = "product_name";
    public static final String ITEMCODE = "item_code";
    public static final String VENDORCODE = "vendor_code";
    public static final String TOTALQUANTITY = "qty";
    public static final String BREAKUPARRAY = "breakup";
    public static final String IGST = "IGST ";
    public static final String CGST = "CGST ";
    public static final String SGST = "SGST ";
    public static final String CASH_DISCOUNT = "CASH_DISCOUNT";
    public static final String IGSTVALUE = "IGST Value";
    public static final String CGSTVALUE = "CGST Value";
    public static final String SGSTVALUE = "SGST Value";
    public static final String CASH_DISCOUNT_PRICE = "CASH_DISCOUNT_PRICE";

    public static final String Y = "Y";
    public static final String SPOTTOTTAL = "SPOT Total";
    public static final String SPOTUNITPRICE = "SPOT Unit Price";
    public static final String SPOTACTUALPRICE = "SPOT Actual Price";
    public static final String SPOTSTOCKROOM = "Spot Stock Room";

    public static final String CARTCHECK = "CHECK cart";

    public static final String CARTPAYMENT = "Payment Process is already on. Please hold adding items to the cart.";
    public static final String CAPITAL_FLOAT = "Capital Float";
    public static final String CARTDELSEQ = "000";


    /*Getting Image*/
    public static final String IMAGEURL_131 = "http://edi.redingtonb2b.in/RedConnect_Images/REDC_Brand/";
    public static final String IMAGEURL = "http://redingtonb2b.in/Ril_Images/REDC_Brand/";
    public static final String IMAGEFORMAT = ".png";

    /*Payment Response*/
    public static final String PAYU_RESPONSE = "payu_response";
    public static final String RUPEES = "\u20B9";

    public static final String PDF = "PDF";
    public static final String PDF_NAME = "RedConnect";
    public static final String PDF_URL = "Url";
    public static final String FILE_NOT = "File not found";


    /*Constants Values*/
    public static final int REQUEST_CAMERA = 100;
    public static final int SELECT_FILE = 200;


    public static final int MY_PERMISSIONS_REQUEST_ACCESS_VOICE = 102;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_VOICEPLAY = 103;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_VOICESEND = 105;
    public static final int MY_PERMISSIONS_REQUEST_VOICE_FOLDERCREATE = 106;

    public static final int SPLASH_TIME_OUT = 6000;
    public static final int BACK_TIME_OUT = 3000;
    public static final int ZERO_VALUE = 0;

    public static final String SUCCESS_URL = "200";
    public static final String FAILURE_URL = "201";
    public static final String BACK ="back";


    public static final String FLOAT_REGISTER = "Capital Float Registration";
    public static final String JBA_REGISTER = "JBA Registration";
    public static final String CUSTOMER_ELIGIBILITY = "Customer Eligibility";

    public static final String TRUE = "True";
    public static final String FALSE = "False";
    public static final String NOT_AVL = "NA";

    /* Edm Adapter (FeedBack) */
    public static final String EDM_FEEDBACK_RESULT = "FeedBack Result";
    public static final int EDM_FEEDBACK_INTENT = 107;
}
