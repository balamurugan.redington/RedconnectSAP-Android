package redington.com.redconnect.constants;


public class JsonKeyConstants {


    private JsonKeyConstants() {
        throw new IllegalStateException("JSON Key Constants");
    }

    /*Change Password*/
    public static final String JCUSERID = "UserID";
    public static final String JCOLD_PASSKEY = "old_passkey";
    public static final String JCNEW_PASSKEY = "new_passkey";

    /*Help desk*/
    public static final String JHP_USERID = "UserID";
    public static final String JHP_TICKET_NUMBER = "Ticket_Number";
    public static final String JHP_TICKET_DESC = "Description";
    /*public static final String JHP_TICKET_DEVICE_ID = "DeviceModel";*/
    public static final String JHP_TICKET_DEVICE_ID = "DeviceID";
    public static final String JHP_TICKET_OS_VERSION = "OSVersion";
    public static final String JHP_TICKET_APP_VERSION = "AppVersion";
    public static final String JHP_TICKET_UPLOAD_IMAGE = "UploadImage";

    public static final String JHP_MOBILE_NO = "MobileNo";
    public static final String JHP_FEEDBACK = "Feedback";
    public static final String JHP_RATING = "Rating";

    /*Invoice Commitment*/
    public static final String JIC_USERID = "UserID";
    public static final String JIC_DATE = "Date";
    public static final String JIC_AMOUNT = "Amount";
    public static final String JIC_TRANSACTION_ID = "Transaction_ID";
    public static final String JIC_STATUS = "Status";
    public static final String JIC_REMARKS = "Remarks";
    public static final String JIC_AMOUNT_RECEIVED = "AmountReceived";
    public static final String JIC_TRANSACTION_RECEIVED = "TransactionReceived";
    public static final String JIC_REFERENCE_NO = "ReferenceNo";
    public static final String JIC_DEVICE_ID = "DeviceID";
    public static final String JIC_UID = "UID";
    public static final String JIC_DATA = "Data";

    public static final String JIC_ACTUAL_AMOUNT = "Actual_Amount";
    public static final String JIC_SERVICE_CHARGE = "Service_Charge";
    public static final String JIC_GST = "Gst";
    public static final String JIC_BANK_NAME = "Bank_name";
    public static final String JIC_TYPE_OF_TRANSACTION = "Type_of_transaction";
    public static final String JIC_TYPE_OF_CARD = "Type_of_card";

        /*PayuMoney*/
    public static final String PAY_U_STATUS = "status";
    public static final String PAY_U_TXNID = "txnid";
    public static final String PAY_U_AMOUNT = "amount";
    public static final String PAY_U_ADDEDON = "addedon";
    public static final String PAY_U_ISSUING_BANK= "issuing_bank";
    public static final String PAY_U_MODE= "mode";
    public static final String PAY_U_CARD_TYPE= "card_type";

    /*Order Generation*/
    public static final String JOG_CUSTOMER_CODE = "CustomerCode";
    public static final String JOG_MODE = "Mode";
    public static final String JOG_STOCK_LOCATION = "StockLocation";
    public static final String JOG_PAYMENT_DAYS = "PaymentDays";
    public static final String JOG_DEL_SEQ = "Del_Seq";
    public static final String JOG_ITEMS = "Items";
    public static final String JOGITEM_CODE = "itemCode";
    public static final String JOGLINE_QTY = "lineQty";
    public static final String JOGUNIT_PRICE = "unitPrice";
    public static final String JOGCASH_DISCOUNT = "cashDiscount";
    public static final String J_PROMO_CODE = "PromoCode";

    public static final String INVOICE_COMMITMENT = "InvoiceCommitment";
    public static final String DATE = "Date";
    public static final String JOG_AMOUNT = "Amount";
    public static final String TRANSACTION_ID = "Transaction_ID";
    public static final String JOG_STATUS = "Status";
    public static final String JOG_REMARKS = "Remarks";
    public static final String JOG_AMOUNT_RECEIVED = "AmountReceived";
    public static final String JOG_TRANSACTION_RECEIVED = "TransactionReceived";
    public static final String JOG_REFERENCE_NO = "ReferenceNo";
    public static final String JOG_USER_ID = "UserID";
    public static final String JOG_DEVICE_ID = "DeviceID";
    public static final String JOG_UID = "UID";

    public static final String JOG_ACTUAL_AMOUNT = "Actual_Amount";
    public static final String JOG_SERVICE_CHARGE = "Service_Charge";
    public static final String JOG_GST = "Gst";
    public static final String JOG_BANK_NAME = "Bank_name";
    public static final String JOG_TYPE_OF_TRANSACTION = "Type_of_transaction";
    public static final String JOG_TYPE_OF_CARD = "Type_of_card";

    public static final String ORDER_VALUE = "OrderValue";

    public static final String JPRO_FEEDBACK = "Feedback";
    public static final String JPRO_REVIEW = "Review";
    public static final String JPRO_USERID = "UserID";

    public static final String FROM_DATE = "FromDate";
    public static final String TO_DATE = "ToDate";
    public static final String PAGE_NO = "PageNo";
    public static final String RECORD_NO = "RecordNo";
    public static final String ORDER_NUMBER = "Order_Number";


    public static final String CART_USERID = "User_ID";
    public static final String ITEM_CODE = "Item_Code";
    public static final String ITEM_DESCRIPTION = "Item_Description";
    public static final String VENDOR_CODE = "Vendor_Code";
    public static final String QUANTITY = "Quantity";
    public static final String UNIT_PRICE = "Unit_Price";
    public static final String CART_STATUS = "Status";
    public static final String MODE = "Mode";
    public static final String WISH_LIST = "WishList";
    public static final String DEL_SEQ = "DelSeq";

    /*Login */
    public static final String PASS_KEY = "Pwdkey";
    public static final String DEVICE = "Device";
    public static final String DEVICE_ID = "Device_ID";
    public static final String IMEI_NUMBER = "IMEI_Number";
    public static final String SOURCE = "Source";
    public static final String VERSION = "Version";

    /*Voice Recording*/
    public static final String USER_ID = "User_ID";
    public static final String MEDIA_TYPE = "Media_Type";
    public static final String MAIL_ID = "Mail_ID";
    public static final String MOBILE_NUMBER = "Mobile_Number";
    public static final String COMMENDS = "Commends";
    public static final String VOICE_DATA = "Voice_Data";


    /*Partner Profile*/
    public static final String PRMT = "PRMT";

    /*Partner Registration*/
    public static final String CUSTOMER_NAME = "CustomerName";
    public static final String CUSTOMER_ADDR1 = "CustomerAddr1";
    public static final String CUSTOMER_ADDR2 = "CustomerAddr2";
    public static final String CUSTOMER_ADDR3 = "CustomerAddr3";
    public static final String CUSTOMER_CITY = "CustomerCity";
    public static final String CUSTOMER_STATE = "CustomerState";
    public static final String PINCODE = "PINCode";

    public static final String PHONE_NO = "PhoneNo";
    public static final String PANNO = "PANNo";
    public static final String FAX_NO1 = "FaxNo1";
    public static final String CONTACT_PERSON = "ContactPerson";
    public static final String EMAIL_ID = "EMailId";
    public static final String LSTNUMBER = "LSTNumber";
    public static final String TYPEOF_CONCERN = "TypeofConcern";
    public static final String RESIDENCE_ADDR1_LN1 = "ResidenceAddr1Ln1";
    public static final String RESIDENCE_ADDR1_LN2 = "ResidenceAddr1Ln2";
    public static final String RESIDENCE_ADDR1_LN3 = "ResidenceAddr1Ln3";
    public static final String RESIDENCE_CITY1 = "ResidenceCity1";
    public static final String RESIDENCE_STATE1 = "ResidenceState1";
    public static final String RESIDENCE_ADDR2_LN1 = "ResidenceAddr2Ln1";
    public static final String RESIDENCE_ADDR2_LN2 = "ResidenceAddr2Ln2";
    public static final String RESIDENCE_ADDR2_LN3 = "ResidenceAddr2Ln3";
    public static final String RESIDENCE_CITY2 = "ResidenceCity2";
    public static final String PART_RESIDENCE_STATE2 = "ResidenceState2";
    public static final String PART_FORM_REMARKS = "FormRemarks";
    public static final String PART_STATUS_FLAG = "StatusFlag";
    public static final String PART_REQ_USER = "RequestedUser";
    public static final String PART_FORM = "Form";
    public static final String PART_AVAILABLE = "available";
    public static final String PART_BRANCH = "branch";
    public static final String PART_CREDITLIMIT = "CreditLimit";
    public static final String PART_CUST_PROFILE = "CustomerProfile";
    public static final String PART_CUST_DIV = "CustomerDivision";


    /*JSON KEY Values*/

    public static final String USERID = "UserID";
    public static final String PAGENUM = "PageNo";
    public static final String RECORDNUM = "RecordNo";
    public static final String CUSTOMERCODE = "CustomerCode";
    public static final String TXN_ID = "TransactionID";

    public static final String JREQUESTNUMBER = "RequestNumber";

    public static final String REFNUMBER = "ReferenceNumber";
    public static final String OTP = "OTP";
    public static final String ORDERVALUE = "OrderValue";
    public static final String FLAG = "Flag";

    public static final String WEEK = "Week";

    public static final String PRECATEGORY = "Category";
    public static final String PREGROUP = "Group";
    public static final String SEARCH = "Search";
    public static final String BRAND = "Brand";
    public static final String ITEMCODE = "ItemCode";
    public static final String SORT = "Sort";
    public static final String AMOUNT = "Amount";
    public static final String STATUS = "Status";
    public static final String FOLDER = "folder";
    public static final String IMAGESTRING = "Imagestring";
    public static final String CDC = "CDC";
    public static final String REF_NUM = "ReferenceNumber";
    public static final String ORC = "ORC";
    public static final String INVOICE_SEQ = "Invoice_Seq";

    /*Capital Float*/
    public static final String JBA_ID = "JBAid";

    public static final String APP_ID_DETAILS = "AppIddetails";
    public static final String ADDRESS_LIST = "address_list";

    public static final String FLOAT_LATITUDE = "latitude";
    public static final String FLOAT_LONGITUDE = "longitude";
    public static final String FLOAT_LANDMARK = "landmark";
    public static final String FLOAT_ADDRESS = "address";
    public static final String FLOAT_CITY = "city";
    public static final String FLOAT_PINCODE = "pincode";
    public static final String FLOAT_STATE = "state";
    public static final String FLOAT_OWNERSHIP_TYPE = "ownership_type";
    public static final String FLOAT_OCCUPANCY_START_DATE = "occupancy_start_date";
    public static final String FLOAT_COUNTRY = "country";
    public static final String FLOAT_POST_OFFICE = "post_office";
    public static final String FLOAT_DISTRICT = "district";
    public static final String FLOAT_ADDRESS_TYPE = "address_type";
    public static final String FLOAT_IS_ACTIVE = "is_active";
    public static final String FLOAT_IS_FI_NEED = "is_fi_needed";
    public static final String FLOAT_ENTITY_TYPE = "entity_type";
    public static final String FLOAT_ADDRESS_OWNER = "address_owner";
    public static final String FLOAT_CONTACT_LIST = "contact_list";

    public static final String FLOAT_APP_CONTEXT = "application_context";
    public static final String FLOAT_INCORPORATION_DATE = "incorporation_date";
    public static final String FLOAT_INDUSTRY_TYPE = "industry_type";
    public static final String FLOAT_MAIN_PRODUCT_CATEGORY = "main_product_category";
    public static final String FLOAT_NATURE_OF_BUSINESS = "nature_of_business";
    public static final String FLOAT_OPERATE_AS = "operate_as";
    public static final String FLOAT_OPERATE_START_DATE = "operation_start_date";
    public static final String FLOAT_REGISTERED_NAME = "registered_name";
    public static final String FLOAT_SERVICES_PROVIDED = "services_provided";
    public static final String FLOAT_CONTACTS_LIST = "contacts_list";

    /*Contact List*/
    public static final String FLOAT_CONTEXT_CONTACT = "contact_context";
    public static final String FLOAT_CONTACT_INFO = "contact_information";
    public static final String FLOAT_CONTACT_TYPE = "contact_type";
    public static final String FLOAT_CONTACT_ENTITY_TYPE = "entity_type";

    /*Meta Data Capital Float*/
    public static final String FLOAT_APP_SOURCE_NAME = "app_source_name";
    public static final String FLOAT_APP_LIFE_CYCLE_ID = "application_lifecycle_id";
    public static final String FLOAT_APP_SEGMENT = "application_segment";
    public static final String FLOAT_APP_TYPE = "application_type";
    public static final String FLOAT_PRODUCT_TYPE = "product_type";
    public static final String FLOAT_VERSION = "version";
    public static final String FLOAT_IS_ARCIVED = "is_archived";

    public static final String FLOAT_HUB_OR_SPOKE = "hub_or_spoke";
    public static final String FLOAT_IN_PROCESS_STATUS = "in_process_status";
    public static final String FLOAT_IS_FS = "is_fs";
    public static final String FLOAT_IS_PERFIOS = "is_perfios";
    public static final String FLOAT_SL_NO = "sl_no";
    public static final String FLOAT_SOURCE_ATTR_JSON = "source_attributes_jsonb";
    public static final String FLOAT_STATUS = "status";
    public static final String FLOAT_TAGS = "tags";
    public static final String FLOAT_THIRD_PARTY_REF_ID = "third_party_reference_id";

    public static final String FLOAT_BANK_ACCOUNTS = "bank_accounts";
    public static final String FLOAT_BUSINESS_DETAILS = "business_details";
    public static final String FLOAT_BUSINESS_FINANCIALS = "business_financials";
    public static final String FLOAT_ECOSYS_DETAILS = "ecosystem_details";
    public static final String FLOAT_FUND_USE_DETAILS = "fund_use_details";
    public static final String FLOAT_INDIVIDUAL_DETAILS = "individual_details";
    public static final String FLOAT_API_INTEGRATION = "api_integration";
    public static final String FLOAT_METADATA = "metadata";

    /*JBA Register*/
    public static final String JBA_EMAIL = "emailId";
    public static final String JBA_MOBILE = "mobileNo";
    public static final String JBA_NAME = "name";
    public static final String JBA_FLOAT = "floatId";
    public static final String JBA_ADDRESS = "address";
    public static final String JBA_CITY = "city";
    public static final String JBA_PINCODE = "pincode";

    /*Credit Block and Otp Verify*/
    public static final String FLOAT_APP_ID = "app_id";
    public static final String FLOAT_ORDER_ID = "order_id";
    public static final String FLOAT_SUB_ORDER_ID = "suborder_id";
    public static final String FLOAT_AMOUNT = "amount";
    public static final String FLOAT_OTP = "otp";

    public static final String STATE = "State";
    public static final String PAYMENT_TYPE = "PaymentType";

    /*Promo Code (SPOT) */
    public static final String JPROMO_CODE = "Promocode";
    public static final String JPROMO_SALE_ORD_NO = "SaleorderNumber";
    public static final String JPROMO_FLAG = "Flag";
}
