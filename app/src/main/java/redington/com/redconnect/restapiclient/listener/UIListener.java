package redington.com.redconnect.restapiclient.listener;


public interface UIListener {
    void onSuccess(Object successObject);

    void onFailure(Object failureObject);

    void onError();
}
