package redington.com.redconnect.restapiclient.service;


import org.json.JSONException;
import org.json.JSONObject;

import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.restapiclient.constants.ServiceConstant;


public class HeaderRequest {

    private HeaderRequest() {
        throw new IllegalStateException(GlobalConstants.HEADERS_REQ);
    }

    public static JSONObject getHeaderRequest(final boolean isAuthUrl, final JSONObject headers) {
        try {
            if (headers != null) {
                if (isAuthUrl) {
                    headers.put(ServiceConstant.Keys.BASE_URL_KEY, ServiceConstant.Urls.BASE_URL_SAP_V1);
                } else {
                    headers.put(ServiceConstant.Keys.BASE_URL_KEY, ServiceConstant.Urls.PRODUCT_BASE_URL);
                }
            } else {
                return null;
            }
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }

}
