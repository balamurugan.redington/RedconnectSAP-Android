package redington.com.redconnect.restapiclient.listener;


import java.util.List;

public interface PermissionResultCallback {
    void permissionGranted(int myRequestCode);

    void partialPermissionGranted(int myRequestCode, List<String> myGrantedPermission);

    void permissionDenied(int myRequestCode);

    void neverAskAgain(int myRequestCode);
}
