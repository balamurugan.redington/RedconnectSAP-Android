package redington.com.redconnect.restapiclient.constants;


public class ServiceConstant
{

    /**  Deepak<br/>
    *    This key controls if ServiceInterceptor will
    *    be added to okhttp3 client in debug mode.
    *    Set Flag to true  to add the Interceptor
    */
    public static boolean SHOULD_LOG = true;

     public class Keys {
        public static final String BASE_URL_KEY = "base_url";

        private Keys() {
            throw new IllegalStateException("Service Constants");
        }
    }

    public class Urls {


        /*Staging Base URL's */
        /*public static final String BASE_URL_LOCAL = "http://edi.redingtonb2b.in/RedConnect-Staging/api/";
        public static final String BASE_URL_QSOMEV1 = "http://edi.redingtonb2b.in/RedConnect-Staging-V1/api/";
        public static final String BASE_URL_QSOMEV2 = "http://edi.redingtonb2b.in/RedConnect-Staging-V2/api/";
        public static final String BASE_URLV1 = "http://edi.redingtonb2b.in/RedConnect-V1/api/";
        public static final String BASE_URLV2 = "http://edi.redingtonb2b.in/RedConnect-V2/api/";
        public static final String BASE_URLV3 = "http://edi.redingtonb2b.in/RedConnect-V3/api/";
        public static final String BASE_URLV4 = "http://edi.redingtonb2b.in/RedConnect-V4/api/"; */    /*Kamesh*/

        public static final String BASE_URL = "http://edi.redingtonb2b.in/RedConnect-V5/api/";   /*Kamesh*/

        public static final String CAPITAL_FLOAT_URL = "http://edi.redingtonb2b.in/EComCreditAPI/api/CreditBlock/";
        /*Production Base URL's */
        public static final String PRODUCT_BASE_URL = "http://edi.redingtonb2b.in/RedConnect-Staging/api/";
        public static final String BASE_URL_SAP = "http://edi.redingtonb2b.in/RedConnectsap-Stg/api/";
        public static final String BASE_URL_SAP_V1 = "http://edi.redingtonb2b.in/RedConnectsap-StgV1/api/";
        public static final String CAPITAL_FLOAT_CLOUD = "http://edi.redingtonb2b.in/redcloudlive/api/RedingtonCloudApi/";
        /*End Point Urls*/
        public static final String LOGIN_URL = "User/UserAccess?";
        /*Change Password*/
        public static final String CHNAGE_PASS = "User/ChangePassword";
        /*Cheque Pending*/
        public static final String CHEQUE_PENDING = "Customers/GetChequePending?";
        /*Help Desk ticket Generation*/
        public static final String TICKET_SUBMISSION = "Ticket/TicketSubmission";
        /*Ticket History*/
        public static final String TICKET_HISTORY = "Ticket/GetTickets?";
        /*Help Desk Ticket Feedback */
        public static final String TICKET_FEEDBACK = "Ticket/TicketFeedBack";
        /*Forget Password*/
        public static final String FORGOT_PASS = "User/ForgetPassword";
        /*Invoice Commitment For Payment Due & Cheque Pending*/
        public static final String INVOICE_COMMITMENT = "Invoice/InvoiceCommitment";
        public static final String ORDER_PLACED = "Preorder/OrderGenerationSplitUp";

        /*Order Placed For Cart*/
        /*Customer Eligibility */
        public static final String CUSTOMER_ELIGIBLE = "Customers/GetCustomerEligibility";
        /*Product Feed bacK*/
        public static final String PRODUCT_FEEDBACK = "Product/ProductFeedbackReview";
        public static final String CARTDATA = "Preorder/ShoppingCartwithoutEncrypt";

        /*Cart Insert , Update and Delete*/
        /*User Profile*/
        public static final String USERDETAILS = "User/UserProfile?";
        /*Pre Order Cat*/
        public static final String PREORDER_CAT = "Product/GetItemCategory?";
        /* PreOrder */
        public static final String PREORDER_DELIVER = "Customers/GetDeliverySequence?";
        /*PreOrder List, Sort and Product Details*/
        public static final String PREORDER_ITEMDETAILS = "Product/GetItemCodeDetails";
        /*Order Status*/
        public static final String GETORDERSTATUS = "Preorder/GetOrders";
        /*Voice Of Customer*/
        public static final String GETVOICERECORDER = "Ticket/GetVoice";
        /*Customer Billing*/
        public static final String CUSTOMER_BILLING = "Preorder/CheckCustomerBilling";
        /*Promo Code*/
        public static final String PROMO_CODE = "Product/PromoCode";
        /*Partner City*/
        public static final String PARTNER_CITY = "Partner/PartnerCity";
        /*Partner Creation*/
        public static final String PARTNER_CREATION = "Partner/PartnerCreation";
        /*DashBoard*/
        public static final String DASHBOARD = "Invoice/Dashboard";
        /*EDM*/
        public static final String EDM = "Invoice/EDMDetails?";
        /*OTP*/
        public static final String OTP = "Invoice/OTPGeneration";
        /*Payment Due New*/
        public static final String CUSTOMER_CREDIT = "Customers/GetPaymentDues?";
        /*Service Tax*/
        public static final String SERVICE_TAX = "Invoice/BankServiceCharge?";
        /*Cart Status Updation*/
        public static final String CART_STATUS_CHECK = "Invoice/AddCartStatusUpdate?";
        /*PDF*/
        public static final String GET_PDF = "Preorder/DocumentCollection?";
        /*Image Uplaod*/
        public static final String IMAGE_UPLOAD = "User/UserProfileUpload";
        /*Retry Function*/
        public static final String RETRY_REQ = "Product/CheckingWebOrderEntry?";
        /*Get branch List*/
        public static final String GET_BRANCH = "Partner/BranchSelection?";
        /*Re Order*/
        public static final String RE_ORDER = "Preorder/ErrorReOrder";
        /*Capital Float*/
        public static final String CHECK_JBA_ID = "CheckJBAId?";
        public static final String REGISTER_CAPITAL_FLOAT = "CF_Jcall";
        public static final String REGISTER_JBA_FLOAT = "InsertNewUsers";
        public static final String CHECK_CREDIT_BLOCK = "CF_JcreditBlock";
        public static final String OTP_VERIFY = "CF_OTPVerify";
        public static final String OTP_RESENT = "CF_ResendOTP";

        private Urls() {
            throw new IllegalStateException("Service Constants");
        }

    }
}
