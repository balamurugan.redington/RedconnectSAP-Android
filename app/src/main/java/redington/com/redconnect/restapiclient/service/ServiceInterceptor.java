package redington.com.redconnect.restapiclient.service;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created By   Deepak<br/>
 * Created Date 27/11/2018<br/><br/>
 * Interceptor Interface that Logs all service calls
 * with url , request params & responseBody
 * works only in Debug Mode<br/><br/>
 *
 * @see redington.com.redconnect.restapiclient.constants.ServiceConstant#SHOULD_LOG
 */
public class ServiceInterceptor implements Interceptor
{
    private static final String tag = "url";

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request originalRequest = chain.request(); //Current Request
        Response response = chain.proceed(originalRequest); //Get response of the request
        {
            String bodyString = response.body().string();

            //Logging Request
            Log.d(tag, originalRequest.method() + " " + String.valueOf(originalRequest.url()));
            Log.d(tag, "Request Body : " + requestBodyToString(originalRequest.body()));

            //UnComment to Log the headders
            /*Headers headders = originalRequest.headers();
            if(headders!=null && headders.size()>0)
            {
                for(int i=0;i<headders.size();i++)
                    Log.d(tag,headders.name(i)+" : "+headders.value(i));
            }
            */

            //Logging Response
            Log.d(tag, "<-- " + response.code() + " " + response.message() + " " + String.valueOf(originalRequest.url()));
            Log.d(tag, "Response Body : " + bodyString);
            response = response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
        }
        return response;
    }

    private String requestBodyToString(RequestBody requestBody)
    {
        String reqBody = "No Request Body.";
        Buffer buffer = null;

        try
        {
            buffer = new Buffer();
            if (requestBody != null) {
                requestBody.writeTo(buffer);
                reqBody = buffer.readUtf8();
                buffer.close();
            }
        } catch (Exception e) {
            if(buffer!=null)
                buffer.close();
            reqBody = "Error When Converting Request Body";
        } finally {
            if(buffer!=null)
            buffer.close();
        }

        return reqBody;
    }
}

