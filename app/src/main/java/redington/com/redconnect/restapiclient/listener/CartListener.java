package redington.com.redconnect.restapiclient.listener;


public interface CartListener {

    void cartSuccess(Object successObject);

    void cartFailure(Object failureObject);

    void cartError();
}
