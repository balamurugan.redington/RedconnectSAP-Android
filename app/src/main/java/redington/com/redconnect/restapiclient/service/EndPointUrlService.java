package redington.com.redconnect.restapiclient.service;


import okhttp3.RequestBody;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.capitalfloat.model.CheckJbaIdReponse;
import redington.com.redconnect.redconnect.capitalfloat.model.CreditBlockResponse;
import redington.com.redconnect.redconnect.capitalfloat.model.FloatRegisterResponse;
import redington.com.redconnect.redconnect.capitalfloat.model.RegisterJbaResponse;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingJsonResponse;
import redington.com.redconnect.redconnect.dashboard.model.DashboardJsonresponse;
import redington.com.redconnect.redconnect.dashboard.model.EDMJsonResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketFeedbackResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryJsonResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketSubmissionJsonResponse;
import redington.com.redconnect.redconnect.login.model.BranchResponse;
import redington.com.redconnect.redconnect.login.model.CityJsonResponse;
import redington.com.redconnect.redconnect.login.model.ForgotPassJsonResponse;
import redington.com.redconnect.redconnect.login.model.LoginJsonResponse;
import redington.com.redconnect.redconnect.login.model.PartnerRegistrationJsonResponse;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailJsonResponse;
import redington.com.redconnect.redconnect.orderstatus.model.OrderStatusJsonResponse;
import redington.com.redconnect.redconnect.payment.model.CartUpdationJsonResponse;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.payment.model.ServiceTaxJsonResponse;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.EncryptedInvoiceResponse;
import redington.com.redconnect.redconnect.preorder.model.OtpJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ChangePasswordJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ImageUploadJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ProfileJsonResponse;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackJsonResponse;
import redington.com.redconnect.redconnect.ratingfeedback.model.RatingFeedbackJsonResponse;
import redington.com.redconnect.redconnect.settings.model.Pdfjsonresponse;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.redconnect.voiceofcustomer.model.VoiceRecordJsonResponse;
import redington.com.redconnect.restapiclient.constants.ServiceConstant;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

interface EndPointUrlService {

    /*Change Password*/
    @POST(ServiceConstant.Urls.CHNAGE_PASS)
    Call<ChangePasswordJsonResponse> getPassword(@Body RequestBody requestBody);


    /*Cheques Pending*/
    @GET(ServiceConstant.Urls.CHEQUE_PENDING)
    Call<ChequePendingJsonResponse> getCheques(@Query(JsonKeyConstants.USERID) String userID,
                                               @Query(JsonKeyConstants.PAGENUM) String pageNum,
                                               @Query(JsonKeyConstants.RECORDNUM) String recordNum);

    /*Ticket History*/
    @GET(ServiceConstant.Urls.TICKET_HISTORY)
    Call<TicketHistoryJsonResponse> getTicketHistory(@Query(JsonKeyConstants.USERID) String userId);

    /*Pre Order CATEGORY*/
    @GET(ServiceConstant.Urls.PREORDER_CAT)
    Call<PreorderCategoryJsonResponse> getPreorderCatListNew(@Query(JsonKeyConstants.PRECATEGORY) String category,
                                                             @Query(JsonKeyConstants.PREGROUP) String brand,
                                                             @Query(JsonKeyConstants.CUSTOMERCODE) String userId);


    /*Pre Order Item List*/
    @POST(ServiceConstant.Urls.PREORDER_ITEMDETAILS)
    Call<PreorderItemListJsonResponse> getPreorderItemList(@Body RequestBody requestBody);

    /*Pre Order Item Details*/
    @POST(ServiceConstant.Urls.PREORDER_ITEMDETAILS)
    Call<PreorderItemDetailsJsonResponse> getPreorderItemDetails(@Body RequestBody requestBody);

    /*Pre Order Delivery Sequence*/
    @GET(ServiceConstant.Urls.PREORDER_DELIVER)
    Call<PreorderAddressJsonResponse> getPreorderAddressList(@Query(JsonKeyConstants.USERID) String userId);


    /* User Profile Details*/
    @GET(ServiceConstant.Urls.USERDETAILS)
    Call<ProfileJsonResponse> getUserProfileDetails(@Query(JsonKeyConstants.USERID) String userId);

    /*Login*/
    @POST(ServiceConstant.Urls.LOGIN_URL)
    Call<LoginJsonResponse> getLoginList(@Body RequestBody body);

    /*Ticket Submission*/
    @POST(ServiceConstant.Urls.TICKET_SUBMISSION)
    Call<TicketSubmissionJsonResponse> getTickSubmit(@Body RequestBody body);

    /*Ticket Feedback*/
    @POST(ServiceConstant.Urls.TICKET_FEEDBACK)
    Call<TicketFeedbackResponse> getTicketFeedback(@Body RequestBody requestBody);


    /*Forgot Password*/
    @POST(ServiceConstant.Urls.FORGOT_PASS)
    Call<ForgotPassJsonResponse> getForgotPass(@Body RequestBody requestBody);

    /*Invoice Commitment*/
    @POST(ServiceConstant.Urls.INVOICE_COMMITMENT)
    Call<EncryptedInvoiceResponse> getInvoiceCommitmentServiceCall(@Body RequestBody requestBody);

    /*Order Status*/
    @POST(ServiceConstant.Urls.GETORDERSTATUS)
    Call<OrderStatusJsonResponse> getOrderStatusService(@Body RequestBody requestBody);

    /*Order Status Details*/
    @POST(ServiceConstant.Urls.GETORDERSTATUS)
    Call<OrderDetailJsonResponse> getOrderDetailService(@Body RequestBody requestBody);

    /*Voice Recording*/
    @POST(ServiceConstant.Urls.GETVOICERECORDER)
    Call<VoiceRecordJsonResponse> getVoiceRecordServic(@Body RequestBody requestBody);

    /*Order Placed*/
    @POST(ServiceConstant.Urls.ORDER_PLACED)
    Call<EncryptedOrderResponse> getSkipPlaceOrder(@Body RequestBody requestBody);

    /*Customer Eligibility */
    @POST(ServiceConstant.Urls.CUSTOMER_ELIGIBLE)
    Call<CustomerEligibilityJsonResponse> getEligibility(@Body RequestBody body);

    /*Product FeedBack*/
    @POST(ServiceConstant.Urls.PRODUCT_FEEDBACK)
    Call<RatingFeedbackJsonResponse> getRaitngFeedBack(@Body RequestBody requestBody);

    /*Cart Insert , Update and Delete*/
    @POST(ServiceConstant.Urls.CARTDATA)
    Call<CartJsonResponse> getCartData(@Body RequestBody body);


    /*Get Promo Code*/
    @POST(ServiceConstant.Urls.PROMO_CODE)
    Call<PromoCodeJsonResponse> getPromo(@Body RequestBody body);


    /*Partner City*/
    @POST(ServiceConstant.Urls.PARTNER_CITY)
    Call<CityJsonResponse> getCity(@Body RequestBody body);

    /*Partner Registration*/
    @POST(ServiceConstant.Urls.PARTNER_CREATION)
    Call<PartnerRegistrationJsonResponse> getPartner(@Body RequestBody body);


    /*DashBoard Response*/
    @POST(ServiceConstant.Urls.DASHBOARD)
    Call<DashboardJsonresponse> getDashboard(@Body RequestBody body);

    /*EDM response*/
    @GET(ServiceConstant.Urls.EDM)
    Call<EDMJsonResponse> getEDM(@Query(JsonKeyConstants.CUSTOMERCODE) String userId,
                                 @Query(JsonKeyConstants.JREQUESTNUMBER) String month);

    /*OTP*/
    @POST(ServiceConstant.Urls.OTP)
    Call<OtpJsonResponse> postOtp(@Body RequestBody body);

    /*Payment Due*/
    @GET(ServiceConstant.Urls.CUSTOMER_CREDIT)
    Call<PaymentDueResponse> getPaymentDue(@Query(JsonKeyConstants.CUSTOMERCODE) String userId,
                                           @Query(JsonKeyConstants.WEEK) String week);

    /*Service Tax*/
    @GET(ServiceConstant.Urls.SERVICE_TAX)
    Call<ServiceTaxJsonResponse> getTax(@Query(JsonKeyConstants.AMOUNT) String amount);

    /*Feedback*/
    @POST(ServiceConstant.Urls.CUSTOMER_BILLING)
    Call<FeedbackJsonResponse> feedBackCall(@Body RequestBody body);


    /*Cart Updation*/
    @GET(ServiceConstant.Urls.CART_STATUS_CHECK)
    Call<CartUpdationJsonResponse> getCartUpdation(@Query(JsonKeyConstants.USERID) String userid,
                                                   @Query(JsonKeyConstants.STATUS) String status);

    /*PDF List*/
    @GET(ServiceConstant.Urls.GET_PDF)
    Call<Pdfjsonresponse> getPdfList(@Query(JsonKeyConstants.FOLDER) String value);

    /*Image Upload*/
    @POST(ServiceConstant.Urls.IMAGE_UPLOAD)
    Call<ImageUploadJsonResponse> imageUpload(@Body RequestBody requestBody);

    /*Retry API Call*/
    @GET(ServiceConstant.Urls.RETRY_REQ)
    Call<RetryResponse> checkRetry(@Query(JsonKeyConstants.CUSTOMERCODE) String custCode);

    /*Branch List*/
    @GET(ServiceConstant.Urls.GET_BRANCH)
    Call<BranchResponse> getBranch(@Query(JsonKeyConstants.STATE) String selectedState);

    /*Re Order */
    @POST(ServiceConstant.Urls.RE_ORDER)
    Call<EncryptedOrderResponse> getReOrder(@Body RequestBody requestBody);

    /*Capital Float*/
    @GET(ServiceConstant.Urls.CHECK_JBA_ID)
    Call<CheckJbaIdReponse> checkJbaId(@Query(JsonKeyConstants.JBA_ID) String userId);

    @POST(ServiceConstant.Urls.REGISTER_CAPITAL_FLOAT)
    Call<FloatRegisterResponse> registerCapitalFLoat(@Body RequestBody requestBody);

    @POST(ServiceConstant.Urls.REGISTER_JBA_FLOAT)
    Call<RegisterJbaResponse> registerJba(@Body RequestBody body);

    @POST(ServiceConstant.Urls.CHECK_CREDIT_BLOCK)
    Call<CreditBlockResponse> checkCreditBlock(@Body RequestBody requestBody);

    @POST(ServiceConstant.Urls.OTP_VERIFY)
    Call<CreditBlockResponse> otpVerification(@Body RequestBody requestBody);

    @POST(ServiceConstant.Urls.OTP_RESENT)
    Call<CreditBlockResponse> resentOtp(@Body RequestBody requestBody);


}

