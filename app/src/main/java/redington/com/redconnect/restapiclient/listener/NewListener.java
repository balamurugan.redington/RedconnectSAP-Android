package redington.com.redconnect.restapiclient.listener;


public interface NewListener  {
    void mSuccessObject(Object successObject);

    void mFailureObject(Object failureObject);

    void mError();
}
