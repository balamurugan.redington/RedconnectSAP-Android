package redington.com.redconnect.restapiclient.service;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import redington.com.redconnect.BuildConfig;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.redconnect.capitalfloat.model.CheckJbaIdReponse;
import redington.com.redconnect.redconnect.capitalfloat.model.CreditBlockResponse;
import redington.com.redconnect.redconnect.capitalfloat.model.FloatRegisterResponse;
import redington.com.redconnect.redconnect.capitalfloat.model.RegisterJbaResponse;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingJsonResponse;
import redington.com.redconnect.redconnect.dashboard.model.DashboardJsonresponse;
import redington.com.redconnect.redconnect.dashboard.model.EDMJsonResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketFeedbackResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryJsonResponse;
import redington.com.redconnect.redconnect.helpdesk.model.TicketSubmissionJsonResponse;
import redington.com.redconnect.redconnect.login.model.BranchResponse;
import redington.com.redconnect.redconnect.login.model.CityJsonResponse;
import redington.com.redconnect.redconnect.login.model.ForgotPassJsonResponse;
import redington.com.redconnect.redconnect.login.model.LoginJsonResponse;
import redington.com.redconnect.redconnect.login.model.PartnerRegistrationJsonResponse;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailJsonResponse;
import redington.com.redconnect.redconnect.orderstatus.model.OrderStatusJsonResponse;
import redington.com.redconnect.redconnect.payment.model.CartUpdationJsonResponse;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.payment.model.ServiceTaxJsonResponse;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.EncryptedInvoiceResponse;
import redington.com.redconnect.redconnect.preorder.model.OtpJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ChangePasswordJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ImageUploadJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ProfileJsonResponse;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackJsonResponse;
import redington.com.redconnect.redconnect.ratingfeedback.model.RatingFeedbackJsonResponse;
import redington.com.redconnect.redconnect.settings.model.Pdfjsonresponse;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.redconnect.voiceofcustomer.model.VoiceRecordJsonResponse;
import redington.com.redconnect.restapiclient.constants.ServiceConstant;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static JSONObject mHeaderObject = null;
    private EndPointUrlService apiService;
    private String baseUrl = "";


    private RestClient() {
        initService();
    }

    public static RestClient getInstance(JSONObject headerObject) {
        mHeaderObject = headerObject;
        return new RestClient();
    }

    public static RestClient getInstance(JSONObject headerObject,int flag) {
        mHeaderObject = headerObject;
        return new RestClient();
    }

    /*Login API*/
    public Call<LoginJsonResponse>
    getloginList(RequestBody values) {
        return apiService.getLoginList(values);
    }

    /*Change Password*/
    public Call<ChangePasswordJsonResponse> getChangePassword(RequestBody values) {
        return apiService.getPassword(values);
    }

    /*Cheque Pending*/
    public Call<ChequePendingJsonResponse> getChequesList(String userID, String pageNum, String recordNum) {
        return apiService.getCheques(userID, pageNum, recordNum);
    }


    /*Ticket History*/
    public Call<TicketHistoryJsonResponse> getTicketHistoryList(String userId) {
        return apiService.getTicketHistory(userId);
    }


    /*Ticket Submission*/
    public Call<TicketSubmissionJsonResponse> getTicketSubmit(RequestBody values) {
        return apiService.getTickSubmit(values);
    }

    /*Ticket Feedback*/
    public Call<TicketFeedbackResponse> getTicketFeedback(RequestBody requestBody) {
        return apiService.getTicketFeedback(requestBody);
    }

    /*Pre Order CATEGORY API*/
    public Call<PreorderCategoryJsonResponse> getPreorderCatList(String category, String brand, String userID) {
        return apiService.getPreorderCatListNew(category, brand, userID);
    }

    /*Pre Order Product List API*/
    public Call<PreorderItemListJsonResponse> getPreorderItemList(RequestBody requestBody) {
        return apiService.getPreorderItemList(requestBody);
    }

    /*Pre Order Product Details API*/
    public Call<PreorderItemDetailsJsonResponse> getPreorderItemDetails(RequestBody requestBody) {
        return apiService.getPreorderItemDetails(requestBody);
    }

    /*Pre Order Delivery Sequence API*/
    public Call<PreorderAddressJsonResponse> getPreorderAddressList(String userId) {
        return apiService.getPreorderAddressList(userId);
    }


    /*Forgot Password*/
    public Call<ForgotPassJsonResponse> getForgotPass(RequestBody requestBody) {
        return apiService.getForgotPass(requestBody);
    }

    /*Invoice Commitment*/
    public Call<EncryptedInvoiceResponse> getInvoiceCommitmentServiceCall(RequestBody requestBody) {
        return apiService.getInvoiceCommitmentServiceCall(requestBody);
    }

    /*Order Status Details*/
    public Call<OrderDetailJsonResponse> getOrderDetailService(RequestBody requestBody) {
        return apiService.getOrderDetailService(requestBody);
    }

    /*Voice Recording*/
    public Call<VoiceRecordJsonResponse> getVoiceRecordServic(RequestBody requestBody) {
        return apiService.getVoiceRecordServic(requestBody);
    }

    /*Order Status*/
    public Call<OrderStatusJsonResponse> getOrderStatusService(RequestBody requestBody) {
        return apiService.getOrderStatusService(requestBody);
    }

    /*Skip and Place Order*/
    public Call<EncryptedOrderResponse> getOrderPlacedServiceCall(RequestBody requestBody) {
        return apiService.getSkipPlaceOrder(requestBody);
    }

    /*Customer Eligibility */
    public Call<CustomerEligibilityJsonResponse> getCustomerEligible(RequestBody body) {
        return apiService.getEligibility(body);
    }

    /*Product FeedBack*/
    public Call<RatingFeedbackJsonResponse> getRaitngFeedBack(RequestBody requestBody) {
        return apiService.getRaitngFeedBack(requestBody);
    }


    /*Cart Insert , Update and Delete*/
    public Call<CartJsonResponse> getCartServiceCall(RequestBody body) {
        return apiService.getCartData(body);
    }

    /* User Profile Details */
    public Call<ProfileJsonResponse> getUserProfileDetails(String userId) {
        return apiService.getUserProfileDetails(userId);
    }


    /*Get Promo Code*/
    public Call<PromoCodeJsonResponse> getPromoCode(RequestBody body) {
        return apiService.getPromo(body);
    }


    /*Partner City*/
    public Call<CityJsonResponse> getCityCall(RequestBody body) {
        return apiService.getCity(body);
    }

    /*PartnerRegistration*/
    public Call<PartnerRegistrationJsonResponse> getPartnerRegistration(RequestBody body) {
        return apiService.getPartner(body);
    }


    /*Dashboard Response*/
    public Call<DashboardJsonresponse> gerDashboardCall(RequestBody body) {
        return apiService.getDashboard(body);
    }

    /*EDM Details*/
    public Call<EDMJsonResponse> getEDMList(String userId, String requestNumber) {
        return apiService.getEDM(userId, requestNumber);
    }

    /*OTP */
    public Call<OtpJsonResponse> postOtpCall(RequestBody body) {
        return apiService.postOtp(body);
    }

    /*Payment Due*/
    public Call<PaymentDueResponse> getPayment(String userId, String week) {
        return apiService.getPaymentDue(userId, week);
    }

    /*Service Tax*/
    public Call<ServiceTaxJsonResponse> getServiceTax(String amount) {
        return apiService.getTax(amount);
    }

    /*FeedBack Call*/
    public Call<FeedbackJsonResponse> getFeedback(RequestBody requestBody) {
        return apiService.feedBackCall(requestBody);
    }

    /*Cart Updation*/
    public Call<CartUpdationJsonResponse> getCartUpdation(String userID, String status) {
        return apiService.getCartUpdation(userID, status);
    }

    /*Get PDF List*/
    public Call<Pdfjsonresponse> getPdfList(String value) {
        return apiService.getPdfList(value);
    }


    /*Image Uplaod*/
    public Call<ImageUploadJsonResponse> getImageUpload(RequestBody requestBody) {
        return apiService.imageUpload(requestBody);
    }

    /*Capital Float*/
    public Call<CheckJbaIdReponse> checkJbaId(String userId) {
        return apiService.checkJbaId(userId);
    }

    public Call<FloatRegisterResponse> floatRegister(RequestBody requestBody) {
        return apiService.registerCapitalFLoat(requestBody);
    }

    public Call<RegisterJbaResponse> getRegisterJba(RequestBody requestBody) {
        return apiService.registerJba(requestBody);
    }

    /*Retry Function*/
    public Call<RetryResponse> getRetryReq(String custCode) {
        return apiService.checkRetry(custCode);
    }


    /*Credit Block*/
    public Call<CreditBlockResponse> creditBlock(RequestBody requestBody) {
        return apiService.checkCreditBlock(requestBody);
    }

    /*OTP Verification*/
    public Call<CreditBlockResponse> otpVerify(RequestBody body) {
        return apiService.otpVerification(body);
    }

    /*Resent OTP*/
    public Call<CreditBlockResponse> resentOtpVerify(RequestBody requestBody) {
        return apiService.resentOtp(requestBody);
    }

    /*Get Branch list*/
    public Call<BranchResponse> getBranchList(String selectedState) {
        return apiService.getBranch(selectedState);
    }

    /*Re Order Generation*/
    public Call<EncryptedOrderResponse> reOrderPlacedCall(RequestBody requestBody) {
        return apiService.getReOrder(requestBody);
    }


    private void initService() {
        Retrofit retrofit = createAdapter().build();
        apiService = retrofit.create(EndPointUrlService.class);
    }




    private Retrofit.Builder createAdapter() {

        try {
            baseUrl = mHeaderObject.getString(ServiceConstant.Keys.BASE_URL_KEY);

        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(addCustomHeaders(mHeaderObject))
                .addConverterFactory(GsonConverterFactory.create());
    }

    private OkHttpClient addCustomHeaders(final JSONObject authorizationValue) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.writeTimeout(30, TimeUnit.SECONDS);

        /*Deepak*/
        /** Add the Interceptor only in Debug Builds */
        if (BuildConfig.DEBUG && ServiceConstant.SHOULD_LOG )
            httpClient.addInterceptor(new ServiceInterceptor());
        /*Kamesh*/
        //httpClient.addInterceptor((new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)));

        /*Kamesh*/
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if (authorizationValue != null) {
                    final Request.Builder request = chain.request().newBuilder();

                    Iterator<String> iterator = authorizationValue.keys();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        try {
                            if (!key.contains(ServiceConstant.Keys.BASE_URL_KEY)) {
                                Object value = authorizationValue.get(key);
                                request.addHeader(key, value.toString());
                            }
                        } catch (JSONException e) {
                            BaseActivity.logd(e.getMessage());
                        }
                    }

                    return chain.proceed(request.build());
                }

                return null;
            }
        });

        mHeaderObject = null;

        return httpClient.build();
    }


}
