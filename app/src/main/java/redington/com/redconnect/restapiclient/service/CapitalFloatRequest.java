package redington.com.redconnect.restapiclient.service;

import org.json.JSONException;
import org.json.JSONObject;

import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.restapiclient.constants.ServiceConstant;

public class CapitalFloatRequest {

    private CapitalFloatRequest() {
        throw new IllegalStateException("Header Request");
    }

    public static JSONObject getHeaderRequest(final boolean isAuthUrl, final JSONObject headers) {
        try {
            if (headers != null) {
                if (isAuthUrl) {
                    headers.put(ServiceConstant.Keys.BASE_URL_KEY, ServiceConstant.Urls.CAPITAL_FLOAT_URL);
                } else {
                    headers.put(ServiceConstant.Keys.BASE_URL_KEY, ServiceConstant.Urls.CAPITAL_FLOAT_CLOUD);
                }
            } else {
                return null;
            }
        } catch (JSONException e) {
            BaseActivity.logd(e.getMessage());
        }
        return headers;
    }

}
