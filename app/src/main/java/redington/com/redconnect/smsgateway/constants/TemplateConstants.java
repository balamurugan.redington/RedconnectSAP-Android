package redington.com.redconnect.smsgateway.constants;


import redington.com.redconnect.constants.GlobalConstants;

public class TemplateConstants {

    private TemplateConstants() {
        throw new IllegalStateException("Template Constants");
    }

    public static final String ORDERCONFIRMATIONMNUMBER = "7506";
    public static final String TICKETGENERATION = "7515";
    public static final String TICKETFEEDBACK = "7517";

    public static final String MOBILENUM = GlobalConstants.GetSharedValues.getSpMobileNo();

    public static final String PIPESYMBOL = "|";
    public static final String SYMBOL = "~";

    public static final String MESSAGE = "&message=";
    public static final String TEAM = "Team Redington";

    private static final String PASS = "redinprotxn";

    public static final String BASE_URL = "http://www.meru.co.in/wip/sendsms?username=redinprotxn&password=" + PASS + "&FROM=REDGTL&to=";
}
