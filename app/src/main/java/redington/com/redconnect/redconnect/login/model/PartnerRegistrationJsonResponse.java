
package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;


public class PartnerRegistrationJsonResponse {

    @SerializedName("Data")
    private PaymentRegisterData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public PaymentRegisterData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
