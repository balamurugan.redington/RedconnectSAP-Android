package redington.com.redconnect.redconnect.dashboard.activity;

import android.Manifest;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.support.v4.app.ActivityCompat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.FileDownloader;
import redington.com.redconnect.util.PermissionUtils;

public class WebViewActivity extends BaseActivity implements PermissionResultCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    private ProgressBar progressBar;
    private WebView mWebView;
    private FloatingActionButton fileDownload;
    /*Kamesh*/
    private Context mContext;
    private ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;
    private CommonUtils mCommonUtils;
    private String pdfUrl;
    /*Kamesh*/
    /*Kamesh -> Added Write_External_Storage Permission with implements.*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        /*Kamesh*/
        mContext = WebViewActivity.this;
        mCommonUtils = new CommonUtils(mContext);
        /*Kamesh*/

        progressBar = findViewById(R.id.progressBar);
        fileDownload = findViewById(R.id.fab);
        fileDownload.setVisibility(View.GONE);
        /*Kamesh*/
        permissionUtils = new PermissionUtils(mContext);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        /*Kamesh*/

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            pdfUrl = bundle.getString(GlobalConstants.IMAGE_URL);    /*Kamesh*/
            mWebView = findViewById(R.id.webview);
            mWebView.setWebViewClient(new MyWebClient());
            mWebView.getSettings().setJavaScriptEnabled(true);
            String url = GlobalConstants.LOAD_PDF + pdfUrl;
            mWebView.loadUrl(url);
           /*Kamesh*/
            fileDownload.setOnClickListener(v -> permissionUtils.checkPermission(permissions, GlobalConstants.ALLOW_PERMIT, 1));
           /*Kamesh*/
        } else {
            finish();
            shortToast(mContext, GlobalConstants.NO_DATA);
        }

        initViews();


    }

    private void initViews() {
        toolbar(IntentConstants.NEWS_FEED);
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);


    }

    public class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mWebView.loadUrl("javascript:(function() { " +
                    "document.querySelector('[role=\"toolbar\"]').remove();})()");
            progressBar.setVisibility(View.GONE);
            mWebView.setVisibility(View.VISIBLE);
            fileDownload.setVisibility(View.VISIBLE);
        }
    }

    private void downloadPdf(String pdfUrl) {
        new DownloadFile().execute(pdfUrl);
    }


    private class DownloadFile extends AsyncTask<String, Void, Boolean> {
        /*Kamesh*/
        String filePath;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mCommonUtils.showProgressDialog();
        }
        /*Kamesh*/

        @Override
        protected Boolean doInBackground(String... strings) {  /*Kamesh*/
            String fileUrl = strings[0];
            String[] seperate = fileUrl.split("/emagazine/");
            String fileName = seperate[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, IntentConstants.NEWS_FEED);
            filePath = "Path : Internal Storage/" + IntentConstants.NEWS_FEED + "/" + seperate[1];   /*Kamesh*/
            if (!folder.exists()) {
                folder.mkdir();
            }

            File pdfFile = new File(folder, fileName);
            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                logd(e.getMessage());
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);

            return FileDownloader.downloadFile(fileUrl, pdfFile);  /*Kamesh*/
        }

        /*Kamesh*/
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mCommonUtils.dismissProgressDialog();
            runOnUiThread(() -> {
                if (aBoolean) {
                    String pathDoenload = GlobalConstants.SUCCESS_E_MAGAZINE + filePath;
                    longToast(mContext, pathDoenload);
                } else {
                    shortToast(mContext, GlobalConstants.FAILED_E_MAGAZINE);
                }
            });

        }
        /*Kamesh*/

    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionGranted(int myRequestCode) {
        downloadPdf(pdfUrl);
    }

    @Override
    public void partialPermissionGranted(int myRequestCode, List<String> myGrantedPermission) {

    }

    @Override
    public void permissionDenied(int myRequestCode) {

    }

    @Override
    public void neverAskAgain(int myRequestCode) {

    }
}
