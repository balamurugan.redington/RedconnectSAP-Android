package redington.com.redconnect.redconnect.spot.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.dashboard.model.FestivalOffer;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.spot.adapter.SpotAdapter;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.GridSpaceItemDecoration;
import redington.com.redconnect.util.LaunchIntentManager;

public class SPOTActivity extends BaseActivity implements CartListener, NewListener {

    /*Kamesh (Note) ->  Added NewListener for CheckingWebOrderEntry (Retry) API*/

    private Context mContext;
    private ArrayList<SubPromoCode> subPromoCodes;
    private ArrayList<FestivalOffer> offerArrayList;
    private SpotAdapter spotAdapter;
    private String comingFrom;
    private String mUserID;
    private CommonUtils commonUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spot);

        mContext = SPOTActivity.this;
        mUserID = GlobalConstants.GetSharedValues.getSpUserId();
        commonUtils = new CommonUtils(mContext);

        RelativeLayout relativeLayout = findViewById(R.id.RL_continue);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String toolbarTile = bundle.getString(GlobalConstants.TOOLBAR_TITLE);
            comingFrom = bundle.getString(GlobalConstants.COMING_FROM);
            toolbar(toolbarTile);
            if (comingFrom.equals(IntentConstants.SPOT)) {
                relativeLayout.setVisibility(View.VISIBLE);
                subPromoCodes = (ArrayList<SubPromoCode>) bundle.getSerializable(GlobalConstants.SPOT_ARRAY);

            } else if (comingFrom.equals(GlobalConstants.MFESTIVALS)) {
                relativeLayout.setVisibility(View.GONE);
                offerArrayList = (ArrayList<FestivalOffer>) bundle.getSerializable(GlobalConstants.RECENTFESTIVALS);
            }
            setInitViews();
        }

        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

    }

    private void setSequence(ArrayList<SubPromoCode> subPromoCodes) {
        setBillingSeq(subPromoCodes.get(0).getDeliverySeq());
        setCartDelSeqUpdate(subPromoCodes.get(0).getDeliverySeq());
        setCashDiscount(GlobalConstants.NO);
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


    public void mProceedToPay(View view) {
        logd(view.getContext().getClass().getSimpleName());
        /*Kamesh*/
        callRetryAPI(GlobalConstants.GetSharedValues.getSpUserId(), mContext);
        /*getCartData();*/
        /*Kamesh*/
    }

    /*Kamesh*/
    private void callRetryAPI(String userId, Context context) {
        commonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(userId, context, this);
    }
    /*Kamesh*/

    private void getCartData() {
        ArrayList<String> cartArrayList = new ArrayList<>();
        cartArrayList.add(mUserID);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.MODE_SELECT);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        cartApiCall(cartArrayList);
    }

    private void cartApiCall(ArrayList<String> cartArrayList) {
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArrayList, mContext, this);
    }


    private void setInitViews() {
        RecyclerView recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        if (comingFrom.equals(IntentConstants.SPOT)) {
            spotAdapter = new SpotAdapter(mContext, subPromoCodes, IntentConstants.SPOT);
        } else if (comingFrom.equals(GlobalConstants.MFESTIVALS)) {
            spotAdapter = new SpotAdapter(mContext, offerArrayList, 1);
        }
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
        recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
        recyclerView.setAdapter(spotAdapter);
    }

    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    shortToast(mContext, GlobalConstants.CLEAR_CART);
                    /*9-1-19*/
                    /*setCartUpdate(cartJson.getData().size());*/
                    setCartUpdate(cartJson.getData().get(0).getCart().size());
                    break;
                case IntentConstants.FAILURE_URL:
                    int cartSize = cartJson.getData().size();
                    if (cartSize == 0) {
                        setSequence(subPromoCodes);
                        SharedPreferences.Editor editor = cartData.edit();
                        editor.putBoolean(GlobalConstants.SPOT_BOOELAN, true);
                        editor.putString(GlobalConstants.SPOT_CODE, subPromoCodes.get(0).getPromoCode());
                        editor.apply();
                        Bundle bundle = new Bundle();
                        bundle.putString(GlobalConstants.FROM, IntentConstants.SPOT);
                        LaunchIntentManager.routeToActivityStackBundle(mContext, MyCartActivity.class, bundle);
                    } else {
                        shortToast(mContext, cartJson.getDescription());
                    }
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    /*Kamesh*/
    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String transactionId = retryResponse.getData().getPendOrd().get(0).getTransactionID();
                    String referenceId = retryResponse.getData().getPendOrd().get(0).getReferenceNo();
                    if (!transactionId.isEmpty() || !referenceId.isEmpty()) {
                        showPopup();
                    } else {
                        getCartData();
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    getCartData();
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }


    }

    @Override
    public void mFailureObject(Object failureObject) {
        errorDialogPage();
    }

    @Override
    public void mError() {
        errorDialogPage();
    }

    private void errorDialogPage() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup() {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(mDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        alertContent.setText(GlobalConstants.DASHBOARD_RETRY);

        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });

        mDialog.show();
    }
    /*Kamesh*/
}
