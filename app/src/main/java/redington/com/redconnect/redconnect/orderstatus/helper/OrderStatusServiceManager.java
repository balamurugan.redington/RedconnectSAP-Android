package redington.com.redconnect.redconnect.orderstatus.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.orderstatus.model.OrderStatusJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusServiceManager {

    private OrderStatusServiceManager() {
        throw new IllegalStateException(IntentConstants.ORDERDETAIL);
    }

    public static void getOrderStatusServiceManager(String userID, String fromDate, String toDate, String pageNum, String recordNum,
                                                    final Context context,
                                                    final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getOrderStatusService(getRequestBody(userID, fromDate, toDate, pageNum, recordNum)).enqueue(new Callback<OrderStatusJsonResponse>() {
                @Override
                public void onResponse(Call<OrderStatusJsonResponse> call, Response<OrderStatusJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<OrderStatusJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(String userID, String fromDate, String toDate, String pageNum, String recordNum) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CART_USERID, userID);
            jsonValues.put(JsonKeyConstants.FROM_DATE, fromDate);
            jsonValues.put(JsonKeyConstants.TO_DATE, toDate);
            jsonValues.put(JsonKeyConstants.PAGE_NO, pageNum);
            jsonValues.put(JsonKeyConstants.RECORD_NO, recordNum);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
