package redington.com.redconnect.redconnect.profile.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.profile.adapter.ProfileAdapter;
import redington.com.redconnect.redconnect.profile.helper.ImageUploadManager;
import redington.com.redconnect.redconnect.profile.helper.UserProfileServiceManager;
import redington.com.redconnect.redconnect.profile.model.ImageUploadJsonResponse;
import redington.com.redconnect.redconnect.profile.model.ProfileBean;
import redington.com.redconnect.redconnect.profile.model.ProfileData;
import redington.com.redconnect.redconnect.profile.model.ProfileJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.PermissionUtils;

public class ProfileScreen extends BaseActivity implements NewListener, UIListener, PermissionResultCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private CommonUtils commonUtils;
    private Context mContext;

    private static int[] images = {
            R.mipmap.ic_phone,
            R.mipmap.ic_fax,
            R.mipmap.ic_com_regnum,
            R.mipmap.ic_address,
            R.mipmap.ic_credit_limit,
            R.mipmap.ic_high_exposure_date,
            R.mipmap.ic_high_exposure,
            R.mipmap.ic_payment_details,
            R.mipmap.ic_last_invoice,
            R.mipmap.ic_no_of_cheque

    };
    private String[] profileTitle;
    private CircleImageView profileImage;
    ArrayList<String> permissions = new ArrayList<>();
    private PermissionUtils permissionUtils;
    private String userID;
    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme1);
        setContentView(R.layout.activity_userprofile);

        mContext = ProfileScreen.this;
        commonUtils = new CommonUtils(mContext);
        userID = GlobalConstants.GetSharedValues.getSpUserId();
        profileTitle = getResources().getStringArray(R.array.profileText);

        profileImage = findViewById(R.id.profileImage);
        ImageView imageBack = findViewById(R.id.imageBack);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageBack.setColorFilter(Color.WHITE);

        scrollView = findViewById(R.id.scrollView);
        permissionUtils = new PermissionUtils(mContext);
        profileTitle = getResources().getStringArray(R.array.profileText);

        permissions.add(Manifest.permission.CAMERA);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        callProfileInfo(userID);

        toolbar(IntentConstants.USER_PROFILE);

        profileImage.setOnClickListener(v -> permissionUtils.checkPermission(permissions, GlobalConstants.CAM_ERROR, 1));


    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


    private void callProfileInfo(String userID) {
        commonUtils.showProgressDialog();
        UserProfileServiceManager.getUserProfileServiceManager(userID, mContext, this);
    }

    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        profileDetailsObjects(successObject);
    }


    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void profileDetailsObjects(Object successObject) {
        ProfileJsonResponse profileJsonResponse = (ProfileJsonResponse) successObject;
        if (profileJsonResponse != null) {

            switch (profileJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    RecyclerView recyclerView = findViewById(R.id.profile_recycler_view);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                    ArrayList<ProfileData> arrayList = new ArrayList(profileJsonResponse.getData());
                    ArrayList<ProfileBean> androidVersions = prepareData();
                    recyclerView.setAdapter(new ProfileAdapter(mContext, androidVersions, arrayList));
                    setValues(arrayList);
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }


    private void setValues(ArrayList<ProfileData> arrayList) {
        TextView custCode = findViewById(R.id.cust_code);
        TextView mobNumber = findViewById(R.id.mobileNumber);
        TextView customerName = findViewById(R.id.customerName);
        TextView customerMail = findViewById(R.id.customerMail);

        customerName.setText(arrayList.get(0).getmCUSTOMERNAME());
        customerMail.setText(arrayList.get(0).getmEmailAddress1l());
        mobNumber.setText(arrayList.get(0).getmPHONENUMBER1());
        custCode.setText(arrayList.get(0).getmCUSTOMERCODE());


        Glide.with(mContext).load(arrayList.get(0).getmImageURL())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.mipmap.ic_whitemen)
                .error(R.mipmap.ic_whitemen)
                .skipMemoryCache(true)
                .into(profileImage);


    }

    private ArrayList<ProfileBean> prepareData() {
        ArrayList<ProfileBean> profileBeans = new ArrayList<>();
        for (int i = 0; i < profileTitle.length; i++) {
            profileBeans.add(new ProfileBean(profileTitle[i], images[i]));
        }
        return profileBeans;
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        ImageUploadJsonResponse imageUploadJsonResponse = (ImageUploadJsonResponse) successObject;
        if (imageUploadJsonResponse != null) {
            switch (imageUploadJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    scrollView.smoothScrollTo(0, 0);
                    shortToast(mContext, GlobalConstants.IMAGE_UPLOAD);
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, imageUploadJsonResponse.getData());
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }


    /*Request Permission Result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void permissionGranted(int myRequestCode) {
        if (myRequestCode == 1) {
            captureImage();
        }
    }


    @Override
    public void partialPermissionGranted(int myRequestCode, List<String> myGrantedPermission) {
        //No data
    }

    @Override
    public void permissionDenied(int myRequestCode) {
        //No data
    }

    @Override
    public void neverAskAgain(int myRequestCode) {
        //No data
    }

    private void captureImage() {
        pickImgae();
    }

    private void pickImgae() {

        final CharSequence[] items = {GlobalConstants.CAPTURE_PHOTO, GlobalConstants.CHOOSE_GALLERY};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(GlobalConstants.ADD_PHOTO);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(GlobalConstants.CAPTURE_PHOTO)) {
                cameraIntent();
            } else if (items[item].equals(GlobalConstants.CHOOSE_GALLERY)) {
                galleryIntent();
            }
        });

        builder.show();

    }

    /*Gallery intent*/
    private void galleryIntent() {
        Intent intent1 = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent1, IntentConstants.SELECT_FILE);
    }

    /*Camera Intent*/
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, IntentConstants.REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstants.SELECT_FILE && data != null && data.getData() != null) {
            getImageFromGallery(data);
        } else if (requestCode == IntentConstants.REQUEST_CAMERA && data != null) {
            onCaptureImageResult(data);
        }

    }

    /*Camera Function*/
    private void onCaptureImageResult(Intent data) {
        try {
            Bitmap mphoto = (Bitmap) data.getExtras().get("data");
            convertBitmapAndSet(mphoto);
        } catch (NullPointerException e) {
            logd(e.getMessage());
        }
    }


    /*Gallery Function*/
    private void getImageFromGallery(final Intent data) {

        runOnUiThread(() -> {
            if (data.getData() != null) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    convertBitmapAndSet(bitmap);
                } catch (IOException e) {
                    logd(e.getMessage());
                }
            }
        });

    }

    /*Convert Image And Set*/
    private void convertBitmapAndSet(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);

        Glide.with(mContext).load(baos.toByteArray())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.mipmap.ic_whitemen)
                .error(R.mipmap.ic_whitemen)
                .skipMemoryCache(true)
                .into(profileImage);

        String imageUploadString = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

        imageUpload(imageUploadString);


    }

    private void imageUpload(String imageString) {
        commonUtils.showProgressDialog();

        ImageUploadManager.imageUplaod(userID, imageString, mContext, this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            callProfileInfo(userID);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }

}
