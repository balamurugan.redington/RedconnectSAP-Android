package redington.com.redconnect.redconnect.orderstatus.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.orderstatus.adapter.OrderDetailAdapter;
import redington.com.redconnect.redconnect.orderstatus.helper.OrderDetailServiceManager;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailData;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class OrderStatusDetailActivity extends BaseActivity implements UIListener {

    private TextView totalAmount;
    private String userId;
    private String orderNum;
    private CommonUtils commonUtils;
    private Context mContext;
    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status_detail);

        mContext = OrderStatusDetailActivity.this;
        commonUtils = new CommonUtils(mContext);
        userId = GlobalConstants.GetSharedValues.getSpUserId();

        mCoordinatorLayout = findViewById(R.id.drawer);
        mCoordinatorLayout.setVisibility(View.GONE);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        ScrollView scrollView = findViewById(R.id.scrollView);

        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        orderNum = getIntent().getStringExtra(GlobalConstants.ORDERNUM);

        TextView mOrderNum = findViewById(R.id.tv_orderNo);


        totalAmount = findViewById(R.id.txt_total_amount);
        mOrderNum.setText(orderNum);

        scrollView.smoothScrollTo(0, 0);

        toolbar(IntentConstants.ORDERDETAIL);

        callService(userId, orderNum);

    }


    @SuppressLint("SetTextI18n")
    private void readOrderDetail(ArrayList<OrderDetailData> arrayList) {
        RecyclerView mReviewRecyclerView = findViewById(R.id.detailOrder_Recyclerview);
        mReviewRecyclerView.setHasFixedSize(true);
        mReviewRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mReviewRecyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mReviewRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        mReviewRecyclerView.addItemDecoration(mDividerItemDecoration);
        mReviewRecyclerView.setAdapter(new OrderDetailAdapter(arrayList));

        Double count = 0.0d;
        for (int i = 0; i < arrayList.size(); i++) {
            count += arrayList.get(i).getUnitPrice() * (double) arrayList.get(i).getQuantity();
        }
        totalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + isValidNumberFormat().format(count));


        setAddress(arrayList);

    }

    /*Address*/
    private void setAddress(ArrayList<OrderDetailData> arrayList) {

        TextView mBillAddress1 = findViewById(R.id.txt_billAddress1);
        TextView mBillAddress2 = findViewById(R.id.txt_billAddress2);
        TextView mBillAddress3 = findViewById(R.id.txt_billAddress3);
        TextView mBillAddress4 = findViewById(R.id.txt_billAddress4);
        TextView mBillAddress5 = findViewById(R.id.txt_billAddress5);

        TextView mCustName = findViewById(R.id.text_custName);
        TextView mPaymentMode = findViewById(R.id.txt_payment_ode);
        TextView mInvoiceNum = findViewById(R.id.txt_invoice_no);
        TextView mInvoiceDate = findViewById(R.id.txt_invoice_date);
        TextView mOrderStatus = findViewById(R.id.text_status);

        String mBillingAddress1 = arrayList.get(0).getmDeliveryAddress1();
        String mBillingAddress2 = arrayList.get(0).getmDeliveryAddress2();
        String mBillingAddress3 = arrayList.get(0).getmDeliveryAddress3() +
                arrayList.get(0).getmDeliveryAddress4();
        String mBillingAddress4 = arrayList.get(0).getmDeliveryAddress5() +
                arrayList.get(0).getmDeliveryAddress6();
        String mBillingAddress5 = arrayList.get(0).getmDeliveryAddress7();

        if (!mBillingAddress1.isEmpty())
            mBillAddress1.setText(mBillingAddress1);
        else
            mBillAddress1.setVisibility(View.GONE);
        if (!mBillingAddress2.isEmpty())
            mBillAddress2.setText(mBillingAddress2);
        else
            mBillAddress2.setVisibility(View.GONE);
        if (!mBillingAddress3.isEmpty())
            mBillAddress3.setText(mBillingAddress3);
        else
            mBillAddress3.setVisibility(View.GONE);
        if (!mBillingAddress4.isEmpty())
            mBillAddress4.setText(mBillingAddress4);
        else
            mBillAddress4.setVisibility(View.GONE);
        if (!mBillingAddress5.isEmpty())
            mBillAddress5.setText(mBillingAddress5);
        else
            mBillAddress5.setVisibility(View.GONE);


        mCustName.setText(arrayList.get(0).getName());
        mInvoiceNum.setText(arrayList.get(0).getInvoiceNumber());
        mInvoiceDate.setText(arrayList.get(0).getInvoiceDate());
        mOrderStatus.setText(arrayList.get(0).getStatus());
        mPaymentMode.setText(arrayList.get(0).getPaymentMethod());

        setbillAddress(arrayList);
    }

    private void setbillAddress(ArrayList<OrderDetailData> arrayList) {

        TextView mShipAddress1 = findViewById(R.id.txt_shipAdress1);
        TextView mShipAddress2 = findViewById(R.id.txt_shipAdress2);
        TextView mShipAddress3 = findViewById(R.id.txt_shipAdress3);
        TextView mShipAddress4 = findViewById(R.id.txt_shipAdress4);
        TextView mShipAddress5 = findViewById(R.id.txt_shipAdress5);

        String mShippingAddress1 = arrayList.get(0).getmBillingAddress1();
        String mShippingAddress2 = arrayList.get(0).getmBillingAddress2();
        String mShippingAddress3 = arrayList.get(0).getmBillingAddress3() + arrayList.get(0).getmBillingAddress4();
        String mShippingAddress4 = arrayList.get(0).getmBillingAddress5() + arrayList.get(0).getmBillingAddress6();
        String mShippingAddress5 = arrayList.get(0).getmBillingAddress7();

        if (!mShippingAddress1.isEmpty())
            mShipAddress1.setText(mShippingAddress1);
        else
            mShipAddress1.setVisibility(View.GONE);
        if (!mShippingAddress2.isEmpty())
            mShipAddress2.setText(mShippingAddress2);
        else
            mShipAddress2.setVisibility(View.GONE);
        if (!mShippingAddress3.isEmpty())
            mShipAddress3.setText(mShippingAddress3);
        else
            mShipAddress3.setVisibility(View.GONE);
        if (!mShippingAddress4.isEmpty())
            mShipAddress4.setText(mShippingAddress4);
        else
            mShipAddress4.setVisibility(View.GONE);
        if (!mShippingAddress5.isEmpty())
            mShipAddress5.setText(mShippingAddress5);
        else
            mShipAddress5.setVisibility(View.GONE);
    }

    private void callService(String userId, String orderNo) {
        commonUtils.showProgressDialog();
        OrderDetailServiceManager.getOrderDetailServiceManager(userId,
                GlobalConstants.NULL_DATA,
                GlobalConstants.NULL_DATA,
                orderNo,
                OrderStatusDetailActivity.this, this);
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            callService(userId, orderNum);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        OrderDetailJsonResponse orderDetailJsonResponse = (OrderDetailJsonResponse) successObject;
        if (orderDetailJsonResponse != null) {
            switch (orderDetailJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    mCoordinatorLayout.setVisibility(View.VISIBLE);
                    int dataCount = orderDetailJsonResponse.getData().size();
                    if (dataCount > 0) {
                        ArrayList<OrderDetailData> arrayList = new ArrayList(orderDetailJsonResponse.getData());
                        readOrderDetail(arrayList);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }

    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }
}
