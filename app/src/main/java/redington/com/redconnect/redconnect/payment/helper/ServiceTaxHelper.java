package redington.com.redconnect.redconnect.payment.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.payment.model.ServiceTaxJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceTaxHelper {

    private ServiceTaxHelper() {
        throw new IllegalStateException(IntentConstants.SERVICE_CHARGE);
    }

    public static void serviceTaxCall(String amount, final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getServiceTax(amount).enqueue(new Callback<ServiceTaxJsonResponse>() {
                @Override
                public void onResponse(Call<ServiceTaxJsonResponse> call, Response<ServiceTaxJsonResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<ServiceTaxJsonResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });
        } else {
            listener.mError();
        }
    }
}
