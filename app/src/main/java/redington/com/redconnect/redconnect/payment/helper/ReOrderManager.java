package redington.com.redconnect.redconnect.payment.helper;

import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReOrderManager {

    private ReOrderManager() {
        throw new IllegalStateException("");
    }

    public static void getReorderCall(String user, final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).reOrderPlacedCall(getRequestBody(user))
                    .enqueue(new Callback<EncryptedOrderResponse>() {
                        @Override
                        public void onResponse(Call<EncryptedOrderResponse> call, Response<EncryptedOrderResponse> response) {
                            listener.mSuccessObject(response.body());
                        }

                        @Override
                        public void onFailure(Call<EncryptedOrderResponse> call, Throwable t) {
                            listener.mFailureObject(t.toString());
                        }
                    });
        } else {
            listener.mError();
        }
    }

    private static RequestBody getRequestBody(String userId) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.USERID, userId);

            String mEncryptedString = BaseActivity.mEncrypt(String.valueOf(jsonValues));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JsonKeyConstants.JIC_DATA, mEncryptedString);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }

}
