
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class EDMData implements Serializable {

    @SerializedName("Brand")
    private String mBrand;
    @SerializedName("CategoryDescription")
    private String mCategoryDescription;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("ItemCategory")
    private String mItemCategory;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDescription")
    private String mItemDescription;
    @SerializedName("MajorCode")
    private String mMajorCode;
    @SerializedName("MinorCode")
    private String mMinorCode;
    @SerializedName("MinorDesc")
    private String mMinorDesc;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("ProgramHeading")
    private String mProgramHeading;
    @SerializedName("RequestNumber")
    private String mRequestNumber;
    @SerializedName("Stock")
    private String mStock;
    @SerializedName("VendorCode")
    private String mVendorCode;

    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;

    public Double getmActualPrice() {
        return CommonMethod.getValidDouble(mActualPrice);
    }


    public Double getmDiscountPrice() {
        return CommonMethod.getValidDouble(mDiscountPrice);
    }


    public String getmDiscountPercent() {
        return CommonMethod.getValidDoubleString(mDiscountPercent);
    }


    public String getBrand() {
        return mBrand;
    }


    public String getCategoryDescription() {
        return mCategoryDescription;
    }


    public String getImageURL() {
        return mImageURL;
    }


    public String getItemCategory() {
        return mItemCategory;
    }


    public String getItemCode() {
        return mItemCode;
    }


    public String getItemDescription() {
        return mItemDescription;
    }


    public String getMajorCode() {
        return mMajorCode;
    }


    public String getMinorCode() {
        return mMinorCode;
    }


    public String getMinorDesc() {
        return mMinorDesc;
    }


    public String getProductName() {
        return mProductName;
    }


    public String getProgramHeading() {
        return mProgramHeading;
    }


    public String getRequestNumber() {
        return mRequestNumber;
    }


    public String getStock() {
        return mStock;
    }


    public String getVendorCode() {
        return mVendorCode;
    }


}
