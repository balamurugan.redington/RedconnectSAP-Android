
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class PaymentDueTable implements Serializable {

    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("DueAmount")
    private String mDueAmount;
    @SerializedName("DueDate")
    private String mDueDate;
    @SerializedName("InvoiceDate")
    private String mInvoiceDate;
    @SerializedName("InvoiceNumber")
    private String mInvoiceNumber;
    @SerializedName("InvoiceValue")
    private String mInvoiceValue;
    @SerializedName("ODDays")
    private String mODDays;
    @SerializedName("Week")
    private String mWeek;

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public Double getDueAmount() {
        return CommonMethod.getValidDouble(mDueAmount);
    }

    public String getDueDate() {
        return mDueDate;
    }

    public String getInvoiceDate() {
        return mInvoiceDate;
    }

    public String getInvoiceNumber() {
        return mInvoiceNumber;
    }

    public Double getInvoiceValue() {
        return CommonMethod.getValidDouble(mInvoiceValue);
    }

    public Double getODDays() {
        return CommonMethod.getValidDouble(mODDays);
    }

    public Double getWeek() {
        return CommonMethod.getValidDouble(mWeek);
    }


}
