
package redington.com.redconnect.redconnect.orderstatus.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class OrderStatusData implements Serializable {

    @SerializedName("OrderList")
    private List<OrderList> mOrderList;
    @SerializedName("ShippingList")
    private List<ShippingList> mShippingList;

    public List<OrderList> getOrderList() {
        return mOrderList;
    }
    
    public List<ShippingList> getShippingList() {
        return mShippingList;
    }


}
