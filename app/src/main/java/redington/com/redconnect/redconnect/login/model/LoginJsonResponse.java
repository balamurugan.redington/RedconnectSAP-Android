package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginJsonResponse implements Serializable {

    @SerializedName("Data")
    private LoginData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public LoginData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
