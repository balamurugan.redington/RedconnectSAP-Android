package redington.com.redconnect.redconnect.dashboard.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.preorder.activity.PreOrderActivityList;
import redington.com.redconnect.redconnect.preorder.activity.ProductSubCategoryActivity;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryData;
import redington.com.redconnect.util.LaunchIntentManager;

import static android.content.Context.MODE_PRIVATE;


public class MainPreOrderCatAdapter extends RecyclerView.Adapter<MainPreOrderCatAdapter.CategoryHolder> {

    private final List<PreorderCategoryData> mArrayList;
    private Context context;
    private CommonMethod commonMethod;


    public MainPreOrderCatAdapter(Context context, List<PreorderCategoryData> itemList) {
        this.mArrayList = itemList;
        this.context = context;
        commonMethod = new CommonMethod(context);
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_main_preorder_recylce, viewGroup, false);
        return new CategoryHolder(view);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {

        holder.productName.setText(mArrayList.get(position).getDescription());


        Picasso.with(context)
                .load(mArrayList.get(position).getmImage1())
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .into(holder.imagePreOrder);


        final String mFlag = mArrayList.get(position).getFlag();
        final int pos = holder.getAdapterPosition();
        holder.totalLayout.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                if (mFlag.equals("Y")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalConstants.PRODUCT_ACTIVITY, GlobalConstants.M_DASHBOARD);
                    bundle.putString(GlobalConstants.PRODUCT_CATEGORY, mArrayList.get(pos).getCategory());
                    bundle.putString(GlobalConstants.PRODUCT_BRAND, mArrayList.get(pos).getBRND68());
                    LaunchIntentManager.routeToActivityStackBundle(context, PreOrderActivityList.class, bundle);
                } else if (mFlag.equals("")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalConstants.CATEGORY, mArrayList.get(pos).getCategory());
                    bundle.putString(GlobalConstants.STATUS, mArrayList.get(pos).getDescription());
                    SharedPreferences.Editor editor1 = context.getSharedPreferences(GlobalConstants.CHECK, MODE_PRIVATE).edit();
                    editor1.putBoolean(GlobalConstants.LOCKED, false).apply();
                    LaunchIntentManager.routeToActivityStackBundle(context, ProductSubCategoryActivity.class, bundle);
                }

            }
        });
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    class CategoryHolder extends RecyclerView.ViewHolder {
        private LinearLayout totalLayout;
        private TextView productName;
        private ImageView imagePreOrder;


        private CategoryHolder(View view) {
            super(view);

            context = view.getContext();

            totalLayout = view.findViewById(R.id.LL_recycleView);
            productName = view.findViewById(R.id.txt_productName);
            imagePreOrder = view.findViewById(R.id.imagePreOrder);


        }
    }


}
