package redington.com.redconnect.redconnect.dashboard.model;

public class DashboardBean {

    private String id;
    private String userId;
    private String dashboardData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDashboardData() {
        return dashboardData;
    }

    public void setDashboardData(String dashboardData) {
        this.dashboardData = dashboardData;
    }
}
