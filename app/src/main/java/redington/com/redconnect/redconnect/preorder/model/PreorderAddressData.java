package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;

public class PreorderAddressData implements Serializable {

    @SerializedName("Address1")
    private String mAddress1;
    @SerializedName("Address2")
    private String mAddress2;
    @SerializedName("Address3")
    private String mAddress3;
    @SerializedName("Address4")
    private String mAddress4;
    @SerializedName("Address5")
    private String mAddress5;
    @SerializedName("Customer_Code")
    private String mCustomerCode;
    @SerializedName("Customer_Name")
    private String mCustomerName;
    @SerializedName("Delivery_Sequence")
    private String mDeliverySequence;
    @SerializedName("Location_Code")
    private String mLocationCode;
    @SerializedName("PIN_Code")
    private String mPINCode;
    @SerializedName("Payment_Days")
    private String mPaymentDays;
    @SerializedName("Payment_Method")
    private String mPaymentMethod;

    public String getAddress1() {
        return mAddress1;
    }


    public String getAddress2() {
        return mAddress2;
    }


    public String getAddress3() {
        return mAddress3;
    }


    public String getAddress4() {
        return mAddress4;
    }


    public String getAddress5() {
        return mAddress5;
    }


    public String getCustomerCode() {
        return mCustomerCode;
    }


    public String getCustomerName() {
        return mCustomerName;
    }


    public String getDeliverySequence() {
        return mDeliverySequence;
    }


    public String getLocationCode() {
        return mLocationCode;
    }


    public String getPINCode() {
        return mPINCode;
    }


    public Integer getPaymentDays() {
        return CommonMethod.getValidInt(mPaymentDays);
    }


    public String getPaymentMethod() {
        return mPaymentMethod;
    }


}
