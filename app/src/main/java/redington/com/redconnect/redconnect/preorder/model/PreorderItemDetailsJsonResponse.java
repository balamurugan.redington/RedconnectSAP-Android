package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreorderItemDetailsJsonResponse implements Serializable {

    @SerializedName("Data")
    private PreorderItemDetailsData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public PreorderItemDetailsData getmData() {
        return mData;
    }

    public void setmData(PreorderItemDetailsData mData) {
        this.mData = mData;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmStatusCode() {
        return mStatusCode;
    }

    public void setmStatusCode(String mStatusCode) {
        this.mStatusCode = mStatusCode;
    }

    public String getmStatusMessage() {
        return mStatusMessage;
    }

    public void setmStatusMessage(String mStatusMessage) {
        this.mStatusMessage = mStatusMessage;
    }
}
