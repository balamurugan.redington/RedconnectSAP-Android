package redington.com.redconnect.redconnect.login.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.helper.PartnerRegisterService;
import redington.com.redconnect.redconnect.login.model.PartnerRegistrationJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

public class Partnercreation4 extends BaseActivity implements UIListener {
    private Context mContext;
    private CommonUtils commonUtils;
    private ArrayList<String> thirdArray;
    private LinearLayout linearLayout;
    private RadioGroup mGroup1;
    private RadioGroup mGroup2;
    private RadioGroup mGroup3;
    private RadioGroup mGroup4;
    private RadioGroup mGroup5;
    private RadioGroup mGroup6;
    private RadioGroup mGroup7;
    private RadioGroup mGroup8;
    private RadioGroup mGroup9;
    private RadioGroup mGroup10;
    private EditText mRemarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partner_fragment4);
        mContext = Partnercreation4.this;
        commonUtils = new CommonUtils(mContext);

        toolbar(IntentConstants.CUSTOMERPROFILE);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            thirdArray = bundle.getStringArrayList(IntentConstants.THIRDARRAY);
        }
        linearLayout = findViewById(R.id.fragment4);
        mGroup1 = findViewById(R.id.doc_grp_1);
        mGroup2 = findViewById(R.id.doc_grp_2);
        mGroup3 = findViewById(R.id.doc_grp_3);
        mGroup4 = findViewById(R.id.doc_grp_4);
        mGroup5 = findViewById(R.id.doc_grp_5);
        mGroup6 = findViewById(R.id.doc_grp_6);
        mGroup7 = findViewById(R.id.doc_grp_7);
        mGroup8 = findViewById(R.id.doc_grp_8);
        mGroup9 = findViewById(R.id.doc_grp_9);
        mGroup10 = findViewById(R.id.doc_grp_10);
        mRemarks = findViewById(R.id.doc_remarksif);


        findViewById(R.id.part_buttonPrevious).setOnClickListener(v -> finish());

        findViewById(R.id.part_buttonSubmit).setOnClickListener(v -> validationPart(mContext));
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    private void validationPart(Context context) {
        if (mGroup1.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_DEALER_REG);
            mGroup1.setFocusableInTouchMode(true);
            mGroup1.requestFocus();
        } else if (mGroup2.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_PAN_PROOF);
            mGroup2.setFocusableInTouchMode(true);
            mGroup2.requestFocus();
        } else if (mGroup3.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_TIN_PROOF);
            mGroup3.setFocusableInTouchMode(true);
            mGroup3.requestFocus();
        } else if (mGroup4.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_RES_PRO_PART_DIRECTOR);
            mGroup4.setFocusableInTouchMode(true);
            mGroup4.requestFocus();
        } else if (mGroup5.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_PARTNERSHIP_DEED);
            mGroup5.setFocusableInTouchMode(true);
            mGroup5.requestFocus();
        } else if (mGroup6.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_CETIFY_INCORPORATION);
            mGroup6.setFocusableInTouchMode(true);
            mGroup6.requestFocus();
        } else if (mGroup7.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_MDA);
            mGroup7.setFocusableInTouchMode(true);
            mGroup7.requestFocus();
        } else if (mGroup8.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_AOA);
            mGroup8.setFocusableInTouchMode(true);
            mGroup8.requestFocus();
        } else if (mGroup9.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_BANK_STATE);
            mGroup9.setFocusableInTouchMode(true);
            mGroup9.requestFocus();
        } else if (mGroup10.getCheckedRadioButtonId() == -1) {
            shortToast(mContext, GlobalConstants.PARTNER_FINANCIAL);
            mGroup10.setFocusableInTouchMode(true);
            mGroup10.requestFocus();
        } else if (mRemarks.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.remarksif));
            mRemarks.setFocusableInTouchMode(true);
            mRemarks.requestFocus();
        } else {
            String[] radioButton = {
                    ((RadioButton) findViewById(mGroup1.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup2.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup3.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup4.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup5.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup6.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup7.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup8.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup9.getCheckedRadioButtonId())).getText().toString().substring(0, 1),
                    ((RadioButton) findViewById(mGroup10.getCheckedRadioButtonId())).getText().toString().substring(0, 1)
            };

            String statusFlag = GlobalConstants.M_STATUS_FLAG;
            String reqUser = GlobalConstants.M_REQ_UESR;
            ArrayList<String> overAllArray = new ArrayList<>(thirdArray);
            overAllArray.add(statusFlag);
            overAllArray.add(reqUser);
            overAllArray.add(mRemarks.getText().toString().trim());
            partnerApi(overAllArray, radioButton, context);
        }
    }

    private void partnerApi(ArrayList<String> overAllArray, String[] radioButton, Context context) {
        commonUtils.showProgressDialog();
        PartnerRegisterService.partnerRegisterServiceCall(overAllArray, radioButton, context, this);
    }

    private String plsEnter(int id) {
        return GlobalConstants.ENTER_TEXT + getString(id);
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        PartnerRegistrationJsonResponse registrationJsonResponse = (PartnerRegistrationJsonResponse) successObject;
        if (registrationJsonResponse != null) {
            switch (registrationJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String refNum = registrationJsonResponse.getData().getReferenceNumber();
                    showDialogSuccess(mContext, refNum, GlobalConstants.PARTNER_SUCCESS);
                    break;
                case IntentConstants.FAILURE_URL:
                    snackBar(GlobalConstants.NO_DATA);
                    break;
                default:
                    snackBar(GlobalConstants.SERVER_ERROR_BAR);
                    break;
            }
        } else {
            snackBar(GlobalConstants.SERVER_ERROR_BAR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.SERVER_ERROR_BAR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.INTERNET_ERROR);
    }


    /*Show Snack Bar*/
    private void snackBar(String newString) {
        Snackbar snackbar = Snackbar.make(linearLayout, newString, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @SuppressLint("NewApi")
    public void showDialogSuccess(final Context con, String refNum, String mDesciption) {
        final Dialog mDialog = new Dialog(con);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        TextView title = mDialog.findViewById(R.id.pre_ref_title);
        TextView content = mDialog.findViewById(R.id.pre_ref_cont);
        Button button = mDialog.findViewById(R.id.pre_ref_btm);

        content.setTextColor(ContextCompat.getColor(con, R.color.new_buttonColor));
        title.setText(mDesciption);
        content.setText(refNum);
        button.setOnClickListener(v -> {
            LaunchIntentManager.routeToActivity(con, LoginActivity.class);
            mDialog.dismiss();
        });

        mDialog.show();
    }
}
