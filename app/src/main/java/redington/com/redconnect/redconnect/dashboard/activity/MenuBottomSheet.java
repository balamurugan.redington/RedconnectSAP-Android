package redington.com.redconnect.redconnect.dashboard.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.chequepending.activity.ChequePendingActivity;
import redington.com.redconnect.redconnect.helpdesk.activity.TicketHistoryActivity;
import redington.com.redconnect.redconnect.orderstatus.activity.OrderStatusActivity;
import redington.com.redconnect.redconnect.paymentdue.activity.PaymentDue;
import redington.com.redconnect.redconnect.preorder.model.PreOrderBean;
import redington.com.redconnect.redconnect.profile.activity.ProfileScreen;
import redington.com.redconnect.redconnect.settings.activity.SettingsActivity;
import redington.com.redconnect.redconnect.shipmenttracking.activity.ShipmentTrackingActivity;
import redington.com.redconnect.redconnect.spot.activity.PromoCodeActivity;
import redington.com.redconnect.redconnect.voiceofcustomer.activity.VoiceOfCustomerActitvity;
import redington.com.redconnect.util.LaunchIntentManager;

public class MenuBottomSheet extends BottomSheetDialogFragment {
    private static int[] image = {
            R.mipmap.ic_menu_promos,
            R.mipmap.ic_menu_payment_due,
            R.mipmap.ic_menu_cheque,
            R.mipmap.ic_menu_voice_customer,
            R.mipmap.ic_menu_helpdesk,
            R.mipmap.ic_menu_order_status,
            R.mipmap.ic_menu_track,
            R.mipmap.ic_menu_settings


    };
    private Context mContext;
    private RecyclerView recyclerView;
    private String[] names;
    private String activityName;

    /**
     * static helper method to generate an object instance
     *
     * @return  Default Instance of the MenuBottomSheet class
     */
    public static MenuBottomSheet getInstance() {
        return new MenuBottomSheet();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_menu, viewGroup, false);
        mContext = v.getContext();

        LinearLayout mProfileLayout = v.findViewById(R.id.profile_layout);
        mProfileLayout.setOnClickListener(v1 -> {
            dismiss();
            LaunchIntentManager.routeToActivityStack(mContext, ProfileScreen.class);
        });
        final TextView mCustomerName = v.findViewById(R.id.customerName);
        mCustomerName.setText(GlobalConstants.GetSharedValues.getSpUserName());
        final TextView mEmailid = v.findViewById(R.id.emailId);
        mEmailid.setText(GlobalConstants.GetSharedValues.getSpMail());

        recyclerView = v.findViewById(R.id.menu_recyclerView);
        names = getResources().getStringArray(R.array.menu);
        activityName = getArguments().getString(GlobalConstants.MENU_ACTIVITY);
        intiViews();
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources resources = getResources();

        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert getView() != null;
            View parent = (View) getView().getParent();
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) parent.getLayoutParams();
            layoutParams.setMargins(
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left), // 64dp
                    0,
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_right), // 64dp
                    0
            );
            parent.setLayoutParams(layoutParams);
        }
    }

    private void intiViews() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(mContext, 4);
        recyclerView.setLayoutManager(layoutManager);
        ArrayList<PreOrderBean> preOrderBeans = prepareData();
        MenuAdapter1 adapter = new MenuAdapter1(activityName, mContext, preOrderBeans);
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<PreOrderBean> prepareData() {

        ArrayList<PreOrderBean> preOrderBeans = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            preOrderBeans.add(new PreOrderBean(names[i], names[i], image[i]));
        }
        return preOrderBeans;
    }

    /*Adapter*/
    private class MenuAdapter1 extends RecyclerView.Adapter<MenuAdapter1.MenuHolder> {

        private final String activityName;
        private final List<PreOrderBean> mMenuList;
        private Context mContext;
        private CommonMethod commonMethod;
        private Intent intent;

        private MenuAdapter1(String activityName, Context context, List<PreOrderBean> preOrderBeans) {
            this.activityName = activityName;
            this.mContext = context;
            this.mMenuList = preOrderBeans;
            commonMethod = new CommonMethod(context);
        }

        @NonNull
        @Override
        public MenuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sample_menu_recycler, parent, false);
            return new MenuHolder(view);
        }

        @SuppressLint("NewApi")
        @Override
        public void onBindViewHolder(@NonNull MenuHolder holder, final int position) {
            holder.text.setText(mMenuList.get(position).getName());
            Glide.with(mContext).load(mMenuList.get(position).getImages())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.mImageview);
            holder.totalLayout.setOnClickListener(view -> {
                if (commonMethod.listAccess()) {
                    switch (position) {
                        case 0: /*SPOT*/
                            checkActivity(PromoCodeActivity.class);
                            break;
                        case 1: /*Payment Due*/
                            checkActivity(PaymentDue.class);
                            break;
                        case 2: /*Cheque Pending*/
                            checkActivity(ChequePendingActivity.class);
                            break;
                        case 3: /*Voice of Customer*/
                            checkActivity(VoiceOfCustomerActitvity.class);
                            break;
                        case 4:/*Help Desk*/
                            checkActivity(TicketHistoryActivity.class);
                            break;
                        case 5:/*Order Status*/
                            checkActivity(OrderStatusActivity.class);
                            break;
                        case 6:/*Shipment Tracking*/
                            checkActivity(ShipmentTrackingActivity.class);
                            break;
                        case 7:/*Settings Activity*/
                            checkActivity(SettingsActivity.class);
                            break;
                        default:
                            break;
                    }
                    if (intent != null) {
                        mContext.startActivity(intent);
                    }

                    dismiss();
                }

            });


        }

        @SuppressLint("NewApi")
        private void checkActivity(final Class<? extends Activity> menuActivity) {
            if (!Objects.equals(activityName, menuActivity.getSimpleName())) {
                intent = new Intent(mContext, menuActivity);

            }
        }

        @Override
        public int getItemCount() {
            return mMenuList.size();
        }

        class MenuHolder extends RecyclerView.ViewHolder {

            private final TextView text;
            private final ImageView mImageview;
            private final LinearLayout totalLayout;

            MenuHolder(View itemView) {
                super(itemView);

                mContext = itemView.getContext();

                text = itemView.findViewById(R.id.tv_android);
                mImageview = itemView.findViewById(R.id.circle_imageview);
                totalLayout = itemView.findViewById(R.id.LL_recycleView);
            }
        }
    }
}





