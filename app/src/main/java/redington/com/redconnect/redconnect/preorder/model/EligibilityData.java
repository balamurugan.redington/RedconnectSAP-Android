package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

public class EligibilityData {

    @SerializedName("Reason")
    private String mReason;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;

    public String getReason() {
        return mReason;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getReferenceNumber() {
        return mReferenceNumber;
    }
}
