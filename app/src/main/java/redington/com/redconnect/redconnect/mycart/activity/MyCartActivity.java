package redington.com.redconnect.redconnect.mycart.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.adapter.MyCartAdapter;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.preorder.activity.ConfirmationActivity;
import redington.com.redconnect.redconnect.preorder.activity.PreOrderAddress;
import redington.com.redconnect.redconnect.preorder.helper.PreorderDeliveryServiceManager;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressData;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressJsonResponse;
import redington.com.redconnect.redconnect.spot.helper.PromoCodeServiceManager;
import redington.com.redconnect.redconnect.spot.model.PromoCodeData;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.EmptyCartActivity;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class MyCartActivity extends BaseActivity implements CartListener, UIListener {

    private TextView totAmt;
    private TextView totAmt1;
    private Context context;
    private CommonUtils commonUtils;
    private String userID;
    private String productItemCode = "";
    private String selectItemcode = "";
    private String selectItemDesc;
    private String selectvendorCode;
    private int selectqty;
    private double selectPrice;

    private TextView mCustName;
    private TextView mAddress1;
    private TextView mAddress2;
    private TextView mAddress3;
    private TextView mChangeAddress;
    private TextView mChangeAddress1;
    private String delSeq;
    private String stockLocation;
    private String paymentDays;
    private String cameFrom = "";
    private LinearLayout mCardView;

    private RecyclerView recyclerView;
    private SharedPreferences preferences;
    private String apiString = "";
    private String spotString = "";
    private boolean changeAddress = false;

    private TextView mAddress4;
    private TextView mAddress5;
    private TextView mAddress6;
    private boolean mSpotBoolean;
    private LinearLayout footer;
    private String paymentOptions = "";
    private String spotNumber;
    private ArrayList<SubPromoCode> subPromoCodeArrayList;
    private LinearLayout addressLayout;
    private android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout;
    private boolean swipeboolean = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder_cartview);
        context = MyCartActivity.this;

        commonUtils = new CommonUtils(context);
        userID = GlobalConstants.GetSharedValues.getSpUserId();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        setInitViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            if (IntentConstants.SPOT.equals(bundle.getString(GlobalConstants.FROM))) {
                cameFrom = IntentConstants.SPOT;
                callDeliSeq(userID);
                toolbar(IntentConstants.MYSPOT);
            }
        } else {
            toolbar(IntentConstants.CART);
            callDeliSeq(userID);
        }

        addressLayout = findViewById(R.id.addressLayout);


        mChangeAddress.setOnClickListener(v -> goToAddress(context.getString(R.string.deliveryadd)));

        mChangeAddress1.setOnClickListener(v -> goToAddress(context.getString(R.string.billAdd)));

        swipeRefreshLayout.setOnRefreshListener(() -> new android.os.Handler().postDelayed(() -> {
            if (!mSpotBoolean) {
                swipeRefreshLayout.setRefreshing(true);
                swipeboolean = true;
                bindToActivity();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                swipeboolean = false;
            }
        }, 1000L));

    }

    /*Address Screen*/
    private void goToAddress(String address) {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.ADDRESS, address);
        LaunchIntentManager.routeToActivityStackBundle(context, PreOrderAddress.class, bundle);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    /*Move to Confirm Screen*/
    private void confirmValues() {
        Bundle bundle1 = new Bundle();
        bundle1.putString(IntentConstants.DELIVERY_SEQUENCE, delSeq);
        bundle1.putString(IntentConstants.LOCATION_CODE, stockLocation);
        bundle1.putString(IntentConstants.PAYMENT_DAYS, paymentDays);
        bundle1.putString(IntentConstants.ADDRESS_1, mAddress1.getText().toString().trim());
        bundle1.putString(IntentConstants.ADDRESS_2, mAddress2.getText().toString().trim());
        bundle1.putString(IntentConstants.ADDRESS_3, mAddress3.getText().toString().trim());
        bundle1.putString(IntentConstants.ADDRESS_4, mAddress4.getText().toString().trim());
        bundle1.putString(IntentConstants.ADDRESS_5, mAddress5.getText().toString().trim());
        bundle1.putString(IntentConstants.ADDRESS_6, mAddress6.getText().toString().trim());
        bundle1.putString(IntentConstants.PAYOPTION, paymentOptions);
        bundle1.putString(IntentConstants.CUST_NAME, mCustName.getText().toString());
        LaunchIntentManager.routeToActivityStackBundle(context, ConfirmationActivity.class, bundle1);
    }

    private void setInitViews() {

        mChangeAddress = findViewById(R.id.changeAddress);
        mChangeAddress1 = findViewById(R.id.changeAddress1);
        mCustName = findViewById(R.id.text_custName);
        mAddress1 = findViewById(R.id.address1);
        mAddress2 = findViewById(R.id.address2);
        mAddress3 = findViewById(R.id.address3);

        mAddress4 = findViewById(R.id.address4);
        mAddress5 = findViewById(R.id.address5);
        mAddress6 = findViewById(R.id.address6);

        mAddress1.setPadding(10, 3, 3, 3);
        mAddress2.setPadding(10, 3, 3, 3);
        mAddress3.setPadding(10, 3, 3, 3);

        mAddress4.setPadding(10, 3, 3, 3);
        mAddress5.setPadding(10, 3, 3, 3);
        mAddress6.setPadding(10, 3, 3, 3);

        footer = findViewById(R.id.LL_footer);

        preferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        mSpotBoolean = preferences.getBoolean(GlobalConstants.SPOT_BOOELAN, false);

        totAmt = findViewById(R.id.totAmt);
        totAmt1 = findViewById(R.id.totAmt1);

        ImageButton imageButton = findViewById(R.id.imageButton);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        mCardView = findViewById(R.id.li_tot);
        recyclerView = findViewById(R.id.cart_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

    }

    public void menuBack(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        setMenuBack(view.getContext());
    }

    /*Get Cart Call*/
    private void bindToActivity() {
        ArrayList<String> cartArray = new ArrayList<>();
        cartArray.add(userID);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.MODE_SELECT);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        if (paymentOptions.equals(context.getString(R.string.pay_now))) {
            setCashDiscount(GlobalConstants.YES);
        } else {
            setCashDiscount(GlobalConstants.NO);
        }
        cartArray.add(GlobalConstants.GetSharedValues.getCashDiscount());
        getCartAPI(cartArray);
    }

    /*Select Cart Call*/
    public void selectCartApi(String itemCode, String itemDesc, String vendorCode, int quantity, Double price) {
        productItemCode = "";
        selectItemcode = itemCode;
        selectItemDesc = itemDesc;
        selectvendorCode = vendorCode;
        selectqty = quantity;
        selectPrice = price;

        ArrayList<String> cartArraySelect = new ArrayList<>();
        cartArraySelect.add(userID);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.MODE_SELECT);
        cartArraySelect.add(GlobalConstants.NULL_DATA);
        cartArraySelect.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArraySelect.add(GlobalConstants.GetSharedValues.getCashDiscount());
        getCartAPI(cartArraySelect);
    }

    /*Update Cart Call*/
    private void updateCart() {
        productItemCode = selectItemcode;

        ArrayList<String> cartArrayUpdate = new ArrayList<>();
        cartArrayUpdate.add(userID);
        cartArrayUpdate.add(selectItemcode);
        cartArrayUpdate.add(selectItemDesc);
        cartArrayUpdate.add(selectvendorCode);
        cartArrayUpdate.add(String.valueOf(selectqty));
        cartArrayUpdate.add(String.valueOf(selectPrice));
        cartArrayUpdate.add(GlobalConstants.NULL_DATA);
        cartArrayUpdate.add(GlobalConstants.MODE_INSERT);
        cartArrayUpdate.add(GlobalConstants.NULL_DATA);
        cartArrayUpdate.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayUpdate.add(GlobalConstants.GetSharedValues.getCashDiscount());
        getCartAPI(cartArrayUpdate);

    }

    /*Delete Cart Call*/
    public void deleteCart(String itemCode, String itemDesc, String vendorCode, int quantity, Double price, String wishlist) {
        productItemCode = "";
        selectItemcode = "";
        ArrayList<String> cartArrayDelete = new ArrayList<>();
        cartArrayDelete.add(userID);
        cartArrayDelete.add(itemCode);
        cartArrayDelete.add(itemDesc);
        cartArrayDelete.add(vendorCode);
        cartArrayDelete.add(String.valueOf(quantity));
        cartArrayDelete.add(String.valueOf(price));
        cartArrayDelete.add(GlobalConstants.CART_STATUS);
        cartArrayDelete.add(GlobalConstants.MODE_CANCEL);

        cartArrayDelete.add(wishlist);
        cartArrayDelete.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayDelete.add(GlobalConstants.GetSharedValues.getCashDiscount());
        getCartAPI(cartArrayDelete);
    }


    /*Call API*/
    private void getCartAPI(ArrayList<String> cartArray) {
        if (!swipeboolean) {
            commonUtils.showProgressDialog();
        }
        CartServiceManager.getCartServiceCall(cartArray, context, this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_SELECT_CART, MODE_PRIVATE);
        Boolean selectCart = sharedPreferences.getBoolean(GlobalConstants.SP_SELECT_CART_BOOLEAN, false);

        SharedPreferences preferences1 = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = preferences1.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }

        if (selectCart) {
            selectCartFalse();
            callDeliSeq(userID);
            changeAddress = true;
        }
        if (errorCheck) {
            internetCheckFalse();
            callDeliSeq(userID);
        }

    }

    /*Cart Response */

    @Override
    public void cartSuccess(Object successObject) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeboolean = false;
            swipeRefreshLayout.setRefreshing(false);
        } else {
            commonUtils.dismissProgressDialog();
        }

        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    addressLayout.setVisibility(View.VISIBLE);
                    footer.setVisibility(View.VISIBLE);
                    /*9-1-2019*/
                    /*ArrayList<CartData> data = new ArrayList<>();
                    for (int i = 0; i < cartJson.getData().size(); i++) {
                        data.addAll(cartJson.getData().get(i).getCart());
                    }*/
                    ArrayList<CartData> data = new ArrayList<>(cartJson.getData().get(0).getCart());
                    checkValues(data, cartJson);
                    changeAddress = false;
                    break;
                case IntentConstants.FAILURE_URL:
                    if (GlobalConstants.CART_NOT_DELETE.equals(cartJson.getDescription())) {
                        shortToast(context, IntentConstants.CARTPAYMENT);
                    } else {
                        setCartUpdate(0);
                        if (changeAddress) {
                            changeAddress = false;
                            showPopup();
                        } else {
                            setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
                            LaunchIntentManager.routeToActivity(context, EmptyCartActivity.class);
                        }
                    }
                    break;
                default:
                    stockError();
                    break;
            }
        }

    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        stockError();
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        if (changeAddress) {
            changeAddress = false;
            setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
            LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
        } else {
            LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
        }

    }

    private void stockError() {
        if (changeAddress) {
            changeAddress = false;
            showPopup();
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }

    /*Cart Validation*/
    private void checkValues(ArrayList<CartData> data, CartJsonResponse cartJsonResponse) {
        if (apiString.equals(GlobalConstants.CART_CHECK)) {
            apiString = "";
            if (data.get(0).getStatus().equals(IntentConstants.Y)) {
                shortToast(context, IntentConstants.CARTPAYMENT);
            } else {
                confirmValues();
            }
        } else {
            apiString = "";
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            /*9-1-19*/
            /*setCartUpdate(cartJsonResponse.getData().size());*/
            setCartUpdate(cartJsonResponse.getData().get(0).getCart().size());
            recyclerView.setVisibility(View.VISIBLE);
            mCardView.setVisibility(View.VISIBLE);
            MyCartAdapter newCartAdapter = new MyCartAdapter(context, data, 1);
            recyclerView.setAdapter(newCartAdapter);
            newCartAdapter.notifyDataSetChanged();
            calculateTotalValue(data);
            cartValidation(data);
        }
    }


    /*Total Amount*/
    @SuppressLint("SetTextI18n")
    private void calculateTotalValue(ArrayList<CartData> data) {
        Double totq = 0.00;

        int sizTot = data.size();
        for (int i = 0; i < sizTot; i++) {
            totq += data.get(i).getmTotalValue();
        }

        totAmt.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(totq));
        totAmt1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(totq));
    }

    /*validate Cart Qty*/
    private void cartValidation(ArrayList<CartData> data) {
        if (!TextUtils.isEmpty(productItemCode)) {
            boolean itemCodeCheck = true;
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getItemCode().contains(productItemCode)) {
                    int avaliableQty = data.get(i).getmAvailableQuantity();
                    int userQty = data.get(i).getQuantity();
                    /*itemCodeCheck = avaliableQty >= userQty;*/
                    itemCodeCheck = (selectqty <= avaliableQty) && (selectqty == userQty);
                    break;
                }
            }
            if (!itemCodeCheck) {
                longToast(context, context.getString(R.string.qunatityText));
            }
        }
        updateValidation(data);

    }

    /*Check cart payment Process*/
    private void updateValidation(ArrayList<CartData> data) {
        if (!TextUtils.isEmpty(selectItemcode)) {
            boolean itemCodeCheck1 = true;
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getItemCode().contains(selectItemcode)) {
                    String status = data.get(i).getStatus();
                    if (status.equals(IntentConstants.Y)) {
                        itemCodeCheck1 = false;
                    } else {
                        itemCodeCheck1 = true;
                        updateCart();
                        selectItemcode = "";
                    }
                    break;
                }
            }
            if (!itemCodeCheck1) {
                longToast(context, IntentConstants.CARTPAYMENT);
            }
        }
    }


    /*Address Response*/

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        if (spotString.equals(GlobalConstants.PROMOCODE_ARRAY)) {
            spotResponse(successObject);
        } else if (spotString.equals(GlobalConstants.ADDRESS)) {
            addressResponse(successObject);
        }

    }

    @Override
    public void onFailure(Object failureObject) {
        logd(String.valueOf(failureObject));
        commonUtils.dismissProgressDialog();
        changeAddress = false;
        noRecords(context, GlobalConstants.SERVER_ERR);

    }

    @Override
    public void onError() {
        changeAddress = false;
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivity(context, InternetErrorCheck.class);

    }

    private void spotResponse(Object successObject) {
        changeAddress = false;
        PromoCodeJsonResponse promoCodeJsonResonse = (PromoCodeJsonResponse) successObject;
        if (promoCodeJsonResonse != null) {
            switch (promoCodeJsonResonse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    addressLayout.setVisibility(View.VISIBLE);
                    ArrayList<PromoCodeData> promoCodeData = new ArrayList<>(promoCodeJsonResonse.getData());
                    for (int i = 0; i < promoCodeJsonResonse.getData().size(); i++) {
                        if (spotNumber.equals(promoCodeData.get(i).getPromoCode())) {
                            subPromoCodeArrayList = new ArrayList<>(promoCodeData.get(i).getSubPromoCode());
                        }
                    }

                    if (subPromoCodeArrayList != null) {
                        recyclerView.setAdapter(new MyCartAdapter(context, subPromoCodeArrayList, IntentConstants.SPOT));
                        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        mCardView.setVisibility(View.VISIBLE);
                        footer.setVisibility(View.VISIBLE);

                        calculateSpot(subPromoCodeArrayList);
                    } else {
                        noRecords(context, GlobalConstants.NO_RECORDS);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(context, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }

    private void addressResponse(Object successObject) {
        PreorderAddressJsonResponse preorderAddressJsonResponse = (PreorderAddressJsonResponse) successObject;
        if (preorderAddressJsonResponse != null) {
            switch (preorderAddressJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    addressValidation(preorderAddressJsonResponse.getData());
                    billingAddress(preorderAddressJsonResponse.getData());
                    if (cameFrom.equals(IntentConstants.SPOT)) {
                        bindSpotValues();
                    } else {
                        bindToActivity();
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(context, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }


    @SuppressLint("SetTextI18n")
    private void addressValidation(List<PreorderAddressData> data) {
        int addressSize = data.size();
        if (addressSize > 0) {
            if (GlobalConstants.GetSharedValues.getCartDelSeq().equals(IntentConstants.CARTDELSEQ)) {
                mCustName.setText(data.get(0).getCustomerName());
                mAddress1.setText(data.get(0).getAddress1() + GlobalConstants.SPACE + data.get(0).getAddress2());
                mAddress2.setText(data.get(0).getAddress3() + GlobalConstants.SPACE + data.get(0).getAddress4());
                mAddress3.setText(data.get(0).getAddress5() + GlobalConstants.SPACE + data.get(0).getPINCode());
                stockLocation = data.get(0).getLocationCode();
                paymentDays = String.valueOf(data.get(0).getPaymentDays());
                delSeq = data.get(0).getDeliverySequence();
            } else {
                for (int i = 0; i < data.size(); i++) {
                    if (GlobalConstants.GetSharedValues.getCartDelSeq().
                            equals(data.get(i).getDeliverySequence())) {
                        mCustName.setText(data.get(i).getCustomerName());
                        mAddress1.setText(data.get(i).getAddress1() + GlobalConstants.SPACE + data.get(i).getAddress2());
                        mAddress2.setText(data.get(i).getAddress3() + GlobalConstants.SPACE + data.get(i).getAddress4());
                        mAddress3.setText(data.get(i).getAddress5() + GlobalConstants.SPACE + data.get(i).getPINCode());
                        stockLocation = data.get(i).getLocationCode();
                        paymentDays = String.valueOf(data.get(i).getPaymentDays());
                        delSeq = data.get(i).getDeliverySequence();
                    }
                }
            }
        } else {
            mChangeAddress.setVisibility(View.GONE);
        }
    }


    /*Billing address validation*/
    @SuppressLint("SetTextI18n")
    private void billingAddress(List<PreorderAddressData> data) {
        int addressSize = data.size();
        if (addressSize > 0) {
            if (GlobalConstants.GetSharedValues.getBillingSeq().equals(IntentConstants.CARTDELSEQ)) {
                mAddress4.setText(data.get(0).getAddress1() + GlobalConstants.SPACE + data.get(0).getAddress2());
                mAddress5.setText(data.get(0).getAddress3() + GlobalConstants.SPACE + data.get(0).getAddress4());
                mAddress6.setText(data.get(0).getAddress5() + GlobalConstants.SPACE + data.get(0).getPINCode());
            } else {
                for (int i = 0; i < data.size(); i++) {
                    if (GlobalConstants.GetSharedValues.getBillingSeq().
                            equals(data.get(i).getDeliverySequence())) {
                        mAddress4.setText(data.get(i).getAddress1() + GlobalConstants.SPACE + data.get(i).getAddress2());
                        mAddress5.setText(data.get(i).getAddress3() + GlobalConstants.SPACE + data.get(i).getAddress4());
                        mAddress6.setText(data.get(i).getAddress5() + GlobalConstants.SPACE + data.get(i).getPINCode());
                    }
                }
            }
        } else {
            mChangeAddress1.setVisibility(View.GONE);
        }
    }

    public void payNow(View view) {
        paymentOptions = view.getContext().getString(R.string.pay_now);
        buttonClick();
    }


    public void payLater(View view) {
        paymentOptions = view.getContext().getString(R.string.skip_place);
        buttonClick();
    }

    private void buttonClick() {
        if (mSpotBoolean) {
            confirmValues();
        } else {
            apiString = GlobalConstants.CART_CHECK;
            bindToActivity();
        }
    }

    private void bindSpotValues() {
        spotString = GlobalConstants.PROMOCODE_ARRAY;
        spotNumber = preferences.getString(GlobalConstants.SPOT_CODE, "");
        commonUtils.showProgressDialog();
        PromoCodeServiceManager.promoCodeServicecall(userID,
                GlobalConstants.GetSharedValues.getCartDelSeq(),
                "", "", "1", context, this);
    }

    @SuppressLint("SetTextI18n")
    private void calculateSpot(ArrayList<SubPromoCode> subPromoCodes) {
        Double spotAmount = 0.00;
        int sizTot = subPromoCodes.size();
        for (int i = 0; i < sizTot; i++) {
            spotAmount += subPromoCodes.get(i).getmTOTAL();
        }
        totAmt.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(spotAmount));
        totAmt1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(spotAmount));
    }

    /*Delivery Sequence Api Call*/
    private void callDeliSeq(String userID) {
        spotString = GlobalConstants.ADDRESS;
        commonUtils.showProgressDialog();
        PreorderDeliveryServiceManager.getPreorderAddressServiceCall(userID, context, this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences preferences1 = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        preferences1.edit().clear().apply();
    }


    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup() {
        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(context.getString(R.string.alert));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        alertContent.setText(context.getString(R.string.noStock));


        textOk.setOnClickListener(v -> {
            setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
            callDeliSeq(userID);
            mDialog.dismiss();
        });
        mDialog.show();
    }


}
