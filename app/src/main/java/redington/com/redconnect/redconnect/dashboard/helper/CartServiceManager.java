package redington.com.redconnect.redconnect.dashboard.helper;


import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartServiceManager {

    private CartServiceManager() {
        throw new IllegalStateException(IntentConstants.CART);
    }

    public static void getCartServiceCall(List<String> cartData, final Context context, final CartListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getCartServiceCall(getRequestBody(cartData)).enqueue(new Callback<CartJsonResponse>() {
                @Override
                public void onResponse(Call<CartJsonResponse> call, Response<CartJsonResponse> response) {
                    listener.cartSuccess(response.body());
                }

                @Override
                public void onFailure(Call<CartJsonResponse> call, Throwable t) {
                    listener.cartFailure(t.toString());
                }
            });
        } else {
            listener.cartError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(List<String> cartData) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CART_USERID, cartData.get(0));
            jsonValues.put(JsonKeyConstants.ITEM_CODE, cartData.get(1));
            jsonValues.put(JsonKeyConstants.ITEM_DESCRIPTION, cartData.get(2));
            jsonValues.put(JsonKeyConstants.VENDOR_CODE, cartData.get(3));
            jsonValues.put(JsonKeyConstants.QUANTITY, cartData.get(4));
            jsonValues.put(JsonKeyConstants.UNIT_PRICE, cartData.get(5));
            jsonValues.put(JsonKeyConstants.CART_STATUS, cartData.get(6));
            jsonValues.put(JsonKeyConstants.MODE, cartData.get(7));
            jsonValues.put(JsonKeyConstants.WISH_LIST, cartData.get(8));
            /*jsonValues.put(JsonKeyConstants.DEL_SEQ, cartData.get(9));*/
            if (cartData.get(9).equals("000") || cartData.get(9).equals(cartData.get(0))) {
                jsonValues.put(JsonKeyConstants.DEL_SEQ, cartData.get(0));
            } else {
                jsonValues.put(JsonKeyConstants.DEL_SEQ, cartData.get(0) + "-" + cartData.get(9));
            }
            jsonValues.put(JsonKeyConstants.CDC, cartData.get(10));

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
