
package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OrderData implements Serializable {


    @SerializedName("UID")
    private String mUID;
    @SerializedName("Order_Number")
    private String mOrderNumber;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;
    @SerializedName("Invoice_Message")
    private String mInvoiceMessage;
    @SerializedName("Status")
    private String mStatus;


    public String getUID() {
        return mUID;
    }

    public void setUID(String uid) {
        mUID = uid;
    }

    public String getmOrderNumber() {
        return mOrderNumber;
    }

    public void setmOrderNumber(String mOrderNumber) {
        this.mOrderNumber = mOrderNumber;
    }

    public String getmReferenceNumber() {
        return mReferenceNumber;
    }

    public void setmReferenceNumber(String mReferenceNumber) {
        this.mReferenceNumber = mReferenceNumber;
    }

    public String getmInvoiceMessage() {
        return mInvoiceMessage;
    }

    public void setmInvoiceMessage(String mInvoiceMessage) {
        this.mInvoiceMessage = mInvoiceMessage;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }
}
