package redington.com.redconnect.redconnect.ratingfeedback.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackServiceCall {
    private FeedbackServiceCall() {
        throw new IllegalStateException(IntentConstants.PURCHASED);
    }

    public static void getFeedBackCall(String customerCode, String recordNum, String pageNum, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getFeedback(getRequestBody(customerCode, recordNum, pageNum)).enqueue(new Callback<FeedbackJsonResponse>() {
                @Override
                public void onResponse(Call<FeedbackJsonResponse> call, Response<FeedbackJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<FeedbackJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(String customerCode, String recordNum, String pageNum) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CUSTOMERCODE, customerCode);
            jsonValues.put(JsonKeyConstants.RECORDNUM, recordNum);
            jsonValues.put(JsonKeyConstants.PAGENUM, pageNum);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }

}
