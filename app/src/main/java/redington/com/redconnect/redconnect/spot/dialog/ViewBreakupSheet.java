package redington.com.redconnect.redconnect.spot.dialog;


import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;

public class ViewBreakupSheet extends BottomSheetDialogFragment {


    public static ViewBreakupSheet getInstance() {
        return new ViewBreakupSheet();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View itemView = inflater.inflate(R.layout.list_show_breakup, viewGroup, false);


        TextView mLocation = itemView.findViewById(R.id.location);
        LinearLayout mQty = itemView.findViewById(R.id.layout_qty);
        mQty.setVisibility(View.GONE);

        LinearLayout laypotIGST = itemView.findViewById(R.id.layout_igst);
        LinearLayout laypotCGST = itemView.findViewById(R.id.layout_cgst);
        LinearLayout laypotSGST = itemView.findViewById(R.id.layout_sgst);
        LinearLayout laypotCDC = itemView.findViewById(R.id.cdcDiscount);
        LinearLayout actualPriceLayout = itemView.findViewById(R.id.unitPriceLayout);
        actualPriceLayout.setVisibility(View.VISIBLE);
        TextView mIGST = itemView.findViewById(R.id.text_igst);
        TextView mCGST = itemView.findViewById(R.id.text_cgst);
        TextView mSGST = itemView.findViewById(R.id.text_sgst);
        TextView mDiscountPercent = itemView.findViewById(R.id.discountPercent);
        TextView mIGSTTitle = itemView.findViewById(R.id.igst_title);
        TextView mCGSTTitle = itemView.findViewById(R.id.cgst_title);
        TextView mSGSTTitle = itemView.findViewById(R.id.sgst_title);
        TextView mActualPrice = itemView.findViewById(R.id.unit_price);

        TextView mPrice = itemView.findViewById(R.id.text_price);
        TextView mTotalPrice = itemView.findViewById(R.id.totalPrice);
        TextView mDiscountAmount = itemView.findViewById(R.id.discountAmount);

        mLocation.setText(getArguments().getString(IntentConstants.SPOTSTOCKROOM));
        double unitPrice = Double.parseDouble(getArguments().getString(IntentConstants.SPOTUNITPRICE));
        double totalPrice = Double.parseDouble(getArguments().getString(IntentConstants.SPOTTOTTAL));
        mPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(unitPrice));
        mTotalPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(totalPrice));

        double cashDiscount = Double.parseDouble(getArguments().getString(IntentConstants.CASH_DISCOUNT));
        double cashDiscountPrice = Double.parseDouble(getArguments().getString(IntentConstants.CASH_DISCOUNT_PRICE));


        double igst = Double.parseDouble(getArguments().getString(IntentConstants.IGSTVALUE));
        double cgst = Double.parseDouble(getArguments().getString(IntentConstants.CGSTVALUE));
        double sgst = Double.parseDouble(getArguments().getString(IntentConstants.SGSTVALUE));

        double actualPrice = Double.parseDouble(getArguments().getString(IntentConstants.SPOTACTUALPRICE));

        mActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(actualPrice));

        if (cashDiscount == 0.0) {
            laypotCDC.setVisibility(View.GONE);
        } else {
            laypotCDC.setVisibility(View.VISIBLE);
        }

        if (igst != 0.00) {
            laypotIGST.setVisibility(View.VISIBLE);
        } else {
            laypotIGST.setVisibility(View.GONE);
        }

        if (cgst != 0.00) {
            laypotCGST.setVisibility(View.VISIBLE);
        } else {
            laypotCGST.setVisibility(View.GONE);
        }

        if (sgst != 0.00) {
            laypotSGST.setVisibility(View.VISIBLE);
        } else {
            laypotSGST.setVisibility(View.GONE);
        }

        mDiscountPercent.setText(cashDiscount + IntentConstants.PERCENT + GlobalConstants.SPACE + GlobalConstants.DISCOUNT);
        mDiscountAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(cashDiscountPrice));

        mIGSTTitle.setText(IntentConstants.IGST + getArguments().getString(IntentConstants.IGST) + IntentConstants.PERCENT);
        mIGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(igst));


        mCGSTTitle.setText(IntentConstants.CGST + getArguments().getString(IntentConstants.CGST) + IntentConstants.PERCENT);
        mCGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(cgst));


        mSGSTTitle.setText(IntentConstants.SGST + getArguments().getString(IntentConstants.SGST) + IntentConstants.PERCENT);
        mSGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(sgst));

        return itemView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources resources = getResources();

        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert getView() != null;
            View parent = (View) getView().getParent();
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) parent.getLayoutParams();
            layoutParams.setMargins(
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left), // 64dp
                    0,
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_right), // 64dp
                    0
            );
            parent.setLayoutParams(layoutParams);
        }
    }

}
