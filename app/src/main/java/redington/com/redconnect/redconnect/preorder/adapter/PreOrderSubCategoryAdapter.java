package redington.com.redconnect.redconnect.preorder.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.activity.PreOrderActivityList;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryData;
import redington.com.redconnect.util.LaunchIntentManager;


public class PreOrderSubCategoryAdapter extends RecyclerView.Adapter<PreOrderSubCategoryAdapter.PreOrderSubCategoryHolder> {

    private final List<PreorderCategoryData> itemList;
    private Context context;
    private CommonMethod commonMethod;


    public PreOrderSubCategoryAdapter(Context context, List<PreorderCategoryData> itemList) {
        this.itemList = itemList;
        this.context = context;
        commonMethod = new CommonMethod(context);
    }

    @NonNull
    @Override
    public PreOrderSubCategoryAdapter.PreOrderSubCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_preorder_recylce, viewGroup, false);
        return new PreOrderSubCategoryAdapter.PreOrderSubCategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderSubCategoryHolder holder, int position) {
        holder.productName.setText(itemList.get(position).getBRND68());
        String brnd68 = itemList.get(position).getBRND68();
        String image = IntentConstants.IMAGEURL + brnd68 + IntentConstants.IMAGEFORMAT;

        holder.productName.setBackgroundColor(ContextCompat.getColor(context, R.color.black_light));
        holder.productName.setTextColor(ContextCompat.getColor(context, R.color.white));
        Glide.with(context).load(image)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .fitCenter()
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .override(120, 120)
                .into(holder.imagePreOrder);
        final int pos = holder.getAdapterPosition();
        holder.totalLayout.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.PRODUCT_ACTIVITY, GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST);
                bundle.putString(GlobalConstants.PRODUCT_CATEGORY, itemList.get(pos).getCategory());
                bundle.putString(GlobalConstants.PRODUCT_BRAND, itemList.get(pos).getBRND68());
                LaunchIntentManager.routeToActivityStackBundle(context, PreOrderActivityList.class, bundle);
            }
        });
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }


    class PreOrderSubCategoryHolder extends RecyclerView.ViewHolder {
        private final LinearLayout totalLayout;
        private final TextView productName;
        private final ImageView imagePreOrder;


        PreOrderSubCategoryHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            totalLayout = itemView.findViewById(R.id.LL_recycleView);
            productName = itemView.findViewById(R.id.txt_productName);
            imagePreOrder = itemView.findViewById(R.id.imagePreOrder);

        }
    }


}
