
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class PaymentDueData implements Serializable {

    @SerializedName("DataMessage")
    private List<PaymentDueDataMessage> mDataMessage;
    @SerializedName("Table")
    private List<PaymentDueTable> mTable;

    public List<PaymentDueDataMessage> getDataMessage() {
        return mDataMessage;
    }

    public List<PaymentDueTable> getTable() {
        return mTable;
    }


}
