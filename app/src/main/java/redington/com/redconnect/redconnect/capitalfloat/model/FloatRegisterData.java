
package redington.com.redconnect.redconnect.capitalfloat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class FloatRegisterData implements Serializable {

    @SerializedName("app_id")
    private String mAppId;
    @SerializedName("status")
    private Long mStatus;

    public String getAppId() {
        return mAppId;
    }

    public Long getStatus() {
        return mStatus;
    }


}
