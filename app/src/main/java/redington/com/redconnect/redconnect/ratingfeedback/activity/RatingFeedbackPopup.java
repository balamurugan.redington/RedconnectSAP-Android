package redington.com.redconnect.redconnect.ratingfeedback.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.ratingfeedback.helper.RaitngFeedBackServiceManager;
import redington.com.redconnect.redconnect.ratingfeedback.model.RatingFeedbackJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class RatingFeedbackPopup extends Activity implements View.OnClickListener, UIListener {

    private String productItemCode;
    private String userID;

    private EditText feedback;
    private String[] rateText = {"", "Worst", "Bad", "Good", "Excellent", "Awesome"};
    private CommonUtils commonUtils;
    private Context context;
    private String ratingBarValue = "0";


    @SuppressLint({"ClickableViewAccessibility", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_feedback);

        context = RatingFeedbackPopup.this;

        TextView mProductName = findViewById(R.id.txt_productName);
        TextView mItemCode = findViewById(R.id.txt_itemCode);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String productName = bundle.getString(GlobalConstants.PRODUCT_NAME);
            productItemCode = bundle.getString(GlobalConstants.PRODUCT_ITEM_CODE);
            mProductName.setText(productName);
        }

        RelativeLayout relativeLayout = findViewById(R.id.menu_tit);


        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(ContextCompat.getColor(context, R.color.new_black_light));
        float[] cornerValues = {15.0f, 15.0f, 15.0f, 15.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        gradientDrawable.setCornerRadii(cornerValues);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            relativeLayout.setBackground(gradientDrawable);
        }

        commonUtils = new CommonUtils(context);
        userID = GlobalConstants.GetSharedValues.getSpUserId();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mItemCode.setText(productItemCode);

        RatingBar ratingBar = findViewById(R.id.ratingBar1);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(context, R.color.new_buttonColor), PorterDuff.Mode.SRC_ATOP);
        TextView rateTxt = findViewById(R.id.rate_txt);
        Button ratingBarSubmit = findViewById(R.id.btn_ratingBar_submit);
        feedback = findViewById(R.id.enter_feedback);

        ratingBar.setOnRatingBarChangeListener((ratingBar1, ratingValue, b) -> {

            ratingBarValue = Float.toString(ratingBar1.getRating());
            ratingBarValue = ratingBarValue.substring(0, 1);

            rateTxt.setText(rateText[(int) ratingValue]);
        });


        ratingBarSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_ratingBar_submit) {
            String mFeedback = feedback.getText().toString();
            if (TextUtils.isEmpty(mFeedback)) {
                BaseActivity.shortToast(RatingFeedbackPopup.this, GlobalConstants.FEED_BACK);
            } else if (ratingBarValue.equals("0")) {
                BaseActivity.shortToast(RatingFeedbackPopup.this, GlobalConstants.ZERO_RATING_VALUE);
            } else {
                mCallAPI();
            }

        }
    }


    private void mCallAPI() {
        callFeedBack(productItemCode, ratingBarValue, feedback.getText().toString(), userID);
    }


    private void callFeedBack(String itemCode, String feedBack, String review, String userID) {
        commonUtils.showProgressDialog();
        RaitngFeedBackServiceManager.getRaitngFeedBackServiceManager(itemCode, feedBack, review, userID,
                RatingFeedbackPopup.this, this);
    }

    /*>>>> Service API Call <<<<*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        RatingFeedbackJsonResponse ratingFeedback = (RatingFeedbackJsonResponse) successObject;
        if (ratingFeedback != null) {
            switch (ratingFeedback.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    if (ratingFeedback.getData().getConfirmation().equals("Y")) {
                       /* BaseActivity.shortToast(context, ratingFeedback.getDescription());
                        finish();*/
                        Intent intent = new Intent();
                        intent.putExtra(IntentConstants.EDM_FEEDBACK_RESULT,ratingFeedback.getDescription());
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    BaseActivity.shortToast(context, ratingFeedback.getDescription());
                    finish();
                    break;
                default:
                    BaseActivity.shortToast(context, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            BaseActivity.shortToast(context, GlobalConstants.SERVER_ERROR);
        }


    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(context, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            mCallAPI();
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            finish();
        }
    }

    private void internetCheckFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        editor.apply();
    }


    private void backButtonFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        editor.apply();
    }
}
