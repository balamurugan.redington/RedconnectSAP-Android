package redington.com.redconnect.redconnect.spot.activity;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.spot.adapter.PromoCodeAdapter;
import redington.com.redconnect.redconnect.spot.helper.PromoCodeServiceManager;
import redington.com.redconnect.redconnect.spot.model.PromoCodeData;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class PromoCodeActivity extends BaseActivity implements UIListener {
    private Context mContext;
    private CommonUtils mCommonUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);
        mContext = PromoCodeActivity.this;
        mCommonUtils = new CommonUtils(mContext);

        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        mImageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.VISIBLE);

        toolbar(IntentConstants.SPOT);

        mCallApi();

    }

    private void mCallApi() {
        String mUserid = GlobalConstants.GetSharedValues.getSpUserId();
        mCommonUtils.showProgressDialog();
        PromoCodeServiceManager.promoCodeServicecall(mUserid,
                GlobalConstants.GetSharedValues.getCartDelSeq(),
                "", "", "1", mContext, this);
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }


    /*Promo Code Response*/
    @Override
    public void onSuccess(Object successObject) {
        mCommonUtils.dismissProgressDialog();
        PromoCodeJsonResponse promoCodeJsonResonse = (PromoCodeJsonResponse) successObject;
        if (promoCodeJsonResonse != null) {
            switch (promoCodeJsonResonse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PromoCodeData> arrayList = new ArrayList(promoCodeJsonResonse.getData());
                    RecyclerView mRecyclerView = findViewById(R.id.recycle_view);
                    mRecyclerView.setHasFixedSize(true);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                    mRecyclerView.setAdapter(new PromoCodeAdapter(mContext, arrayList));
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }

    }

    @Override
    public void onFailure(Object failureObject) {
        mCommonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);

    }

    @Override
    public void onError() {
        mCommonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            mCallApi();
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }
}

