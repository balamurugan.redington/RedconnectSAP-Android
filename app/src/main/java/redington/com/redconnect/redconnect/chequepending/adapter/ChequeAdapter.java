package redington.com.redconnect.redconnect.chequepending.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.activity.PaymentCommitment;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingData;
import redington.com.redconnect.util.LaunchIntentManager;


public class ChequeAdapter extends RecyclerView.Adapter<ChequeAdapter.MyViewHolder> {

    private final List<ChequePendingData> itemList;
    private CommonMethod commonMethod;


    public ChequeAdapter(Context context, List<ChequePendingData> itemList) {
        this.itemList = itemList;
        commonMethod = new CommonMethod(context);
    }

    public void chequePendingServiceUpdate(List<ChequePendingData> item) {
        itemList.addAll(item);
    }

    @NonNull
    @Override
    public ChequeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_cheques_pending, parent, false);
        return new ChequeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChequeAdapter.MyViewHolder holder, int position) {
        NumberFormat nf = DecimalFormat.getInstance();
        nf.setMaximumFractionDigits(0);

        holder.mInvoiceNum.setText(itemList.get(position).getInvoiceNumber());
        holder.mInvoiceValue.setText(String.format("%s %s", IntentConstants.RUPEES, String.valueOf(itemList.get(position).getInvoiceValue())));
        holder.mInvoiceDate.setText(itemList.get(position).getInvoiceDate());
        holder.mDueDate.setText(itemList.get(position).getDueDate());
        holder.mOldDays.setText(nf.format(itemList.get(position).getOverDueDays()));
        holder.mPendDays.setText(nf.format(itemList.get(position).getPendingDays()));

        if (position % 2 == 0) {
            holder.recycleView.setBackgroundResource(R.color.white_100);
        } else {
            holder.recycleView.setBackgroundResource(R.color.black_light_200);
        }

        final int pos = holder.getAdapterPosition();

        holder.recycleView.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.BIZ_DESC, itemList.get(pos).getBusinessDESC());
                bundle.putString(GlobalConstants.INV_NO, itemList.get(pos).getInvoiceNumber());
                bundle.putString(GlobalConstants.INV_VALUE, String.valueOf(itemList.get(pos).getInvoiceValue()));
                bundle.putString(GlobalConstants.INV_DATE, itemList.get(pos).getInvoiceDate());
                bundle.putString(GlobalConstants.DUE_DATE, itemList.get(pos).getDueDate());
                bundle.putString(GlobalConstants.OLD_DAYS, String.valueOf(itemList.get(pos).getOverDueDays()));
                bundle.putString(GlobalConstants.PENDING_DAYS, String.valueOf(itemList.get(pos).getPendingDays()));
                bundle.putString(GlobalConstants.BALANCE_DUEAMOUNT, itemList.get(pos).getmBalanceDueAmount());
                LaunchIntentManager.routeToActivityStackBundle(v.getContext(), PaymentCommitment.class, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout recycleView;
        private final TextView mInvoiceNum;
        private final TextView mInvoiceValue;
        private final TextView mInvoiceDate;
        private final TextView mDueDate;
        private final TextView mOldDays;
        private final TextView mPendDays;

        MyViewHolder(View view) {
            super(view);
            mInvoiceNum = view.findViewById(R.id.tv_invoice_no);
            mInvoiceValue = view.findViewById(R.id.tv_invoice_value);
            mInvoiceDate = view.findViewById(R.id.tv_invoice_date);
            mDueDate = view.findViewById(R.id.tv_due_date);
            mOldDays = view.findViewById(R.id.tv_old_days);
            mPendDays = view.findViewById(R.id.tv_pnd_days);

            recycleView = view.findViewById(R.id.LL_recycleView);


        }
    }
}

