
package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class ServiceTaxdata implements Serializable {

    @SerializedName("GST")
    private String mGST;
    @SerializedName("Percentage")
    private String mPercentage;
    @SerializedName("ServiceCharge")
    private String mServiceCharge;
    @SerializedName("Total")
    private String mTotal;
    @SerializedName("Type")
    private String mType;
    @SerializedName("Value")
    private String mValue;

    public Double getGST() {
        return CommonMethod.getValidDouble(mGST);
    }


    public String getPercentage() {
        return mPercentage;
    }


    public Double getServiceCharge() {
        return CommonMethod.getValidDouble(mServiceCharge);
    }


    public Double getTotal() {
        return CommonMethod.getValidDouble(mTotal);
    }


    public String getType() {
        return mType;
    }


    public Double getValue() {
        return CommonMethod.getValidDouble(mValue);
    }


}
