package redington.com.redconnect.redconnect.preorder.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreorderCategoryData implements Serializable {

    @SerializedName("BRND68")
    private String mBRND68;
    @SerializedName("Categories")
    private String mCategory;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Image2")
    private String mImage2;
    @SerializedName("FLAG")
    private String mFlag;

    public String getBRND68() {
        return mBRND68;
    }

    public void setBRND68(String bRND68) {
        mBRND68 = bRND68;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getmImage1() {
        return mImage1;
    }

    public void setmImage1(String mImage1) {
        this.mImage1 = mImage1;
    }

    public String getmImage2() {
        return mImage2;
    }

    public void setmImage2(String mImage2) {
        this.mImage2 = mImage2;
    }

    public String getFlag() {
        return mFlag;
    }

    public void setFlag(String mFlag) {
        this.mFlag = mFlag;
    }
}

