
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class PaymentDueDataMessage implements Serializable {

    @SerializedName("ACCOUNT")
    private String mACCOUNT;
    @SerializedName("ADDTN.OUTSDN")
    private String mADDTNOUTSDN;
    @SerializedName("AMOUNT1")
    private String mAMOUNT1;
    @SerializedName("AMOUNT2")
    private String mAMOUNT2;
    @SerializedName("AMOUNT3")
    private String mAMOUNT3;
    @SerializedName("AMOUNT4")
    private String mAMOUNT4;
    @SerializedName("AMOUNT5")
    private String mAMOUNT5;
    @SerializedName("AMOUNT6")
    private String mAMOUNT6;
    @SerializedName("ARITEMS")
    private String mARITEMS;
    @SerializedName("AVG.DAYSPAY")
    private String mAVGDAYSPAY;
    @SerializedName("AVG.DAYSSLOW")
    private String mAVGDAYSSLOW;
    @SerializedName("AVL.CREDIT")
    private String mAVLCREDIT;
    @SerializedName("COLLECTION.DOC")
    private String mCOLLECTIONDOC;
    @SerializedName("COVERCHEQUE")
    private String mCOVERCHEQUE;
    @SerializedName("CRD.LIMIT")
    private String mCRDLIMIT;
    @SerializedName("CUSTOMERNAME")
    private String mCUSTOMERNAME;
    @SerializedName("DAY.SALE.OUTSTND'")
    private String mDAYSALEOUTSTND;
    @SerializedName("DEALBASED.OUTSTDN'")
    private String mDEALBASEDOUTSTDN;
    @SerializedName("DEBTORDAYS")
    private String mDEBTORDAYS;
    @SerializedName("DOUBTFULBAL")
    private String mDOUBTFULBAL;
    @SerializedName("DUNNING")
    private String mDUNNING;
    @SerializedName("ESCROW.OUTSTDN")
    private String mESCROWOUTSTDN;
    @SerializedName("GROUPEXPOSURE")
    private String mGROUPEXPOSURE;
    @SerializedName("HIGH.OS")
    private String mHIGHOS;
    @SerializedName("HIGH.OSDATE")
    private String mHIGHOSDATE;
    @SerializedName("INSURANCELIMIT")
    private String mINSURANCELIMIT;
    @SerializedName("INTEREST")
    private String mINTEREST;
    @SerializedName("INTRST.DATE")
    private String mINTRSTDATE;
    @SerializedName("LASTPAY.DATE")
    private String mLASTPAYDATE;
    @SerializedName("LASTPAYMENT")
    private String mLASTPAYMENT;
    @SerializedName("LASTSALE")
    private String mLASTSALE;
    @SerializedName("LASTSALEDATE")
    private String mLASTSALEDATE;
    @SerializedName("NOTYETDUE")
    private String mNOTYETDUE;
    @SerializedName("ORDERS")
    private String mORDERS;
    @SerializedName("OSBALANCE")
    private String mOSBALANCE;
    @SerializedName("OVERDUEBAL")
    private String mOVERDUEBAL;
    @SerializedName("STATEMENT")
    private String mSTATEMENT;
    @SerializedName("STMT.DATE")
    private String mSTMTDATE;
    @SerializedName("TOT.OUTSDN")
    private String mTOTOUTSDN;
    @SerializedName("YTDCASH")
    private String mYTDCASH;
    @SerializedName("YTD.DISCOUNT")
    private String mYTDDISCOUNT;
    @SerializedName("YTDNETCREDIT")
    private String mYTDNETCREDIT;
    @SerializedName("YTDNETSALES")
    private String mYTDNETSALES;

    public String getACCOUNT() {
        return mACCOUNT;
    }

    public Double getADDTNOUTSDN() {
        return CommonMethod.getValidDouble(mADDTNOUTSDN);
    }

    public Double getAMOUNT1() {
        return CommonMethod.getValidDouble(mAMOUNT1);
    }

    public Double getAMOUNT2() {
        return CommonMethod.getValidDouble(mAMOUNT2);
    }

    public Double getAMOUNT3() {
        return CommonMethod.getValidDouble(mAMOUNT3);
    }

    public Double getAMOUNT4() {
        return CommonMethod.getValidDouble(mAMOUNT4);
    }

    public Double getAMOUNT5() {
        return CommonMethod.getValidDouble(mAMOUNT5);
    }

    public Double getAMOUNT6() {
        return CommonMethod.getValidDouble(mAMOUNT6);
    }

    public Double getARITEMS() {
        return CommonMethod.getValidDouble(mARITEMS);
    }

    public Double getAVGDAYSPAY() {
        return CommonMethod.getValidDouble(mAVGDAYSPAY);
    }

    public Double getAVGDAYSSLOW() {
        return CommonMethod.getValidDouble(mAVGDAYSSLOW);
    }

    public Double getAVLCREDIT() {
        return CommonMethod.getValidDouble(mAVLCREDIT);
    }

    public Double getCOLLECTIONDOC() {
        return CommonMethod.getValidDouble(mCOLLECTIONDOC);
    }

    public String getCOVERCHEQUE() {
        return CommonMethod.getValidDoubleString(mCOVERCHEQUE);
    }

    public Double getCRDLIMIT() {
        return CommonMethod.getValidDouble(mCRDLIMIT);
    }

    public String getCUSTOMERNAME() {
        return mCUSTOMERNAME;
    }

    public Double getDAYSALEOUTSTND() {
        return CommonMethod.getValidDouble(mDAYSALEOUTSTND);
    }

    public Double getDEALBASEDOUTSTDN() {
        return CommonMethod.getValidDouble(mDEALBASEDOUTSTDN);
    }

    public Double getDEBTORDAYS() {
        return CommonMethod.getValidDouble(mDEBTORDAYS);
    }

    public Double getDOUBTFULBAL() {
        return CommonMethod.getValidDouble(mDOUBTFULBAL);
    }

    public Double getDUNNING() {
        return CommonMethod.getValidDouble(mDUNNING);
    }

    public Double getESCROWOUTSTDN() {
        return CommonMethod.getValidDouble(mESCROWOUTSTDN);
    }

    public Double getGROUPEXPOSURE() {
        return CommonMethod.getValidDouble(mGROUPEXPOSURE);
    }

    public Double getHIGHOS() {
        return CommonMethod.getValidDouble(mHIGHOS);
    }

    public String getHIGHOSDATE() {
        return mHIGHOSDATE;
    }

    public Double getINSURANCELIMIT() {
        return CommonMethod.getValidDouble(mINSURANCELIMIT);
    }

    public Double getINTEREST() {
        return CommonMethod.getValidDouble(mINTEREST);
    }

    public String getINTRSTDATE() {
        return mINTRSTDATE;
    }

    public String getLASTPAYDATE() {
        return mLASTPAYDATE;
    }

    public Double getLASTPAYMENT() {
        return CommonMethod.getValidDouble(mLASTPAYMENT);
    }

    public Double getLASTSALE() {
        return CommonMethod.getValidDouble(mLASTSALE);
    }

    public String getLASTSALEDATE() {
        return mLASTSALEDATE;
    }

    public Double getNOTYETDUE() {
        return CommonMethod.getValidDouble(mNOTYETDUE);
    }

    public Double getORDERS() {
        return CommonMethod.getValidDouble(mORDERS);
    }

    public Double getOSBALANCE() {
        return CommonMethod.getValidDouble(mOSBALANCE);
    }

    public Double getOVERDUEBAL() {
        return CommonMethod.getValidDouble(mOVERDUEBAL);
    }

    public Double getSTATEMENT() {
        return CommonMethod.getValidDouble(mSTATEMENT);
    }

    public String getSTMTDATE() {
        return mSTMTDATE;
    }

    public Double getTOTOUTSDN() {
        return CommonMethod.getValidDouble(mTOTOUTSDN);
    }

    public Double getYTDCASH() {
        return CommonMethod.getValidDouble(mYTDCASH);
    }

    public Double getYTDDISCOUNT() {
        return CommonMethod.getValidDouble(mYTDDISCOUNT);
    }

    public Double getYTDNETCREDIT() {
        return CommonMethod.getValidDouble(mYTDNETCREDIT);
    }

    public Double getYTDNETSALES() {
        return CommonMethod.getValidDouble(mYTDNETSALES);
    }


}
