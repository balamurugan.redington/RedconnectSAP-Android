package redington.com.redconnect.redconnect.preorder.helper;


import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.redconnect.paymentcall.model.PaymentPojo;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPlacedManager {


    public OrderPlacedManager(List<String> locationData, List<CartData> cartData, List<PaymentPojo> arrayList,
                              List<SubPromoCode> subPromoCodes, ArrayList<String> serviceCharge,
                              final Context context, final UIListener listener) {

        getOrderPlacedCall(locationData, cartData, arrayList, subPromoCodes, serviceCharge, context, listener);
    }

    private void getOrderPlacedCall(List<String> locationData, List<CartData> cartData, List<PaymentPojo> arrayList,
                                    List<SubPromoCode> subPromoCodes, ArrayList<String> serviceCharge,
                                    final Context context, final UIListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getOrderPlacedServiceCall(getRequestBody(locationData, cartData, arrayList, subPromoCodes, serviceCharge)).enqueue(new Callback<EncryptedOrderResponse>() {
                @Override
                public void onResponse(Call<EncryptedOrderResponse> call, Response<EncryptedOrderResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<EncryptedOrderResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(List<String> locationData, List<CartData> cartData,
                                              List<PaymentPojo> arrayList,
                                              List<SubPromoCode> subPromoCodes,
                                              ArrayList<String> serviceCharge) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JOG_CUSTOMER_CODE, locationData.get(0));
            jsonValues.put(JsonKeyConstants.JOG_MODE, locationData.get(1));
            jsonValues.put(JsonKeyConstants.JOG_STOCK_LOCATION, locationData.get(2));
            jsonValues.put(JsonKeyConstants.JOG_PAYMENT_DAYS, locationData.get(3));
            jsonValues.put(JsonKeyConstants.JOG_DEL_SEQ, locationData.get(4));
            jsonValues.put(JsonKeyConstants.INVOICE_SEQ, locationData.get(5));
            jsonValues.put(JsonKeyConstants.CDC, locationData.get(6));
            jsonValues.put(JsonKeyConstants.REF_NUM, locationData.get(7));
            jsonValues.put(JsonKeyConstants.PAYMENT_TYPE, locationData.get(8));
            jsonValues.put(JsonKeyConstants.INVOICE_COMMITMENT, locationData.get(4));

            JSONObject object = new JSONObject();
            object.put(JsonKeyConstants.DATE, arrayList.get(0).getmDate());
            object.put(JsonKeyConstants.JOG_AMOUNT, arrayList.get(0).getmAmount());
            object.put(JsonKeyConstants.TRANSACTION_ID, arrayList.get(0).getmTransactionID());
            object.put(JsonKeyConstants.JOG_STATUS, arrayList.get(0).getmStatus());
            object.put(JsonKeyConstants.JOG_REMARKS, arrayList.get(0).getmRemarks());
            object.put(JsonKeyConstants.JOG_AMOUNT_RECEIVED, arrayList.get(0).getmAmountReceived());
            object.put(JsonKeyConstants.JOG_TRANSACTION_RECEIVED, arrayList.get(0).getmTransactionRecieved());
            object.put(JsonKeyConstants.JOG_REFERENCE_NO, arrayList.get(0).getmReferenceNum());
            object.put(JsonKeyConstants.JOG_USER_ID, arrayList.get(0).getmUserID());
            object.put(JsonKeyConstants.JOG_DEVICE_ID, arrayList.get(0).getmDeviceID());
            object.put(JsonKeyConstants.JOG_UID, arrayList.get(0).getmUID());
            /*SAP Extra Params*/
            object.put(JsonKeyConstants.JOG_ACTUAL_AMOUNT, serviceCharge.get(0));
            object.put(JsonKeyConstants.JOG_SERVICE_CHARGE, serviceCharge.get(1));
            object.put(JsonKeyConstants.JOG_GST, serviceCharge.get(2));
            object.put(JsonKeyConstants.JOG_BANK_NAME, serviceCharge.get(3));
            object.put(JsonKeyConstants.JOG_TYPE_OF_TRANSACTION, serviceCharge.get(4));
            object.put(JsonKeyConstants.JOG_TYPE_OF_CARD, serviceCharge.get(5));

            jsonValues.put(JsonKeyConstants.INVOICE_COMMITMENT, object);

            JSONArray jsonArray = new JSONArray();
            if (arrayList.get(0).getmStatus().equals("success") || arrayList.get(0).getmStatus().equals("")) {

                if (cartData != null && !cartData.isEmpty()) {
                    for (int i = 0; i < cartData.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(JsonKeyConstants.JOGITEM_CODE, cartData.get(i).getItemCode());
                        jsonObject.put(JsonKeyConstants.JOGLINE_QTY, cartData.get(i).getmAvailableQuantity());
                        jsonObject.put(JsonKeyConstants.JOGUNIT_PRICE, cartData.get(i).getmTotalValue());
                        jsonObject.put(JsonKeyConstants.JOGCASH_DISCOUNT, "0");
                        jsonObject.put(JsonKeyConstants.J_PROMO_CODE, "");
                        jsonObject.put(JsonKeyConstants.ORC, 0);
                        jsonArray.put(i, jsonObject);
                    }
                }
                if (subPromoCodes != null && !subPromoCodes.isEmpty()) {
                    for (int i = 0; i < subPromoCodes.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(JsonKeyConstants.JOGITEM_CODE, subPromoCodes.get(i).getItemCode());
                        jsonObject.put(JsonKeyConstants.JOGLINE_QTY, subPromoCodes.get(i).getQuantity());
                        jsonObject.put(JsonKeyConstants.JOGUNIT_PRICE, subPromoCodes.get(i).getActualPrice());
                        jsonObject.put(JsonKeyConstants.JOGCASH_DISCOUNT, subPromoCodes.get(i).getCASHDISCOUNT());
                        jsonObject.put(JsonKeyConstants.J_PROMO_CODE, subPromoCodes.get(i).getDiscountNumber());
                        jsonObject.put(JsonKeyConstants.ORC, subPromoCodes.get(i).getmORCAmount());

                        jsonArray.put(i, jsonObject);

                    }
                }
            }
            jsonValues.put(JsonKeyConstants.JOG_ITEMS, jsonArray);

            String mEncryptedString = BaseActivity.mEncrypt(String.valueOf(jsonValues));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JsonKeyConstants.JIC_DATA, mEncryptedString);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
