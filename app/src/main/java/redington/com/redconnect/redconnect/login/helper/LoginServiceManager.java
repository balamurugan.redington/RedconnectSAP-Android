package redington.com.redconnect.redconnect.login.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.login.model.LoginJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginServiceManager {

    private LoginServiceManager() {
        throw new IllegalStateException(IntentConstants.LOGIN);
    }

    public static void getLoginServiceCall(String userid, String userpwd, String dviceId, String iMEIno, String version,
                                           final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getloginList(getRequestBody(userid, userpwd, dviceId, iMEIno, version))
                    .enqueue(new Callback<LoginJsonResponse>() {
                        @Override
                        public void onResponse(Call<LoginJsonResponse> call, Response<LoginJsonResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<LoginJsonResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(String userid, String userpwd, String dviceId, String iMEIno,String version) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.USERID, userid);
            jsonValues.put(JsonKeyConstants.PASS_KEY, userpwd);
            JSONObject jsonValues1 = new JSONObject();
            jsonValues1.put(JsonKeyConstants.DEVICE_ID, dviceId);
            jsonValues1.put(JsonKeyConstants.IMEI_NUMBER, iMEIno);
            jsonValues1.put(JsonKeyConstants.SOURCE, GlobalConstants.MOBILE_TYPE);
            jsonValues1.put(JsonKeyConstants.VERSION, version);
            jsonValues.put(JsonKeyConstants.DEVICE, jsonValues1);


            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
