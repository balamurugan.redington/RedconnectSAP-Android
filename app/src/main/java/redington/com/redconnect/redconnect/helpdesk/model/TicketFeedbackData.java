package redington.com.redconnect.redconnect.helpdesk.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TicketFeedbackData implements Serializable {

    @SerializedName("Message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }


}
