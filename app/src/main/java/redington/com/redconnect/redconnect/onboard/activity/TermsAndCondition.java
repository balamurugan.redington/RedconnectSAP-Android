package redington.com.redconnect.redconnect.onboard.activity;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.activity.LoginActivity;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.PermissionUtils;

public class TermsAndCondition extends BaseActivity implements ActivityCompat.OnRequestPermissionsResultCallback
        , PermissionResultCallback {
    ArrayList<String> permissions = new ArrayList<>();
    private String className;
    private PermissionUtils permissionUtils;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_terms_condition);

        Context mContext = TermsAndCondition.this;
        Bundle bundle1 = getIntent().getExtras();
        if (bundle1 != null) {
            className = bundle1.getString(GlobalConstants.TERMS_DATA);
        }
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);


        permissionUtils = new PermissionUtils(mContext);

        permissions.add(Manifest.permission.READ_PHONE_STATE);
        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        permissions.add(Manifest.permission.RECORD_AUDIO);
        permissions.add(Manifest.permission.SEND_SMS);
        permissions.add(Manifest.permission.RECEIVE_SMS);
        permissions.add(Manifest.permission.READ_SMS);

        setInstance();
        permissionUtils.checkPermission(permissions, GlobalConstants.ALLOW_PERMIT, 1);

        toolbar(IntentConstants.POLICY);

    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    private void setInstance() {
        TextView mTextAccept = findViewById(R.id.text_accept);
        ImageButton mBackImage = findViewById(R.id.imageBack);
        if (className.equals("Profile")) {
            mTextAccept.setVisibility(View.GONE);
            mBackImage.setVisibility(View.VISIBLE);
        } else {
            mTextAccept.setVisibility(View.VISIBLE);
            mBackImage.setVisibility(View.GONE);
        }

        sharedPreferenceClearing(sp);
        sharedPreferenceClearing(spInternet);
        sharedPreferenceClearing(spBack);
        sharedPreferenceClearing(selectCart);
        sharedPreferenceClearing(cartData);
        sharedPreferenceClearing(cartCount);
        sharedPreferenceClearing(cartDeliverySequence);
        sharedPreferenceClearing(billingaddressSeq);
        sharedPreferenceClearing(cashDiscount);

        mTextAccept.setOnClickListener(view -> {
            SharedPreferences.Editor editor = BaseActivity.sp1.edit();
            editor.putBoolean(SP_FIRST_TIME, true);
            editor.apply();
            LaunchIntentManager.routeToActivity(TermsAndCondition.this, LoginActivity.class);
        });
    }

    private void sharedPreferenceClearing(SharedPreferences preferences) {
        if (preferences != null) {
            SharedPreferences.Editor editor = preferences.edit();
            if (editor != null) {
                editor.clear();
                editor.apply();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void permissionGranted(int myRequestCode) {
        // No Data
    }

    @Override
    public void partialPermissionGranted(int myRequestCode, List<String> myGrantedPermission) {
        // No Data
    }

    @Override
    public void permissionDenied(int myRequestCode) {
        // No Data
    }

    @Override
    public void neverAskAgain(int myRequestCode) {
        // No Data
    }


}
