
package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OtpData implements Serializable {

    @SerializedName("Message")
    private String mMessage;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;
    @SerializedName("Status")
    private String mStatus;

    public String getMessage() {
        return mMessage;
    }


    public String getReferenceNumber() {
        return mReferenceNumber;
    }


    public String getStatus() {
        return mStatus;
    }


}
