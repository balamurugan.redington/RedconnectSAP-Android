package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OrderPlacedData implements Serializable {
    @SerializedName("UID")
    private String mUid;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("Invoice_Message")
    private String mInvoiceMessage;
    @SerializedName("Order_Number")
    private String mOrderNumber;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;

    public String getInvoiceMessage() {
        return mInvoiceMessage;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public String getReferenceNumber() {
        return mReferenceNumber;
    }

    public String getmStatus() {
        return mStatus;
    }

    public String getmUid() {
        return mUid;
    }
}
