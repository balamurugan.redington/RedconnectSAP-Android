package redington.com.redconnect.redconnect.dashboard.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.EDMData;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;
import redington.com.redconnect.redconnect.ratingfeedback.activity.RatingFeedbackPopup;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackData;

public class EdmAdapter extends RecyclerView.Adapter<EdmAdapter.EdmHolder> {
    private Context mContext;
    private List<EDMData> edmData;
    private String mType = "";
    private int type = 0;
    private List<FeedbackData> feedbackArrayList;

    public EdmAdapter(Context mContext, List<EDMData> data, String type) {
        this.mContext = mContext;
        this.edmData = data;
        this.mType = type;
    }

    public EdmAdapter(Context context, List<FeedbackData> feedbackData, int type) {
        this.mContext = context;
        this.feedbackArrayList = feedbackData;
        this.type = type;
    }

    public void feedbackupdate(List<FeedbackData> data) {
        feedbackArrayList.addAll(data);
    }

    @NonNull
    @Override
    public EdmAdapter.EdmHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layoutRes;
        if (type == 1 && mType.equals("")) {
            layoutRes = R.layout.list_preorder_recycler_gridview;
        } else {
            layoutRes = R.layout.view_all_recycle_list;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new EdmAdapter.EdmHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull EdmAdapter.EdmHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        if (type == 1 && mType.equals("")) {
            holder.mProductName.setText(feedbackArrayList.get(position).getProductName());
            holder.mProductPrice.setVisibility(View.VISIBLE);
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(Double.parseDouble(feedbackArrayList.get(position).getUnitPrice()))));
            holder.mStockCheck.setVisibility(View.GONE);
            holder.mItemCode.setText(feedbackArrayList.get(position).getItemCode());
            holder.mItemCode.setTextSize(12f);

            Glide.with(mContext).load(feedbackArrayList.get(position).getImages().get(0).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mFeedbackImage);

            holder.mTotalLayout.setOnClickListener(view -> {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.PRODUCT_NAME, feedbackArrayList.get(pos).getProductName());
                bundle.putString(GlobalConstants.PRODUCT_ITEM_CODE, feedbackArrayList.get(pos).getItemCode());
                /*LaunchIntentManager.routeToActivityStackBundle(mContext, RatingFeedbackPopup.class, bundle);*/
                Intent intent = new Intent(mContext, RatingFeedbackPopup.class);
                intent.putExtras(bundle);
                ((Activity) mContext).startActivityForResult(intent, IntentConstants.EDM_FEEDBACK_INTENT);
            });
        } else if (type == 0 && mType.equals(GlobalConstants.M_EDM)) {
            holder.mCardProductName.setText(edmData.get(position).getProductName());
            double disPercent = Double.parseDouble(edmData.get(position).getmDiscountPercent());
            if (disPercent == 0.0) {
                holder.mOfferLayout.setVisibility(View.GONE);
                holder.mCardProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        String.valueOf(BaseActivity.isValidNumberFormat().format(edmData.get(position).getmDiscountPrice())));
            } else {
                holder.mOfferLayout.setVisibility(View.VISIBLE);
                holder.mTextOffer.setText(edmData.get(position).getmDiscountPercent() + " Off");
                holder.mCardDisLayout.setVisibility(View.VISIBLE);
                holder.mCardProductPrice.setVisibility(View.GONE);
                holder.mCardDisPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(edmData.get(position).getmDiscountPrice()));
                holder.mCardActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(edmData.get(position).getmActualPrice()));
                holder.mCardActualPrice.setPaintFlags(holder.mCardActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            holder.mCardProductQty.setText(edmData.get(position).getItemCode());
            Glide.with(mContext).load(edmData.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mCardImage);
            holder.mCardLayout.setOnClickListener(view -> setClickFunc(
                    edmData.get(pos).getProductName(),
                    edmData.get(pos).getItemCode(),
                    edmData.get(pos).getVendorCode()));
        }
    }

    private void setClickFunc(String productName, String itemCode, String vendorCode) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, productName);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        if (type == 1 && mType.equals("")) {
            return feedbackArrayList.size();
        } else if (type == 0 && mType.equals(GlobalConstants.M_EDM)) {
            return edmData.size();
        } else {
            return 0;
        }
    }

    class EdmHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTotalLayout;
        private ImageView mFeedbackImage;
        private TextView mProductName;
        private TextView mProductPrice;
        private TextView mStockCheck;

        private CardView mCardLayout;
        private ImageView mCardImage;
        private TextView mCardProductName;
        private LinearLayout mCardDisLayout;
        private TextView mCardDisPrice;
        private TextView mCardActualPrice;
        private TextView mCardProductPrice;
        private TextView mCardProductQty;
        private TextView mItemCode;
        private FrameLayout mOfferLayout;
        private TextView mTextOffer;

        EdmHolder(View view) {
            super(view);
            mTotalLayout = view.findViewById(R.id.LL_recycleView);
            mFeedbackImage = view.findViewById(R.id.imageView);
            mProductName = view.findViewById(R.id.productName);
            mProductPrice = view.findViewById(R.id.productPrice);
            mStockCheck = view.findViewById(R.id.stockCheck);
            mItemCode = view.findViewById(R.id.itemCode);

            mCardLayout = view.findViewById(R.id.cardView);
            mCardImage = view.findViewById(R.id.image);
            mCardProductName = view.findViewById(R.id.text_productname);
            mCardDisLayout = view.findViewById(R.id.discountLayout);
            mCardDisPrice = view.findViewById(R.id.text_discountprice);
            mCardActualPrice = view.findViewById(R.id.text_actualprice);
            mCardProductPrice = view.findViewById(R.id.text_price);
            mCardProductQty = view.findViewById(R.id.text_quantity);

            mOfferLayout = itemView.findViewById(R.id.offerLayout);
            mTextOffer = itemView.findViewById(R.id.text_flatOffer);

        }
    }
}
