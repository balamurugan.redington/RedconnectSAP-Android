
package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreorderItemDetailsSpecification implements Serializable {

    @SerializedName("Header")
    private String mHeader;
    @SerializedName("HeaderValue")
    private String mHeaderValue;

    public String getHeader() {
        return mHeader;
    }

    public String getHeaderValue() {
        return mHeaderValue;
    }


}
