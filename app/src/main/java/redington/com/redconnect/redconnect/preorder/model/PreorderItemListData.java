package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.common.methods.CommonMethod;

public class PreorderItemListData implements Serializable {
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("Availability")
    private String mAvailability;
    @SerializedName("Biz_Code")
    private String mBizCode;
    @SerializedName("Brand")
    private String mBrand;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("Category_Description")
    private String mCategoryDescription;
    @SerializedName("Images")
    private List<PreorderItemListImage> mImages;
    @SerializedName("Item_Category")
    private String mItemCategory;
    @SerializedName("Item_Code")
    private String mItemCode;
    @SerializedName("Item_Description")
    private String mItemDescription;
    @SerializedName("Major_Code")
    private String mMajorCode;
    @SerializedName("Minor_Code")
    private String mMinorCode;
    @SerializedName("Minor_Description")
    private String mMinorDescription;
    @SerializedName("Vendor_Code")
    private String mVendorCode;
    @SerializedName("PageNo")
    private String mPageNo;
    @SerializedName("TotalPage")
    private String mTotalPage;
    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;


    public String getmActualPrice() {

        return CommonMethod.getValidDoubleString(mActualPrice);
    }


    public String getmDiscountPercent() {
        return CommonMethod.getValidDoubleString(mDiscountPercent);
    }


    public String getmDiscountPrice() {
        return CommonMethod.getValidDoubleString(mDiscountPrice);
    }


    public String getAvailability() {
        return mAvailability;
    }

    public String getBizCode() {
        return mBizCode;
    }

    public String getBrand() {
        return mBrand;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getCategoryDescription() {
        return mCategoryDescription;
    }

    public List<PreorderItemListImage> getImages() {
        return mImages;
    }

    public String getItemCategory() {
        return mItemCategory;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public String getItemDescription() {
        return mItemDescription;
    }

    public String getMajorCode() {
        return mMajorCode;
    }

    public String getMinorCode() {
        return mMinorCode;
    }

    public String getMinorDescription() {
        return mMinorDescription;
    }

    public String getVendorCode() {
        return mVendorCode;
    }

    public String getmPageNo() {
        return mPageNo;
    }

    public String getmTotalPage() {
        return mTotalPage;
    }

    public String getProductName() {
        return mProductName;
    }


}
