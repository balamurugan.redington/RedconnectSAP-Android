package redington.com.redconnect.redconnect.helpdesk.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.helpdesk.activity.HelpDeskActivityStatus;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryData;


public class TicketHistoryAdapter extends RecyclerView.Adapter<TicketHistoryAdapter.MyViewHolder> {

    private final List<TicketHistoryData> mArrayList;
    private final Context context;
    private CommonMethod commonMethod;


    public TicketHistoryAdapter(Context context, List<TicketHistoryData> itemList) {
        this.mArrayList = itemList;
        this.context = context;
        commonMethod = new CommonMethod(context);
    }

    @NonNull
    @Override
    public TicketHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ticket_history_recycle, parent, false);
        return new TicketHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TicketHistoryAdapter.MyViewHolder holder, int position) {


        if (position % 2 == 0) {
            holder.recycleViewLayout.setBackgroundResource(R.color.white_100);
        } else {
            holder.recycleViewLayout.setBackgroundResource(R.color.black_light_200);
        }


        holder.ticketNum.setText(mArrayList.get(position).getTICKETNO());
        holder.date.setText(mArrayList.get(position).getTICKETRISEDATE());
        holder.status.setText(mArrayList.get(position).getSTATUS());


        if (mArrayList.get(position).getSTATUS().equalsIgnoreCase(GlobalConstants.CLOSED)) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.category));
        } else if (mArrayList.get(position).getSTATUS().equalsIgnoreCase(GlobalConstants.PENDING)) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.login_icon));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.completed));
        }

        final int pos = holder.getAdapterPosition();
        holder.recycleViewLayout.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                Intent i = new Intent(context, HelpDeskActivityStatus.class);
                i.putExtra(GlobalConstants.MOBILE_NUM, mArrayList.get(pos).getPHONENUMBER());
                i.putExtra(GlobalConstants.TICKET_NUM, mArrayList.get(pos).getTICKETNO());
                i.putExtra(GlobalConstants.HP_DESC, mArrayList.get(pos).getDESCRIPTION());
                i.putExtra(GlobalConstants.HP_COMMNETS, mArrayList.get(pos).getHELPDESKCOMMENTS());
                i.putExtra(GlobalConstants.HP_FEED_BACK, mArrayList.get(pos).getFEEDBACKCOMMENTS());
                i.putExtra(GlobalConstants.HP_RATING_BAR, mArrayList.get(pos).getFEEDBACKSTAR());
                i.putExtra(GlobalConstants.STATUS, mArrayList.get(pos).getSTATUS());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout recycleViewLayout;
        private final TextView ticketNum;
        private final TextView date;
        private final TextView status;

        MyViewHolder(View view) {
            super(view);
            ticketNum = view.findViewById(R.id.tv_ticket_no);
            date = view.findViewById(R.id.tv_ticket_date);
            status = view.findViewById(R.id.tv_ticket_status);

            recycleViewLayout = view.findViewById(R.id.LL_recycleView);

        }
    }
}


