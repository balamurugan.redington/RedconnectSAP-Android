package redington.com.redconnect.redconnect.spot.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.spot.activity.SPOTActivity;
import redington.com.redconnect.redconnect.spot.model.PromoCodeData;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.util.LaunchIntentManager;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeAdapter.PromoCodeHolder> {
    private static int[] backgroundColors = {R.color.colorPrimaryDark, R.color.md_deep_orange_300, R.color.md_light_blue_300,
            R.color.md_deep_purple_300, R.color.md_pink_300, R.color.md_cyan_300, R.color.md_red_300};
    private Context mContext;
    private List<PromoCodeData> mArrayList;

    public PromoCodeAdapter(Context context, List<PromoCodeData> arrayList) {
        this.mArrayList = arrayList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public PromoCodeAdapter.PromoCodeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_promo_code, parent, false);
        return new PromoCodeAdapter.PromoCodeHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PromoCodeAdapter.PromoCodeHolder holder, int position) {

        holder.mPromocode.setText(mArrayList.get(position).getPromoCode());
        holder.mActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE
                + BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getTotalActualPrice()));
        holder.mUnitPrice.setText(String.format("%s %s", IntentConstants.RUPEES,
                String.valueOf(BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getTotalUnitPrice()))));
        holder.mValidtill.setText(String.valueOf(mArrayList.get(position).getValidTill()));
        double value = mArrayList.get(position).getTotalDiscountPercentage();
        if (value != 0.0)
            holder.mDiscount.setText(mArrayList.get(position).getTotalDiscountPercentage() + " off");
        else
            holder.mDiscount.setText(GlobalConstants.SPACE);

        int bgColor = ContextCompat.getColor(mContext, backgroundColors[position % 7]);
        holder.mrlayoutContent.setBackgroundColor(bgColor);


        final ArrayList<SubPromoCode> subPromoCodes = (ArrayList<SubPromoCode>) mArrayList.get(position).getSubPromoCode();

        holder.mTotalLayout.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString(GlobalConstants.COMING_FROM, IntentConstants.SPOT);
            bundle.putString(GlobalConstants.TOOLBAR_TITLE, IntentConstants.SPOT);
            bundle.putSerializable(GlobalConstants.SPOT_ARRAY, subPromoCodes);
            LaunchIntentManager.routeToActivityStackBundle(mContext, SPOTActivity.class, bundle);
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class PromoCodeHolder extends RecyclerView.ViewHolder {
        private TextView mPromocode;
        private TextView mActualPrice;
        private TextView mUnitPrice;
        private TextView mValidtill;
        private TextView mDiscount;
        private LinearLayout mTotalLayout;
        private RelativeLayout mrlayoutContent;

        PromoCodeHolder(View itemView) {
            super(itemView);

            mPromocode = itemView.findViewById(R.id.text_promocode);
            mActualPrice = itemView.findViewById(R.id.text_actualprice);
            mUnitPrice = itemView.findViewById(R.id.text_unitprice);
            mValidtill = itemView.findViewById(R.id.text_validTill);
            mDiscount = itemView.findViewById(R.id.text_discount);
            mTotalLayout = itemView.findViewById(R.id.totallayout);
            mrlayoutContent = itemView.findViewById(R.id.rlayout_content);
        }
    }

}

