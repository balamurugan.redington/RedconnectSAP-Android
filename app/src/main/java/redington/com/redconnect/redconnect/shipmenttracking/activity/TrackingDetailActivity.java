package redington.com.redconnect.redconnect.shipmenttracking.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.IntentConstants;


public class TrackingDetailActivity extends BaseActivity {

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_detail);

        Context mContext = TrackingDetailActivity.this;

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        TextView mOrderno = findViewById(R.id.tv_orderNo);
        TextView mCustomerName = findViewById(R.id.customerName);
        TextView mOrderStatus = findViewById(R.id.tv_orderStatus);
        TextView mDispatchDate = findViewById(R.id.ship_dispatched_date);
        TextView mDispatchTime = findViewById(R.id.ship_dispatched_time);

        FrameLayout mFrame1 = findViewById(R.id.frameLayout1);
        FrameLayout mFrame2 = findViewById(R.id.frameLayout2);
        FrameLayout mFrame3 = findViewById(R.id.frameLayout3);

        FrameLayout mFrameLayout1 = findViewById(R.id.frameLayout_1);
        FrameLayout mFrameLayout2 = findViewById(R.id.frameLayout_2);
        FrameLayout mFrameLayout3 = findViewById(R.id.frameLayout_3);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(GradientDrawable.OVAL);
            gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.buttonColor));
            mFrame1.setBackground(gradientDrawable);
            mFrame2.setBackground(gradientDrawable);
            mFrame3.setBackground(gradientDrawable);

            GradientDrawable gradientDrawable1 = new GradientDrawable();
            gradientDrawable1.setShape(GradientDrawable.OVAL);
            gradientDrawable1.setColor(Color.WHITE);
            mFrameLayout1.setBackground(gradientDrawable1);
            mFrameLayout2.setBackground(gradientDrawable1);
            mFrameLayout3.setBackground(gradientDrawable1);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mOrderno.setText(bundle.getString(IntentConstants.ORDER_NUM));
            mCustomerName.setText(bundle.getString(IntentConstants.CUST_NAME));
            mOrderStatus.setText(bundle.getString(IntentConstants.STATUS));
            mDispatchDate.setText(bundle.getString(IntentConstants.DATE));
            mDispatchTime.setText(bundle.getString(IntentConstants.TIME));
        }

        toolbar(IntentConstants.TRACKDETAILS);

    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


}
