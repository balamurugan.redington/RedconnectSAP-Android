package redington.com.redconnect.redconnect.preorder.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.helper.PreorderDeliveryServiceManager;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressData;
import redington.com.redconnect.redconnect.preorder.model.PreorderAddressJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class PreOrderAddress extends BaseActivity implements View.OnClickListener, UIListener {

    private String userID;
    private CommonUtils commonUtils;
    private ArrayList<PreorderAddressData> arrayListOrder;
    private Context context;
    boolean addressChk = false;
    int mOrderStatus = -1;
    private String cameAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        addressChk = false;

        context = PreOrderAddress.this;
        commonUtils = new CommonUtils(context);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cameAddress = bundle.getString(GlobalConstants.ADDRESS);
        }

        userID = GlobalConstants.GetSharedValues.getSpUserId();

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        toolbar(cameAddress);

        initViews();
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_placeOrder) {
            if (addressChk) {
                if (cameAddress.equals(context.getString(R.string.billAdd))) {
                    String warehouse = arrayListOrder.get(mOrderStatus).getDeliverySequence();
                    if (warehouse.equals(GlobalConstants.EXWAREHOUSE)) {
                        shortToast(context, GlobalConstants.ADDRESS_ERROR);
                    } else {
                        setBillingSeq(arrayListOrder.get(mOrderStatus).getDeliverySequence().trim());
                        SharedPreferences.Editor editor = selectCart.edit();
                        editor.putBoolean(GlobalConstants.SP_SELECT_CART_BOOLEAN, true);
                        editor.apply();
                        finish();
                    }
                } else if (cameAddress.equals(context.getString(R.string.deliveryadd))) {
                    setCartDelSeqUpdate(arrayListOrder.get(mOrderStatus).getDeliverySequence().trim());
                    SharedPreferences.Editor editor = selectCart.edit();
                    editor.putBoolean(GlobalConstants.SP_SELECT_CART_BOOLEAN, true);
                    editor.apply();
                    finish();
                }


            } else {
                shortToast(getApplicationContext(), GlobalConstants.PRO_ADDRESS);
            }

        }

    }


    /*Initialize Views*/
    private void initViews() {
        Button mPlaceOrder = findViewById(R.id.btn_placeOrder);
        mPlaceOrder.setOnClickListener(this);
        bindActivity(userID);
    }

    /*Recycler Views Address List*/
    private void bindActivity(String userID) {
        commonUtils.showProgressDialog();
        PreorderDeliveryServiceManager.getPreorderAddressServiceCall(userID, PreOrderAddress.this, this);
    }

    /*Response API*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        PreorderAddressJsonResponse preorderAddressJsonResponse = (PreorderAddressJsonResponse) successObject;

        if (preorderAddressJsonResponse != null) {
            switch (preorderAddressJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PreorderAddressData> arrayList = new ArrayList(preorderAddressJsonResponse.getData());
                    arrayListOrder = arrayList;
                    RecyclerView mAddressRecyclerView = findViewById(R.id.address_recyclerview);
                    mAddressRecyclerView.setHasFixedSize(true);
                    mAddressRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                    mAddressRecyclerView.setAdapter(new AddressAdapter(arrayList));

                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(context, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(context, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
    }

    /*On Resume*/
    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            bindActivity(userID);
        }
        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }

    }

    public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressHolder> {
        private final List<PreorderAddressData> mArrayList;

        private int selectedPosition = -1;

        private AddressAdapter(List<PreorderAddressData> itemList) {
            this.mArrayList = itemList;

        }

        @NonNull
        @Override
        public AddressAdapter.AddressHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_address_recycle_new, parent, false);
            return new AddressAdapter.AddressHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AddressAdapter.AddressHolder holder, int position) {
            String delSeq = mArrayList.get(position).getDeliverySequence();
            setDeliverySeq(holder, delSeq);

            holder.mAddress1.setText(mArrayList.get(position).getCustomerName());
            if (!mArrayList.get(position).getAddress1().isEmpty()) {
                holder.mAddress2.setText(mArrayList.get(position).getAddress1() + " ,");
            } else {
                holder.mAddress2.setVisibility(View.GONE);
            }
            if (!mArrayList.get(position).getAddress2().isEmpty() &&
                    !mArrayList.get(position).getAddress3().isEmpty()) {
                holder.mAddress3.setText(mArrayList.get(position).getAddress2() + " , " +
                        mArrayList.get(position).getAddress3());
            } else if (!mArrayList.get(position).getAddress2().isEmpty()) {
                holder.mAddress3.setText(mArrayList.get(position).getAddress2());
            } else if (!mArrayList.get(position).getAddress3().isEmpty()) {
                holder.mAddress3.setText(mArrayList.get(position).getAddress3());
            } else {
                holder.mAddress3.setVisibility(View.GONE);
            }

            if (!mArrayList.get(position).getAddress4().isEmpty() &&
                    !mArrayList.get(position).getPINCode().isEmpty()) {
                holder.mAddress4.setText(mArrayList.get(position).getAddress4() + " - " +
                        mArrayList.get(position).getPINCode());
            } else if (!mArrayList.get(position).getAddress4().isEmpty()) {
                holder.mAddress4.setText(mArrayList.get(position).getAddress4());
            } else if (!mArrayList.get(position).getPINCode().isEmpty()) {
                holder.mAddress4.setText(mArrayList.get(position).getPINCode());
            } else {
                holder.mAddress4.setVisibility(View.GONE);
            }

            holder.mAddress5.setText(mArrayList.get(position).getAddress5());

            holder.mCheckboxAddress.setTag(position);
            if (position == selectedPosition) {
                holder.mCheckboxAddress.setChecked(true);
            } else {
                holder.mCheckboxAddress.setChecked(false);
            }


            holder.mCheckboxAddress.setOnClickListener(onStateChangedListener(holder.mCheckboxAddress, position));
        }

        private void setDeliverySeq(AddressHolder holder, String delSeq) {
            if (delSeq.equals(GlobalConstants.EXWAREHOUSE)) {
                holder.mTopLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.failure_color));
                holder.mAddress2.setTextSize(18f);
                holder.mAddress2.setTextColor(ContextCompat.getColor(context, R.color.new_black));
            } else {
                holder.mTopLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.new_black_light));
                holder.mAddress2.setTextSize(14f);
                holder.mAddress2.setTextColor(ContextCompat.getColor(context, R.color.black_ligh_1001));
            }
        }

        private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
            return v -> {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                    mOrderStatus = selectedPosition;
                    addressChk = true;
                } else {
                    selectedPosition = position;
                    mOrderStatus = selectedPosition;
                }
                notifyDataSetChanged();

            };
        }


        @Override
        public int getItemCount() {
            return mArrayList.size();
        }

        class AddressHolder extends RecyclerView.ViewHolder {

            private TextView mAddress1;
            private TextView mAddress2;
            private TextView mAddress3;
            private TextView mAddress4;
            private TextView mAddress5;
            private CheckBox mCheckboxAddress;
            private RelativeLayout mTopLayout;


            AddressHolder(View itemView) {
                super(itemView);

                mAddress1 = itemView.findViewById(R.id.txt_address1);
                mAddress2 = itemView.findViewById(R.id.txt_address2);
                mAddress3 = itemView.findViewById(R.id.txt_address3);
                mAddress4 = itemView.findViewById(R.id.txt_address4);
                mAddress5 = itemView.findViewById(R.id.txt_address5);
                mCheckboxAddress = itemView.findViewById(R.id.checkboxAddress);
                mTopLayout = itemView.findViewById(R.id.topLayout);

            }
        }

    }
}
