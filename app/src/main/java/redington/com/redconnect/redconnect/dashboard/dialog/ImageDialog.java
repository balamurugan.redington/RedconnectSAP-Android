package redington.com.redconnect.redconnect.dashboard.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.dashboard.activity.WebViewActivity;
import redington.com.redconnect.util.LaunchIntentManager;

public class ImageDialog extends Activity {

    private String pdfUrl = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_imageview);

        Context mContext = ImageDialog.this;

        ImageView imageView = findViewById(R.id.image);

        Bundle getBundle = getIntent().getExtras();
        if (getBundle != null) {
            String imageString = getBundle.getString(GlobalConstants.IMAGE);
            pdfUrl = getBundle.getString(GlobalConstants.IMAGE_URL);
            Glide.with(mContext).load(imageString)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(imageView);
        }


        imageView.setOnClickListener(v -> {
            if (pdfUrl != null) {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.IMAGE_URL, pdfUrl);
                LaunchIntentManager.routeToActivityStackBundleStack(mContext, WebViewActivity.class, bundle);
            } else {
                BaseActivity.shortToast(mContext, GlobalConstants.NO_DATA);
            }

        });
    }


}
