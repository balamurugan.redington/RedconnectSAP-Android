package redington.com.redconnect.redconnect.payment.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RemainingItems implements Serializable {

    @SerializedName("ItemCode")
    private String mItemCode;

    public String getmItemCode() {
        return mItemCode;
    }

    public void setmItemCode(String mItemCode) {
        this.mItemCode = mItemCode;
    }
}
