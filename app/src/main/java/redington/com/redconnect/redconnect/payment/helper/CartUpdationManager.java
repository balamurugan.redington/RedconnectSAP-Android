package redington.com.redconnect.redconnect.payment.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.payment.model.CartUpdationJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartUpdationManager {

    private CartUpdationManager(){
        throw new IllegalStateException(IntentConstants.CART_UPDATE);
    }

    public static void cartUpdateCall(String userId, String status, final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());

            RestClient.getInstance(loginHeader).getCartUpdation(userId, status).enqueue(new Callback<CartUpdationJsonResponse>() {
                @Override
                public void onResponse(Call<CartUpdationJsonResponse> call, Response<CartUpdationJsonResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<CartUpdationJsonResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });

        } else {
            listener.mError();
        }
    }
}
