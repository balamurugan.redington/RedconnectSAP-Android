package redington.com.redconnect.redconnect.preorder.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.mycart.activity.ShowBreakupActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.spot.dialog.ViewBreakupSheet;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.util.LaunchIntentManager;

public class OrderConfirmationAdapter extends RecyclerView.Adapter<OrderConfirmationAdapter.OrderHolder> {

    private List<CartData> mArrayList;
    private List<SubPromoCode> mPromoArrayList;
    private String mType = "";
    private int cameFrom = 0;
    private Context mContext;


    public OrderConfirmationAdapter(Context context, List<CartData> arrayList, String type) {
        this.mContext = context;
        this.mArrayList = arrayList;
        this.mType = type;
    }


    public OrderConfirmationAdapter(Context context, List<SubPromoCode> promoCodes, int type) {
        this.mContext = context;
        this.mPromoArrayList = promoCodes;
        this.cameFrom = type;
    }

    @NonNull
    @Override
    public OrderConfirmationAdapter.OrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_confirm_order, parent, false);
        return new OrderConfirmationAdapter.OrderHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderConfirmationAdapter.OrderHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (mType.equals(IntentConstants.CART) && cameFrom == 0) {
            holder.mProductName.setText(mArrayList.get(position).getItemDescription());
            holder.mCartItemcode.setText(mArrayList.get(position).getItemCode());
            holder.mCartQty.setText(String.valueOf(mArrayList.get(position).getmAvailableQuantity()));
            holder.mCartTotalPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getmTotalValue()));

            String status = mArrayList.get(position).getCDCStatus();
            if (status.equals(GlobalConstants.YES)) {
                holder.mCashDiscount.setVisibility(View.VISIBLE);
            } else {
                holder.mCashDiscount.setVisibility(View.GONE);
            }

            holder.mCartShowBreakup.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstants.PRODUCT_NAME, mArrayList.get(pos).getItemDescription());
                bundle.putString(IntentConstants.ITEMCODE, mArrayList.get(pos).getItemCode());
                bundle.putString(IntentConstants.VENDORCODE, mArrayList.get(pos).getVendorCode());
                bundle.putString(IntentConstants.TOTALQUANTITY, String.valueOf(mArrayList.get(pos).getmAvailableQuantity()));
                bundle.putSerializable(IntentConstants.BREAKUPARRAY, (Serializable) mArrayList.get(pos).getBreakup());
                LaunchIntentManager.routeToActivityStackBundle(mContext, ShowBreakupActivity.class, bundle);
            });

            Glide.with(mContext).load(mArrayList.get(position).getImages())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .into(holder.mCartImage);

        } else if (mType.equals("") && cameFrom == 1) {

            holder.mProductName.setText(mPromoArrayList.get(position).getmProductName());
            holder.mCartItemcode.setText(mPromoArrayList.get(position).getItemCode());
            holder.mCartQty.setText(String.valueOf(mPromoArrayList.get(position).getQuantity()));
            holder.mCartTotalPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(mPromoArrayList.get(position).getmTOTAL()));

            double cashDiscount = mPromoArrayList.get(pos).getCASHDISCOUNT();
            if (cashDiscount == 0) {
                holder.mCashDiscount.setVisibility(View.GONE);
            } else {
                holder.mCashDiscount.setVisibility(View.VISIBLE);
            }
            holder.mCartShowBreakup.setOnClickListener(v -> {
                ViewBreakupSheet viewBreakupSheet = ViewBreakupSheet.getInstance();
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstants.CGST, mPromoArrayList.get(pos).getmCGSTPercent());
                bundle.putString(IntentConstants.SGST, mPromoArrayList.get(pos).getmSGSTPercent());
                bundle.putString(IntentConstants.IGST, mPromoArrayList.get(pos).getmIGSTPercent());
                bundle.putString(IntentConstants.CASH_DISCOUNT, String.valueOf(mPromoArrayList.get(pos).getCASHDISCOUNT()));
                bundle.putString(IntentConstants.CGSTVALUE, mPromoArrayList.get(pos).getmCGSTVALUE());
                bundle.putString(IntentConstants.SGSTVALUE, mPromoArrayList.get(pos).getmSGSTVALUE());
                bundle.putString(IntentConstants.IGSTVALUE, mPromoArrayList.get(pos).getmIGSTVALUE());
                bundle.putString(IntentConstants.CASH_DISCOUNT_PRICE, String.valueOf(mPromoArrayList.get(pos).getCASHDISCOUNTPRICE()));
                bundle.putString(IntentConstants.SPOTTOTTAL, String.valueOf(mPromoArrayList.get(pos).getmTOTAL()));
                bundle.putString(IntentConstants.SPOTUNITPRICE, String.valueOf(mPromoArrayList.get(pos).getUnitPrice()));
                bundle.putString(IntentConstants.SPOTACTUALPRICE, String.valueOf(mPromoArrayList.get(pos).getActualPrice()));
                bundle.putString(IntentConstants.SPOTSTOCKROOM, String.valueOf(mPromoArrayList.get(pos).getmSTOCKROOMDESC()));
                viewBreakupSheet.setArguments(bundle);
                viewBreakupSheet.show(((FragmentActivity) mContext).getSupportFragmentManager(), "Bottom Sheet");

            });

            Glide.with(mContext).load(mPromoArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .into(holder.mCartImage);


        }
    }


    @Override
    public int getItemCount() {
        if (mType.equals(IntentConstants.CART) && cameFrom == 0) {
            return mArrayList.size();
        } else if (mType.equals("") && cameFrom == 1) {
            return mPromoArrayList.size();
        } else {
            return 0;
        }
    }


    class OrderHolder extends RecyclerView.ViewHolder {
        private final TextView mProductName;

        private LinearLayout mCashDiscount;
        private ImageView mCartImage;
        private TextView mCartQty;
        private TextView mCartItemcode;
        private TextView mCartTotalPrice;
        private TextView mCartShowBreakup;

        OrderHolder(View itemView) {
            super(itemView);

            mProductName = itemView.findViewById(R.id.product_name);

            mCartImage = itemView.findViewById(R.id.imageView);
            mCartQty = itemView.findViewById(R.id.Qty);
            mCartItemcode = itemView.findViewById(R.id.itemcode);
            mCartTotalPrice = itemView.findViewById(R.id.totalPrice);
            mCartShowBreakup = itemView.findViewById(R.id.showBreakup);
            mCashDiscount = itemView.findViewById(R.id.cash_discount);


        }
    }
}
