package redington.com.redconnect.redconnect.orderstatus.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class OrderList implements Serializable {

    @SerializedName("Customer_Code")
    private String mCustomerCode;
    @SerializedName("Customer_Name")
    private String mCustomerName;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Invoice_Date")
    private String mInvoiceDate;
    @SerializedName("Invoice_Number")
    private String mInvoiceNumber;
    @SerializedName("Order_Date")
    private String mOrderDate;
    @SerializedName("Order_Number")
    private String mOrderNumber;
    @SerializedName("Order_Status")
    private String mOrderStatus;
    @SerializedName("Order_Value")
    private String mOrderValue;
    @SerializedName("Page")
    private String mPage;
    @SerializedName("Payment_Method")
    private String mPaymentMethod;
    @SerializedName("Shipping_Date")
    private String mShippingDate;
    @SerializedName("Shipping_Time")
    private String mShippingTime;
    @SerializedName("Time")
    private String mTime;
    @SerializedName("TotalPage")
    private String mTotalPage;

    public String getCustomerCode() {
        return mCustomerCode;
    }


    public String getCustomerName() {
        return mCustomerName;
    }


    public String getDate() {
        return mDate;
    }


    public String getInvoiceDate() {
        return mInvoiceDate;
    }


    public String getInvoiceNumber() {
        return mInvoiceNumber;
    }


    public String getOrderDate() {
        return mOrderDate;
    }


    public String getOrderNumber() {
        return mOrderNumber;
    }


    public String getOrderStatus() {
        return mOrderStatus;
    }


    public Double getOrderValue() {
        return CommonMethod.getValidDouble(mOrderValue);
    }


    public Double getPage() {
        return CommonMethod.getValidDouble(mPage);
    }


    public String getPaymentMethod() {
        return mPaymentMethod;
    }


    public String getShippingDate() {
        return mShippingDate;
    }


    public String getShippingTime() {
        return mShippingTime;
    }


    public Double getTime() {
        return CommonMethod.getValidDouble(mTime);
    }


    public int getTotalPage() {
        return CommonMethod.getValidInt(mTotalPage);
    }


}
