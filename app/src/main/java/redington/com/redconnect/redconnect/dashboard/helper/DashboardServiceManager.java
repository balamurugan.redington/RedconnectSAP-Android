package redington.com.redconnect.redconnect.dashboard.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.dashboard.model.DashboardJsonresponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardServiceManager {

    private DashboardServiceManager() {
        throw new IllegalStateException(IntentConstants.DASHBOARD);
    }

    public static void getDashboardServiceCall(String userId,
                                               final Context context,
                                               final NewListener listener)
    {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).gerDashboardCall(getRequestBody(userId)).enqueue(new Callback<DashboardJsonresponse>() {
                @Override
                public void onResponse(Call<DashboardJsonresponse> call, Response<DashboardJsonresponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<DashboardJsonresponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });
        } else {
            listener.mError();
        }
    }

    private static RequestBody getRequestBody(String userId) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CUSTOMERCODE, userId);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }

}
