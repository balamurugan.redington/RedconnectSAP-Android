package redington.com.redconnect.redconnect.helpdesk.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TicketSubmissionJsonResponse implements Serializable {

    @SerializedName("Data")
    private TicketSubmitData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;


    public TicketSubmitData getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }

}
