
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class TopSelling implements Serializable {

    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;
    @SerializedName("HeaderName")
    private String mHeaderName;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDesc")
    private String mItemDesc;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("VendorCode")
    private String mVendorCode;

    public String getActualPrice() {
        return CommonMethod.getValidDoubleString(mActualPrice);
    }

    public String getDiscountPercent() {
        return CommonMethod.getValidDoubleString(mDiscountPercent);
    }

    public Double getDiscountPrice() {
        return CommonMethod.getValidDouble(mDiscountPrice);
    }

    public String getHeaderName() {
        return mHeaderName;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public String getItemDesc() {
        return mItemDesc;
    }

    public String getProductName() {
        return mProductName;
    }

    public String getVendorCode() {
        return mVendorCode;
    }


}
