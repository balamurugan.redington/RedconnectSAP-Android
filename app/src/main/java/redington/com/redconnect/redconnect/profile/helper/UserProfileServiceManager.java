package redington.com.redconnect.redconnect.profile.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.profile.model.ProfileJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileServiceManager {

    private UserProfileServiceManager() {
        throw new IllegalStateException(IntentConstants.USER_PROFILE);
    }

    public static void getUserProfileServiceManager(String userId, final Context context, final NewListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headRequest).getUserProfileDetails(userId).enqueue(new Callback<ProfileJsonResponse>() {
                @Override
                public void onResponse(Call<ProfileJsonResponse> call, Response<ProfileJsonResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<ProfileJsonResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });

        } else {
            listener.mError();
        }


    }

}
