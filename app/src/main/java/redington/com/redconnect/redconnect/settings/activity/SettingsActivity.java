package redington.com.redconnect.redconnect.settings.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.capitalfloat.activity.CapitalFloatRegistration;
import redington.com.redconnect.redconnect.capitalfloat.helper.CheckJbaIdManager;
import redington.com.redconnect.redconnect.capitalfloat.model.CheckJbaIdReponse;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.preorder.model.PreOrderBean;
import redington.com.redconnect.redconnect.settings.adapter.SettingsAdapter;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

public class SettingsActivity extends BaseActivity implements UIListener {

    private Context mContext;
    private CommonUtils commonUtils;
    private String userId;

    private static int[] image = {
            R.drawable.apple,
            R.drawable.changepass,
            R.drawable.settings,
            R.drawable.rating,
            R.drawable.report,
            R.drawable.policies,
            R.drawable.care,
            R.drawable.logout
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mContext = SettingsActivity.this;
        commonUtils = new CommonUtils(mContext);

        userId = GlobalConstants.GetSharedValues.getSpUserId();
        ImageView mCapFloat = findViewById(R.id.capitalfloat);
        ImageView mIndifi = findViewById(R.id.indifi);
        TextView textCapitalFloat = findViewById(R.id.text_capitalFloat);
        TextView textIndifi = findViewById(R.id.textIndifi);
        /*Kamesh*/
        /*mCapFloat.setOnClickListener(v -> checkJbaId());*/
        mCapFloat.setOnClickListener(v -> shortToast(mContext, GlobalConstants.COMING_SOON));
        /*Kamesh*/

        String textValue = mContext.getString(R.string.new_user_signup);
        SpannableString spannableString = new SpannableString(textValue);
        spannableString.setSpan(new RelativeSizeSpan(1.5f), 11, 18, 0);
        textCapitalFloat.setText(spannableString);
        textIndifi.setText(spannableString);

        mIndifi.setOnClickListener(v -> shortToast(mContext, GlobalConstants.COMING_SOON));
        setInitViews();
    }

    private void checkJbaId() {
        commonUtils.showProgressDialog();
        CheckJbaIdManager.checkJbaIdManager(userId, mContext, this);
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    private void setInitViews() {
        toolbar(IntentConstants.SETTINGS);
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        setInstance();
    }

    private void setInstance() {
        RecyclerView recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setPadding(25, 0, 0, 0);
        recyclerView.setHasFixedSize(true);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        ArrayList<PreOrderBean> preOrderBeans = prepareData();

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new SettingsAdapter(mContext, preOrderBeans));
    }


    /*Prepare Data's*/
    private ArrayList<PreOrderBean> prepareData() {
        String versionName = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            logd(e.getMessage());
        }
        String[] values = getResources().getStringArray(R.array.settings);
        String[] values1 = new String[]{"", "", versionName, "", "", "", "", ""};
        ArrayList<PreOrderBean> arrayList = new ArrayList<>();
        for (int i = 0; i < values.length; i++) {
            arrayList.add(new PreOrderBean(values[i], values1[i], image[i]));
        }
        return arrayList;
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CheckJbaIdReponse checkJbaIdReponse = (CheckJbaIdReponse) successObject;
        if (checkJbaIdReponse != null) {
            switch (checkJbaIdReponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    switch (checkJbaIdReponse.getData().getUserSts()) {
                        case IntentConstants.TRUE:
                            showPopup(IntentConstants.CAPITAL_FLOAT_REGISTERED);
                            break;
                        case IntentConstants.FALSE:
                            showPopup(IntentConstants.NOT_ACTIVE);
                            break;
                        case IntentConstants.NOT_AVL:
                            LaunchIntentManager.routeToActivityStack(mContext, CapitalFloatRegistration.class);
                            break;
                        default:
                            finish();
                            shortToast(mContext, GlobalConstants.SERVER_ERROR);
                            break;
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    @SuppressLint("NewApi")
    private void showPopup(String errorOrNot) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);

        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);

        alertContent.setText(errorOrNot);


        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });


        mDialog.show();
    }
}
