
package redington.com.redconnect.redconnect.ratingfeedback.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class FeedbackImage implements Serializable {

    @SerializedName("ImageURL")
    private String mImageURL;

    public String getImageURL() {
        return mImageURL;
    }


}
