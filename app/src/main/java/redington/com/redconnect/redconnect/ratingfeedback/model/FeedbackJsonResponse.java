
package redington.com.redconnect.redconnect.ratingfeedback.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class FeedbackJsonResponse implements Serializable {

    @SerializedName("Data")
    private List<FeedbackData> mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public List<FeedbackData> getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
