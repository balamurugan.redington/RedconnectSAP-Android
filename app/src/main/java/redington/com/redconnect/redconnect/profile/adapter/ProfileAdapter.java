package redington.com.redconnect.redconnect.profile.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.profile.model.ProfileBean;
import redington.com.redconnect.redconnect.profile.model.ProfileData;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileHolder> {

    private final List<ProfileBean> itemList;
    private final List<ProfileData> arrayList;
    private Context context;

    public ProfileAdapter(Context context, List<ProfileBean> itemList, List<ProfileData> arrayList) {
        this.itemList = itemList;
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProfileAdapter.ProfileHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_profile_list, viewGroup, false);
        return new ProfileAdapter.ProfileHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProfileHolder holder, int position) {
        holder.text1.setText(itemList.get(position).getText1());

        Picasso.with(context).load(itemList.get(position).getImage()).into(holder.mImageView);

        if (position == 0) {
            holder.text2.setText(arrayList.get(0).getmPHONENUMBER2());
        } else if (position == 1) {
            holder.text2.setText(arrayList.get(0).getmFAXNUMBER());
        } else if (position == 2) {
            holder.text2.setText(arrayList.get(0).getmCompanyRegisterNumber());
        } else if (position == 3) {
            holder.text2.setText(arrayList.get(0).getmADDRESS1() + ", " +
                    arrayList.get(0).getmADDRESS2() + ", " +
                    arrayList.get(0).getmADDRESS3() + ", " +
                    arrayList.get(0).getmADDRESS4() + ", " +
                    arrayList.get(0).getmADDRESS5() + ", " +
                    arrayList.get(0).getmPOSTCODE());
        } else if (position == 4) {
            holder.text2.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(arrayList.get(0).getmCREDITLIMIT())));
        } else if (position == 5) {
            holder.text2.setText(arrayList.get(0).getmHIGHESTEXPOSUREDATE());
        } else if (position == 6) {
            holder.text2.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(arrayList.get(0).getmHIGHESTEXPOSURE())));
        } else if (position == 7) {
            holder.text2.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(arrayList.get(0).getmLASTPAYMENT())));
        } else if (position == 8) {
            holder.text2.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(arrayList.get(0).getmLASTINVOICEVALUE())));
        } else if (position == 9) {
            holder.text2.setText(arrayList.get(0).getmNumOfChq());
        }

    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ProfileHolder extends RecyclerView.ViewHolder {
        private final TextView text1;
        private final TextView text2;

        private final ImageView mImageView;

        ProfileHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            text1 = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
            mImageView = itemView.findViewById(R.id.imgview);
        }
    }


}

