package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class Deal implements Serializable
{
    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("Discount")
    private String mDiscount;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDesc")
    private String mItemDesc;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("VendorCode")
    private String mVendorCode;
    @SerializedName("HeaderName")
    private String mHeaderName;

    @SerializedName("StartingDate")
    private String mStartingDate;
    @SerializedName("EndingDate")
    private String mEndingDate;
    @SerializedName("ServerTime")
    private String mServerTime;
    @SerializedName("EndTime")
    private String mEndTime;
    @SerializedName("ServerDate")
    private String mServerDate;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;

    public String getmDiscountPercent() {

        return CommonMethod.getValidDoubleString(mDiscountPercent);
    }


    public String getmServerDate() {
        return mServerDate;
    }


    public Double getActualPrice() {
        return CommonMethod.getValidDouble(mActualPrice);
    }


    public String getCustomerCode() {
        return mCustomerCode;
    }


    public String getDiscount() {
        return CommonMethod.getValidDoubleString(mDiscount);
    }


    public Double getDiscountPrice() {
        return CommonMethod.getValidDouble(mDiscountPrice);
    }


    public String getImageURL() {
        return mImageURL;
    }


    public String getItemCode() {
        return mItemCode;
    }


    public String getItemDesc() {
        return mItemDesc;
    }


    public String getProductName() {
        return mProductName;
    }


    public String getVendorCode() {
        return mVendorCode;
    }


    public String getHeaderName() {
        return mHeaderName;
    }


    public String getStartingDate() {
        return mStartingDate;
    }


    public String getEndingDate() {
        return mEndingDate;
    }


    public String getServerTime() {
        return mServerTime;
    }


    public String getEndTime() {
        return mEndTime;
    }

}



