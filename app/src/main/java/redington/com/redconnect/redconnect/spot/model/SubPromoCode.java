package redington.com.redconnect.redconnect.spot.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;

public class SubPromoCode implements Serializable {

    @SerializedName("ACTUALPRICE")
    private String mActualPrice;
    @SerializedName("BUSINESSCODE")
    private String mBusinessCode;
    @SerializedName("CUSTOMERCODE")
    private String mCustomerCode;
    @SerializedName("DELIVERYSEQ")
    private String mDeliverySeq;
    @SerializedName("DISCOUNTNUMBER")
    private String mDiscountNumber;
    @SerializedName("DISCOUNTPERCENT")
    private String mDiscountPercent;
    @SerializedName("DISCOUNTPRICE")
    private String mDISCOUNTPRICE;

    @SerializedName("DISCOUNTTYPE")
    private String mDiscountType;
    @SerializedName("IMAGEURL")
    private String mImageURL;
    @SerializedName("ITEMCODE")
    private String mItemCode;
    @SerializedName("LINENUMBER")
    private String mLineNumber;
    @SerializedName("PROMOCODE")
    private String mPromoCode;
    @SerializedName("QUANTITY")
    private String mQuantity;
    @SerializedName("UNITPRICE")
    private String mUnitPrice;
    @SerializedName("VALIDTILL")
    private String mValidTill;
    @SerializedName("VENDORREFERENCE")
    private String mVendorReference;
    @SerializedName("PRODUCTNAME")
    private String mProductName;
    @SerializedName("VENDORCODE")
    private String mVendorCode;

    @SerializedName("STOCKROOM")
    private String mSTOCKROOM;
    @SerializedName("STOCKROOMDESC")
    private String mSTOCKROOMDESC;
    @SerializedName("CGSTCODE")
    private String mCGSTCODE;
    @SerializedName("IGSTCODE")
    private String mIGSTCODE;
    @SerializedName("CGSTPercent")
    private String mCGSTPercent;
    @SerializedName("SGSTPercent")
    private String mSGSTPercent;
    @SerializedName("IGSTPercent")
    private String mIGSTPercent;
    @SerializedName("CGSTVALUE")
    private String mCGSTVALUE;
    @SerializedName("SGSTVALUE")
    private String mSGSTVALUE;
    @SerializedName("IGSTVALUE")
    private String mIGSTVALUE;
    @SerializedName("TOTAL")
    private String mTOTAL;

    @SerializedName("CASHDISCOUNT")
    private String mCASHDISCOUNT;
    @SerializedName("CASHDISCOUNTPRICE")
    private String mCASHDISCOUNTPRICE;

    @SerializedName("ORCAMOUNT")
    private String mORCAmount;

    public Double getmORCAmount() {
        return CommonMethod.getValidDouble(mORCAmount);
    }

    public Double getCASHDISCOUNT() {
        return CommonMethod.getValidDouble(mCASHDISCOUNT);
    }

    public Double getCASHDISCOUNTPRICE() {
        return CommonMethod.getValidDouble(mCASHDISCOUNTPRICE);
    }

    public Double getmDISCOUNTPRICE() {
        return CommonMethod.getValidDouble(mDISCOUNTPRICE);
    }

    public Double getActualPrice() {
        return CommonMethod.getValidDouble(mActualPrice);
    }

    public String getBusinessCode() {
        return mBusinessCode;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public String getDeliverySeq() {
        return mDeliverySeq;
    }

    public String getDiscountNumber() {
        return mDiscountNumber;
    }

    public Double getDiscountPercent() {
        return CommonMethod.getValidDouble(mDiscountPercent);
    }

    public String getDiscountType() {
        return mDiscountType;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public int getLineNumber() {
        return CommonMethod.getValidInt(mLineNumber);
    }

    public String getPromoCode() {
        return mPromoCode;
    }

    public int getQuantity() {
        return CommonMethod.getValidInt(mQuantity);
    }

    public Double getUnitPrice() {
        return CommonMethod.getValidDouble(mUnitPrice);
    }

    public String getValidTill() {
        return mValidTill;
    }

    public String getVendorReference() {
        return mVendorReference;
    }

    public String getmProductName() {
        return mProductName;
    }

    public String getmVendorCode() {
        return mVendorCode;
    }

    public String getmSTOCKROOM() {
        return mSTOCKROOM;
    }

    public String getmSTOCKROOMDESC() {
        return mSTOCKROOMDESC;
    }

    public String getmCGSTCODE() {
        return mCGSTCODE;
    }

    public String getmIGSTCODE() {
        return mIGSTCODE;
    }

    public String getmCGSTPercent() {
        return CommonMethod.getValidDoubleString(mCGSTPercent);
    }

    public String getmSGSTPercent() {
        return CommonMethod.getValidDoubleString(mSGSTPercent);
    }

    public String getmIGSTPercent() {
        return CommonMethod.getValidDoubleString(mIGSTPercent);
    }

    public String getmCGSTVALUE() {
        return CommonMethod.getValidDoubleString(mCGSTVALUE);
    }

    public String getmSGSTVALUE() {
        return CommonMethod.getValidDoubleString(mSGSTVALUE);
    }

    public String getmIGSTVALUE() {
        return CommonMethod.getValidDoubleString(mIGSTVALUE);
    }

    public Double getmTOTAL() {
        return CommonMethod.getValidDouble(mTOTAL);
    }
}
