package redington.com.redconnect.redconnect.preorder.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.redconnect.preorder.model.SortingBean;

public class SortingAdapter extends RecyclerView.Adapter<SortingAdapter.SortHolder> {
    private final List<SortingBean> mArrayList;
    private Context mContext;
    private int sortNo;

    public SortingAdapter(Context context, List<SortingBean> itemList, int sortNo) {
        this.mArrayList = itemList;
        this.mContext = context;
        this.sortNo = sortNo;
    }

    @NonNull
    @Override
    public SortingAdapter.SortHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_bottom_sheet, viewGroup, false);
        return new SortingAdapter.SortHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SortingAdapter.SortHolder holder, int position) {

        holder.mTextview.setText(mArrayList.get(position).getText());
        if (sortNo == 5 && position == 0) {
            holder.mTextview.setTextColor(ContextCompat.getColor(mContext, R.color.new_buttonColor));
        } else if (sortNo != 0 && sortNo == position) {
            holder.mTextview.setTextColor(ContextCompat.getColor(mContext, R.color.new_buttonColor));
        } else {
            holder.mTextview.setTextColor(ContextCompat.getColor(mContext, R.color.new_black));
        }

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    class SortHolder extends RecyclerView.ViewHolder {
        final LinearLayout totalLayout;
        private final TextView mTextview;

        SortHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();

            totalLayout = itemView.findViewById(R.id.LL_recycleView);
            mTextview = itemView.findViewById(R.id.txt_listView);


        }
    }


}
