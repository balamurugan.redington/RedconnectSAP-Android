
package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PreorderItemListJsonResponse implements Serializable {

    @SerializedName("Data")
    private List<PreorderItemListData> mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public List<PreorderItemListData> getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }


}
