
package redington.com.redconnect.redconnect.orderstatus.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderStatusJsonResponse implements Serializable {

    @SerializedName("Data")
    private OrderStatusData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public OrderStatusData getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
