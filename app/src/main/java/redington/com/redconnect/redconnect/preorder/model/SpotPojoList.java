package redington.com.redconnect.redconnect.preorder.model;


public class SpotPojoList {
    private int mID;
    private String mUserID;
    private String mItemCode;
    private String mQuantity;
    private String mUnitPrice;
    private String mPromoCode;
    private String mCashdiscountPercent;
    private String mOrcAmount;

    public String getOrcAmount() {
        return mOrcAmount;
    }

    public void setOrcAmount(String mOrcAmount) {
        this.mOrcAmount = mOrcAmount;
    }

    public String getmCashdiscountPercent() {
        return mCashdiscountPercent;
    }

    public void setmCashdiscountPercent(String mCashdiscountPercent) {
        this.mCashdiscountPercent = mCashdiscountPercent;
    }

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public String getmUserID() {
        return mUserID;
    }

    public void setmUserID(String mUserID) {
        this.mUserID = mUserID;
    }

    public String getmItemCode() {
        return mItemCode;
    }

    public void setmItemCode(String mItemCode) {
        this.mItemCode = mItemCode;
    }


    public String getmQuantity() {
        return mQuantity;
    }

    public void setmQuantity(String mQuantity) {
        this.mQuantity = mQuantity;
    }

    public String getmUnitPrice() {
        return mUnitPrice;
    }

    public void setmUnitPrice(String mUnitPrice) {
        this.mUnitPrice = mUnitPrice;
    }


    public String getmPromoCode() {
        return mPromoCode;
    }

    public void setmPromoCode(String mPromoCode) {
        this.mPromoCode = mPromoCode;
    }
}
