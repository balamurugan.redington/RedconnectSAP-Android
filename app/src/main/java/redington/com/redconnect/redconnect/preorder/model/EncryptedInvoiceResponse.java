
package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

public class EncryptedInvoiceResponse {

    @SerializedName("Data")
    private String mData;

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }

}
