
package redington.com.redconnect.redconnect.capitalfloat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class FloatRegisterResponse implements Serializable {

    @SerializedName("Data")
    private FloatRegisterData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public FloatRegisterData getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }


}
