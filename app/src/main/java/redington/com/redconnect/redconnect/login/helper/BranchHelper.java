package redington.com.redconnect.redconnect.login.helper;

import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.model.BranchResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchHelper {

    private BranchHelper() {
        throw new IllegalStateException(IntentConstants.CUSTOMERPROFILE);
    }

    public static void getBranch(String selectedState,
                                 final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getBranchList(selectedState)
                    .enqueue(new Callback<BranchResponse>() {
                        @Override
                        public void onResponse(Call<BranchResponse> call, Response<BranchResponse> response) {
                            listener.mSuccessObject(response.body());
                        }

                        @Override
                        public void onFailure(Call<BranchResponse> call, Throwable t) {
                            listener.mFailureObject(t.toString());
                        }
                    });
        } else {
            listener.mError();
        }
    }
}
