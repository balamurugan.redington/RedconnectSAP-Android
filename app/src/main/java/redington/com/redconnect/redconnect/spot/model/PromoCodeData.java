package redington.com.redconnect.redconnect.spot.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.common.methods.CommonMethod;


public class PromoCodeData implements Serializable {

    @SerializedName("PromoCode")
    private String mPromoCode;
    @SerializedName("Sub_PromoCode")
    private List<SubPromoCode> mSubPromoCode;
    @SerializedName("TotalActualPrice")
    private String mTotalActualPrice;
    @SerializedName("TotalDiscountPercentage")
    private String mTotalDiscountPercentage;
    @SerializedName("TotalUnitPrice")
    private String mTotalUnitPrice;
    @SerializedName("ValidTill")
    private String mValidTill;

    public String getPromoCode() {
        return mPromoCode;
    }

    public List<SubPromoCode> getSubPromoCode() {
        return mSubPromoCode;
    }

    public Double getTotalActualPrice() {
        return CommonMethod.getValidDouble(mTotalActualPrice);
    }

    public Double getTotalDiscountPercentage() {
        return CommonMethod.getValidDouble(mTotalDiscountPercentage);
    }

    public Double getTotalUnitPrice() {
        return CommonMethod.getValidDouble(mTotalUnitPrice);
    }

    public String getValidTill() {
        return mValidTill;
    }


}
