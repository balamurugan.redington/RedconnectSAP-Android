package redington.com.redconnect.redconnect.shipmenttracking.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.adapter.ScrollListener;
import redington.com.redconnect.redconnect.orderstatus.helper.OrderStatusServiceManager;
import redington.com.redconnect.redconnect.orderstatus.model.OrderStatusJsonResponse;
import redington.com.redconnect.redconnect.orderstatus.model.ShippingList;
import redington.com.redconnect.redconnect.shipmenttracking.adapter.ShipmentTrackingAdapter;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class ShipmentTrackingActivity extends BaseActivity implements UIListener {


    private Context mContext;
    private int currentPage = 1;
    private int mTotalPage;
    private CommonUtils mCommonutils;
    private FrameLayout mProgressBar;
    private RelativeLayout mRelLayout;
    private String mUSerID;
    private RecyclerView mRecyclerview;
    private LinearLayoutManager verticalLayout;
    private ShipmentTrackingAdapter shipmentTrackingAdapter;

    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipment_tracking);

        mContext = ShipmentTrackingActivity.this;
        mCommonutils = new CommonUtils(mContext);

        mRelLayout = findViewById(R.id.drawer);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);

        imageButton.setVisibility(View.VISIBLE);
        imageCart.setVisibility(View.GONE);
        mProgressBar = findViewById(R.id.main_progress);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerview = findViewById(R.id.recycle_view);

        toolbar(IntentConstants.SHIPMENTRACK);
        mUSerID = GlobalConstants.GetSharedValues.getSpUserId();
        setInstance();

    }

    private void setInstance() {
        mCommonutils.showProgressDialog();
        mCallAPI();
    }

    private void mCallAPI() {
        callService(mUSerID, currentPage, GlobalConstants.RECORD_NO);
    }

    private void callService(String userId, int currentPage, String recordNo) {
        OrderStatusServiceManager.getOrderStatusServiceManager(userId, GlobalConstants.NULL_DATA, GlobalConstants.NULL_DATA,
                Integer.toString(currentPage), recordNo, mContext, this);
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }

    /*API response*/
    @Override
    public void onSuccess(Object successObject) {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        OrderStatusJsonResponse orderStatusJsonResponse = (OrderStatusJsonResponse) successObject;
        if (orderStatusJsonResponse != null) {
            switch (orderStatusJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    bindRecyclerView(orderStatusJsonResponse);
                    break;
                case IntentConstants.FAILURE_URL:
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.NO_RECORDS);
                    } else {
                        mSnackBar(GlobalConstants.NO_DATA);
                    }
                    break;
                default:
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.SERVER_ERR);
                    } else {
                        mCallSnackBarInternet(GlobalConstants.SERVER_ERROR_BAR);
                    }
                    break;

            }

        } else {
            if (currentPage == 1) {
                noRecords(mContext, GlobalConstants.SERVER_ERR);
            } else {
                mCallSnackBarInternet(GlobalConstants.SERVER_ERROR_BAR);
            }
        }

    }


    @Override
    public void onFailure(Object failureObject) {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        } else {
            mCallSnackBarInternet(GlobalConstants.SERVER_ERROR_BAR);
        }
    }

    @Override
    public void onError() {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
        } else {
            mCallSnackBarInternet(GlobalConstants.INTERNET_ERROR);
        }
    }

    private void bindRecyclerView(OrderStatusJsonResponse orderStatusJsonResponse) {
        ArrayList<ShippingList> arrayList = new ArrayList(orderStatusJsonResponse.getData().getShippingList());
        if (!arrayList.isEmpty()) {
            mTotalPage = arrayList.get(0).getTotalPage();
            if (currentPage == 1) {
                mRecyclerview.setHasFixedSize(true);
                verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                mRecyclerview.setLayoutManager(verticalLayout);
                shipmentTrackingAdapter = new ShipmentTrackingAdapter(mContext, arrayList);
                mRecyclerview.setAdapter(shipmentTrackingAdapter);
            } else {
                shipmentTrackingAdapter.shipmentTrackingPagination(arrayList);
            }
            shipmentTrackingAdapter.notifyDataSetChanged();

            mRecyclerview.addOnScrollListener(new ScrollListener(verticalLayout) {
                @Override
                protected void loadMoreItems() {
                    isLoading = true;
                    currentPage += 1;
                    mRecyclerview.removeOnScrollListener(this);
                    mProgressBar.setVisibility(View.VISIBLE);
                    loadNextPage();
                }

                @Override
                public int getTotalPageCount() {
                    return mTotalPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            });
        } else {
            if (currentPage == 1)
                noRecords(mContext, GlobalConstants.NO_RECORDS);
        }
    }

    private void loadNextPage() {
        isLoading = false;
        if (currentPage <= mTotalPage) {
            callService(mUSerID, currentPage, GlobalConstants.RECORD_NO);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /*Snack Bar*/
    private void mCallSnackBarInternet(String checkError) {

        Snackbar snackbar = Snackbar
                .make(mRelLayout, checkError, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mCallAPI();
                });
        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /*Internet Error*/
    private void mSnackBar(String error) {
        Snackbar snackbar = Snackbar.make(mRelLayout, error, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            setInstance();
        }
    }
}
