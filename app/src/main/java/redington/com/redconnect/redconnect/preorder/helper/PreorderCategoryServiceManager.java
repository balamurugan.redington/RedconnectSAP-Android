package redington.com.redconnect.redconnect.preorder.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreorderCategoryServiceManager {

    private PreorderCategoryServiceManager() {
        throw new IllegalStateException(IntentConstants.PREORDER);
    }

    public static void getPreorderCatServiceCall(String category, String brand, String userID, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getPreorderCatList(category, brand, userID).enqueue(new Callback<PreorderCategoryJsonResponse>() {
                @Override
                public void onResponse(Call<PreorderCategoryJsonResponse> call, Response<PreorderCategoryJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<PreorderCategoryJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }


}
