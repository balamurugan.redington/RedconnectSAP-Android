package redington.com.redconnect.redconnect.capitalfloat.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.capitalfloat.helper.CapitalFloatRegisterManager;
import redington.com.redconnect.redconnect.capitalfloat.helper.JbaRegisterManager;
import redington.com.redconnect.redconnect.capitalfloat.model.FloatRegisterResponse;
import redington.com.redconnect.redconnect.capitalfloat.model.RegisterJbaResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.util.CommonUtils;

public class CapitalFloatRegistration extends BaseActivity implements NewListener {

    private EditText mJbaID;
    private EditText mJbaName;
    private EditText mJbaEmail;
    private EditText mJbaMobileNumber;
    private EditText mJbaAddress;
    private EditText mJbaCity;
    private EditText mJbaPinCode;
    private Context mContext;
    private CommonUtils commonUtils;
    private String apiString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captital_float_registration);

        mContext = CapitalFloatRegistration.this;
        commonUtils = new CommonUtils(mContext);

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        toolbar(this.getString(R.string.signup));

        intiViews();
    }

    private void intiViews() {

        mJbaID = findViewById(R.id.jba_id);
        mJbaName = findViewById(R.id.jba_name);
        mJbaEmail = findViewById(R.id.jba_email);
        mJbaMobileNumber = findViewById(R.id.jba_mobile_number);
        mJbaAddress = findViewById(R.id.jba_address);
        mJbaCity = findViewById(R.id.jba_city);
        mJbaPinCode = findViewById(R.id.jba_pincode);

        mJbaID.setPadding(15, 0, 0, 0);
        mJbaName.setPadding(15, 0, 0, 0);
        mJbaEmail.setPadding(15, 0, 0, 0);
        mJbaMobileNumber.setPadding(15, 0, 0, 0);
        mJbaAddress.setPadding(15, 10, 0, 0);
        mJbaCity.setPadding(15, 0, 0, 0);
        mJbaPinCode.setPadding(15, 0, 0, 0);

        mJbaID.setText(GlobalConstants.GetSharedValues.getSpUserId());
        mJbaEmail.setText(GlobalConstants.GetSharedValues.getSpMail());

        mJbaMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not in use
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mJbaMobileNumber.getText().toString().matches("^0")) {
                    mJbaMobileNumber.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not in use
            }
        });


    }


    public void menuBack(View v) {
        setMenuBack(v.getContext());
    }

    public void capitalFloatRegistration(View v) {

        if (mJbaID.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_jba_id));
            mJbaID.setFocusableInTouchMode(true);
            mJbaID.requestFocus();
        } else if (mJbaName.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_name));
            mJbaName.setFocusableInTouchMode(true);
            mJbaName.requestFocus();
        } else if (mJbaEmail.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_email));
            mJbaEmail.setFocusableInTouchMode(true);
            mJbaEmail.requestFocus();
        } else if (mJbaMobileNumber.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_mobile));
            mJbaMobileNumber.setFocusableInTouchMode(true);
            mJbaMobileNumber.requestFocus();
        } else if (mJbaAddress.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_address));
            mJbaAddress.setFocusableInTouchMode(true);
            mJbaAddress.requestFocus();
        } else if (mJbaCity.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, "Enter City");
            mJbaCity.setFocusableInTouchMode(true);
            mJbaCity.requestFocus();
        } else if (mJbaPinCode.getText().toString().trim().isEmpty()) {
            BaseActivity.shortToast(mContext, this.getString(R.string.enter_pincode));
            mJbaPinCode.setFocusableInTouchMode(true);
            mJbaPinCode.requestFocus();
        } else if (mJbaMobileNumber.length() < 10 || mJbaMobileNumber.length() > 11) {
            BaseActivity.shortToast(mContext, this.getString(R.string.ph_num_digit));
            mJbaMobileNumber.setFocusableInTouchMode(true);
            mJbaMobileNumber.requestFocus();
        } else if (!mJbaEmail.getText().toString().contains("@") && !mJbaEmail.getText().toString().contains(".")) {
            BaseActivity.shortToast(mContext, this.getString(R.string.jba_valid_mail));
            mJbaEmail.setFocusableInTouchMode(true);
            mJbaEmail.requestFocus();
        } else if (mJbaPinCode.getText().length() < 6) {
            BaseActivity.shortToast(mContext, this.getString(R.string.pin_code_digit));
            mJbaPinCode.setFocusableInTouchMode(true);
            mJbaPinCode.requestFocus();
        } else {
            successValues(v.getContext());
        }

    }

    private void successValues(Context context) {
        logd(context.getClass().getSimpleName());

        ArrayList<String> registerList = new ArrayList<>();
        registerList.add(mJbaID.getText().toString());
        registerList.add(mJbaName.getText().toString());
        registerList.add(mJbaEmail.getText().toString());
        registerList.add(mJbaMobileNumber.getText().toString());
        registerList.add(mJbaAddress.getText().toString());
        registerList.add(mJbaCity.getText().toString());
        registerList.add(mJbaPinCode.getText().toString());

        commonUtils.showProgressDialog();
        apiString = IntentConstants.FLOAT_REGISTER;
        CapitalFloatRegisterManager.registerCapitalFloat(registerList, mContext, this);
    }


    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        if (apiString.equals(IntentConstants.FLOAT_REGISTER)) {
            FloatRegisterResponse floatRegisterResponse = (FloatRegisterResponse) successObject;
            if (floatRegisterResponse != null) {
                switch (floatRegisterResponse.getStatusCode()) {
                    case IntentConstants.SUCCESS_URL:
                        String appId = floatRegisterResponse.getData().getAppId();
                        jbaRegister(appId);
                        break;
                    case IntentConstants.FAILURE_URL:
                        shortToast(mContext, floatRegisterResponse.getStatusMessage());
                        break;
                    default:
                        shortToast(mContext, GlobalConstants.SERVER_ERROR);
                        break;
                }
            } else {
                shortToast(mContext, GlobalConstants.SERVER_ERROR);
            }

        } else if (apiString.equals(IntentConstants.JBA_REGISTER)) {
            RegisterJbaResponse registerJbaResponse = (RegisterJbaResponse) successObject;
            regisReponse(registerJbaResponse);
        }
    }


    @Override
    public void mFailureObject(Object failureObject) {
        logd(String.valueOf(failureObject));
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        logd(IntentConstants.CAPITAL_FLOAT);
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    /*Jba Registration*/
    private void jbaRegister(String appId) {
        apiString = IntentConstants.JBA_REGISTER;
        ArrayList<String> registerJba = new ArrayList<>();
        registerJba.add(mJbaID.getText().toString());
        registerJba.add(mJbaEmail.getText().toString());
        registerJba.add(mJbaMobileNumber.getText().toString());
        registerJba.add(mJbaName.getText().toString());
        registerJba.add(appId);
        registerJba.add(mJbaAddress.getText().toString());
        registerJba.add(mJbaCity.getText().toString());
        registerJba.add(mJbaPinCode.getText().toString());

        commonUtils.showProgressDialog();
        JbaRegisterManager.registerJbaCall(registerJba, mContext, this);

    }

    private void regisReponse(RegisterJbaResponse registerJbaResponse) {
        if (registerJbaResponse != null) {
            switch (registerJbaResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    shortToast(mContext, GlobalConstants.CAPITAL_FLOAT_INSERT);
                    finish();
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, registerJbaResponse.getStatusMessage());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }


}
