package redington.com.redconnect.redconnect.settings.dialog;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import redington.com.redconnect.R;

public class CareBottomSheet extends BottomSheetDialogFragment {
    public static CareBottomSheet getInstance() {

        return new CareBottomSheet();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.customer_care, viewGroup, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources resources = getResources();

        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert getView() != null;
            View parent = (View) getView().getParent();
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) parent.getLayoutParams();
            layoutParams.setMargins(
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left), // 64dp
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left),
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_right), // 64dp
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left)
            );
            parent.setLayoutParams(layoutParams);
        }
    }

}
