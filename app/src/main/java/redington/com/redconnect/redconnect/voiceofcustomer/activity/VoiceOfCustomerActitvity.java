package redington.com.redconnect.redconnect.voiceofcustomer.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.voiceofcustomer.helper.VoiceRecordServiceManager;
import redington.com.redconnect.redconnect.voiceofcustomer.model.VoiceRecordJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class VoiceOfCustomerActitvity extends BaseActivity implements View.OnClickListener, UIListener {

    private Context mContext;
    private String userId;
    private CommonUtils commonUtils;
    private ProgressBar mProgressBar;
    private ImageView mImageViewRecord;
    private ImageView mImageViewStart;
    private ImageView mImageViewRecordStop;
    private ImageView mImageViewPause;
    private ImageView progressBarCircleBg;
    private TextView mTextViewTime;
    private TextView endTime;
    private MediaRecorder mediaRecorder;
    private CountDownTimer countDownTimer;
    private String voiceStoragePath;
    private long totalRecordTime = 120000L;
    private double recordProgressCount = 0;
    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private Button voiceSend;
    private RelativeLayout relativePlay;
    private String b64Str;
    private String bMail;
    private String bMobileNo;
    private String bDiscrip;
    private Dialog mDialog;
    private String mRemarksTxt;
    private Boolean checkRecording = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme1);
        setContentView(R.layout.activity_customervoice);

        userId = GlobalConstants.GetSharedValues.getSpUserId();
        mContext = VoiceOfCustomerActitvity.this;
        commonUtils = new CommonUtils(mContext);
        checkIsPermission(mContext, IntentConstants.MY_PERMISSIONS_REQUEST_VOICE_FOLDERCREATE);

        ImageView imageBack = findViewById(R.id.imageBack);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageBack.setColorFilter(Color.WHITE);
        imageButton.setColorFilter(Color.WHITE);

    }

    private void beginMedia() {
        checkRecording = false;
        voiceStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File audioVoice = new File(voiceStoragePath + File.separator + GlobalConstants.VOICE_FOLDER);
        if (!audioVoice.exists()) {
            audioVoice.mkdir();
        }
        String myFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
        File file = new File(myFilePath);
        if (file.exists()) {
            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(myFilePath);

                textCount();

            } catch (IOException e) {
                logd(e.getMessage());
            }
            relativePlay.setVisibility(View.VISIBLE);
        } else {
            relativePlay.setVisibility(View.GONE);
        }
        mImageViewRecord.setVisibility(View.VISIBLE);
        mImageViewRecordStop.setVisibility(View.GONE);

        textCount();
        voiceStoragePath = voiceStoragePath + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
    }

    private void initViews() {
        toolbar(IntentConstants.CUSTOMERVOICE);
        setInstance();
    }

    private void setInstance() {
        voiceSend = findViewById(R.id.voiceSend);
        relativePlay = findViewById(R.id.relativePlay);
        mProgressBar = findViewById(R.id.progressBarCircle);
        progressBarCircleBg = findViewById(R.id.progressBarCircleBg);
        mImageViewRecord = findViewById(R.id.imageViewRecord);
        mImageViewRecordStop = findViewById(R.id.imageViewRecordStop);
        mImageViewStart = findViewById(R.id.imageViewStart);
        mImageViewPause = findViewById(R.id.imageViewStop);
        mTextViewTime = findViewById(R.id.textViewTime);
        endTime = findViewById(R.id.endTime);
        seekBar = findViewById(R.id.seek_bar);
        String recordhms = "00:00";
        endTime.setText(recordhms);
        seekBar.setEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekBar.setProgressBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.two)));
        }

        initListeners();
    }

    private void initListeners() {
        mImageViewRecord.setOnClickListener(this);
        mImageViewRecordStop.setOnClickListener(this);
        mImageViewStart.setOnClickListener(this);
        mImageViewPause.setOnClickListener(this);
        voiceSend.setOnClickListener(this);
    }

    public void menuBack(View view) {
        checkPlaying();

        if (checkRecording) {
            alertDelete(false);
        } else {

            setMenuBack(view.getContext());
        }
    }


    public void rightNavigation(View v) {

        checkPlaying();
        if (checkRecording) {
            alertDelete(true);
        } else {
            setRightNavigation(v.getContext());
        }


    }

    /*On Click Listeners*/
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.imageViewRecord) {
            checkIsPermission(mContext, IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICE);

        } else if (i == R.id.imageViewRecordStop) {

            saveRecordedFile(mContext);


        } else if (i == R.id.imageViewStart) {
            if (!checkRecording) {
                checkPlaying();
                checkIsPermission(mContext, IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICEPLAY);
            }

        } else if (i == R.id.imageViewStop) {
            stopAudioPlay();

        } else if (i == R.id.voiceSend) {
            if (!checkRecording) {
                callAPI();
            } else {
                shortToast(mContext, GlobalConstants.RECORDING_TOAST);
            }

        }
    }

    private void checkPlaying() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            stopAudioPlay();
        }
    }

    private void callAPI() {

        String myFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
        File file = new File(myFilePath);
        if (file.exists()) {
            checkPlaying();
            showDescription(mContext);
        } else {
            shortToast(mContext, GlobalConstants.RECORD_AGAIN);
        }
    }

    private void recordSound() {
        if (mediaRecorder == null) {
            initializeMediaRecord();
            startAudioRecording();
            progressBarCircleBg.setScaleX(0);
            progressBarCircleBg.setScaleY(0);
            mProgressBar.setMax((int) totalRecordTime);
            countDownTimer = new CountDownTimer(totalRecordTime, 1) {
                public void onTick(long millisUntilFinished) {
                    @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                    recordProgressCount = 0;
                    double div = (double) millisUntilFinished / (double) totalRecordTime;
                    recordProgressCount = 100 - (div * 100);
                    float imgscale = (float) recordProgressCount;
                    progressBarCircleBg.setScaleX(imgscale / 100f);
                    progressBarCircleBg.setScaleY(imgscale / 100f);

                    mProgressBar.setProgress((int) (totalRecordTime - millisUntilFinished));
                    mTextViewTime.setText(hms);
                }

                public void onFinish() {

                    textCount();
                    stopAudioRecording(true);

                }
            }.start();
        }

    }

    /*Start Recording*/
    private void startAudioRecording() {
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();

            mImageViewRecord.setVisibility(View.GONE);
            mImageViewRecordStop.setVisibility(View.VISIBLE);
            mImageViewStart.setVisibility(View.GONE);
            mImageViewPause.setVisibility(View.GONE);
            mImageViewStart.setEnabled(false);
            mImageViewPause.setEnabled(false);
            endTime.setText("02:00");
            checkRecording = true;
            relativePlay.setVisibility(View.GONE);

        } catch (IOException e) {
            logd(e.getMessage());
        }

    }

    /*Stop Recording*/
    private void stopAudioRecording(Boolean stopRecord) {
        countDownTimer.cancel();
        seekBar.setProgress(0);
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            if (stopRecord) {
                mediaRecorder.release();
                relativePlay.setVisibility(View.VISIBLE);
            } else {
                relativePlay.setVisibility(View.GONE);
                mProgressBar.setProgress(0);
                mTextViewTime.setText("02:00");
                mImageViewRecord.setVisibility(View.VISIBLE);
                mImageViewRecordStop.setVisibility(View.GONE);
                String myFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
                File file = new File(myFilePath);
                boolean delete = file.delete();
                if (delete) {
                    logd("File Delete");
                }
            }
            mediaRecorder = null;
            checkRecording = false;

        }

    }

    private void textCount() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(voiceStoragePath);
            mediaPlayer.prepare();
            long millisUntilFinished = 1000 + mediaPlayer.getDuration();
            long endTimer = 120000 - millisUntilFinished;

            mTextViewTime.setText(getTextTimer(endTimer));
            endTime.setText(getTextTimer(millisUntilFinished));
        } catch (IOException e) {
            logd(e.getMessage());
        }

    }

    private void seekBarValidation(final long totCount) {
        seekBar.setMax((int) totCount);
        countDownTimer = new CountDownTimer(totCount, 1) {
            public void onTick(long millisUntilFinished) {
                seekBar.setProgress((int) (totCount - millisUntilFinished));
            }

            public void onFinish() {
                textCount();
                stopAudioPlay();
                seekBar.setProgress(0);
            }
        }.start();
    }

    private String getTextTimer(long millisUntilFinished) {
        @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
        return hms;
    }

    private void checkPlayIsOn() {
        playLastStoredAudioMusic();
        mediaPlayerPlaying();

    }

    private void playLastStoredAudioMusic() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(voiceStoragePath);
        } catch (IOException e) {
            logd(e.getMessage());
        }
        try {
            mediaPlayer.prepare();
            long duration = mediaPlayer.getDuration();

            textCount();
            long mil = mediaPlayer.getDuration();
            long count = TimeUnit.MILLISECONDS.toSeconds(mil) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mil));
            logd(String.valueOf(count));
            seekBarValidation(duration);
            mImageViewRecord.setEnabled(false);
            mImageViewRecordStop.setEnabled(false);
            mImageViewStart.setVisibility(View.GONE);
            mImageViewPause.setVisibility(View.VISIBLE);

        } catch (IOException e) {
            logd(e.getMessage());
        }
        mediaPlayer.start();

    }

    private void mediaPlayerPlaying() {
        if (!mediaPlayer.isPlaying()) {
            stopAudioPlay();
        }
    }

    /*Stop Audio Play */
    private void stopAudioPlay() {
        mImageViewRecord.setVisibility(View.VISIBLE);
        mImageViewRecordStop.setVisibility(View.GONE);
        mImageViewStart.setVisibility(View.GONE);
        mImageViewPause.setVisibility(View.GONE);

        if (mediaPlayer != null) {
            countDownTimer.cancel();
            seekBar.setProgress(0);
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

            textCount();
            mImageViewRecord.setEnabled(true);
            mImageViewRecordStop.setEnabled(true);

            mImageViewPause.setVisibility(View.GONE);
        }
    }

    /*Initialize Media Recorder*/
    private void initializeMediaRecord() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setAudioSamplingRate(44100);
        mediaRecorder.setAudioEncodingBitRate(128);
        mediaRecorder.setOutputFile(voiceStoragePath);
    }

    /*Check Permission to Record a Voice and get Recorded voice*/
    private void checkIsPermission(final Context context, int permissionCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.RECORD_AUDIO) ||
                        ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.RECORD_AUDIO,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            permissionCode);
                    logd("Permission granted");
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.RECORD_AUDIO,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            permissionCode);
                }
            } else {
                switch (permissionCode) {
                    case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICE:
                        recordSound();
                        break;
                    case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICEPLAY:
                        checkPlayIsOn();
                        break;
                    case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICESEND:
                        convertB64(mRemarksTxt);
                        mDialog.dismiss();
                        break;
                    case IntentConstants.MY_PERMISSIONS_REQUEST_VOICE_FOLDERCREATE:
                        initViews();
                        beginMedia();
                        break;
                    default:
                        // No data
                        break;
                }
            }
        } else {
            switch (permissionCode) {
                case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICE:
                    recordSound();
                    break;
                case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICEPLAY:
                    checkPlayIsOn();
                    break;
                case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICESEND:
                    convertB64(mRemarksTxt);
                    mDialog.dismiss();
                    break;
                case IntentConstants.MY_PERMISSIONS_REQUEST_VOICE_FOLDERCREATE:
                    initViews();
                    beginMedia();
                    break;
                default:
                    // No data
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recordSound();
                } else {
                    shortToast(getApplicationContext(), GlobalConstants.SOUND_ERROR);
                }
                break;
            case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICEPLAY:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPlayIsOn();
                } else {
                    shortToast(getApplicationContext(), GlobalConstants.SOUND_PLAY_ERROR);
                }
                break;
            case IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICESEND:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    convertB64(mRemarksTxt);
                    mDialog.dismiss();
                } else {
                    shortToast(getApplicationContext(), GlobalConstants.SOUND_PLAY_ERROR);
                }
                break;
            case IntentConstants.MY_PERMISSIONS_REQUEST_VOICE_FOLDERCREATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initViews();
                    beginMedia();
                } else {
                    shortToast(getApplicationContext(), GlobalConstants.SOUND_ENABLE_ERROR);
                    finish();
                }
                break;

            default:
                //No data
                break;
        }
    }

    /*Convert Base 64 String */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void convertB64(String descrip) {
        String myFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            try (FileInputStream fis = new FileInputStream(new File(myFilePath))) {
                byte[] buf = new byte[1024];
                int n;
                while ((n = fis.read(buf)) != -1) {
                    bos.write(buf, 0, n);
                }
            } finally {
                logd("File Input Stream Error..!");
            }
            b64Str = "";
            b64Str = Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
            bMail = GlobalConstants.GetSharedValues.getSpMail();
            bMobileNo = GlobalConstants.GetSharedValues.getSpMobileNo();
            bDiscrip = descrip;
            if (bDiscrip.isEmpty()) {
                shortToast(mContext, GlobalConstants.DESC_TOAST);
            } else {
                voiceCall(userId,
                        GlobalConstants.VOICE_FORMAT,
                        bMail,
                        bMobileNo,
                        bDiscrip,
                        b64Str);
            }

        } catch (FileNotFoundException e) {
            shortToast(mContext, GlobalConstants.RECORD_AGAIN);
            logd(e.getMessage());
        } catch (IOException e) {
            logd(e.getMessage());
        }
    }


    /*>>>>>>  API Call  <<<<<<<*/
    private void voiceCall(String userId, String media, String mail, String mobileNo, String discrip, String base64) {
        commonUtils.showProgressDialog();
        VoiceRecordServiceManager.getVoiceRecordServiceManager(
                userId, media, mail, mobileNo, discrip, base64,
                VoiceOfCustomerActitvity.this, this);
    }

    /*API response*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        VoiceRecordJsonResponse voiceRecordJsonResponse = (VoiceRecordJsonResponse) successObject;
        if (voiceRecordJsonResponse != null) {
            switch (voiceRecordJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String myFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + GlobalConstants.RECORDING_FILE_SOUCRE;
                    File file = new File(myFilePath);
                    Boolean chk = file.delete();
                    if (chk) {
                        shortToast(mContext, GlobalConstants.REQUEST_SENT);
                        LaunchIntentManager.routeToActivity(this, DashboardActivity.class);
                    } else {
                        shortToast(mContext, GlobalConstants.TRY_AGAIN);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, voiceRecordJsonResponse.getDescription());
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    @Override
    public void onBackPressed() {
        checkPlaying();
        if (checkRecording) {
            alertDelete(false);
        } else {
            finish();
            overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.slide_down);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        checkPlaying();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            voiceCall(userId,
                    GlobalConstants.VOICE_FORMAT,
                    bMail,
                    bMobileNo,
                    bDiscrip,
                    b64Str);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }

    /*Get Description by showing popup*/
    @SuppressLint("NewApi")
    private void showDescription(Context context) {
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_remarks);
        mDialog.setCanceledOnTouchOutside(false);

        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);

        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        final EditText mRemarks = mDialog.findViewById(R.id.ed_remarks);
        final TextView tRemarks = mDialog.findViewById(R.id.dialog_remarks);
        Button btnRemarks = mDialog.findViewById(R.id.btn_remarks);

        btnRemarks.setText(this.getString(R.string.ok));
        tRemarks.setText(GlobalConstants.DIALOG_DESCRIPTION);

        btnRemarks.setOnClickListener(v -> {
            mRemarksTxt = mRemarks.getText().toString().trim();
            checkIsPermission(VoiceOfCustomerActitvity.this, IntentConstants.MY_PERMISSIONS_REQUEST_ACCESS_VOICESEND);
        });
        mDialog.show();
    }

    /*Want to Interrupt a Recording*/
    @SuppressLint("NewApi")
    private void alertDelete(final Boolean check) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_popup_new);
        dialog.setCanceledOnTouchOutside(false);

        ImageView alertImage = dialog.findViewById(R.id.alertImage);
        TextView alertTitle = dialog.findViewById(R.id.titleText);
        TextView alertContent = dialog.findViewById(R.id.textContent);
        TextView textSuccess = dialog.findViewById(R.id.text_success);
        TextView textFailure = dialog.findViewById(R.id.text_failure);

        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.dialog_failure));
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            textFailure.setBackground(gradientDrawable);
        }

        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        alertContent.setText(GlobalConstants.RECORD_TEXT);

        textSuccess.setOnClickListener(v -> {
            if (check) {
                stopAudioRecording(false);
                setRightNavigation(mContext);
            } else {
                mediaRecorder.release();
                finish();
                overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.slide_down);
            }

            dialog.dismiss();
        });

        textFailure.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    /*Save Recorded File Popup*/
    private void saveRecordedFile(Context mContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(GlobalConstants.VOICE_SAVED_NAME)
                .setPositiveButton(GlobalConstants.Y, (dialogInterface, i) -> {
                    stopAudioRecording(true);
                    mImageViewRecord.setVisibility(View.VISIBLE);
                    mImageViewRecordStop.setVisibility(View.GONE);
                    mImageViewStart.setVisibility(View.VISIBLE);
                    mImageViewPause.setVisibility(View.GONE);
                    mImageViewStart.setEnabled(true);
                    mImageViewPause.setEnabled(true);
                    textCount();
                    relativePlay.setVisibility(View.VISIBLE);
                })
                .setNegativeButton(GlobalConstants.N, (dialog, which) -> {
                    stopAudioRecording(false);
                    dialog.cancel();
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

}
