package redington.com.redconnect.redconnect.preorder.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetailZoomer;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsImage;
import redington.com.redconnect.util.LaunchIntentManager;


public class ProductViewPagerAdapter extends PagerAdapter {


    private final List<PreorderItemDetailsImage> itemDetailsImages;
    private final LayoutInflater inflater;
    private final Context mContext;
    private Boolean imgChk = false;
    private CommonMethod commonMethod;


    public ProductViewPagerAdapter(Context context, List<PreorderItemDetailsImage> mImages) {
        this.mContext = context;
        this.itemDetailsImages = mImages;
        inflater = LayoutInflater.from(context);
        imgChk = false;
        commonMethod = new CommonMethod(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return itemDetailsImages.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        final View imageLayout = inflater.inflate(R.layout.view_pager_detail, view, false);

        assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.img_slide);


        if (!itemDetailsImages.get(position).getImageURL().equals("")) {
            imgChk = true;
        }


        Glide.with(mContext)
                .load(itemDetailsImages.get(position).getImageURL())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .into(imageView);


        view.addView(imageLayout, 0);

        imageView.setOnClickListener(view1 -> {
            if (commonMethod.listAccess() && imgChk) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(GlobalConstants.PRODUCT_IMAGE_URL, (Serializable) itemDetailsImages);
                LaunchIntentManager.routeToActivityStackBundle(mContext, ProductDetailZoomer.class, bundle);

            }
        });

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //No data
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}

