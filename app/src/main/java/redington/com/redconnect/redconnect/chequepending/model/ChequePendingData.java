package redington.com.redconnect.redconnect.chequepending.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class ChequePendingData implements Serializable {

    @SerializedName("BusinessCode")
    private String mBusinessCode;
    @SerializedName("BusinessDESC")
    private String mBusinessDESC;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("DueDate")
    private String mDueDate;
    @SerializedName("InvoiceDate")
    private String mInvoiceDate;
    @SerializedName("InvoiceNumber")
    private String mInvoiceNumber;
    @SerializedName("InvoiceValue")
    private String mInvoiceValue;
    @SerializedName("OverDueDays")
    private String mOverDueDays;
    @SerializedName("PendingDays")
    private String mPendingDays;
    @SerializedName("BalanceDueAmount")
    private String mBalanceDueAmount;
    @SerializedName("Page")
    private String mPageNum;
    @SerializedName("TotalPage")
    private String mTotalPage;

    public String getBusinessCode() {
        return mBusinessCode;
    }


    public String getBusinessDESC() {
        return mBusinessDESC;
    }


    public String getCustomerCode() {
        return mCustomerCode;
    }


    public String getDueDate() {
        return mDueDate;
    }


    public String getInvoiceDate() {
        return mInvoiceDate;
    }


    public String getInvoiceNumber() {
        return mInvoiceNumber;
    }


    public Double getInvoiceValue() {
        return CommonMethod.getValidDouble(mInvoiceValue);
    }

    public Double getOverDueDays() {
        return CommonMethod.getValidDouble(mOverDueDays);
    }

    public Double getPendingDays() {
        return CommonMethod.getValidDouble(mPendingDays);
    }

    public String getmBalanceDueAmount() {
        return mBalanceDueAmount;
    }

    public String getmPageNum() {
        return mPageNum;
    }

    public int getmTotalPage() {
        return CommonMethod.getValidInt(mTotalPage);
    }
}
