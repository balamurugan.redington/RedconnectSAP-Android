
package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OrderPlacedResponse implements Serializable {

    @SerializedName("Data")
    private OrderData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public static OrderPlacedResponse fromString(String decryptedOrderString) {
        return new Gson().fromJson(decryptedOrderString, OrderPlacedResponse.class);
    }

    public OrderData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
