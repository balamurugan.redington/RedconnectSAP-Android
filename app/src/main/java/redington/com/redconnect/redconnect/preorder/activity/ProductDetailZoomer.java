package redington.com.redconnect.redconnect.preorder.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.preorder.adapter.ZoomImageAdapter;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsImage;
import redington.com.redconnect.util.TouchImageView;

public class ProductDetailZoomer extends BaseActivity {

    private Context mContext;
    private TouchImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_zommer);

        mContext = ProductDetailZoomer.this;
        img = new TouchImageView(mContext);
        Bundle recieve = getIntent().getExtras();
        if (recieve != null) {
            ArrayList<PreorderItemDetailsImage> imageZoomerList = (ArrayList<PreorderItemDetailsImage>) recieve.getSerializable(GlobalConstants.PRODUCT_IMAGE_URL);
            RecyclerView listImage = findViewById(R.id.listImage);
            listImage.setHasFixedSize(true);
            listImage.setNestedScrollingEnabled(false);
            LinearLayoutManager verticalLayout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            listImage.setLayoutManager(verticalLayout);
            ZoomImageAdapter zoomImageAdapter = new ZoomImageAdapter(this, imageZoomerList);
            listImage.setAdapter(zoomImageAdapter);
            FrameLayout imageFrame = findViewById(R.id.imageFrame);

            img.setImageResource(R.drawable.redinton_image);
            assert imageZoomerList != null;
            Glide.with(mContext)
                    .load(imageZoomerList.get(0).getImageURL())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .fitCenter()
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .into(img);
            img.setMaxZoom(4.0f);
            imageFrame.addView(img);
        }


    }

    public void setImage(String imgScr) {
        img.setImageResource(R.drawable.redinton_image);
        Glide.with(mContext)
                .load(imgScr)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .fitCenter()
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .into(img);
        img.setMaxZoom(4.0f);

    }


}
