
package redington.com.redconnect.redconnect.mycart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;

public class ShowBreakup implements Serializable {

    @SerializedName("CGST")
    private String mCGST;
    @SerializedName("CGSTCODE")
    private String mCGSTCODE;
    @SerializedName("CGSTVALUE")
    private String mCGSTVALUE;
    @SerializedName("CITY")
    private String mCITY;
    @SerializedName("IGST")
    private String mIGST;
    @SerializedName("IGSTCODE")
    private String mIGSTCODE;
    @SerializedName("IGSTVALUE")
    private String mIGSTVALUE;
    @SerializedName("ITEMMINORCODE")
    private String mITEMMINORCODE;
    @SerializedName("SGST")
    private String mSGST;
    @SerializedName("SGSTVALUE")
    private String mSGSTVALUE;
    @SerializedName("STATE")
    private String mSTATE;
    @SerializedName("STATECODE")
    private String mSTATECODE;
    @SerializedName("STOCKROOM")
    private String mSTOCKROOM;
    @SerializedName("STOCKROOMDESC")
    private String mSTOCKROOMDESC;
    @SerializedName("STOCKROOMSITE")
    private String mSTOCKROOMSITE;
    @SerializedName("TATINDAYS")
    private String mTATINDAYS;
    @SerializedName("TOTAL")
    private String mTOTAL;
    @SerializedName("TOTALPRICE")
    private String mTOTALPRICE;
    @SerializedName("UTILISEDQTY")
    private String mUTILISEDQTY;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDescription")
    private String mItemDescription;

    @SerializedName("UnitPrice")
    private String mUnitPrice;

    @SerializedName("CDCDiscount")
    private String mCDCDiscount;
    @SerializedName("CDCDiscountPRICE")
    private String mCDCDiscountPRICE;
    @SerializedName("CDCFinalValue")
    private String mCDCFinalValue;

    public String getCGST() {
        return mCGST;
    }


    public String getCGSTCODE() {
        return mCGSTCODE;
    }


    public String getCGSTVALUE() {
        return CommonMethod.getValidDoubleString(mCGSTVALUE);
    }


    public String getCITY() {
        return mCITY;
    }


    public String getIGST() {
        return mIGST;
    }


    public String getIGSTCODE() {
        return mIGSTCODE;
    }


    public String getIGSTVALUE() {
        return CommonMethod.getValidDoubleString(mIGSTVALUE);
    }


    public String getITEMMINORCODE() {
        return mITEMMINORCODE;
    }


    public String getSGST() {
        return mSGST;
    }


    public String getSGSTVALUE() {
        return CommonMethod.getValidDoubleString(mSGSTVALUE);
    }


    public String getSTATE() {
        return mSTATE;
    }


    public String getSTATECODE() {
        return mSTATECODE;
    }


    public String getSTOCKROOM() {
        return mSTOCKROOM;
    }


    public String getSTOCKROOMDESC() {
        return mSTOCKROOMDESC;
    }


    public String getSTOCKROOMSITE() {
        return mSTOCKROOMSITE;
    }


    public String getTATINDAYS() {
        return mTATINDAYS;
    }


    public Double getTOTAL() {
        return CommonMethod.getValidDouble(mTOTAL);
    }


    public Double getTOTALPRICE() {
        return CommonMethod.getValidDouble(mTOTALPRICE);
    }


    public Integer getUTILISEDQTY() {
        return CommonMethod.getValidInt(mUTILISEDQTY);
    }


    public String getmItemCode() {
        return mItemCode;
    }


    public String getmItemDescription() {
        return mItemDescription;
    }


    public String getmUnitPrice() {
        return CommonMethod.getValidDoubleString(mUnitPrice);
    }

    public String getCDCDiscount() {
        return CommonMethod.getValidDoubleString(mCDCDiscount);
    }

    public String getCDCDiscountPRICE() {
        return mCDCDiscountPRICE;
    }

    public String getCDCFinalValue() {
        return mCDCFinalValue;
    }
}
