package redington.com.redconnect.redconnect.ratingfeedback.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.ratingfeedback.model.RatingFeedbackJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RaitngFeedBackServiceManager {
    private RaitngFeedBackServiceManager() {
        throw new IllegalStateException(IntentConstants.PURCHASED);
    }

    public static void getRaitngFeedBackServiceManager(String itemCode, String feedback, String review, String userid,
                                                       final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject header = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(header).getRaitngFeedBack(getRequestBody(itemCode, feedback, review, userid)).enqueue(new Callback<RatingFeedbackJsonResponse>() {
                @Override
                public void onResponse(Call<RatingFeedbackJsonResponse> call, Response<RatingFeedbackJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<RatingFeedbackJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(String itemCode, String feedback,
                                              String review, String userid) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.ITEM_CODE, itemCode);
            jsonValues.put(JsonKeyConstants.JPRO_FEEDBACK, feedback);
            jsonValues.put(JsonKeyConstants.JPRO_REVIEW, review);
            jsonValues.put(JsonKeyConstants.JPRO_USERID, userid);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
