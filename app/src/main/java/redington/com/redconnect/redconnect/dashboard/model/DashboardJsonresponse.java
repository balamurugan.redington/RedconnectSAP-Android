
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DashboardJsonresponse implements Serializable {

    @SerializedName("Data")
    private DashboardData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public static DashboardJsonresponse fromString(String data) {
        return new Gson().fromJson(data, DashboardJsonresponse.class);
    }

    public String toString() {
        return new Gson().toJson(this);
    }

    public DashboardData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
