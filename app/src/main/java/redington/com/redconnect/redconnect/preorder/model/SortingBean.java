package redington.com.redconnect.redconnect.preorder.model;


public class SortingBean {
    private String text;

    public SortingBean(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
