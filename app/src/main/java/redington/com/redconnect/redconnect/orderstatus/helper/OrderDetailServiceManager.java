package redington.com.redconnect.redconnect.orderstatus.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailServiceManager {

    private OrderDetailServiceManager() {
        throw new IllegalStateException(IntentConstants.ORDERHISTORY);
    }

    public static void getOrderDetailServiceManager(String userID, String fromDate, String toDate, String ordNo,
                                                    final Context context,
                                                    final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getOrderDetailService(getRequestBody(userID, fromDate, toDate, ordNo)).enqueue(new Callback<OrderDetailJsonResponse>() {
                @Override
                public void onResponse(Call<OrderDetailJsonResponse> call, Response<OrderDetailJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<OrderDetailJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(String userID, String fromDate, String toDate, String ordNo) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CART_USERID, userID);
            jsonValues.put(JsonKeyConstants.FROM_DATE, fromDate);
            jsonValues.put(JsonKeyConstants.TO_DATE, toDate);
            jsonValues.put(JsonKeyConstants.ORDER_NUMBER, ordNo);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}


