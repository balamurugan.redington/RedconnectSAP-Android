package redington.com.redconnect.redconnect.payment.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.helper.InvoiceCommitmentServiceManager;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.payment.helper.ReOrderManager;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.redconnect.payment.model.OrderPlacedResponse;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.paymentcall.model.PaymentPojo;
import redington.com.redconnect.redconnect.preorder.helper.OrderPlacedManager;
import redington.com.redconnect.redconnect.preorder.model.EncryptedInvoiceResponse;
import redington.com.redconnect.redconnect.preorder.model.InvoiceCommitJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

public class Ordergeneration extends BaseActivity implements UIListener, NewListener {

    private TextView mTextTitle;
    private TextView mTextStatus;
    private TextView textErrorMsg;
    private TextView textOrdernum;
    private TextView textButton;
    private ImageView successFailureImage;
    private LinearLayout contentLayout;
    private ProgressBar progressBar;
    private Context mContext;
    private String decryptedOrderString;
    private LinearLayout layoutColor;
    private TextView textprogressbar;
    private String cameString = " ";
    private String transactionID;
    private String status;
    private String userId;
    private TextView textTransactionId;
    private String decryptedString;
    private String orderGenerate = "";
    private boolean bundleRetry;     /*Kamesh*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_order_generate);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mContext = Ordergeneration.this;

        userId = GlobalConstants.GetSharedValues.getSpUserId();

        initViews();
        /*Kamesh*/
        bundleRetry = true;
        bundleValidation();   /*Method create*/
        /*Kamesh*/
    }

    private void bundleValidation() {  /*Created seperate Method for ReCall Ordergenation & Invoice Commit API (When Network Service Problem)*/
        ArrayList<String> serviceCharges;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String cameFrom = bundle.getString(GlobalConstants.FROM);
            assert cameFrom != null;
            switch (cameFrom) {
                case IntentConstants.PLACEORDER:
                    serviceCharges = (ArrayList<String>) bundle.getSerializable(GlobalConstants.ORDER_BANK_INFO);
                    ArrayList<String> locationData = (ArrayList<String>) bundle.getSerializable(GlobalConstants.LOCATION);
                    ArrayList<CartData> cartArray = (ArrayList<CartData>) bundle.getSerializable(GlobalConstants.CART_ARRAY);
                    ArrayList<PaymentPojo> pojoList = (ArrayList<PaymentPojo>) bundle.getSerializable(GlobalConstants.POJO_ARRAY);
                    ArrayList<SubPromoCode> promocodes = (ArrayList<SubPromoCode>) bundle.getSerializable(GlobalConstants.PROMOCODE_ARRAY);
                    transactionID = bundle.getString(GlobalConstants.TXN_ID);
                    status = bundle.getString(GlobalConstants.STATUS);

                    cameString = IntentConstants.PAYMENTOPTIONS;

                    orderGenerateApi(locationData, cartArray, pojoList, promocodes, serviceCharges);

                    break;
                case IntentConstants.PAYMENTDUE:
                    status = bundle.getString(GlobalConstants.STATUS);
                    transactionID = bundle.getString(GlobalConstants.TXN_ID);
                    serviceCharges = (ArrayList<String>) bundle.getSerializable(GlobalConstants.ORDER_BANK_INFO);
                    ArrayList<String> paymentDueModels = (ArrayList<String>) bundle.getSerializable(IntentConstants.PAYMENTDUE);

                    cameString = IntentConstants.PAYMENTCOMMIT;

                    invoiceApiCall(paymentDueModels, serviceCharges);
                    break;
                case IntentConstants.CHEQUEPENDING:
                    status = bundle.getString(GlobalConstants.STATUS);
                    transactionID = bundle.getString(GlobalConstants.TXN_ID);
                    serviceCharges = (ArrayList<String>) bundle.getSerializable(GlobalConstants.ORDER_BANK_INFO);
                    ArrayList<String> chequesPendingModels = (ArrayList<String>) bundle.getSerializable(IntentConstants.CHEQUEPENDING);

                    cameString = IntentConstants.PAYMENTCOMMIT;

                    invoiceApiCall(chequesPendingModels, serviceCharges);
                    break;
                default:
                    serviceCharges = (ArrayList<String>) bundle.getSerializable(GlobalConstants.ORDER_BANK_INFO);
                    locationData = (ArrayList<String>) bundle.getSerializable(GlobalConstants.LOCATION);
                    cartArray = (ArrayList<CartData>) bundle.getSerializable(GlobalConstants.CART_ARRAY);
                    pojoList = (ArrayList<PaymentPojo>) bundle.getSerializable(GlobalConstants.POJO_ARRAY);
                    promocodes = (ArrayList<SubPromoCode>) bundle.getSerializable(GlobalConstants.PROMOCODE_ARRAY);
                    transactionID = bundle.getString(GlobalConstants.TXN_ID);
                    cameString = " ";

                    orderGenerateApi(locationData, cartArray, pojoList, promocodes, serviceCharges);

                    break;
            }

        }
    }

    private void invoiceApiCall(ArrayList<String> stringList, ArrayList<String> serviceCharge) {
        InvoiceCommitmentServiceManager.getInvoiceCommitmentServiceCall(stringList,serviceCharge, mContext, this);
    }

    private void orderGenerateApi(ArrayList<String> locationData, ArrayList<CartData> cartArrayList,
                                  ArrayList<PaymentPojo> pojoArrayList, ArrayList<SubPromoCode> subPromoCodes,
                                  ArrayList<String> serviceCharge) {
        new OrderPlacedManager(locationData, cartArrayList, pojoArrayList, subPromoCodes,serviceCharge, mContext, this);
    }

    private void initViews() {
        mTextTitle = findViewById(R.id.text_title);
        mTextStatus = findViewById(R.id.status);
        successFailureImage = findViewById(R.id.image_success_failure);
        textErrorMsg = findViewById(R.id.text_error_msg);
        textOrdernum = findViewById(R.id.text_ordernum);
        textButton = findViewById(R.id.text_button);
        contentLayout = findViewById(R.id.content_layout);
        progressBar = findViewById(R.id.progressBar);

        textTransactionId = findViewById(R.id.text_transactionID);
        textTransactionId.setVisibility(View.GONE);
        layoutColor = findViewById(R.id.layoutColor);
        textprogressbar = findViewById(R.id.text_process_order);
        textprogressbar.setSelected(true);

        textButton.setOnClickListener(v -> {
            String buttonCheck = textButton.getText().toString();
            if (buttonCheck.equals(mContext.getString(R.string.retry))) {
                orderGenerate = GlobalConstants.RETRY;
                retryClick();
            } else {
                LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
            }
        });


    }


    private void retryClick() {
        if (CommonUtils.isNetworkAvailable(mContext)) {
            contentLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            textprogressbar.setVisibility(View.VISIBLE);
            RetryServiceHelper.retryCall(userId, mContext, this);
        } else {
            shortToast(mContext, GlobalConstants.INTERNET_CHECK);
        }

    }


    @Override
    public void onSuccess(Object successObject) {
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        switch (cameString) {
            case IntentConstants.PAYMENTOPTIONS:
                ordePlacedData(successObject);
                break;
            case IntentConstants.PAYMENTCOMMIT:
                invoiceResponse(successObject);
                break;
            default:
                ordePlacedData(successObject);
                break;
        }
    }


    @Override
    public void onFailure(Object failureObject) {
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        showError();
    }

    @Override
    public void onError() {
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        switch (cameString) {
            case IntentConstants.PAYMENTOPTIONS:
                clearSharedValues();
                setCartPaymentValues(GlobalConstants.NULL_DATA, GlobalConstants.SPACE);
                break;
            case IntentConstants.PAYMENTCOMMIT:
                setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                break;
            case GlobalConstants.SPACE:
                setValues(GlobalConstants.SPACE, GlobalConstants.INTERNET_CHECK);
                break;
            default:
                //Not in use
                break;
        }
    }

    /*Order Response*/
    public void ordePlacedData(Object successObject) {
        EncryptedOrderResponse encryptedOrderResponse = (EncryptedOrderResponse) successObject;
        if (encryptedOrderResponse != null) {
            try {
                decryptedOrderString = mDecypt(encryptedOrderResponse.getData());
            } catch (Exception e) {
                BaseActivity.logd(e.getMessage());
            }
            ArrayList<OrderPlacedResponse> orderPlacedJsonResponse = new ArrayList<>();

            OrderPlacedResponse jsonResponse = OrderPlacedResponse.fromString(decryptedOrderString);
            orderPlacedJsonResponse.add(jsonResponse);

            switch (orderPlacedJsonResponse.get(0).getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    bundleRetry = false;  /*Kamesh*/
                    clearSharedValues();
                    String mSkipOrderNum = orderPlacedJsonResponse.get(0).getData().getUID();
                    if (cameString.equals(IntentConstants.PAYMENTOPTIONS)) {
                        setCartPaymentValues(mSkipOrderNum, orderPlacedJsonResponse.get(0).getData().getmReferenceNumber());
                    } else {
                        setValues(mSkipOrderNum, GlobalConstants.SUCCESS);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    showError();
                    break;
                default:
                    showError();
                    break;
            }
        } else {
            showError();
        }
    }

    /*Invoice ReOrder response*/
    private void invoiceReOrderResponse(Object successObject) {
        EncryptedOrderResponse encryptedInvoiceResponse = (EncryptedOrderResponse) successObject;
        if (encryptedInvoiceResponse != null) {

            try {
                decryptedString = BaseActivity.mDecypt(encryptedInvoiceResponse.getData());
            } catch (Exception e) {
                logd(e.getMessage());
            }

            ArrayList<InvoiceCommitJsonResponse> invoiceCommitJsonResponse = new ArrayList<>();
            InvoiceCommitJsonResponse jsonResponse = InvoiceCommitJsonResponse.fromString(decryptedString);
            invoiceCommitJsonResponse.add(jsonResponse);

            switch (invoiceCommitJsonResponse.get(0).getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    bundleRetry = false;  /*Kamesh*/
                    String referenceNumber = invoiceCommitJsonResponse.get(0).getData().getReferenceNumber();
                    setInvoiceValues(referenceNumber, GlobalConstants.SPACE);
                    break;
                case IntentConstants.FAILURE_URL:
                    setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                    break;
                default:
                    setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                    break;
            }
        } else {
            setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
        }
    }

    /*Invoice response*/
    private void invoiceResponse(Object successObject) {
        EncryptedInvoiceResponse encryptedInvoiceResponse = (EncryptedInvoiceResponse) successObject;
        if (encryptedInvoiceResponse != null) {

            try {
                decryptedString = BaseActivity.mDecypt(encryptedInvoiceResponse.getData());
            } catch (Exception e) {
                logd(e.getMessage());
            }

            ArrayList<InvoiceCommitJsonResponse> invoiceCommitJsonResponse = new ArrayList<>();
            InvoiceCommitJsonResponse jsonResponse = InvoiceCommitJsonResponse.fromString(decryptedString);
            invoiceCommitJsonResponse.add(jsonResponse);

            switch (invoiceCommitJsonResponse.get(0).getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    bundleRetry = false;  /*Kamesh*/
                    String referenceNumber = invoiceCommitJsonResponse.get(0).getData().getReferenceNumber();
                    setInvoiceValues(referenceNumber, GlobalConstants.SPACE);
                    break;
                case IntentConstants.FAILURE_URL:
                    setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                    break;
                default:
                    setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                    break;
            }
        } else {
            setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
        }
    }


    private void showError() {
        switch (cameString) {
            case IntentConstants.PAYMENTOPTIONS:
                clearSharedValues();
                setCartPaymentValues(GlobalConstants.NULL_DATA, GlobalConstants.SPACE);
                break;
            case IntentConstants.PAYMENTCOMMIT:
                setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                break;
            case GlobalConstants.SPACE:
                setValues(GlobalConstants.SPACE, GlobalConstants.FAILURE);
                break;
            default:
                //Not in use
                break;
        }
    }

    /*Set Cart and Spot Values Via OTP Screen*/
    @SuppressLint("SetTextI18n")
    private void setValues(String mSkipOrderNum, String status) {
        contentLayout.setVisibility(View.VISIBLE);

        if (!mSkipOrderNum.equals("") && status.equals(GlobalConstants.SUCCESS)) {
            layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_color));
            mTextStatus.setText(status);
            successFailureImage.setImageResource(R.drawable.success);
            textErrorMsg.setText(GlobalConstants.ORDER_PLACED);
            textOrdernum.setText(mContext.getString(R.string.orderNum) + mContext.getString(R.string.colon)
                    + GlobalConstants.SPACE + mSkipOrderNum);
            textButton.setTextColor(ContextCompat.getColor(mContext, R.color.green_color));
            textButton.setText(R.string.back_to_home);
        } else if (status.equals(GlobalConstants.FAILURE)) {
            mTextTitle.setText(R.string.oops);
            layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.failure_color));
            mTextStatus.setText(GlobalConstants.ORDER_FAILED);
            textErrorMsg.setText(GlobalConstants.SERVER_ERROR);
            textButton.setTextColor(ContextCompat.getColor(mContext, R.color.failure_color));
            successFailureImage.setImageResource(R.drawable.failure);
            textOrdernum.setText(GlobalConstants.TRY_AGAIN);
            textButton.setText(R.string.retry);
        } else {
            mTextTitle.setText(R.string.oops);
            mTextStatus.setText(GlobalConstants.PENDING);
            layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.pending_color));
            textErrorMsg.setText(GlobalConstants.INTERNET_CHECK);
            successFailureImage.setImageResource(R.drawable.pending);
            successFailureImage.setColorFilter(ContextCompat.getColor(mContext, R.color.pending_color));
            textButton.setText(R.string.retry);
            textOrdernum.setText(GlobalConstants.TRY_AGAIN);
            textButton.setTextColor(ContextCompat.getColor(mContext, R.color.new_black_light));
        }
    }

    /*Clear Shared Values*/
    private void clearSharedValues() {
        SharedPreferences preferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        preferences.edit().clear().apply();
        setBillingSeq(IntentConstants.CARTDELSEQ);
        setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
        setCashDiscount(GlobalConstants.NO);
    }

    @Override
    public void onBackPressed() {
        //Not in use
    }

    /*Cart and SPOT values Via Payment gateway*/
    @SuppressLint("SetTextI18n")
    private void setCartPaymentValues(String orderNum, String referenceNumber) {

        contentLayout.setVisibility(View.VISIBLE);
        textTransactionId.setVisibility(View.VISIBLE);
        textTransactionId.setText(mContext.getString(R.string.transactioID) + GlobalConstants.SPACE +
                mContext.getString(R.string.colon) + GlobalConstants.SPACE + transactionID);
        if (!orderNum.equals("") && status.equals(GlobalConstants.SUCCESS_VALIDATION)) {
            successValues(orderNum);
        } else if (orderNum.equals("") && status.equals(GlobalConstants.FAILURE_VALIDATION) && !referenceNumber.equals("")) {
            failureValues(referenceNumber);
        } else if (orderNum.equals("") && status.equals(GlobalConstants.CANCEL_VALIDATION) && !referenceNumber.equals("")) {
            cancelValues(referenceNumber);
        } else {
            errorValues();
        }
    }

    /*Set Invoice Values*/
    @SuppressLint("SetTextI18n")
    private void setInvoiceValues(String referenceNum, String values) {
        contentLayout.setVisibility(View.VISIBLE);
        textTransactionId.setVisibility(View.VISIBLE);
        textTransactionId.setText(mContext.getString(R.string.transactioID) + GlobalConstants.SPACE +
                mContext.getString(R.string.colon) + GlobalConstants.SPACE + transactionID);
        if (status.equals(GlobalConstants.SUCCESS_VALIDATION) && values.equals(GlobalConstants.SPACE)) {
            successValues(referenceNum);
        } else if (status.equals(GlobalConstants.FAILURE_VALIDATION) && values.equals(GlobalConstants.SPACE)) {
            failureValues(referenceNum);
        } else if (status.equals(GlobalConstants.CANCEL_VALIDATION) && values.equals(GlobalConstants.SPACE)) {
            cancelValues(referenceNum);
        } else {
            errorValues();
        }
    }

    @SuppressLint("SetTextI18n")
    private void successValues(String referenceNum) {
        setCartUpdate(0);
        mTextTitle.setText(GlobalConstants.SPACE);
        layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_color));
        mTextStatus.setText(IntentConstants.TXN_SUCCESS);
        successFailureImage.setImageResource(R.drawable.success);

        if (cameString.equals(IntentConstants.PAYMENTCOMMIT)) {
            textErrorMsg.setText(GlobalConstants.INVOICE_PLACED);
            textOrdernum.setText(mContext.getString(R.string.refNum) + GlobalConstants.SPACE + mContext.getString(R.string.colon)
                    + GlobalConstants.SPACE + referenceNum);
        } else {
            textOrdernum.setText(mContext.getString(R.string.orderNum) + GlobalConstants.SPACE + mContext.getString(R.string.colon)
                    + GlobalConstants.SPACE + referenceNum);
            textErrorMsg.setText(GlobalConstants.ORDER_PLACED);
        }

        textButton.setTextColor(ContextCompat.getColor(mContext, R.color.green_color));
        textButton.setText(R.string.back_to_home);
    }

    @SuppressLint("SetTextI18n")
    private void failureValues(String referenceNum) {
        mTextTitle.setText(R.string.oops);
        layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.failure_color));
        mTextStatus.setText(IntentConstants.TXN_FAILUE);
        textErrorMsg.setText(GlobalConstants.ORDER_NOT_PLACED_DATA);
        textButton.setTextColor(ContextCompat.getColor(mContext, R.color.failure_color));
        successFailureImage.setImageResource(R.drawable.failure);
        textOrdernum.setText(mContext.getString(R.string.refNum) + GlobalConstants.SPACE +
                mContext.getString(R.string.colon) + GlobalConstants.SPACE + referenceNum);
        textButton.setText(R.string.back_to_home);
    }

    @SuppressLint("SetTextI18n")
    private void cancelValues(String referenceNum) {
        mTextTitle.setText(R.string.oops);
        layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.md_purple_300));
        mTextStatus.setText(IntentConstants.TXN_CANCEL);
        textErrorMsg.setText(GlobalConstants.TRANSACTION_CANCELLED);
        textButton.setTextColor(ContextCompat.getColor(mContext, R.color.md_purple_300));
        successFailureImage.setImageResource(R.drawable.failure);
        successFailureImage.setColorFilter(ContextCompat.getColor(mContext, R.color.md_purple_300));
        textOrdernum.setText(mContext.getString(R.string.refNum) + GlobalConstants.SPACE +
                mContext.getString(R.string.colon) + GlobalConstants.SPACE + referenceNum);
        textButton.setText(R.string.back_to_home);
    }

    @SuppressLint("SetTextI18n")
    private void errorValues() {
        mTextTitle.setText(R.string.oops);
        switch (status) {
            case GlobalConstants.SUCCESS_VALIDATION:
                mTextStatus.setText(mContext.getString(R.string.payment_Status) + GlobalConstants.SPACE + mContext.getString(R.string.colon)
                        + GlobalConstants.SPACE + GlobalConstants.SUCCESS);
                break;
            case GlobalConstants.FAILURE_VALIDATION:
                mTextStatus.setText(mContext.getString(R.string.payment_Status) + GlobalConstants.SPACE + mContext.getString(R.string.colon)
                        + GlobalConstants.SPACE + GlobalConstants.FAILURE);
                break;
            default:
                mTextStatus.setText(mContext.getString(R.string.payment_Status) + GlobalConstants.SPACE + mContext.getString(R.string.colon)
                        + GlobalConstants.SPACE + GlobalConstants.CANCEL);
                break;
        }
        successFailureImage.setImageResource(R.drawable.pending);
        layoutColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.md_deep_orange_400));
        textErrorMsg.setText(mContext.getString(R.string.alertText));
        textButton.setText(R.string.retry);
        textOrdernum.setText(GlobalConstants.BACKGROUND_PROCESS);
        textButton.setTextColor(ContextCompat.getColor(mContext, R.color.md_deep_orange_400));
    }


    /*Retry Response*/
    @Override
    public void mSuccessObject(Object successObject) {
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        if (orderGenerate.equals(GlobalConstants.RETRY)) {
            switch (cameString) {
                case IntentConstants.PAYMENTOPTIONS:
                    retryResponse(successObject);
                    break;
                case IntentConstants.PAYMENTCOMMIT:
                    retryInvoiceResponse(successObject);
                    break;
                default:
                    retryPayLaterResponse(successObject);
                    break;
            }
        } else if (orderGenerate.equals(GlobalConstants.RE_ORDER)) {
            switch (cameString) {
                case IntentConstants.PAYMENTOPTIONS:
                    ordePlacedData(successObject);
                    break;
                case IntentConstants.PAYMENTCOMMIT:
                    invoiceReOrderResponse(successObject);
                    break;
                default:
                    ordePlacedData(successObject);
                    break;
            }
        }
    }


    @Override
    public void mFailureObject(Object failureObject) {
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        switch (cameString) {
            case IntentConstants.PAYMENTOPTIONS:
                setCartPaymentValues(GlobalConstants.SPACE, GlobalConstants.SPACE);
                break;
            case IntentConstants.PAYMENTCOMMIT:
                setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                break;
            default:
                setValues(GlobalConstants.SPACE, GlobalConstants.FAILURE);
                break;
        }

    }

    @Override
    public void mError() {
        logd(GlobalConstants.RETRY);
        progressBar.setVisibility(View.GONE);
        textprogressbar.setVisibility(View.GONE);
        switch (cameString) {
            case IntentConstants.PAYMENTOPTIONS:
                setCartPaymentValues(GlobalConstants.SPACE, GlobalConstants.SPACE);
                break;
            case IntentConstants.PAYMENTCOMMIT:
                setInvoiceValues(GlobalConstants.SPACE, GlobalConstants.BACKGROUND_PROCESS);
                break;
            default:
                setValues(GlobalConstants.SPACE, GlobalConstants.INTERNET_CHECK);
                break;
        }

    }

    /*Invoice Response*/
    private void retryInvoiceResponse(Object successObject) {
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    successPart(retryResponse);
                    break;
                case IntentConstants.FAILURE_URL:
                    failurePart();
                    break;
                default:
                    failurePart();
                    break;
            }
        } else {
            failurePart();
        }
    }


    private void failurePart() {
        showPopup(GlobalConstants.CUST_CARE);
    }

    private void successPart(RetryResponse response) {
        /*Kamesh*/
        String lastReferenceNo = response.getData().getLstSucOrd().get(0).getReferenceNo();
        String lastTransactionNo = response.getData().getLstSucOrd().get(0).getTransactionID();
        String penidngTransactionNo = response.getData().getPendOrd().get(0).getTransactionID();
        String retryMode = response.getData().getMode();

        if (penidngTransactionNo.equals(transactionID) && retryMode.equals(GlobalConstants.INVOICE_COMMITMENT)) {
            reOrderApiCall(userId);
        } else if (!lastTransactionNo.isEmpty() && lastTransactionNo.equals(transactionID)
                && status.equals(GlobalConstants.SUCCESS) && retryMode.equals(GlobalConstants.INVOICE_COMMITMENT)) {
            showPopup(lastTransactionNo);
        } else if (!lastTransactionNo.isEmpty() && lastTransactionNo.equals(transactionID)
                && retryMode.equals(GlobalConstants.INVOICE_COMMITMENT)) {
            showPopup(lastReferenceNo);
        } else if (bundleRetry) {
            bundleRetry = false;
            bundleValidation();
            /*Kamesh*/
        } else {
            showPopup(GlobalConstants.CUST_CARE);
        }

    }

    /*Retry Order Generation Response*/
    private void retryResponse(Object successObject) {
        contentLayout.setVisibility(View.VISIBLE);
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*Kamesh*/
                    String webOrderNo = retryResponse.getData().getLstSucOrd().get(0).getWeborderNo();
                    String lastSuccessTransactionID = retryResponse.getData().getLstSucOrd().get(0).getTransactionID();
                    String pendingTransactionId = retryResponse.getData().getPendOrd().get(0).getTransactionID();
                    String retryMode = retryResponse.getData().getMode();

                    if (pendingTransactionId.equals(transactionID)) {
                        reOrderApiCall(userId);
                    } else if (!lastSuccessTransactionID.isEmpty() && lastSuccessTransactionID.equals(transactionID)
                            && status.equals(GlobalConstants.SUCCESS) && retryMode.equals(GlobalConstants.ORDER_GENERATION)) {
                        String orderNo = (!webOrderNo.isEmpty()) ? webOrderNo : lastSuccessTransactionID;
                        showPopup(orderNo);
                    } else if (!lastSuccessTransactionID.isEmpty() && lastSuccessTransactionID.equals(transactionID)
                            && retryMode.equals(GlobalConstants.ORDER_GENERATION)) {
                        showPopup(lastSuccessTransactionID);
                    } else if (bundleRetry) {
                        bundleRetry = false;
                        bundleValidation();
                    } else {
                        showPopup(GlobalConstants.CUST_CARE);
                    }
                    /*Kamesh*/
                    break;
                case IntentConstants.FAILURE_URL:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
                default:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
            }
        } else {
            showPopup(GlobalConstants.CUST_CARE);
        }
    }

    private void retryPayLaterResponse(Object successObject) {
        contentLayout.setVisibility(View.VISIBLE);
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*Kamesh*/
                    String lastReferenceNo = retryResponse.getData().getLstSucOrd().get(0).getReferenceNo();
                    String pendingReferenceNo = retryResponse.getData().getLstSucOrd().get(0).getReferenceNo();
                    String webOrderNumber = retryResponse.getData().getLstSucOrd().get(0).getWeborderNo();
                    String retryMode = retryResponse.getData().getMode();

                    if (pendingReferenceNo.equals(transactionID)) {
                        reOrderApiCall(userId);
                    } else if (!lastReferenceNo.isEmpty() && lastReferenceNo.equals(transactionID)
                            && retryMode.equals(GlobalConstants.ORDER_GENERATION)) {
                        String orderNo = (!webOrderNumber.isEmpty()) ? webOrderNumber : lastReferenceNo;
                        showPopup(orderNo);
                    } else if (bundleRetry) {
                        bundleRetry = false;
                        bundleValidation();
                    } else {
                        showPopup(GlobalConstants.CUST_CARE);
                    }
                    /*Kamesh*/
                    break;
                case IntentConstants.FAILURE_URL:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
                default:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
            }
        } else {
            showPopup(GlobalConstants.CUST_CARE);
        }
    }


    /*Re Order API Call*/
    private void reOrderApiCall(String userId) {
        progressBar.setVisibility(View.VISIBLE);
        textprogressbar.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
        orderGenerate = GlobalConstants.RE_ORDER;
        ReOrderManager.getReorderCall(userId, mContext, this);
    }


    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup(String responseString) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        alertContent.setText(responseString);
        alertContent.setTextSize(11f);
        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });


        mDialog.show();
    }


}
