package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class DashboardData implements Serializable {

    @SerializedName("Deals")
    private List<Deal> mDeals;
    @SerializedName("EDM")
    private List<EDM> mEDM;
    @SerializedName("RecentPurchase")
    private List<RecentPurchase> mRecentPurchase;
    @SerializedName("RecentView")
    private List<RecentView> mRecentView;
    @SerializedName("WishListEnquiry")
    private List<WishListEnquiry> mWishListEnquiry;
    @SerializedName("TopSelling")
    private List<TopSelling> mTopSelling;
    @SerializedName("FestivalOffer")
    private List<FestivalOffer> mFestivalOffer;
    @SerializedName("emagazine")
    private List<Emagazine> mEmagazine;


    public List<Deal> getDeals() {
        return mDeals;
    }


    public List<EDM> getEDM() {
        return mEDM;
    }


    public List<RecentPurchase> getRecentPurchase() {
        return mRecentPurchase;
    }


    public List<RecentView> getRecentView() {
        return mRecentView;
    }


    public List<WishListEnquiry> getWishListEnquiry() {
        return mWishListEnquiry;
    }


    public List<TopSelling> getmTopSelling() {
        return mTopSelling;
    }


    public List<FestivalOffer> getmFestivalOffer() {
        return mFestivalOffer;
    }

    public List<Emagazine> getEmagazine() {
        return mEmagazine;
    }

}
