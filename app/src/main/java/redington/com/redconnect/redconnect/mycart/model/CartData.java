
package redington.com.redconnect.redconnect.mycart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.common.methods.CommonMethod;

public class CartData implements Serializable {

    @SerializedName("Breakup")
    private List<ShowBreakup> mBreakup;
    @SerializedName("Images")
    private String mImages;
    @SerializedName("Item_Code")
    private String mItemCode;
    @SerializedName("Item_Description")
    private String mItemDescription;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Unit_Price")
    private String mUnitPrice;
    @SerializedName("User_ID")
    private String mUserID;
    @SerializedName("Vendor_Code")
    private String mVendorCode;
    @SerializedName("TotalValue")
    private String mTotalValue;
    @SerializedName("AvailableQuantity")
    private String mAvailableQuantity;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("CDCStatus")
    private String mCDCStatus;

    public List<ShowBreakup> getBreakup() {
        return mBreakup;
    }

    public void setBreakup(List<ShowBreakup> breakup) {
        mBreakup = breakup;
    }

    public String getImages() {
        return mImages;
    }

    public void setImages(String images) {
        mImages = images;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public void setItemCode(String itemCode) {
        mItemCode = itemCode;
    }

    public String getItemDescription() {
        return mItemDescription;
    }

    public void setItemDescription(String itemDescription) {
        mItemDescription = itemDescription;
    }

    public Integer getQuantity() {
        return CommonMethod.getValidInt(mQuantity);
    }

    public void setQuantity(Integer quantity) {
        mQuantity = String.valueOf(quantity);
    }

    public Double getUnitPrice() {
        return CommonMethod.getValidDouble(mUnitPrice);
    }

    public void setUnitPrice(Double unitPrice) {
        mUnitPrice = String.valueOf(unitPrice);
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public String getVendorCode() {
        return mVendorCode;
    }

    public void setVendorCode(String vendorCode) {
        mVendorCode = vendorCode;
    }

    public Double getmTotalValue() {
        return CommonMethod.getValidDouble(mTotalValue);
    }

    public void setmTotalValue(Double mTotalValue) {
        this.mTotalValue = String.valueOf(mTotalValue);
    }

    public Integer getmAvailableQuantity() {
        return CommonMethod.getValidInt(mAvailableQuantity);
    }

    public void setmAvailableQuantity(Integer mAvailableQuantity) {
        this.mAvailableQuantity = String.valueOf(mAvailableQuantity);
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getCDCStatus() {
        return mCDCStatus;
    }

    public void setCDCStatus(String mCDCStatus) {
        this.mCDCStatus = mCDCStatus;
    }
}
