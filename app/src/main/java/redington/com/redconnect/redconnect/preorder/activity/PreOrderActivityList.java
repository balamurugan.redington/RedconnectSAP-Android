package redington.com.redconnect.redconnect.preorder.activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.adapter.ScrollListener;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.preorder.adapter.PreorderListAdapter;
import redington.com.redconnect.redconnect.preorder.dialog.SortingCustomSheetDialog;
import redington.com.redconnect.redconnect.preorder.helper.PreorderItemListServiceManager;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListData;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListJsonResponse;
import redington.com.redconnect.redconnect.search.activity.SearchActivity;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.EmptyCartActivity;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class PreOrderActivityList extends BaseActivity implements View.OnClickListener, UIListener, CartListener, NewListener {

    /*Kamesh (Note) ->  Added NewListener for CheckingWebOrderEntry (Retry) API*/

    private ImageButton mList;
    private ImageButton mGrid;
    private boolean list = true;
    private PreorderListAdapter adapter;
    private String productAct = "";
    private String productCat = "";
    private String productBrand = "";
    private String mSearchString = "";
    private int sortNo = 0;
    private CommonUtils commonUtils;
    private ArrayList<PreorderItemListData> arrayList;

    private Context mContext;
    private LinearLayoutManager verticalLayout;
    private FrameLayout mProgressBar;
    private boolean isLoading = false;
    private int currentPage = 1;
    private RecyclerView recyclerView;
    private int mTotalPages = 0;
    private Bundle bundle;
    private CoordinatorLayout mLayout;
    private boolean cartCheck = false;
    private LinearLayout mCartLayout;

    private String mUserid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder_list);

        mContext = PreOrderActivityList.this;
        commonUtils = new CommonUtils(mContext);
        bundle = getIntent().getExtras();

        mLayout = findViewById(R.id.coordinatorLayout);
        productAct = bundle.getString(GlobalConstants.PRODUCT_ACTIVITY);
        mProgressBar = findViewById(R.id.main_progress);
        recyclerView = findViewById(R.id.preorder_recycler_view);
        mProgressBar.setVisibility(View.GONE);

        mUserid = GlobalConstants.GetSharedValues.getSpUserId();
        callAPI();
        setInstance();
    }

    private void callAPI() {

        if (productAct.equals(GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST) || productAct.equals(GlobalConstants.M_DASHBOARD)) {
            commonUtils.showProgressDialog();
            productCat = bundle.getString(GlobalConstants.PRODUCT_CATEGORY);
            productBrand = bundle.getString(GlobalConstants.PRODUCT_BRAND);

            toolbarTitle(productBrand);
            productListApiCall();

        } else if (productAct.equals(GlobalConstants.SEARCH_ACTIVITY_LIST)) {
            mSearchString = bundle.getString(GlobalConstants.SEARCH_TEXT);
            toolbarTitle(mSearchString);
            arrayList = (ArrayList<PreorderItemListData>) bundle.getSerializable(GlobalConstants.SEARCH_DATA);
            if (arrayList != null) {
                bindActivityList(productAct, arrayList, false);
            }
            if (currentPage == 1)
                getCartCount();
        }
    }

    /*Api Call*/
    private void productListApiCall() {
        ArrayList<String> preOrderList = new ArrayList<>();
        preOrderList.add(GlobalConstants.NULL_DATA);
        preOrderList.add(productCat);
        preOrderList.add(productBrand);
        preOrderList.add(GlobalConstants.NULL_DATA);
        preOrderList.add(GlobalConstants.RECORD_NO);
        preOrderList.add(String.valueOf(currentPage));
        preOrderList.add(String.valueOf(sortNo));
        preOrderList.add(mUserid);

        callProductList(preOrderList);
    }

    private void productSearchListCall(String searchString) {
        ArrayList<String> preOrderList = new ArrayList<>();
        preOrderList.add(searchString);
        preOrderList.add(productCat);
        preOrderList.add(productBrand);
        preOrderList.add(GlobalConstants.NULL_DATA);
        preOrderList.add(GlobalConstants.RECORD_NO);
        preOrderList.add(String.valueOf(currentPage));
        preOrderList.add(String.valueOf(sortNo));
        preOrderList.add(mUserid);

        callProductList(preOrderList);
    }

    private void setInstance() {
        initViews();
    }


    private void callProductList(ArrayList<String> preOrderList) {
        PreorderItemListServiceManager.getPreorderItemListServiceManager(preOrderList,
                mContext, this);
    }

    private void toolbarTitle(String value) {
        Toolbar toolbar = findViewById(R.id.toolbar_preorderList);
        TextView toolbarTitle = findViewById(R.id.main_toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(value);
    }

    @SuppressLint("NewApi")
    private void initViews() {
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        RelativeLayout imageSearch = findViewById(R.id.searchLayout);

        imageSearch.setOnClickListener(view -> {
            Bundle bundle2 = new Bundle();
            bundle2.putString(GlobalConstants.FROM, GlobalConstants.SEARCH);
            LaunchIntentManager.routeToActivityStackBundle(mContext, SearchActivity.class, bundle2);
        });

        ImageView mCartImage = findViewById(R.id.imageviewCart);
        mCartImage.setImageResource(R.mipmap.ic_cart);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mCartImage.setImageTintList(ContextCompat.getColorStateList(mContext, R.color.new_black));
        } else {
            mCartImage.setColorFilter(ContextCompat.getColor(mContext, R.color.new_black));
        }
        mCartLayout = findViewById(R.id.LL_cartText);
        mCartLayout.setVisibility(View.GONE);

        imageCart.setOnClickListener(v -> {
            callRetryAPI();   /*Kamesh*/
        });
        mList = findViewById(R.id.img_btn_list);
        mGrid = findViewById(R.id.img_btn_grid);
        ImageButton mSort = findViewById(R.id.img_btn_sort);
        mList.setOnClickListener(this);
        mGrid.setOnClickListener(this);
        mSort.setOnClickListener(this);
    }

    /*Kamesh*/
    private void callRetryAPI() {
        commonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(mUserid, mContext, this);
    }

    private void cartNavigation() {
        TextView txtCount = findViewById(R.id.txtCount);
        int cartSize = Integer.parseInt(txtCount.getText().toString());
        if (cartSize == 0) {
            LaunchIntentManager.routeToActivityStack(mContext, EmptyCartActivity.class);
        } else {
            LaunchIntentManager.routeToActivityStack(mContext, MyCartActivity.class);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }
    /*Kamesh*/


    /*Recycler View*/
    private void bindActivityList(String productAct, List<PreorderItemListData> arrayList, Boolean getGrid) {
        String totalPages = arrayList.get(0).getmTotalPage();
        mTotalPages = Integer.parseInt(totalPages);
        if (getGrid) {
            recyclerView.setHasFixedSize(true);
            if (list) {
                verticalLayout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            } else {
                verticalLayout = new GridLayoutManager(this, 2);
            }
            recyclerView.setLayoutManager(verticalLayout);
            if (productAct.equals(GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST)) {
                adapter.preOrderUpation(list);
                recyclerView.setAdapter(adapter);
            }

            if (productAct.equals(GlobalConstants.SEARCH_ACTIVITY_LIST) || productAct.equals(GlobalConstants.M_DASHBOARD)) {
                adapter.preOrderUpation(list);
                recyclerView.setAdapter(adapter);
            }

        } else {
            callCurrentPage(productAct, arrayList);

        }
        adapter.notifyDataSetChanged();
        recyclerView.addOnScrollListener(new ScrollListener(verticalLayout) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                recyclerView.removeOnScrollListener(this);
                mProgressBar.setVisibility(View.VISIBLE);
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return mTotalPages;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }

    /*If current Page is equal to 1*/
    private void callCurrentPage(String productAct, List<PreorderItemListData> arrayList) {
        if (currentPage == 1) {
            recyclerView.setHasFixedSize(true);
            if (list) {
                verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            } else {
                verticalLayout = new GridLayoutManager(mContext, 2);
            }

            recyclerView.setLayoutManager(verticalLayout);
            if (productAct.equals(GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST)) {
                adapter = new PreorderListAdapter(mContext, arrayList, list);
                recyclerView.setAdapter(adapter);
            }
            if (productAct.equals(GlobalConstants.SEARCH_ACTIVITY_LIST) || productAct.equals(GlobalConstants.M_DASHBOARD)) {
                adapter = new PreorderListAdapter(mContext, arrayList, list);
                recyclerView.setAdapter(adapter);
            }
        } else {
            if (productAct.equals(GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST)) {
                adapter.preOrderListPagination(arrayList, list);
            }

            if (productAct.equals(GlobalConstants.SEARCH_ACTIVITY_LIST) || productAct.equals(GlobalConstants.M_DASHBOARD)) {
                adapter.preOrderListPagination(arrayList, list);
            }
        }
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.img_btn_sort) {
            SortingCustomSheetDialog bottomSheetDialog = SortingCustomSheetDialog.getInstance();
            Bundle bundle1 = new Bundle();
            bundle1.putInt(GlobalConstants.SORT_NO, sortNo);
            bottomSheetDialog.setArguments(bundle1);
            bottomSheetDialog.show(getSupportFragmentManager(), GlobalConstants.CUSTOM_BOTTOM_SHEET);

        } else if (i == R.id.img_btn_list) {
            list = true;
            mGrid.setVisibility(View.VISIBLE);
            mList.setVisibility(View.GONE);
            bindActivityList(productAct, arrayList, true);

        } else if (i == R.id.img_btn_grid) {
            list = false;
            mGrid.setVisibility(View.GONE);
            mList.setVisibility(View.VISIBLE);
            bindActivityList(productAct, arrayList, true);

        }
    }

    public void reCallProductList(int sortNos) {
        sortNo = sortNos;
        currentPage = 1;
        commonUtils = new CommonUtils(mContext);
        commonUtils.showProgressDialog();
        switch (productAct) {
            case GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST:

                productListApiCall();
                break;
            case GlobalConstants.SEARCH_ACTIVITY_LIST:

                productSearchListCall(mSearchString);
                break;
            case GlobalConstants.M_DASHBOARD:
                productListApiCall();
                break;

            default:
                //Not in Use
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            if (cartCheck) {
                getCartCount();
                cartCheck = false;
            } else {
                callAPI();
            }
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
        getCartUpdate();
    }


    /*Response API*/
    @Override
    public void onSuccess(Object successObject) {
        PreorderItemListJsonResponse preItemList = (PreorderItemListJsonResponse) successObject;
        commonUtils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (preItemList != null) {
            preOrderListresponse(preItemList);
        } else {
            if (currentPage == 1) {
                noRecords(mContext, GlobalConstants.SERVER_ERR);
            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
            }
        }

    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        } else {
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
        }
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            LaunchIntentManager.routeToActivityStack(this, InternetErrorCheck.class);
        } else {
            callSnackBar(GlobalConstants.INTERNET_ERROR);
        }

    }

    /*Response*/
    private void preOrderListresponse(PreorderItemListJsonResponse preItemList) {
        switch (preItemList.getStatusCode()) {
            case IntentConstants.SUCCESS_URL:
                arrayList = new ArrayList(preItemList.getData());
                bindActivityList(productAct, arrayList, false);
                if (currentPage == 1)
                    getCartCount();
                break;
            case IntentConstants.FAILURE_URL:
                if (sortNo == 5) {
                    shortToast(mContext, IntentConstants.POPULAR);
                } else {
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.NO_RECORDS);
                    }
                }
                break;
            default:
                if (currentPage == 1) {
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                } else {
                    callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
                }
                break;
        }

    }

    /*SnackBar Server Error*/
    private void callSnackBar(String snackString) {
        Snackbar snackbar = Snackbar
                .make(mLayout, snackString, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    mProgressBar.setVisibility(View.VISIBLE);
                    switch (productAct) {
                        case GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST:
                            productListApiCall();
                            break;
                        case GlobalConstants.SEARCH_ACTIVITY_LIST:
                            productSearchListCall(mSearchString);
                            break;
                        case GlobalConstants.M_DASHBOARD:
                            productListApiCall();
                            break;

                        default:
                            // No data
                            break;
                    }

                });
        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }


    /*Get Cart Count*/
    private void getCartCount() {
        String userID = GlobalConstants.GetSharedValues.getSpUserId();
        ArrayList<String> cartArrayList = new ArrayList<>();
        cartArrayList.add(userID);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.MODE_SELECT);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        cartApiCall(cartArrayList);
    }

    private void cartApiCall(ArrayList<String> cartArrayList) {
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArrayList, mContext, this);
    }


    @SuppressLint({"NewApi", "SetTextI18n"})
    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();

        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*9-1-19*/
                    /*int getDataSize = cartJson.getData().size();*/
                    int getDataSize = cartJson.getData().get(0).getCart().size();
                    mCartLayout.setVisibility(View.VISIBLE);
                    TextView count = findViewById(R.id.txtCount);
                    count.setText(Integer.toString(getDataSize));
                    int currentAPIVersion = Build.VERSION.SDK_INT;
                    if (currentAPIVersion < Build.VERSION_CODES.M) {
                        GradientDrawable gradientDrawable = new GradientDrawable();
                        gradientDrawable.setShape(GradientDrawable.OVAL);
                        gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.white_100));
                        count.setBackground(gradientDrawable);
                    }
                    setCartUpdate(getDataSize);
                    break;
                case IntentConstants.FAILURE_URL:
                    setCartUpdate(0);
                    break;
                default:
                    mCartLayout.setVisibility(View.GONE);
                    callCartSnackBar();
                    break;
            }
        } else {
            mCartLayout.setVisibility(View.GONE);
            callCartSnackBar();
        }

    }


    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        mCartLayout.setVisibility(View.GONE);
        callCartSnackBar();
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        cartCheck = true;
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void loadNextPage() {
        isLoading = false;
        if (currentPage <= mTotalPages) {
            switch (productAct) {
                case GlobalConstants.PRODUCT_SUB_CATEGORY_ACTIVITY_LIST:
                    productListApiCall();
                    break;
                case GlobalConstants.SEARCH_ACTIVITY_LIST:
                    productSearchListCall(mSearchString);
                    break;
                case GlobalConstants.M_DASHBOARD:
                    productListApiCall();
                    break;
                default:
                    //Not in use
                    break;
            }
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /*Cart Snack Bar*/
    private void callCartSnackBar() {
        Snackbar snackbar = Snackbar
                .make(mLayout, GlobalConstants.SERVER_ERROR_BAR, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> getCartCount());
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

    /*Kamesh*/
    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        RetryResponse retryJsonResponse = (RetryResponse) successObject;
        if (retryJsonResponse != null) {
            switch (retryJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String pendingOrder = retryJsonResponse.getData().getPendOrd().get(0).getTransactionID();
                    String referenceOrder = retryJsonResponse.getData().getPendOrd().get(0).getReferenceNo();
                    if (pendingOrder.isEmpty() && referenceOrder.isEmpty()) {
                        cartNavigation();
                    } else {
                        shortToast(mContext, IntentConstants.CARTPAYMENT);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    cartNavigation();
                    break;
                default:
                    errorDialogPage();
                    break;
            }

        } else {
            errorDialogPage();
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        errorDialogPage();
    }

    @Override
    public void mError() {
        errorDialogPage();
    }

    private void errorDialogPage() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }
    /*Kamesh*/
}
