package redington.com.redconnect.redconnect.paymentdue.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentDueManager {

    private PaymentDueManager() {
        throw new IllegalStateException(IntentConstants.PAYMENTDUE);
    }

    public static void getPaymentDueServiceCall(String userId,
                                                String week,
                                                final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getPayment(userId, week).enqueue(new Callback<PaymentDueResponse>() {
                @Override
                public void onResponse(Call<PaymentDueResponse> call, Response<PaymentDueResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<PaymentDueResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }


}
