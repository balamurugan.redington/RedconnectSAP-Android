package redington.com.redconnect.redconnect.orderstatus.activity;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.adapter.ScrollListener;
import redington.com.redconnect.redconnect.orderstatus.adapter.OrderStatusAdapter;
import redington.com.redconnect.redconnect.orderstatus.helper.OrderStatusServiceManager;
import redington.com.redconnect.redconnect.orderstatus.model.OrderList;
import redington.com.redconnect.redconnect.orderstatus.model.OrderStatusJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class OrderStatusActivity extends BaseActivity implements UIListener, View.OnClickListener {

    private Context mContext;
    private int currentPage = 1;
    private int mTotalPage;
    private CommonUtils mCommonutils;
    private FrameLayout mProgressBar;
    private RelativeLayout mRelLayout;
    private String mUSerID;
    private String mFromDate;
    private String mToDate;
    private RecyclerView mRecyclerview;
    private LinearLayoutManager verticalLayout;
    private boolean isLoading = false;
    private OrderStatusAdapter orderStatusAdapter;
    private EditText fromDate;
    private EditText toDate;
    private ImageView mOk;
    private SimpleDateFormat format;
    private DatePickerDialog toDatePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        mContext = OrderStatusActivity.this;
        mCommonutils = new CommonUtils(mContext);

        hideKeyboard();

        mRelLayout = findViewById(R.id.drawer);
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);

        imageButton.setVisibility(View.VISIBLE);
        mImageCart.setVisibility(View.GONE);
        mProgressBar = findViewById(R.id.main_progress);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerview = findViewById(R.id.deliverytatus_recyclerview);

        toolbar(IntentConstants.ORDERHISTORY);
        mUSerID = GlobalConstants.GetSharedValues.getSpUserId();

        format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        fromDate = findViewById(R.id.frmDt);
        toDate = findViewById(R.id.toDt);
        mOk = findViewById(R.id.btn_submit);

        Date date = new Date();
        mToDate = dateFormat(date);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date result = cal.getTime();
        mFromDate = dateFormat(result);
        fromDate.setText(mFromDate);
        toDate.setText(mToDate);

        mOk.setOnClickListener(this);
        fromDate.setOnClickListener(this);
        toDate.setOnClickListener(this);

        mOk.setEnabled(false);
        mOk.setAlpha(0.7f);

        setInstance();


    }

    private void setInstance() {
        mCommonutils.showProgressDialog();
        currentPage = 1;
        mCallAPI();
    }

    private void mCallAPI() {
        callService(mUSerID, mFromDate, mToDate, currentPage, GlobalConstants.RECORD_NO);
    }

    private void callService(String userId, String fromDate, String toDate, int currentPage, String recordNo) {
        OrderStatusServiceManager.getOrderStatusServiceManager(userId, fromDate, toDate,
                Integer.toString(currentPage), recordNo, mContext, this);
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }

    @Override
    public void onSuccess(Object successObject) {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        OrderStatusJsonResponse orderStatusJsonResponse = (OrderStatusJsonResponse) successObject;
        if (orderStatusJsonResponse != null) {
            switch (orderStatusJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    bindRecyclerView(orderStatusJsonResponse);
                    break;
                case IntentConstants.FAILURE_URL:
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.NO_RECORDS);
                    } else {
                        mSnackBar(GlobalConstants.NO_DATA);
                    }
                    break;
                default:
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.SERVER_ERR);
                    } else {
                        mCallSnackBarInternetError(GlobalConstants.SERVER_ERROR_BAR);
                    }
                    break;
            }

        } else {
            if (currentPage == 1) {
                noRecords(mContext, GlobalConstants.SERVER_ERR);
            } else {
                mCallSnackBarInternetError(GlobalConstants.SERVER_ERROR_BAR);
            }
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        } else {
            mCallSnackBarInternetError(GlobalConstants.SERVER_ERROR_BAR);
        }
    }

    @Override
    public void onError() {
        mCommonutils.dismissProgressDialog();
        mProgressBar.setVisibility(View.GONE);
        if (currentPage == 1) {
            LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
        } else {
            mCallSnackBarInternetError(GlobalConstants.INTERNET_ERROR);
        }
    }


    private void bindRecyclerView(OrderStatusJsonResponse orderStatusJsonResponse) {
        ArrayList<OrderList> arrayList = new ArrayList(orderStatusJsonResponse.getData().getOrderList());
        if (!arrayList.isEmpty()) {
            mTotalPage = arrayList.get(0).getTotalPage();
            if (currentPage == 1) {
                mRecyclerview.setHasFixedSize(true);
                verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                mRecyclerview.setLayoutManager(verticalLayout);
                orderStatusAdapter = new OrderStatusAdapter(mContext, arrayList);
                mRecyclerview.setAdapter(orderStatusAdapter);
            } else {
                orderStatusAdapter.orderStatusPagination(arrayList);
            }
            orderStatusAdapter.notifyDataSetChanged();

            mRecyclerview.addOnScrollListener(new ScrollListener(verticalLayout) {
                @Override
                protected void loadMoreItems() {
                    isLoading = true;
                    currentPage += 1;
                    mRecyclerview.removeOnScrollListener(this);
                    mProgressBar.setVisibility(View.VISIBLE);
                    loadNextPage();
                }

                @Override
                public int getTotalPageCount() {
                    return mTotalPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            });
        } else {
            if (currentPage == 1)
                noRecords(mContext, GlobalConstants.NO_RECORDS);
        }
    }


    private void loadNextPage() {
        isLoading = false;
        if (currentPage <= mTotalPage) {
            callService(mUSerID, mFromDate, mToDate, currentPage, GlobalConstants.RECORD_NO);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /*Snack Bar*/
    private void mCallSnackBarInternetError(String checkError) {

        Snackbar snackbar = Snackbar
                .make(mRelLayout, checkError, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mCallAPI();
                });
        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /*Internet Error*/
    private void mSnackBar(String error) {
        Snackbar snackbar = Snackbar.make(mRelLayout, error, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.frmDt) {
            Date fDate = null;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            try {
                fDate = simpleDateFormat.parse(fromDate.getText().toString());

            } catch (ParseException e) {
                logd(e.getMessage());
            }

            Calendar apicalendar = Calendar.getInstance();
            Calendar newCalendar = Calendar.getInstance();
            apicalendar.setTime(fDate);
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, (view1, year, monthOfYear, dayOfMonth) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDate.setText(dateFormat(newDate.getTime()));
                toDate.setText("");

            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.updateDate(apicalendar.get(Calendar.YEAR), apicalendar.get(Calendar.MONTH), apicalendar.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

            datePickerDialog.show();

        } else if (i == R.id.toDt) {

            toDateValidation();


        } else if (i == R.id.btn_submit) {
            mFromDate = fromDate.getText().toString().trim();
            mToDate = toDate.getText().toString().trim();

            if (mFromDate.isEmpty()) {
                shortToast(mContext, GlobalConstants.FROM_DATE);
            } else if (mToDate.isEmpty()) {
                shortToast(mContext, GlobalConstants.TO_DATE);
            } else {
                setInstance();
                mOk.setEnabled(false);
                mOk.setAlpha(0.7f);

            }

        }
    }

    /*Select To Date*/
    private void toDateValidation() {
        String frmDate = fromDate.getText().toString().trim();
        if (frmDate.isEmpty()) {
            shortToast(mContext, GlobalConstants.FROM_DATE);
        } else {
            Date dateObj = null;
            Date dateObj1 = null;
            String s = toDate.getText().toString().trim();
            try {
                if (s.isEmpty()) {
                    dateObj = format.parse(fromDate.getText().toString());
                } else {
                    dateObj = format.parse(s);
                    dateObj1 = format.parse(fromDate.getText().toString());
                }
            } catch (ParseException e) {
                logd(e.getMessage());
            }

            setDateTimeField();

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(dateObj);
            if (!s.isEmpty()) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateObj1);
                toDatePickerDialog.updateDate(calendar2.get(Calendar.YEAR), calendar2.get(Calendar.MONTH), calendar2.get(Calendar.DATE));
                toDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            } else {
                toDatePickerDialog.getDatePicker().setMinDate(calendar2.getTimeInMillis());
            }

            toDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            toDatePickerDialog.show();
        }
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();

        toDatePickerDialog = new DatePickerDialog(mContext, (view, year, monthOfYear, dayOfMonth) -> {

            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            toDate.setText(dateFormat(newDate.getTime()));
            mOk.setEnabled(true);
            mOk.setAlpha(1.0f);
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            setInstance();
        }

    }
}
