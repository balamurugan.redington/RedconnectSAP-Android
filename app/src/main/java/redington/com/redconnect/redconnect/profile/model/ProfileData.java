package redington.com.redconnect.redconnect.profile.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;

public class ProfileData implements Serializable {

    @SerializedName("CUSTOMER_CODE")
    private String mCUSTOMERCODE;
    @SerializedName("CUSTOMER_NAME")
    private String mCUSTOMERNAME;
    @SerializedName("ADDRESS_1")
    private String mADDRESS1;
    @SerializedName("ADDRESS_2")
    private String mADDRESS2;
    @SerializedName("ADDRESS_3")
    private String mADDRESS3;
    @SerializedName("ADDRESS_4")
    private String mADDRESS4;
    @SerializedName("ADDRESS_5")
    private String mADDRESS5;
    @SerializedName("PHONE_NUMBER1")
    private String mPHONENUMBER1;
    @SerializedName("PHONE_NUMBER2")
    private String mPHONENUMBER2;
    @SerializedName("PHONE_NUMBER3")
    private String mPHONENUMBER3;
    @SerializedName("CONTACT_PERSON1")
    private String mCONTACTPERSON1;
    @SerializedName("CONTACT_PERSON2")
    private String mCONTACTPERSON2;
    @SerializedName("SALUTATION")
    private String mSALUTATION;
    @SerializedName("CompanyRegisterNumber")
    private String mCompanyRegisterNumber;
    @SerializedName("ALPHA_SEQUENCE")
    private String mALPHASEQUENCE;
    @SerializedName("DATE_OPENED")
    private String mDATEOPENED;
    @SerializedName("CRD_LMT_CHANGED_DATE")
    private String mCRDLMTCHANGEDDATE;
    @SerializedName("HIERARCHY_TYPE")
    private String mHIERARCHYTYPE;
    @SerializedName("HIER_STATEMENT")
    private String mHIERSTATEMENT;
    @SerializedName("POST_CODE")
    private String mPOSTCODE;
    @SerializedName("FAX_NUMBER")
    private String mFAXNUMBER;
    @SerializedName("FAX_NUMBER 2")
    private String mFAXNUMBER2;
    @SerializedName("LANGUAGE")
    private String mLANGUAGE;
    @SerializedName("SALESMAN")
    private String mSALESMAN;
    @SerializedName("REGION")
    private String mREGION;
    @SerializedName("BAL_BF")
    private String mBALBF;
    @SerializedName("CATEGORY")
    private String mCATEGORY;
    @SerializedName("CREDITLIMITDAYS")
    private String mCREDITLIMITDAYS;
    @SerializedName("PAYMENTDAYS")
    private String mPAYMENTDAYS;

    @SerializedName("EmailAddress1")
    private String mEmailAddress1l;
    @SerializedName("EmailAddress2")
    private String mEmailAddress2;
    @SerializedName("EmailAddress3")
    private String mEmailAddress3;

    @SerializedName("OVERDUE")
    private String mOVERDUE;
    @SerializedName("CREDIT_LIMIT")
    private String mCREDITLIMIT;
    @SerializedName("LAST_INVOICE")
    private String mLASTINVOICE;
    @SerializedName("LAST_INVOICE_DATE")
    private String mLASTINVOICEDATE;
    @SerializedName("LAST_INVOICE_VALUE")
    private String mLASTINVOICEVALUE;
    @SerializedName("HIGHEST_EXPOSURE")
    private String mHIGHESTEXPOSURE;
    @SerializedName("HIGHST_EXPOSURE_DATE")
    private String mHIGHESTEXPOSUREDATE;
    @SerializedName("LAST_PAYMENT")
    private String mLASTPAYMENT;
    @SerializedName("LAST_PAYMENT_DATE")
    private String mLASTPAYMENTDATE;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("NoOfCoversChq")
    private String mNumOfChq;

    public String getmImageURL() {
        return mImageURL;
    }

    public String getmCUSTOMERCODE() {
        return mCUSTOMERCODE;
    }


    public String getmCUSTOMERNAME() {
        return mCUSTOMERNAME;
    }


    public String getmADDRESS1() {
        return mADDRESS1;
    }


    public String getmADDRESS2() {
        return mADDRESS2;
    }


    public String getmADDRESS3() {
        return mADDRESS3;
    }


    public String getmADDRESS4() {
        return mADDRESS4;
    }


    public String getmADDRESS5() {
        return mADDRESS5;
    }


    public String getmPHONENUMBER1() {
        return mPHONENUMBER1;
    }


    public String getmPHONENUMBER2() {
        return mPHONENUMBER2;
    }


    public String getmPHONENUMBER3() {
        return mPHONENUMBER3;
    }


    public String getmCONTACTPERSON1() {
        return mCONTACTPERSON1;
    }


    public String getmCONTACTPERSON2() {
        return mCONTACTPERSON2;
    }


    public String getmSALUTATION() {
        return mSALUTATION;
    }


    public String getmCompanyRegisterNumber() {
        return mCompanyRegisterNumber;
    }


    public String getmALPHASEQUENCE() {
        return mALPHASEQUENCE;
    }


    public String getmDATEOPENED() {
        return mDATEOPENED;
    }


    public String getmCRDLMTCHANGEDDATE() {
        return mCRDLMTCHANGEDDATE;
    }


    public String getmHIERARCHYTYPE() {
        return mHIERARCHYTYPE;
    }


    public String getmHIERSTATEMENT() {
        return mHIERSTATEMENT;
    }


    public String getmPOSTCODE() {
        return mPOSTCODE;
    }


    public String getmFAXNUMBER() {
        return mFAXNUMBER;
    }


    public String getmFAXNUMBER2() {
        return mFAXNUMBER2;
    }


    public String getmLANGUAGE() {
        return mLANGUAGE;
    }


    public String getmSALESMAN() {
        return mSALESMAN;
    }


    public String getmREGION() {
        return mREGION;
    }


    public String getmBALBF() {
        return mBALBF;
    }


    public String getmCATEGORY() {
        return mCATEGORY;
    }


    public String getmCREDITLIMITDAYS() {
        return mCREDITLIMITDAYS;
    }


    public String getmPAYMENTDAYS() {
        return mPAYMENTDAYS;
    }


    public String getmOVERDUE() {
        return mOVERDUE;
    }


    public String getmCREDITLIMIT() {
        return CommonMethod.getValidDoubleString(mCREDITLIMIT);
    }


    public String getmLASTINVOICE() {
        return mLASTINVOICE;
    }


    public String getmLASTINVOICEDATE() {
        return mLASTINVOICEDATE;
    }


    public String getmLASTINVOICEVALUE() {
        return CommonMethod.getValidDoubleString(mLASTINVOICEVALUE);
    }


    public String getmHIGHESTEXPOSURE() {
        return CommonMethod.getValidDoubleString(mHIGHESTEXPOSURE);
    }


    public String getmHIGHESTEXPOSUREDATE() {
        return mHIGHESTEXPOSUREDATE;
    }


    public String getmLASTPAYMENT() {
        return CommonMethod.getValidDoubleString(mLASTPAYMENT);
    }


    public String getmLASTPAYMENTDATE() {
        return mLASTPAYMENTDATE;
    }


    public String getmEmailAddress1l() {
        return mEmailAddress1l;
    }


    public String getmEmailAddress2() {
        return mEmailAddress2;
    }


    public String getmEmailAddress3() {
        return mEmailAddress3;
    }

    public String getmNumOfChq() {
        return mNumOfChq;
    }
}
