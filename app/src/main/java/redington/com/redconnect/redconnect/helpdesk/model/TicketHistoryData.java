package redington.com.redconnect.redconnect.helpdesk.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TicketHistoryData implements Serializable {

    @SerializedName("CALL TYPE")
    private String mCALLTYPE;
    @SerializedName("CNAM10")
    private String mCNAM10;
    @SerializedName("CUSN10")
    private String mCUSN10;
    @SerializedName("DATE_UPDATE")
    private String mDATEUPDATE;
    @SerializedName("DESCRIPTION")
    private String mDESCRIPTION;
    @SerializedName("FEEDBACK_COMMENTS")
    private String mFEEDBACKCOMMENTS;
    @SerializedName("FEEDBACK_STAR")
    private String mFEEDBACKSTAR;
    @SerializedName("HELPDESK_COMMENTS")
    private String mHELPDESKCOMMENTS;
    @SerializedName("HELP_DESK_RISE_DATE")
    private String mHELPDESKRISEDATE;
    @SerializedName("HELP_DESK_RISE_TIME")
    private String mHELPDESKRISETIME;
    @SerializedName("LOCATION_CODE")
    private String mLOCATIONCODE;
    @SerializedName("PHONE_NUMBER")
    private String mPHONENUMBER;
    @SerializedName("STATUS")
    private String mSTATUS;
    @SerializedName("TICKETNO")
    private String mTICKETNO;
    @SerializedName("TICKET_RISEDATE")
    private String mTICKETRISEDATE;
    @SerializedName("TICKET_RISETIME")
    private String mTICKETRISETIME;
    @SerializedName("TIME_UPDATE")
    private String mTIMEUPDATE;
    @SerializedName("USER_UPDATE")
    private String mUSERUPDATE;
    @SerializedName("UserID_RAISED")
    private String mUserIDRAISED;

    public String getCALLTYPE() {
        return mCALLTYPE;
    }


    public String getCNAM10() {
        return mCNAM10;
    }


    public String getCUSN10() {
        return mCUSN10;
    }


    public String getDATEUPDATE() {
        return mDATEUPDATE;
    }


    public String getDESCRIPTION() {
        return mDESCRIPTION;
    }


    public String getFEEDBACKCOMMENTS() {
        return mFEEDBACKCOMMENTS;
    }


    public String getFEEDBACKSTAR() {
        return mFEEDBACKSTAR;
    }


    public String getHELPDESKCOMMENTS() {
        return mHELPDESKCOMMENTS;
    }


    public String getHELPDESKRISEDATE() {
        return mHELPDESKRISEDATE;
    }


    public String getHELPDESKRISETIME() {
        return mHELPDESKRISETIME;
    }


    public String getLOCATIONCODE() {
        return mLOCATIONCODE;
    }


    public String getPHONENUMBER() {
        return mPHONENUMBER;
    }


    public String getSTATUS() {
        return mSTATUS;
    }


    public String getTICKETNO() {
        return mTICKETNO;
    }


    public String getTICKETRISEDATE() {
        return mTICKETRISEDATE;
    }


    public String getTICKETRISETIME() {
        return mTICKETRISETIME;
    }


    public String getTIMEUPDATE() {
        return mTIMEUPDATE;
    }


    public String getUSERUPDATE() {
        return mUSERUPDATE;
    }


    public String getUserIDRAISED() {
        return mUserIDRAISED;
    }


}
