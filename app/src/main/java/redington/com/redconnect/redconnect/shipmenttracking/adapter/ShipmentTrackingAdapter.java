package redington.com.redconnect.redconnect.shipmenttracking.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.orderstatus.model.ShippingList;
import redington.com.redconnect.redconnect.shipmenttracking.activity.ShipmentTrackingActivity;
import redington.com.redconnect.redconnect.shipmenttracking.activity.TrackingDetailActivity;
import redington.com.redconnect.util.LaunchIntentManager;


public class ShipmentTrackingAdapter extends RecyclerView.Adapter<ShipmentTrackingAdapter.ShipHolder> {

    private static String[] statusTxt = {
            GlobalConstants.ORD_STATUS_PROCESSING,
            GlobalConstants.ORD_STATUS_PICKED,
            GlobalConstants.ORD_STATUS_INVOICE,
            GlobalConstants.ORD_STATUS_DISPATCH,
            GlobalConstants.ORD_STATUS_DELIVER};
    private static int[] imageArray = {R.mipmap.dispatched_ic, R.mipmap.picked, R.mipmap.ic_invoice,
            R.mipmap.ship_ic, R.mipmap.deliver_ic};
    private final List<ShippingList> mArrayList;
    private Context mContext;
    private CommonMethod commonMethod;

    public ShipmentTrackingAdapter(Context context, List<ShippingList> itemList) {
        this.mArrayList = itemList;
        this.mContext = context;
        commonMethod = new CommonMethod(context);
    }

    public void shipmentTrackingPagination(List<ShippingList> item) {
        mArrayList.addAll(item);
    }

    @NonNull
    @Override
    public ShipmentTrackingAdapter.ShipHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_order_status_recycle, parent, false);
        return new ShipmentTrackingAdapter.ShipHolder(view);

    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    public void onBindViewHolder(@NonNull ShipmentTrackingAdapter.ShipHolder holder, int position) {
        holder.mOrderNum.setText(mArrayList.get(position).getOrderNumber());
        holder.mOrderDate.setText(mArrayList.get(position).getOrderDate());
        holder.mShipName.setText(mArrayList.get(position).getCustomerName());
        holder.mInvoiceNum.setText(mArrayList.get(position).getInvoiceNumber());
        holder.mInvoiceDate.setText(mArrayList.get(position).getInvoiceDate());
        holder.mOrderStatus.setText(mArrayList.get(position).getOrderStatus());
        holder.mTotalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE
                + BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getOrderValue()));
        int cnt = Arrays.asList(statusTxt).indexOf(mArrayList.get(position).getOrderStatus());
        for (int iCount = 0; iCount < 5; iCount++) {

            if (iCount <= cnt) {
                holder.statusText[iCount].setTextColor(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.new_black_light)));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.statusImage[iCount].setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.buttonColor)));
                    holder.statusFrame[iCount].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.buttonColor)));
                    holder.statusImage[iCount].setImageResource(imageArray[iCount]);
                    holder.statusImage[iCount].setColorFilter(ContextCompat.getColor(mContext, R.color.buttonColor));
                } else {
                    GradientDrawable drawable = new GradientDrawable();
                    drawable.setShape(GradientDrawable.OVAL);
                    drawable.setColor(ContextCompat.getColor(mContext, R.color.buttonColor));
                    holder.statusFrame[iCount].setBackground(drawable);
                    GradientDrawable drawable1 = new GradientDrawable();
                    drawable1.setShape(GradientDrawable.OVAL);
                    drawable1.setColor(Color.WHITE);
                    holder.statusLayout[iCount].setBackground(drawable1);
                    holder.statusImage[iCount].setImageResource(imageArray[iCount]);
                    holder.statusImage[iCount].setColorFilter(ContextCompat.getColor(mContext, R.color.buttonColor));

                }
            } else {

                setValues(iCount, holder);

            }

            validationPart(iCount, cnt, holder);


        }

        holder.mShipmentLayout.setVisibility(View.VISIBLE);
        final int pos = holder.getAdapterPosition();
        holder.mRecyclerView.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstants.CUST_NAME, mArrayList.get(pos).getCustomerName());
                bundle.putString(IntentConstants.ORDER_NUM, mArrayList.get(pos).getOrderNumber());
                bundle.putString(IntentConstants.STATUS, mArrayList.get(pos).getOrderStatus());
                bundle.putString(IntentConstants.DATE, mArrayList.get(pos).getShippingDate());
                bundle.putString(IntentConstants.TIME, mArrayList.get(pos).getShippingTime());
                LaunchIntentManager.routeToActivityStackBundle(mContext, TrackingDetailActivity.class, bundle);
            }
        });
    }

    private void validationPart(int iCount, int cnt, ShipHolder holder) {
        if (iCount != 0) {
            if (iCount <= cnt) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.statusView[iCount - 1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.buttonColor)));
                } else {
                    holder.statusView[iCount - 1].setBackgroundColor(ContextCompat.getColor(mContext, R.color.buttonColor));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.statusView[iCount - 1].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.text_color)));
                } else {
                    holder.statusView[iCount - 1].setBackgroundColor(ContextCompat.getColor(mContext, R.color.text_color));
                }
            }
        }
    }

    @SuppressLint("NewApi")
    private void setValues(int iCount, ShipHolder holder) {
        holder.statusText[iCount].setTextColor(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.text_color)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.statusImage[iCount].setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.text_color)));
            holder.statusFrame[iCount].setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.text_color)));
            holder.statusImage[iCount].setImageResource(imageArray[iCount]);
            holder.statusImage[iCount].setColorFilter(ContextCompat.getColor(mContext, R.color.text_color));
        } else {
            GradientDrawable drawable = new GradientDrawable();
            drawable.setShape(GradientDrawable.OVAL);
            drawable.setColor(ContextCompat.getColor(mContext, R.color.text_color));
            holder.statusFrame[iCount].setBackground(drawable);
            GradientDrawable drawable1 = new GradientDrawable();
            drawable1.setShape(GradientDrawable.OVAL);
            drawable1.setColor(Color.WHITE);
            holder.statusLayout[iCount].setBackground(drawable1);
            holder.statusImage[iCount].setImageResource(imageArray[iCount]);
            holder.statusImage[iCount].setColorFilter(ContextCompat.getColor(mContext, R.color.text_color));


        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class ShipHolder extends RecyclerView.ViewHolder {
        private final RelativeLayout mRecyclerView;
        private final LinearLayout mShipmentLayout;
        private final TextView mOrderNum;
        private final TextView mInvoiceNum;
        private final TextView mInvoiceDate;
        private final TextView mTotalAmount;
        private final TextView mShipName;
        private final TextView mOrderStatus;
        private final TextView mOrderDate;
        private final TextView[] statusText = new TextView[5];
        private final ImageView[] statusImage = new ImageView[5];
        private final FrameLayout[] statusFrame = new FrameLayout[5];
        private final FrameLayout[] statusLayout = new FrameLayout[5];
        private final View[] statusView = new View[5];


        ShipHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();
            mRecyclerView = itemView.findViewById(R.id.RL_recycleView);
            mShipmentLayout = itemView.findViewById(R.id.LL_ship_layout);
            mOrderNum = itemView.findViewById(R.id.txt_orderNum);
            mOrderDate = itemView.findViewById(R.id.text_date);
            mShipName = itemView.findViewById(R.id.txt_ship_name);
            mInvoiceNum = itemView.findViewById(R.id.txt_invoiceNum);
            mTotalAmount = itemView.findViewById(R.id.txt_totalAmount);
            mInvoiceDate = itemView.findViewById(R.id.txt_invoiceDate);
            mOrderStatus = itemView.findViewById(R.id.order_status);
            for (int i = 0; i < 5; i++) {
                int imgId = (((ShipmentTrackingActivity) mContext).getStringResourceByName(GlobalConstants.ORDER_STATUS_IMAGE + (i + 1)));
                int txtId = (((ShipmentTrackingActivity) mContext).getStringResourceByName(GlobalConstants.ORDERSTATUS_TEXT + (i + 1)));
                int frameId = (((ShipmentTrackingActivity) mContext).getStringResourceByName(GlobalConstants.ORDER_FRAME_LAYOUT + (i + 1)));
                int frameLayout = (((ShipmentTrackingActivity) mContext).getStringResourceByName(GlobalConstants.ORDER_FRAME_LAYOUT_WHITE + (i + 1)));

                statusImage[i] = itemView.findViewById(imgId);
                statusText[i] = itemView.findViewById(txtId);
                statusFrame[i] = itemView.findViewById(frameId);
                statusLayout[i] = itemView.findViewById(frameLayout);

                if (i < 4) {
                    int viewId = (((ShipmentTrackingActivity) mContext).getStringResourceByName(GlobalConstants.ORDER_STATUS_VIEW + (i + 1)));
                    statusView[i] = itemView.findViewById(viewId);
                }
            }
        }
    }
}
