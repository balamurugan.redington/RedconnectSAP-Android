package redington.com.redconnect.redconnect.capitalfloat.helper;

import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.capitalfloat.model.CreditBlockResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.CapitalFloatRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResentOtpHelper {

    private ResentOtpHelper() {
        throw new IllegalStateException(IntentConstants.CAPITAL_FLOAT);
    }


    public static void resentOtp(String appId, String orderId,
                                 final Context context, final UIListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = CapitalFloatRequest.getHeaderRequest(true, GlobalConstants.getCapitalFloatHeaders());
            RestClient.getInstance(headRequest).resentOtpVerify(getRequestBody(appId, orderId))
                    .enqueue(new Callback<CreditBlockResponse>() {
                        @Override
                        public void onResponse(Call<CreditBlockResponse> call, Response<CreditBlockResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<CreditBlockResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });

        } else {
            listener.onError();
        }

    }

    private static RequestBody getRequestBody(String appId, String orderId) {
        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.FLOAT_APP_ID, appId);
            jsonValues.put(JsonKeyConstants.FLOAT_ORDER_ID, orderId);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());

        }
        return body;
    }
}
