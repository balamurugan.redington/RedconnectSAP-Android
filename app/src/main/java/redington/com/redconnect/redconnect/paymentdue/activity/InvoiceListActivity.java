package redington.com.redconnect.redconnect.paymentdue.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.paymentdue.adapter.InvoiceAdapter;
import redington.com.redconnect.redconnect.paymentdue.helper.PaymentDueManager;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueDetail;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class InvoiceListActivity extends BaseActivity implements UIListener {

    private Context mContext;
    private CommonUtils commonUtils;
    private TextView mCustomerName;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_list);

        mContext = InvoiceListActivity.this;
        commonUtils = new CommonUtils(mContext);

        userID = GlobalConstants.GetSharedValues.getSpUserId();

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        mCustomerName = findViewById(R.id.txt_cust_name);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        toolbar(IntentConstants.INVOICELIST);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String week = bundle.getString(GlobalConstants.WEEK_VALUE);
            if (week == null) {
                paymentDueApi(GlobalConstants.SPACE);
            } else {
                paymentDueApi(week);
            }

        }


    }

    private void paymentDueApi(String weekValue) {
        commonUtils.showProgressDialog();
        PaymentDueManager.getPaymentDueServiceCall(userID, weekValue, mContext, this);
    }

    private void initViews(ArrayList<PaymentDueDetail> arrayList) {
        RecyclerView recyclerView = findViewById(R.id.invoice_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new InvoiceAdapter(arrayList));
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        PaymentDueResponse paymentDueResponse = (PaymentDueResponse) successObject;
        if (paymentDueResponse != null) {
            switch (paymentDueResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PaymentDueDetail> arrayList = new ArrayList<>(paymentDueResponse.getData().getDetail());
                    mCustomerName.setText(arrayList.get(0).getCustomerName());
                    initViews(arrayList);
                    break;
                case IntentConstants.FAILURE_URL:
                    ArrayList<PaymentDueDetail> arrayList1 = new ArrayList<>(paymentDueResponse.getData().getDetail());
                    mCustomerName.setText(arrayList1.get(0).getCustomerName());
                    if (arrayList1.isEmpty()) {
                        noRecords(mContext, GlobalConstants.NO_RECORDS);
                    } else {
                        initViews(arrayList1);
                    }
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }
}
