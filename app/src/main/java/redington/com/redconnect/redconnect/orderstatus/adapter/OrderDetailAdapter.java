package redington.com.redconnect.redconnect.orderstatus.adapter;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.orderstatus.model.OrderDetailData;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailHolder> {

    private final List<OrderDetailData> mArrayList;


    public OrderDetailAdapter(List<OrderDetailData> itemList) {
        this.mArrayList = itemList;

    }

    @NonNull
    @Override
    public OrderDetailAdapter.OrderDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_order_detail_recycle, parent, false);
        return new OrderDetailAdapter.OrderDetailHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderDetailAdapter.OrderDetailHolder holder, int position) {
        holder.mProductName.setText(mArrayList.get(position).getmProductName());
        holder.mProductQty.setText(Integer.toString(mArrayList.get(position).getQuantity()));
        holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE
                + BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getUnitPrice()));
        holder.mProductItemCode.setText(mArrayList.get(position).getItemCode());
        holder.mProductVendorItem.setText(mArrayList.get(position).getItemVendorCode());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    class OrderDetailHolder extends RecyclerView.ViewHolder {
        private final TextView mProductName;
        private final TextView mProductQty;
        private final TextView mProductPrice;
        private final TextView mProductItemCode;
        private final TextView mProductVendorItem;

        OrderDetailHolder(View itemView) {
            super(itemView);

            mProductName = itemView.findViewById(R.id.product_name);
            mProductQty = itemView.findViewById(R.id.product_qty);
            mProductPrice = itemView.findViewById(R.id.product_price);
            mProductItemCode = itemView.findViewById(R.id.product_itemcode);
            mProductVendorItem = itemView.findViewById(R.id.product_vendor_item);

        }
    }
}
