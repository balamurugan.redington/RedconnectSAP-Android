package redington.com.redconnect.redconnect.mycart.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.CartHolder> {

    private final Context mContext;
    private List<CartData> mArrayList;
    private List<SubPromoCode> subPromoCodes;
    private int cameFrom = 0;
    private String mType = "";

    public MyCartAdapter(Context context, List<CartData> arrayList, int from) {
        this.mArrayList = arrayList;
        this.mContext = context;
        this.cameFrom = from;
    }

    public MyCartAdapter(Context context, List<SubPromoCode> promoCodes, String type) {
        this.subPromoCodes = promoCodes;
        this.mContext = context;
        this.mType = type;
    }

    @NonNull
    @Override
    public MyCartAdapter.CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_new_cart, parent, false);
        return new MyCartAdapter.CartHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyCartAdapter.CartHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (cameFrom == 1 && mType.equals("")) {
            holder.productName.setText(mArrayList.get(position).getItemDescription());
            holder.itemcode.setText(mArrayList.get(position).getItemCode());
            /*holder.quantity.setText(String.valueOf(mArrayList.get(position).getmAvailableQuantity()));*/
            holder.quantity.setText(String.valueOf(mArrayList.get(position).getQuantity()));

            double totq = mArrayList.get(position).getmTotalValue();
            holder.totalPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(totq));

            Glide.with(mContext).load(mArrayList.get(position).getImages())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .into(holder.mCartImage);

            holder.updateQty.setOnClickListener(v -> showDialogSuccess(mContext, pos));

            holder.mCartremove.setOnClickListener(v -> alertDelete(pos));


        } else if (cameFrom == 0 && mType.equals(IntentConstants.SPOT)) {
            holder.productName.setText(subPromoCodes.get(position).getmProductName());
            holder.itemcode.setText(subPromoCodes.get(position).getItemCode());

            holder.quantity.setText(String.valueOf(subPromoCodes.get(position).getQuantity()));


            Double totalPrice = subPromoCodes.get(position).getmTOTAL();
            holder.totalPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(totalPrice));
            holder.updateQty.setVisibility(View.GONE);
            holder.mCartremove.setVisibility(View.GONE);
            Glide.with(mContext).load(subPromoCodes.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .into(holder.mCartImage);

        }
    }

    @SuppressLint("NewApi")
    private void showDialogSuccess(Context con, final int pos) {
        final Dialog mDialog = new Dialog(con);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_quanty);
        mDialog.setCanceledOnTouchOutside(false);

        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        final EditText editable = mDialog.findViewById(R.id.ed_myquantity);

        editable.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not in use
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editable.getText().toString().matches("^0")) {
                    editable.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not in use
            }
        });

        Button button = mDialog.findViewById(R.id.pre_ref_btm);
        button.setOnClickListener(v -> {
            final String ed = editable.getText().toString();

            if (!ed.isEmpty()) {

                ((MyCartActivity) mContext).selectCartApi(mArrayList.get(pos).getItemCode(),
                        mArrayList.get(pos).getItemDescription(),
                        mArrayList.get(pos).getVendorCode(),
                        Integer.parseInt(ed),
                        mArrayList.get(pos).getUnitPrice());
                mDialog.dismiss();
            } else {
                BaseActivity.shortToast(mContext, GlobalConstants.EMPTY_QTY);
            }
        });

        mDialog.show();
    }

    @SuppressLint("NewApi")
    private void alertDelete(final int pos) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);
        mDialog.setCanceledOnTouchOutside(false);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);
        TextView textSuccess = mDialog.findViewById(R.id.text_success);
        TextView textFailure = mDialog.findViewById(R.id.text_failure);

        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.dialog_failure));
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            textFailure.setBackground(gradientDrawable);
        }

        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        alertContent.setText(mContext.getString(R.string.deleteWishList));

        textSuccess.setOnClickListener(v -> {
            ((MyCartActivity) mContext).deleteCart(mArrayList.get(pos).getItemCode(),
                    mArrayList.get(pos).getItemDescription(),
                    mArrayList.get(pos).getVendorCode(),
                    mArrayList.get(pos).getQuantity(),
                    mArrayList.get(pos).getUnitPrice(),
                    GlobalConstants.SAVE);
            mDialog.dismiss();
        });

        textFailure.setOnClickListener(v -> {
            ((MyCartActivity) mContext).deleteCart(mArrayList.get(pos).getItemCode(),
                    mArrayList.get(pos).getItemDescription(),
                    mArrayList.get(pos).getVendorCode(),
                    mArrayList.get(pos).getQuantity(),
                    mArrayList.get(pos).getUnitPrice(),
                    GlobalConstants.NULL_DATA);
            mDialog.dismiss();
        });

        mDialog.show();
    }

    @Override
    public int getItemCount() {
        if (cameFrom == 1 && mType.equals("")) {
            return mArrayList.size();
        } else if (cameFrom == 0 && mType.equals(IntentConstants.SPOT)) {
            return subPromoCodes.size();
        } else {
            return 0;
        }
    }

    class CartHolder extends RecyclerView.ViewHolder {
        private ImageView mCartImage;
        private ImageView mCartremove;
        private TextView productName;
        private TextView quantity;
        private TextView itemcode;
        private TextView totalPrice;
        private ImageView updateQty;


        CartHolder(View itemView) {
            super(itemView);

            mCartImage = itemView.findViewById(R.id.imageView);
            mCartremove = itemView.findViewById(R.id.removeImage);
            productName = itemView.findViewById(R.id.product_name);
            quantity = itemView.findViewById(R.id.Qty);
            itemcode = itemView.findViewById(R.id.itemcode);
            totalPrice = itemView.findViewById(R.id.totalPrice);
            updateQty = itemView.findViewById(R.id.updatqty);
        }
    }
}
