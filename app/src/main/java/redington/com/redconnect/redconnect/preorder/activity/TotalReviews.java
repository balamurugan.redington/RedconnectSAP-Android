package redington.com.redconnect.redconnect.preorder.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.adapter.ReviewsAdapter;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsProductreview;
import redington.com.redconnect.redconnect.settings.helper.PdfServicemanager;
import redington.com.redconnect.redconnect.settings.model.Pdfdata;
import redington.com.redconnect.redconnect.settings.model.Pdfjsonresponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class TotalReviews extends BaseActivity implements UIListener {

    private CommonUtils commonUtils;
    private Context mContext;
    private RecyclerView mReviewRecyclerView;
    private LinearLayoutManager verticalLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder);

        mContext = TotalReviews.this;
        commonUtils = new CommonUtils(mContext);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (IntentConstants.PDF_NAME.equals(bundle.getString(IntentConstants.PDF))) {
                toolbar(IntentConstants.BRANDS_QUICK_GUIDE);
                pdfApi(IntentConstants.PDF_NAME);
            } else {
                ArrayList<PreorderItemDetailsProductreview> arrayList = (ArrayList<PreorderItemDetailsProductreview>) bundle.getSerializable(GlobalConstants.PREORDER_SPEC_BUNDLE);
                bindActivity(arrayList);
                toolbar(IntentConstants.TOTALREVIEW);
            }

        }

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);


    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    /*Recycler View*/
    private void bindActivity(ArrayList<PreorderItemDetailsProductreview> arrayList) {

        mReviewRecyclerView.setAdapter(new ReviewsAdapter(mContext, arrayList, IntentConstants.TOTALREVIEW));
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mReviewRecyclerView.getContext(),
                verticalLayout.getOrientation());
        mReviewRecyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void pdfApi(String value) {
        commonUtils.showProgressDialog();
        PdfServicemanager.getPdf(value, mContext, this);
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        Pdfjsonresponse pdfjsonresponse = (Pdfjsonresponse) successObject;
        if (pdfjsonresponse != null) {
            switch (pdfjsonresponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<Pdfdata> pdfdata = new ArrayList<>(pdfjsonresponse.getData());
                    bindPdf(pdfdata);
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }


    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mReviewRecyclerView = findViewById(R.id.recycle_view);
        mReviewRecyclerView.setHasFixedSize(true);
        verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mReviewRecyclerView.setLayoutManager(verticalLayout);

    }

    private void bindPdf(ArrayList<Pdfdata> pdfdata) {
        mReviewRecyclerView.setPadding(25, 0, 0, 0);
        mReviewRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        mReviewRecyclerView.setAdapter(new ReviewsAdapter(mContext, pdfdata, 1));
    }


}
