package redington.com.redconnect.redconnect.helpdesk.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.helpdesk.helper.TicketSubmissionManager;
import redington.com.redconnect.redconnect.helpdesk.model.TicketSubmissionJsonResponse;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.smsgateway.constants.TemplateConstants;
import redington.com.redconnect.smsgateway.handler.HTTPHandler;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.PermissionUtils;

public class HelpDeskDialog extends Activity implements UIListener, PermissionResultCallback, ActivityCompat.OnRequestPermissionsResultCallback {
    private CommonUtils commonUtils;
    private Context mContext;
    private String userID;
    private String getMobileNumber;
    private String getDescription;
    private String ticketNum;
    private PermissionUtils permissionUtils;
    ArrayList<String> permissions = new ArrayList<>();
    private ImageView mImage;
    private String appVersionName;
    private String imageString = "";
    private String modelName = "";
    private String osVersion = "";


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_helpdesk);

        mContext = HelpDeskDialog.this;
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initViews();

        commonUtils = new CommonUtils(mContext);

        RelativeLayout relativeLayout = findViewById(R.id.menu_title);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.new_black));
        float[] cornerValues = {15.0f, 15.0f, 15.0f, 15.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        gradientDrawable.setCornerRadii(cornerValues);

        permissionUtils = new PermissionUtils(mContext);
        TextView mAddImage = findViewById(R.id.addImage);
        mAddImage.setOnClickListener(v -> permissionUtils.checkPermission(permissions, GlobalConstants.CAM_ERROR, 1));

        mImage = findViewById(R.id.image);
        permissions.add(Manifest.permission.CAMERA);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            relativeLayout.setBackground(gradientDrawable);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            loadTicketSubmission(userID, getMobileNumber, getDescription, modelName, osVersion,
                    appVersionName, imageString);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            finish();
        }
    }

    private void internetCheckFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        editor.apply();
    }

    private void backButtonFalse() {
        SharedPreferences.Editor editor = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        editor.apply();
    }

    private void initViews() {

        final EditText mMobileNumber = findViewById(R.id.ed_mobileNumber);
        mMobileNumber.setPadding(15, 0, 0, 0);
        final EditText mDescription = findViewById(R.id.ed_description);
        mDescription.setPadding(15, 10, 0, 0);
        Button mTicketSubmit = findViewById(R.id.btn_ticket_submit);


        mMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Not in use
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mMobileNumber.getText().toString().matches("^0")) {
                    mMobileNumber.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Not in use
            }
        });
        mTicketSubmit.setOnClickListener(v -> {

            getMobileNumber = mMobileNumber.getText().toString();
            getDescription = mDescription.getText().toString();

            if (getMobileNumber.length() == 0) {
                BaseActivity.shortToast(mContext, this.getString(R.string.enter_mobile));
            } else if (getMobileNumber.length() < 10) {
                BaseActivity.shortToast(mContext, this.getString(R.string.ph_num_digit));
            } else if (getDescription.matches("")) {
                BaseActivity.shortToast(mContext, GlobalConstants.DESC_TOAST);
            } else {

                userID = GlobalConstants.GetSharedValues.getSpUserId();
                loadTicketSubmission(userID, getMobileNumber, getDescription, modelName, osVersion,
                        appVersionName, imageString);
            }

        });

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            BaseActivity.logd(e.getMessage());
        }

        modelName = Build.MANUFACTURER + GlobalConstants.SPACE + Build.MODEL;
        osVersion = Build.VERSION.RELEASE;
    }

    private void loadTicketSubmission(String userid, String mobNum, String description, String modelName,
                                      String osVersion, String appVersion, String imageString) {
        ArrayList<String> ticketGeneration = new ArrayList<>();
        ticketGeneration.add(userid);
        ticketGeneration.add(mobNum);
        ticketGeneration.add(description);
        ticketGeneration.add(modelName);
        ticketGeneration.add(osVersion);
        ticketGeneration.add(appVersion);
        ticketGeneration.add(imageString);

        commonUtils.showProgressDialog();
        TicketSubmissionManager.getTicketSubmissionCall(ticketGeneration, HelpDeskDialog.this, this);
    }

    /*Success Dialog */
    @SuppressLint({"SetTextI18n", "NewApi"})
    private void showDialogSuccess(Context con, String ticketNo) {
        final Dialog mDialog = new Dialog(con);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        TextView mTitle = mDialog.findViewById(R.id.pre_ref_title);
        TextView mContent = mDialog.findViewById(R.id.pre_ref_cont);
        Button button = mDialog.findViewById(R.id.pre_ref_btm);

        mTitle.setText(GlobalConstants.TICKET_GEN);
        mContent.setText(GlobalConstants.TICKET_NO + ticketNo);
        mContent.setTextColor(ContextCompat.getColor(mContext, R.color.new_buttonColor));
        button.setOnClickListener(v -> {
            LaunchIntentManager.routeToActivity(HelpDeskDialog.this, DashboardActivity.class);
            mDialog.dismiss();
        });

        mDialog.show();
    }

    /*API Response*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        TicketSubmissionJsonResponse ticketSubmissionJsonResponse = (TicketSubmissionJsonResponse) successObject;
        if (ticketSubmissionJsonResponse != null) {
            switch (ticketSubmissionJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ticketNum = ticketSubmissionJsonResponse.getData().getTicketNumber();
                    showDialogSuccess(HelpDeskDialog.this, ticketNum);
                    callMessageAPI();
                    break;
                case IntentConstants.FAILURE_URL:
                    BaseActivity.shortToast(mContext, GlobalConstants.TICKET_NOT_GENERATED);
                    break;
                default:
                    BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }

        } else {
            BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }


    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(HelpDeskDialog.this, InternetErrorCheck.class);
    }

    private void callMessageAPI() {

        runOnUiThread(new SMSGateway()::execute);
    }

    /*Request Permission Result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }


    @Override
    public void permissionGranted(int myRequestCode) {
        if (myRequestCode == 1) {
            captureImage();
        }
    }


    @Override
    public void partialPermissionGranted(int myRequestCode, List<String> myGrantedPermission) {
        if (myRequestCode == 1) {
            captureImage();
        }
    }

    @Override
    public void permissionDenied(int myRequestCode) {
        //No data
    }

    @Override
    public void neverAskAgain(int myRequestCode) {
        //No data

    }


    @SuppressLint("StaticFieldLeak")
    private class SMSGateway extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            HTTPHandler handler = new HTTPHandler();
            String url = TemplateConstants.BASE_URL + TemplateConstants.MOBILENUM + TemplateConstants.MESSAGE +
                    TemplateConstants.TICKETGENERATION + TemplateConstants.PIPESYMBOL + ticketNum
                    + TemplateConstants.SYMBOL + TemplateConstants.TEAM;
            String jsonString = handler.makeServiceCall(url);
            BaseActivity.logd(jsonString);
            return null;
        }


    }


    private void captureImage() {
        final CharSequence[] items = {GlobalConstants.CAPTURE_PHOTO, GlobalConstants.CHOOSE_GALLERY};
        AlertDialog.Builder builder = new AlertDialog.Builder(HelpDeskDialog.this);
        builder.setTitle(GlobalConstants.ADD_PHOTO);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals(GlobalConstants.CAPTURE_PHOTO)) {
                cameraIntent();
            } else if (items[item].equals(GlobalConstants.CHOOSE_GALLERY)) {
                galleryIntent();
            }
        });

        builder.show();
    }


    /*Camera Intent*/
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, IntentConstants.REQUEST_CAMERA);
    }

    /*Gallery Intent*/
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IntentConstants.SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstants.SELECT_FILE && data != null && data.getData() != null) {
            getImageFromGallery(data);
        } else if (requestCode == IntentConstants.REQUEST_CAMERA && data != null) {
            onCaptureImageResult(data);
        }
    }

    /*Camera Function*/
    private void onCaptureImageResult(Intent data) {
        try {
            Bitmap mPhoto = (Bitmap) data.getExtras().get("data");
            if (mPhoto != null) {
                convertBitmapAndSet(mPhoto);
            }
        } catch (NullPointerException e) {
            BaseActivity.logd(e.getMessage());
        }
    }

    /*Gallery Function*/
    private void getImageFromGallery(final Intent data) {

        runOnUiThread(() -> {
            if (data.getData() != null) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    convertBitmapAndSet(bitmap);
                } catch (IOException e) {
                    BaseActivity.logd(e.getMessage());
                }
            }
        });

    }

    /*Convert Image And Set*/
    private void convertBitmapAndSet(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
        imageString = Base64.encodeToString(b, Base64.DEFAULT);
        mImage.setImageBitmap(compressedBitmap);
        mImage.setVisibility(View.VISIBLE);
    }

}
