package redington.com.redconnect.redconnect.helpdesk.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.helpdesk.adapter.TicketHistoryAdapter;
import redington.com.redconnect.redconnect.helpdesk.helper.TicketHistoryManager;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryData;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class TicketHistoryActivity extends BaseActivity implements View.OnClickListener, UIListener {

    private LinearLayout ticketLayout;

    private Context context;
    private String userID;
    private ImageView mNoImageFound;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_history);

        context = TicketHistoryActivity.this;

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);

        ticketLayout = findViewById(R.id.LL_ticket);
        ticketLayout.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progressBar);

        mNoImageFound = findViewById(R.id.img_no_records_found);
        mNoImageFound.setVisibility(View.GONE);

        imageButton.setVisibility(View.VISIBLE);
        imageCart.setVisibility(View.GONE);

        toolbar(IntentConstants.TICKETHISTORY);
        userID = GlobalConstants.GetSharedValues.getSpUserId();
        getTicketHistoryList(userID);
        floatingFunction();
    }

    private void getTicketHistoryList(String userID) {
        TicketHistoryManager.getTicketHistoryServiceCall(userID, context, this);
    }

    private void floatingFunction() {
        FloatingActionButton floating = findViewById(R.id.fab);
        floating.setOnClickListener(this);
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            getTicketHistoryList(userID);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }

    }


    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.fab) {
            LaunchIntentManager.routeToActivityStack(this, HelpDeskDialog.class);

        }
    }


    /*Response API*/
    @Override
    public void onSuccess(Object successObject) {
        progressBar.setVisibility(View.GONE);
        RecyclerView recyclerView = findViewById(R.id.vertical_recycle_view);
        TicketHistoryJsonResponse ticketHistoryJsonResponse = (TicketHistoryJsonResponse) successObject;
        if (ticketHistoryJsonResponse != null) {
            switch (ticketHistoryJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<TicketHistoryData> ticketHistoryData = new ArrayList(ticketHistoryJsonResponse.getData());
                    ticketLayout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    recyclerView.setAdapter(new TicketHistoryAdapter(context, ticketHistoryData));
                    break;
                case IntentConstants.FAILURE_URL:
                    ticketLayout.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    mNoImageFound.setVisibility(View.VISIBLE);
                    break;
                default:
                    ticketLayout.setVisibility(View.GONE);
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }

        } else {
            ticketLayout.setVisibility(View.GONE);
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        progressBar.setVisibility(View.GONE);
        noRecords(context, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        progressBar.setVisibility(View.GONE);
        LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
    }


}



