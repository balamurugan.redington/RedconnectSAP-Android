package redington.com.redconnect.redconnect.ratingfeedback.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RatingFeedbackData implements Serializable {

    @SerializedName("Confirmation")
    private String mConfirmation;

    public String getConfirmation() {
        return mConfirmation;
    }

}
