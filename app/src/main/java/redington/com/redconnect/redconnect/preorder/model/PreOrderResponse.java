package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class PreOrderResponse implements Serializable {

    @SerializedName("Category")
    private List<PreorderCategoryData> mCategory;


    public List<PreorderCategoryData> getCategory() {
        return mCategory;
    }

    public void setCategory(List<PreorderCategoryData> category) {
        mCategory = category;
    }


}
