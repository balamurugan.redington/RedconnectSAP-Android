package redington.com.redconnect.redconnect.search.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.redconnect.preorder.model.SortingBean;
import redington.com.redconnect.redconnect.search.activity.SearchActivity;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {
    private final List<SortingBean> mArrayList;
    private Context mContext;

    public SearchAdapter(Context context, List<SortingBean> itemList) {
        this.mArrayList = itemList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public SearchAdapter.SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_list, parent, false);
        return new SearchAdapter.SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchAdapter.SearchHolder holder, int position) {
        holder.mSettingsText.setText(mArrayList.get(position).getText());
        Glide.with(mContext).load(R.drawable.related)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.redinton_image)
                .into(holder.mSettingsImage);

        holder.mTotalLayout.setOnClickListener(v -> {
            String getText = mArrayList.get(holder.getAdapterPosition()).getText();
            ((SearchActivity) mContext).getStringText(getText);
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class SearchHolder extends RecyclerView.ViewHolder {
        private LinearLayout mTotalLayout;
        private TextView mSettingsText;
        private ImageView mSettingsImage;

        private SearchHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();
            mTotalLayout = itemView.findViewById(R.id.LL_recycleView);
            mSettingsImage = itemView.findViewById(R.id.settings_image);
            mSettingsText = itemView.findViewById(R.id.text_settings);

        }
    }
}
