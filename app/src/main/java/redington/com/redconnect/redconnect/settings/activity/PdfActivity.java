package redington.com.redconnect.redconnect.settings.activity;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.IntentConstants;

public class PdfActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        WebView webView = findViewById(R.id.webview);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            String myPdfUrl = bundle.getString(IntentConstants.PDF_URL);
            String url = "http://docs.google.com/gview?embedded=true&url=" + myPdfUrl;
            webView.loadUrl(url);
            toolbar(bundle.getString(IntentConstants.PDF_NAME));
        } else {
            shortToast(this, IntentConstants.FILE_NOT);
        }

    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

}
