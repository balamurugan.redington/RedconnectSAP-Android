
package redington.com.redconnect.redconnect.settings.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Pdfdata implements Serializable {

    @SerializedName("ActualURL")
    private String mActualURL;
    @SerializedName("CreationDate")
    private String mCreationDate;
    @SerializedName("Extension")
    private String mExtension;
    @SerializedName("FileSize")
    private String mFileSize;
    @SerializedName("Filename")
    private String mFilename;

    public String getActualURL() {
        return mActualURL;
    }

    public String getCreationDate() {
        return mCreationDate;
    }

    public String getExtension() {
        return mExtension;
    }

    public String getFileSize() {
        return mFileSize;
    }

    public String getFilename() {
        return mFilename;
    }

}
