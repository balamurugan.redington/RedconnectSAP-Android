package redington.com.redconnect.redconnect.helpdesk.helper;


import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.helpdesk.model.TicketSubmissionJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketSubmissionManager {

    private TicketSubmissionManager() {
        throw new IllegalStateException(String.valueOf(R.string.helpdesk));
    }

    public static void getTicketSubmissionCall(List<String> ticketGen,
                                               final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getTicketSubmit(getRequestBody(ticketGen)).enqueue(new Callback<TicketSubmissionJsonResponse>() {
                @Override
                public void onResponse(Call<TicketSubmissionJsonResponse> call, Response<TicketSubmissionJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<TicketSubmissionJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(List<String> ticketGene) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JHP_USERID, ticketGene.get(0));
            jsonValues.put(JsonKeyConstants.JHP_MOBILE_NO, ticketGene.get(1));
            jsonValues.put(JsonKeyConstants.JHP_TICKET_DESC, ticketGene.get(2));
            jsonValues.put(JsonKeyConstants.JHP_TICKET_DEVICE_ID, ticketGene.get(3));
            jsonValues.put(JsonKeyConstants.JHP_TICKET_OS_VERSION, ticketGene.get(4));
            jsonValues.put(JsonKeyConstants.JHP_TICKET_APP_VERSION, ticketGene.get(5));
            jsonValues.put(JsonKeyConstants.JHP_TICKET_UPLOAD_IMAGE, ticketGene.get(6));
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
