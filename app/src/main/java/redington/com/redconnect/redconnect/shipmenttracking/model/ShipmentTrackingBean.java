package redington.com.redconnect.redconnect.shipmenttracking.model;


public class ShipmentTrackingBean {
    private String name1;
    private String name2;


    public ShipmentTrackingBean(String name1, String name2) {
        this.name1 = name1;
        this.name2 = name2;

    }

    public String getOrderNum() {
        return name1;
    }

    public String getInvoiceNum() {
        return name2;
    }


}
