package redington.com.redconnect.redconnect.search.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.activity.PreOrderActivityList;
import redington.com.redconnect.redconnect.preorder.helper.PreorderItemListServiceManager;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListData;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.SortingBean;
import redington.com.redconnect.redconnect.search.adapter.SearchAdapter;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class SearchActivity extends BaseActivity implements UIListener {
    private EditText mSearchEdit;
    private CommonUtils commonUtils;
    private int sortNum = 0;
    private int pageNum = 1;

    private Context context;
    private boolean stringValue;
    private String getSelectedString;
    private String mUserid;

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_search);

        context = SearchActivity.this;
        commonUtils = new CommonUtils(context);
        mSearchEdit = findViewById(R.id.search_edt);
        Drawable mDrawable = ContextCompat.getDrawable(context, R.mipmap.ic_search);
        mDrawable.mutate().setColorFilter(ContextCompat.getColor(context, R.color.new_black), PorterDuff.Mode.SRC_IN);
        mSearchEdit.setCompoundDrawablesWithIntrinsicBounds(mDrawable, null, null, null);
        ImageButton searchCheck = findViewById(R.id.searchCheck);


        mUserid = GlobalConstants.GetSharedValues.getSpUserId();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String cameFrom = bundle.getString(GlobalConstants.FROM);
            if (cameFrom.equals(IntentConstants.DASHBOARD)) {
                String mFlag = bundle.getString(IntentConstants.TAG);
                setInstance(mFlag);
            } else if (cameFrom.equals(GlobalConstants.SEARCH)) {
                mSearchEdit.setHint("");
            }
        }

        mSearchEdit.setFilters(new InputFilter[]{filter});
        mSearchEdit.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                searchApiCall();
            }
            return false;
        });


        searchCheck.setOnClickListener(view -> searchApiCall());


    }

    private void searchApiCall() {
        if (mSearchEdit.getText().length() != 0) {
            mCallAPI();
        } else {
            shortToast(context, GlobalConstants.ENTER_KEYWORDS);
        }
    }

    /*API Call*/
    private void mCallAPI() {
        stringValue = true;
        ArrayList<String> searchList = new ArrayList<>();
        searchList.add(mSearchEdit.getText().toString().trim());
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.RECORD_NO);
        searchList.add(String.valueOf(pageNum));
        searchList.add(String.valueOf(sortNum));
        searchList.add(mUserid);

        loadSearch(searchList);
    }

    /*Selected String API Call*/
    public void getStringText(String selectedString) {

        stringValue = false;
        getSelectedString = selectedString;
        ArrayList<String> searchList = new ArrayList<>();
        searchList.add(getSelectedString);
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.NULL_DATA);
        searchList.add(GlobalConstants.RECORD_NO);
        searchList.add(String.valueOf(pageNum));
        searchList.add(String.valueOf(sortNum));
        searchList.add(mUserid);
        loadSearch(searchList);
    }

    /*Init Views*/
    private void setInstance(String flag) {
        RecyclerView recyclerView = findViewById(R.id.search_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setPadding(25, 0, 0, 0);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        if (!flag.equals("Y")) {
            List<SortingBean> sortingBeen = prepareData();
            SearchAdapter adapter = new SearchAdapter(context, sortingBeen);
            recyclerView.setAdapter(adapter);
        }


    }

    private ArrayList<SortingBean> prepareData() {
        String[] values = new String[]{"iPhone 7", "EPSON", "Google Pixel", "PC-Notebook",
                "KODAK", "SAMSUNG", "ASUS Zenfone 3", "Cisco", "Redmi", "iPad Mini"};
        ArrayList<SortingBean> arrayList = new ArrayList<>();
        for (String value : values) {

            arrayList.add(new SortingBean(value));

        }
        return arrayList;
    }


    public void searchBack(View view) {
        setMenuBack(view.getContext());
    }

    private void loadSearch(ArrayList<String> searchList) {
        commonUtils.showProgressDialog();
        PreorderItemListServiceManager.getPreorderItemListServiceManager(searchList, SearchActivity.this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean internetCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (internetCheck) {
            ArrayList<String> searchList = new ArrayList<>();
            searchList.add(mSearchEdit.getText().toString().trim());
            searchList.add(GlobalConstants.NULL_DATA);
            searchList.add(GlobalConstants.NULL_DATA);
            searchList.add(GlobalConstants.NULL_DATA);
            searchList.add(GlobalConstants.RECORD_NO);
            searchList.add(String.valueOf(pageNum));
            searchList.add(String.valueOf(sortNum));
            searchList.add(mUserid);
            loadSearch(searchList);
        }


        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();

        PreorderItemListJsonResponse listJsonResponse = (PreorderItemListJsonResponse) successObject;
        if (listJsonResponse != null) {
            switch (listJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PreorderItemListData> arrayList = new ArrayList(listJsonResponse.getData());
                    Bundle bundle = new Bundle();
                    if (stringValue) {
                        bundle.putString(GlobalConstants.SEARCH_TEXT, mSearchEdit.getText().toString());
                    } else {
                        bundle.putString(GlobalConstants.SEARCH_TEXT, getSelectedString);
                    }
                    bundle.putString(GlobalConstants.PRODUCT_ACTIVITY, GlobalConstants.SEARCH_ACTIVITY_LIST);
                    bundle.putSerializable(GlobalConstants.SEARCH_DATA, arrayList);
                    SharedPreferences.Editor editor1 = context.getSharedPreferences(GlobalConstants.CHECK, MODE_PRIVATE).edit();
                    editor1.putBoolean(GlobalConstants.LOCKED, false).apply();

                    LaunchIntentManager.routeToActivityStackBundle(this, PreOrderActivityList.class, bundle);
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(context, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }

    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(context, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(this, InternetErrorCheck.class);
    }

}
