package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreorderItemDetailsProductreview implements Serializable {

    @SerializedName("Item")
    private String mItem;
    @SerializedName("Item_Feedback")
    private String mItemFeedback;
    @SerializedName("Item_Review")
    private String mItemReview;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Time")
    private String mTime;
    @SerializedName("USERID")
    private String mUserid;

    public String getmUserid() {
        return mUserid;
    }

    public String getItem() {
        return mItem;
    }

    public String getItemFeedback() {
        return mItemFeedback;
    }

    public String getItemReview() {
        return mItemReview;
    }

    public String getmDate() {
        return mDate;
    }

    public String getmTime() {
        return mTime;
    }



}
