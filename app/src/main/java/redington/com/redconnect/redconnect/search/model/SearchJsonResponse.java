
package redington.com.redconnect.redconnect.search.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;


public class SearchJsonResponse implements Serializable {

    @SerializedName("Data")
    private List<SearchData> mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public List<SearchData> getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }


}
