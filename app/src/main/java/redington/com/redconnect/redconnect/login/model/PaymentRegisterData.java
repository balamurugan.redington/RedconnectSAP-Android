
package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;


public class PaymentRegisterData {

    @SerializedName("Message")
    private String mMessage;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;
    @SerializedName("Status")
    private String mStatus;

    public String getMessage() {
        return mMessage;
    }


    public String getReferenceNumber() {
        return mReferenceNumber;
    }


    public String getStatus() {
        return mStatus;
    }


}
