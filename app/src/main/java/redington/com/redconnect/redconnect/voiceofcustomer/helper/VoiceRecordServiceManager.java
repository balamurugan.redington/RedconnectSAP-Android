package redington.com.redconnect.redconnect.voiceofcustomer.helper;

import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.voiceofcustomer.model.VoiceRecordJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VoiceRecordServiceManager {

    private VoiceRecordServiceManager() {
        throw new IllegalStateException(IntentConstants.CUSTOMERVOICE);
    }

    public static void getVoiceRecordServiceManager(String userId,
                                                    String media,
                                                    String mail,
                                                    String mobileNo,
                                                    String discrip,
                                                    String base64,
                                                    final Context context,
                                                    final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getVoiceRecordServic(getRequestBody(userId, media, mail, mobileNo, discrip, base64)).enqueue(new Callback<VoiceRecordJsonResponse>() {
                @Override
                public void onResponse(Call<VoiceRecordJsonResponse> call, Response<VoiceRecordJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<VoiceRecordJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }

    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(String userId, String media, String mail, String mobileNo,
                                              String discrip, String base64) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.USER_ID, userId);
            jsonValues.put(JsonKeyConstants.MEDIA_TYPE, media);
            jsonValues.put(JsonKeyConstants.MAIL_ID, mail);
            jsonValues.put(JsonKeyConstants.MOBILE_NUMBER, mobileNo);
            jsonValues.put(JsonKeyConstants.COMMENDS, discrip);
            jsonValues.put(JsonKeyConstants.VOICE_DATA, base64);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }

}
