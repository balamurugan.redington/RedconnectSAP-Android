package redington.com.redconnect.redconnect.capitalfloat.helper;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.capitalfloat.model.RegisterJbaResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.CapitalFloatRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JbaRegisterManager {

    private JbaRegisterManager() {
        throw new IllegalStateException(IntentConstants.CAPITAL_FLOAT);
    }

    public static void registerJbaCall(List<String> registerJbaList, final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = CapitalFloatRequest.getHeaderRequest(true, GlobalConstants.getCapitalFloatHeaders());
            RestClient.getInstance(loginHeader).getRegisterJba(getRequestBody(registerJbaList)).enqueue(new Callback<RegisterJbaResponse>() {
                @Override
                public void onResponse(Call<RegisterJbaResponse> call, Response<RegisterJbaResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<RegisterJbaResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });
        } else {
            listener.mError();
        }
    }

    private static RequestBody getRequestBody(List<String> registerJba) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JBA_ID, registerJba.get(0));
            jsonValues.put(JsonKeyConstants.JBA_EMAIL, registerJba.get(1));
            jsonValues.put(JsonKeyConstants.JBA_MOBILE, registerJba.get(2));
            jsonValues.put(JsonKeyConstants.JBA_NAME, registerJba.get(3));
            jsonValues.put(JsonKeyConstants.JBA_FLOAT, registerJba.get(4));
            jsonValues.put(JsonKeyConstants.JBA_ADDRESS, registerJba.get(5));
            jsonValues.put(JsonKeyConstants.JBA_CITY, registerJba.get(6));
            jsonValues.put(JsonKeyConstants.JBA_PINCODE, registerJba.get(7));

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
