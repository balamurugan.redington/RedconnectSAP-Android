package redington.com.redconnect.redconnect.preorder.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.activity.CreditBlockActivity;
import redington.com.redconnect.redconnect.payment.activity.PaymentScreen;
import redington.com.redconnect.redconnect.preorder.adapter.OrderConfirmationAdapter;
import redington.com.redconnect.redconnect.preorder.helper.CustomerEligibilityManager;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.redconnect.spot.helper.PromoCodeServiceManager;
import redington.com.redconnect.redconnect.spot.model.PromoCodeData;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.EmptyCartActivity;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.PermissionUtils;


public class ConfirmationActivity extends BaseActivity implements CartListener, UIListener,
        ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback, NewListener {

    ArrayList<String> permissions = new ArrayList<>();
    private TextView mTotalAmount;
    private TextView mTotalAmount1;
    private Context mContext;
    private CommonUtils commonUtils;
    private String stockLocation;
    private String paymentDays;
    private String deliverySequence;
    private String userID;
    private String getRemarks;


    private PermissionUtils mPermissionUtils;
    private boolean mSpotBoolean;


    private ArrayList<CartData> list;

    private String paymentOptions;
    private String spotNumber;
    private ArrayList<SubPromoCode> subPromoCodes;


    @Override
    protected void onCreate(Bundle saveInstantState) {
        super.onCreate(saveInstantState);
        setContentView(R.layout.activity_confirm_screen);

        mContext = ConfirmationActivity.this;
        commonUtils = new CommonUtils(mContext);

        mPermissionUtils = new PermissionUtils(mContext);
        permissions.add(Manifest.permission.SEND_SMS);
        permissions.add(Manifest.permission.RECEIVE_SMS);
        permissions.add(Manifest.permission.READ_SMS);

        TextView customerName = findViewById(R.id.customerName);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            deliverySequence = bundle.getString(IntentConstants.DELIVERY_SEQUENCE);
            stockLocation = bundle.getString(IntentConstants.LOCATION_CODE);
            paymentDays = bundle.getString(IntentConstants.PAYMENT_DAYS);
            String address1 = bundle.getString(IntentConstants.ADDRESS_1);
            String address2 = bundle.getString(IntentConstants.ADDRESS_2);
            String address3 = bundle.getString(IntentConstants.ADDRESS_3);

            String address4 = bundle.getString(IntentConstants.ADDRESS_4);
            String address5 = bundle.getString(IntentConstants.ADDRESS_5);
            String address6 = bundle.getString(IntentConstants.ADDRESS_6);
            paymentOptions = bundle.getString(IntentConstants.PAYOPTION);
            String custName = bundle.getString(IntentConstants.CUST_NAME);


            customerName.setText(custName);
            assert address4 != null;
            setBillingValues(address4, address5, address6);

            assert address1 != null;
            setDeliveryValues(address1, address2, address3);

        }


        mTotalAmount = findViewById(R.id.totAmt);
        mTotalAmount1 = findViewById(R.id.totAmt1);

        userID = GlobalConstants.GetSharedValues.getSpUserId();

        SharedPreferences preferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        mSpotBoolean = preferences.getBoolean(GlobalConstants.SPOT_BOOELAN, false);

        if (mSpotBoolean) {
            spotNumber = preferences.getString(GlobalConstants.SPOT_CODE, "");
            callPromoCodeApi();
        } else {
            getCartData();
        }

        findView();

    }

    /*Set Delivery Values*/
    private void setDeliveryValues(String address1, String address2, String address3) {

        TextView mDelAdress1 = findViewById(R.id.txt_delAdress1);
        TextView mDelAdress2 = findViewById(R.id.txt_delAdress2);
        TextView mDelAdress3 = findViewById(R.id.txt_delAdress3);

        if (!address1.isEmpty())
            mDelAdress1.setText(address1);
        else
            mDelAdress1.setVisibility(View.GONE);
        if (!address2.isEmpty())
            mDelAdress2.setText(address2);
        else
            mDelAdress2.setVisibility(View.GONE);

        if (!address3.isEmpty())
            mDelAdress3.setText(address3);
        else
            mDelAdress3.setVisibility(View.GONE);
    }

    /*Billing Values*/
    private void setBillingValues(String address4, String address5, String address6) {
        TextView mBillingAdress1 = findViewById(R.id.txt_billingAdress1);
        TextView mBillingAdress2 = findViewById(R.id.txt_billingAdress2);
        TextView mBillingAdress3 = findViewById(R.id.txt_billingAdress3);

        if (!address4.isEmpty()) {
            mBillingAdress1.setText(address4);
        } else {
            mBillingAdress1.setVisibility(View.GONE);
        }

        if (!address5.isEmpty()) {
            mBillingAdress2.setText(address5);
        } else {
            mBillingAdress2.setVisibility(View.GONE);
        }

        if (!address6.isEmpty()) {
            mBillingAdress3.setText(address6);
        } else {
            mBillingAdress3.setVisibility(View.GONE);
        }
    }

    /*Calculate Total Spot Amount*/
    @SuppressLint("SetTextI18n")
    private void totalAmountCalcSpot(ArrayList<SubPromoCode> subPromoCodes) {
        Double spotAmount = 0.00;
        int sizTot = subPromoCodes.size();
        for (int i = 0; i < sizTot; i++) {

            spotAmount += subPromoCodes.get(i).getmTOTAL();
        }

        mTotalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(spotAmount));
        mTotalAmount1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(spotAmount));
    }

    private void getCartData() {
        ArrayList<String> cartArrayList = new ArrayList<>();
        cartArrayList.add(userID);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.MODE_SELECT);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArrayList, mContext, this);
    }


    public void menuBack(View view) {
        setCashDiscount(GlobalConstants.NO);
        setMenuBack(view.getContext());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setCashDiscount(GlobalConstants.NO);
        overridePendingTransition(IntentConstants.ZERO_VALUE, R.anim.slide_down);
    }

    /*Pay now Button*/
    public void mCheckOut(View view) {
        logd(String.valueOf(view.getContext()));
        if (paymentOptions.equals(mContext.getString(R.string.pay_now))) {
            checkEligibilty();
        } else if (paymentOptions.equals(mContext.getString(R.string.skip_place))) {
            mPermissionUtils.checkPermission(permissions, GlobalConstants.RECEIVE_OTP, 1);
        }
    }


    /*Remarks Popup*/
    @SuppressLint("NewApi")
    private void showRemarks(final Context context) {
        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_remarks);
        mDialog.setCanceledOnTouchOutside(false);

        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);

        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(6.0f);
            mTotalLayout.setBackground(gradientDrawable);
        }

        final EditText mRemarks = mDialog.findViewById(R.id.ed_remarks);
        final TextView tRemarks = mDialog.findViewById(R.id.dialog_remarks);
        Button btnRemarks = mDialog.findViewById(R.id.btn_remarks);
        tRemarks.setText(GlobalConstants.DIALOG_REMARKS);

        btnRemarks.setOnClickListener(v -> {
            mDialog.dismiss();
            getRemarks = mRemarks.getText().toString();
            callPaymentMethod();

        });

        mDialog.show();
    }


    private void callPaymentMethod() {
        String totalamt = mTotalAmount.getText().toString();
        String overallAmount = totalamt.replace("₹ ", "");
        overallAmount = overallAmount.replace(",", "");

        Bundle bundle = new Bundle();
        bundle.putString(IntentConstants.AMOUNT, overallAmount);
        bundle.putString(IntentConstants.TAG, IntentConstants.PLACEORDER);
        bundle.putString(IntentConstants.REMARKS, getRemarks);
        bundle.putString(IntentConstants.STOCKLOCATION, stockLocation);
        bundle.putString(IntentConstants.PAYMENTDAYS_CARD, paymentDays);
        bundle.putString(IntentConstants.DELIVERYSEQUENCE_CARD, deliverySequence);
        bundle.putBoolean(IntentConstants.SPOTBOOLEAN, mSpotBoolean);
        bundle.putSerializable(IntentConstants.SPOTARRAY, subPromoCodes);
        bundle.putSerializable(IntentConstants.CARTARRAY, list);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, PaymentScreen.class, bundle);
    }


    /*Init Views*/
    private void findView() {
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        toolbar(IntentConstants.PLACEORDER);
    }


    /*Cart Response API*/
    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    cartResponse(cartJson);
                    break;
                case IntentConstants.FAILURE_URL:
                    setCartUpdate(0);
                    LaunchIntentManager.routeToActivity(mContext, EmptyCartActivity.class);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);

        }

    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);

    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);

    }

    /*Amount Validation and Response binding*/
    private void cartResponse(CartJsonResponse cartJson) {
        list = new ArrayList<>();
        setCartUpdate(list.size());
        /*9-1-19*/
        /*for (int i = 0; i < cartJson.getData().size(); i++) {
            list.addAll(cartJson.getData().get(i).getCart());
        }*/

        list.addAll(cartJson.getData().get(0).getCart());

        bindRecyclerview();

        calculateAmount(list);

        validateCashDiscount(list);
    }

    private void validateCashDiscount(ArrayList<CartData> cartData) {

        boolean statusCheck = false;
        String cdcStatus = GlobalConstants.GetSharedValues.getCashDiscount();
        for (int j = 0; j < cartData.size(); j++) {
            if (cdcStatus.equals(GlobalConstants.YES)) {
                String status = cartData.get(j).getCDCStatus();
                if (GlobalConstants.YES.equals(status)) {
                    statusCheck = true;
                    break;
                }
            }
        }

        if (cdcStatus.equals(GlobalConstants.YES) && !statusCheck) {
            shortToast(mContext, GlobalConstants.CASH_TOAST);
            setCashDiscount(GlobalConstants.NO);
            getCartData();
        }

    }

    @SuppressLint("SetTextI18n")
    private void calculateAmount(ArrayList<CartData> list) {

        Double totalAmount = 0.0d;

        for (int i = 0; i < list.size(); i++) {
            totalAmount += list.get(i).getmTotalValue();

        }

        mTotalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(totalAmount));
        mTotalAmount1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(totalAmount));

    }


    /*Recycler View*/
    private void bindRecyclerview() {
        RecyclerView mRecyclerView = findViewById(R.id.confirmScreen_Recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(verticalLayout);

        OrderConfirmationAdapter adapter;
        if (mSpotBoolean) {
            adapter = new OrderConfirmationAdapter(mContext, subPromoCodes, 1);
        } else {
            adapter = new OrderConfirmationAdapter(mContext, list, IntentConstants.CART);
        }
        mRecyclerView.setAdapter(adapter);
        LinearLayout layoutTotal = findViewById(R.id.li_tot);
        layoutTotal.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            if (mSpotBoolean) {
                callPromoCodeApi();
            } else {
                getCartData();
            }
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }


    /*Bottom Sheet For OTP*/
    private void moveToBlock() {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.PRICE, mTotalAmount.getText().toString());
        bundle.putString(IntentConstants.REMARKS, "");
        bundle.putString(IntentConstants.STOCKLOCATION, stockLocation);
        bundle.putString(IntentConstants.PAYMENTDAYS_CARD, paymentDays);
        bundle.putString(IntentConstants.DELIVERYSEQUENCE_CARD, deliverySequence);
        bundle.putBoolean(IntentConstants.SPOTBOOLEAN, mSpotBoolean);
        bundle.putSerializable(IntentConstants.SPOTARRAY, subPromoCodes);
        bundle.putSerializable(IntentConstants.CARTARRAY, list);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, CreditBlockActivity.class, bundle);
    }


    @SuppressLint("NewApi")
    private void alertDelete(Context context) {
        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);
        mDialog.setCanceledOnTouchOutside(false);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);
        TextView textSuccess = mDialog.findViewById(R.id.text_success);
        TextView textFailure = mDialog.findViewById(R.id.text_failure);

        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.dialog_failure));
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            textFailure.setBackground(gradientDrawable);
        }

        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));

        alertContent.setText(GlobalConstants.PLACE_SKIP_ORDER);
        textSuccess.setOnClickListener(v -> {
            moveToBlock();
            mDialog.dismiss();
        });

        textFailure.setOnClickListener(view -> mDialog.dismiss());

        mDialog.show();
    }

    /*Request Permission Result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        mPermissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void permissionGranted(int requestCode) {
        if (requestCode == 1) {
            alertDelete(mContext);
        }
    }

    /*Request Permission Results Method's*/
    @Override
    public void partialPermissionGranted(int requestCode, List<String> grantedPermissions) {
        if (requestCode == 1) {
            alertDelete(mContext);
        }
    }

    @Override
    public void permissionDenied(int requestCode) {
        if (requestCode == 1) {
            alertDelete(mContext);
        }
    }

    @Override
    public void neverAskAgain(int requestCode) {
        if (requestCode == 1) {
            alertDelete(mContext);
        }
    }


    private void callPromoCodeApi() {
        commonUtils.showProgressDialog();
        PromoCodeServiceManager.promoCodeServicecall(userID,
                GlobalConstants.GetSharedValues.getCartDelSeq(),
                "", "", "1", mContext, this);
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        PromoCodeJsonResponse promoCodeJsonResonse = (PromoCodeJsonResponse) successObject;
        if (promoCodeJsonResonse != null) {
            switch (promoCodeJsonResonse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PromoCodeData> promoCodeData = new ArrayList<>(promoCodeJsonResonse.getData());
                    for (int i = 0; i < promoCodeJsonResonse.getData().size(); i++) {
                        if (spotNumber.equals(promoCodeData.get(i).getPromoCode())) {
                            subPromoCodes = new ArrayList<>(promoCodeData.get(i).getSubPromoCode());
                        }
                    }
                    if (subPromoCodes != null) {
                        totalAmountCalcSpot(subPromoCodes);
                        bindRecyclerview();
                    } else {
                        noRecords(mContext, GlobalConstants.NO_RECORDS);
                    }

                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        logd(String.valueOf(failureObject));
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void checkEligibilty() {
        String mOrderValue = mTotalAmount.getText().toString();
        if (!mOrderValue.equals(GlobalConstants.SPACE)) {
            mOrderValue = mOrderValue.replace("₹ ", "");
            mOrderValue = mOrderValue.replace(",", "");
            commonUtils.showProgressDialog();
            CustomerEligibilityManager.getEligibilityCall(userID, Double.valueOf(mOrderValue), GlobalConstants.TWO, mContext, this);
        } else {
            shortToast(mContext, GlobalConstants.NO_DATA);
        }
    }


    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        eligibilityData(successObject);
    }


    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    private void eligibilityData(Object successObject) {
        CustomerEligibilityJsonResponse customerEligibilityJsonResponse = (CustomerEligibilityJsonResponse) successObject;
        if (customerEligibilityJsonResponse != null) {
            switch (customerEligibilityJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String status = customerEligibilityJsonResponse.getData().getStatus();
                    String mReason = customerEligibilityJsonResponse.getData().getReason();
                    switch (status) {
                        case GlobalConstants.STATUS_R:
                            showEligibilityPopup(mContext, mReason);
                            break;
                        case GlobalConstants.STATUS_A:
                            showRemarks(mContext);
                            break;
                        default:
                            shortToast(mContext, GlobalConstants.SERVER_ERROR);
                            break;
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, customerEligibilityJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    @SuppressLint("NewApi")
    private void showEligibilityPopup(Context context, final String reason) {

        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);

        TextView prefTitle = mDialog.findViewById(R.id.pre_ref_title);
        TextView prefcont = mDialog.findViewById(R.id.pre_ref_cont);
        Button prefcontbtm = mDialog.findViewById(R.id.pre_ref_btm);
        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        prefTitle.setVisibility(View.GONE);
        prefcont.setText(reason);


        prefcontbtm.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });

        mDialog.show();
    }
}
