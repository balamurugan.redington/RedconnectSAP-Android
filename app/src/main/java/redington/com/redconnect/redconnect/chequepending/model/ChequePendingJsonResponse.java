package redington.com.redconnect.redconnect.chequepending.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ChequePendingJsonResponse implements Serializable {

    @SerializedName("Data")
    private List<ChequePendingData> mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public List<ChequePendingData> getData() {
        return mData;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }


}
