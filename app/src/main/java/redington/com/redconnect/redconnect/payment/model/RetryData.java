
package redington.com.redconnect.redconnect.payment.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;


public class RetryData implements Serializable {

    @SerializedName("LstSucOrd")
    private List<LstSucOrd> mLstSucOrd;
    @SerializedName("mode")
    private String mMode;
    @SerializedName("PendOrd")
    private List<PendOrd> mPendOrd;

    public List<LstSucOrd> getLstSucOrd() {
        return mLstSucOrd;
    }

    public String getMode() {
        return mMode;
    }

    public List<PendOrd> getPendOrd() {
        return mPendOrd;
    }


}
