package redington.com.redconnect.redconnect.preorder.helper;


import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreorderItemListServiceManager {

    private PreorderItemListServiceManager() {
        throw new IllegalStateException(IntentConstants.PREORDER);
    }

    public static void getPreorderItemListServiceManager(List<String> searchList,
                                                         final Context context, final UIListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headRequest).getPreorderItemList(getRequestBody(searchList)).enqueue(new Callback<PreorderItemListJsonResponse>() {
                @Override
                public void onResponse(Call<PreorderItemListJsonResponse> call, Response<PreorderItemListJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<PreorderItemListJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });

        } else {
            listener.onError();
        }


    }

    private static RequestBody getRequestBody(List<String> searchList) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.SEARCH, searchList.get(0));
            jsonValues.put(JsonKeyConstants.PRECATEGORY, searchList.get(1));
            jsonValues.put(JsonKeyConstants.BRAND, searchList.get(2));
            jsonValues.put(JsonKeyConstants.ITEMCODE, searchList.get(3));
            jsonValues.put(JsonKeyConstants.RECORDNUM, searchList.get(4));
            jsonValues.put(JsonKeyConstants.PAGE_NO, searchList.get(5));
            jsonValues.put(JsonKeyConstants.SORT, searchList.get(6));
            jsonValues.put(JsonKeyConstants.CUSTOMERCODE, searchList.get(7));


            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
