package redington.com.redconnect.redconnect.helpdesk.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.helpdesk.helper.TicketFeedbackManager;
import redington.com.redconnect.redconnect.helpdesk.model.TicketFeedbackResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.smsgateway.constants.TemplateConstants;
import redington.com.redconnect.smsgateway.handler.HTTPHandler;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class HelpDeskActivityStatus extends BaseActivity implements UIListener {

    private EditText editFeedback;
    private EditText editMbileNumber;
    private EditText editDescription;
    private EditText editComment;

    private String ticketNum;
    private String userID;
    private String getFeedback;
    private String ratingBarValue = "0";
    private CommonUtils commonUtils;
    private ScrollView scrollView;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpdesk_status);

        hideKeyboard();

        mContext = HelpDeskActivityStatus.this;
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        scrollView = findViewById(R.id.scrollView);
        Button buttonStatus = findViewById(R.id.btn_ticket_status);

        editFeedback = findViewById(R.id.ed_feedback);
        editMbileNumber = findViewById(R.id.ed_mobileNumber);
        editDescription = findViewById(R.id.ed_description);
        editComment = findViewById(R.id.ed_comment);

        setImages();


        RelativeLayout rlRatingBar = findViewById(R.id.RL_ratingBar);
        RatingBar ratingBar = findViewById(R.id.ratingBar);


        TextView txtFeedback = findViewById(R.id.tv_feedback);
        TextView txtRatings = findViewById(R.id.tv_ratings);


        String mStatus = getIntent().getStringExtra(GlobalConstants.STATUS);
        ticketNum = getIntent().getStringExtra(GlobalConstants.TICKET_NUM);
        String mobNum = getIntent().getStringExtra(GlobalConstants.MOBILE_NUM);
        String desc = getIntent().getStringExtra(GlobalConstants.HP_DESC);
        String comment = getIntent().getStringExtra(GlobalConstants.HP_COMMNETS);
        String feedback = getIntent().getStringExtra(GlobalConstants.HP_FEED_BACK);
        String rating = getIntent().getStringExtra(GlobalConstants.HP_RATING_BAR);
        editMbileNumber.setText(mobNum);
        editDescription.setText(desc);
        editComment.setText(comment);

        /*Validation Part for Three Different Status*/
        if (mStatus.equalsIgnoreCase(GlobalConstants.CLOSED)) {
            buttonStatus.setVisibility(View.GONE);
            txtFeedback.setVisibility(View.VISIBLE);
            txtRatings.setVisibility(View.VISIBLE);
            rlRatingBar.setVisibility(View.VISIBLE);
            editFeedback.setVisibility(View.VISIBLE);
            ratingBar.setIsIndicator(true);

            if (rating.equals("")) {
                ratingBar.setRating(Float.parseFloat(" "));
            } else {
                ratingBar.setRating(Float.parseFloat(rating));
            }

            editComment.setFocusable(false);
            editFeedback.setFocusable(false);
            editFeedback.setText(feedback);

        } else if (mStatus.equalsIgnoreCase(GlobalConstants.COMPLETE)) {
            buttonStatus.setVisibility(View.VISIBLE);
            txtFeedback.setVisibility(View.VISIBLE);
            txtRatings.setVisibility(View.VISIBLE);
            rlRatingBar.setVisibility(View.VISIBLE);
            editFeedback.setVisibility(View.VISIBLE);
            ratingBar.setIsIndicator(false);
            editComment.setFocusable(false);
            editFeedback.setFocusable(true);

        } else {
            editComment.setFocusable(false);
        }


        ratingBar.setOnRatingBarChangeListener((rtBar, rating1, fromUser) -> {

            ratingBarValue = Float.toString(ratingBar.getRating());
            ratingBarValue = ratingBarValue.substring(0, 1);

        });

        commonUtils = new CommonUtils(HelpDeskActivityStatus.this);


        buttonStatus.setOnClickListener(v -> {
            getFeedback = editFeedback.getText().toString();

            if (getFeedback.length() == 0) {
                shortToast(mContext, GlobalConstants.ENTER_FEEDBACK);
            } else if (ratingBarValue.equals("0")) {
                shortToast(HelpDeskActivityStatus.this, GlobalConstants.ZERO_RATING_VALUE);
            } else {
                userID = GlobalConstants.GetSharedValues.getSpUserId();
                getTicketFeedback(userID, ticketNum, ratingBarValue, getFeedback);
            }
        });

    }

    private void setImages() {

        Drawable drawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_mobile);
        drawable.mutate().setColorFilter(ContextCompat.getColor(mContext, R.color.new_black_light), PorterDuff.Mode.SRC_IN);
        editMbileNumber.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        Drawable drawable1 = ContextCompat.getDrawable(mContext, R.mipmap.ic_description);
        drawable1.mutate().setColorFilter(ContextCompat.getColor(mContext, R.color.new_black_light), PorterDuff.Mode.SRC_IN);
        editDescription.setCompoundDrawablesWithIntrinsicBounds(drawable1, null, null, null);

        Drawable drawable2 = ContextCompat.getDrawable(mContext, R.mipmap.ic_comment);
        drawable2.mutate().setColorFilter(ContextCompat.getColor(mContext, R.color.new_black_light), PorterDuff.Mode.SRC_IN);
        editComment.setCompoundDrawablesWithIntrinsicBounds(drawable2, null, null, null);

        Drawable drawable3 = ContextCompat.getDrawable(mContext, R.mipmap.ic_feedback);
        drawable3.mutate().setColorFilter(ContextCompat.getColor(mContext, R.color.new_black_light), PorterDuff.Mode.SRC_IN);
        editFeedback.setCompoundDrawablesWithIntrinsicBounds(drawable3, null, null, null);

        toolbar(IntentConstants.HELPDESKSTATUS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            getTicketFeedback(userID, ticketNum, ratingBarValue, getFeedback);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }

    }

    public void menuBack(View v) {
        setMenuBack(v.getContext());
    }

    private void getTicketFeedback(String userID, String ticketNum, String getRating, String getFeedback) {
        commonUtils.showProgressDialog();
        TicketFeedbackManager.getTicketFeedbackCall(userID, ticketNum, getRating, getFeedback, HelpDeskActivityStatus.this, this);

    }

    /*Response From API*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        TicketFeedbackResponse ticketFeedbackResponse = (TicketFeedbackResponse) successObject;
        if (ticketFeedbackResponse != null) {
            switch (ticketFeedbackResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    callMessageAPI();
                    shortToast(mContext, GlobalConstants.FEEDBACK);
                    LaunchIntentManager.routeToActivity(HelpDeskActivityStatus.this, DashboardActivity.class);
                    break;
                case IntentConstants.FAILURE_URL:
                    Snackbar snackbar = Snackbar.make(scrollView, GlobalConstants.NO_FEEDBACK, Snackbar.LENGTH_LONG);
                    snackbar.show();
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }

    }


    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void callMessageAPI() {

        runOnUiThread(new SMSGateway()::execute);

    }


    @SuppressLint("StaticFieldLeak")
    private class SMSGateway extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            HTTPHandler handler = new HTTPHandler();
            String url = TemplateConstants.BASE_URL + TemplateConstants.MOBILENUM + TemplateConstants.MESSAGE +
                    TemplateConstants.TICKETFEEDBACK + TemplateConstants.PIPESYMBOL + TemplateConstants.TEAM
                    + TemplateConstants.SYMBOL + ticketNum;
            String jsonString = handler.makeServiceCall(url);
            logd(jsonString);
            return null;
        }


    }


}

