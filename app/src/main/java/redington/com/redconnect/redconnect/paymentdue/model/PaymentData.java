
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PaymentData implements Serializable {

    @SerializedName("Detail")
    private List<PaymentDueDetail> mDetail;
    @SerializedName("Header")
    private List<PaymentDueHeader> mHeader;

    public List<PaymentDueDetail> getDetail() {
        return mDetail;
    }


    public List<PaymentDueHeader> getHeader() {
        return mHeader;
    }



}
