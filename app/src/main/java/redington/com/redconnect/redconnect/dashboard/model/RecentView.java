
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class RecentView implements Serializable {

    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("Brand")
    private String mBrand;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("CategoryDesc")
    private String mCategoryDesc;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;
    @SerializedName("HeaderName")
    private String mHeaderName;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDesc")
    private String mItemDesc;
    @SerializedName("MajorCode")
    private String mMajorCode;
    @SerializedName("MinorCode")
    private String mMinorCode;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("Source")
    private String mSource;
    @SerializedName("Stock")
    private String mStock;
    @SerializedName("VendorCode")
    private String mVendorCode;

    public String getActualPrice() {
        return CommonMethod.getValidDoubleString(mActualPrice);
    }

    public String getBrand() {
        return mBrand;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getCategoryDesc() {
        return mCategoryDesc;
    }

    public String getCustomerCode() {
        return mCustomerCode;
    }

    public String getDiscountPercent() {
        return CommonMethod.getValidDoubleString(mDiscountPercent);
    }

    public String getDiscountPrice() {
        return CommonMethod.getValidDoubleString(mDiscountPrice);
    }

    public String getHeaderName() {
        return mHeaderName;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public String getItemDesc() {
        return mItemDesc;
    }

    public String getMajorCode() {
        return mMajorCode;
    }

    public String getMinorCode() {
        return mMinorCode;
    }

    public String getProductName() {
        return mProductName;
    }

    public String getSource() {
        return mSource;
    }

    public String getStock() {
        return mStock;
    }

    public String getVendorCode() {
        return mVendorCode;
    }


}
