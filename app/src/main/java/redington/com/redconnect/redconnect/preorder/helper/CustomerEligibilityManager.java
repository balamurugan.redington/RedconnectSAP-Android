package redington.com.redconnect.redconnect.preorder.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerEligibilityManager {

    private CustomerEligibilityManager() {
        throw new IllegalStateException(IntentConstants.CUSTOMER_ELIGIBILITY);
    }

    public static void getEligibilityCall(String userID, Double amount, String flag, final Context context,
                                          final NewListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getCustomerEligible(getRequestBody(userID, amount, flag))
                    .enqueue(new Callback<CustomerEligibilityJsonResponse>() {
                        @Override
                        public void onResponse(Call<CustomerEligibilityJsonResponse> call, Response<CustomerEligibilityJsonResponse> response) {
                            listener.mSuccessObject(response.body());
                        }

                        @Override
                        public void onFailure(Call<CustomerEligibilityJsonResponse> call, Throwable t) {
                            listener.mFailureObject(t.toString());
                        }
                    });
        } else {
            listener.mError();
        }

    }

    private static RequestBody getRequestBody(String userID, Double amount, String flag) {
        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.USER_ID, userID);
            jsonValues.put(JsonKeyConstants.ORDER_VALUE, amount);
            jsonValues.put(JsonKeyConstants.FLAG, flag);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());

        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
