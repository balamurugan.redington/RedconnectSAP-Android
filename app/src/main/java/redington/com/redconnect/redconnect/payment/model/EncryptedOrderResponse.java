package redington.com.redconnect.redconnect.payment.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EncryptedOrderResponse implements Serializable {

    @SerializedName("Data")
    private String mData;

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }
}
