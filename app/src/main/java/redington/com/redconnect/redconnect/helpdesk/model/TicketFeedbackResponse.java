package redington.com.redconnect.redconnect.helpdesk.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TicketFeedbackResponse implements Serializable {

    @SerializedName("InvoiceCommitData")
    private TicketFeedbackData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public TicketFeedbackData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
