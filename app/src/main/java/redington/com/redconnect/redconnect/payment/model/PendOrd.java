package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class PendOrd implements Serializable {

    @SerializedName("TransactionID")
    private String mTransactionID;
    @SerializedName("weborderNo")
    private String mWeborderNo;
    @SerializedName("ReferenceNo")
    private String mReferenceNo;

    public String getTransactionID() {
        return mTransactionID;
    }

    public String getWeborderNo() {
        return mWeborderNo;
    }

    public String getReferenceNo() {
        return mReferenceNo;
    }


}
