package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InvoiceCommitJsonResponse implements Serializable {

    @SerializedName("Data")
    private InvoiceCommitData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public static InvoiceCommitJsonResponse fromString(String result) {
        return new Gson().fromJson(result, InvoiceCommitJsonResponse.class);
    }

    public InvoiceCommitData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
