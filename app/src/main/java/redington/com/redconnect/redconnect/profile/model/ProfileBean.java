package redington.com.redconnect.redconnect.profile.model;


public class ProfileBean {
    private final String text1;
    private final int image;

    public ProfileBean(String text1, int image) {
        this.text1 = text1;
        this.image = image;

    }

    public String getText1() {
        return text1;
    }


    public int getImage() {
        return image;
    }
}
