package redington.com.redconnect.redconnect.dashboard.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.adapter.ScrollListener;
import redington.com.redconnect.redconnect.dashboard.adapter.EdmAdapter;
import redington.com.redconnect.redconnect.dashboard.adapter.ViewAllAdapter;
import redington.com.redconnect.redconnect.dashboard.model.Deal;
import redington.com.redconnect.redconnect.dashboard.model.EDMData;
import redington.com.redconnect.redconnect.dashboard.model.TopSelling;
import redington.com.redconnect.redconnect.ratingfeedback.helper.FeedbackServiceCall;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackData;
import redington.com.redconnect.redconnect.ratingfeedback.model.FeedbackJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.GridSpaceItemDecoration;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class ViewAllActivity extends BaseActivity implements UIListener {

    private Context mContext;
    private ArrayList<TopSelling> topSellings;
    private ArrayList<Deal> deals;
    private ArrayList<EDMData> edmImages;
    private String comingString;
    private CommonUtils commonUtils;
    private int currentPage = 1;
    private String userId;
    private RelativeLayout mRelativeLayout;
    private FrameLayout mProgressBar;
    private RecyclerView mRecyclerview;
    private GridLayoutManager layoutManager;
    private ArrayList<FeedbackData> feedbackArrayList;
    private boolean isLoading = false;
    private int mTotalPage;
    private EdmAdapter edmAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder);

        mContext = ViewAllActivity.this;
        commonUtils = new CommonUtils(mContext);
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);
        mRelativeLayout = findViewById(R.id.drawer);

        userId = GlobalConstants.GetSharedValues.getSpUserId();
        mProgressBar = findViewById(R.id.main_progress);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerview = findViewById(R.id.recycle_view);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            comingString = bundle.getString(GlobalConstants.COMING_FROM);
            String toolBarTitle = bundle.getString(GlobalConstants.TOOLBAR_TITLE);
            toolbar(toolBarTitle);
            switch (comingString) {
                case GlobalConstants.MTOPSELLING:
                    topSellings = (ArrayList<TopSelling>) bundle.getSerializable(GlobalConstants.RECENT_TOP_SELLINGS);
                    bindRecyclerview();
                    break;
                case GlobalConstants.MDEALS:
                    deals = (ArrayList<Deal>) bundle.getSerializable(GlobalConstants.RECENT_DEALS);
                    bindRecyclerview();
                    break;
                case GlobalConstants.M_EDM:
                    edmImages = (ArrayList<EDMData>) bundle.getSerializable(GlobalConstants.RECENTEDM);
                    bindRecyclerview();
                    break;
                case IntentConstants.PURCHASED:
                    callFeedbackApi();
                    break;

                default:
                    //No data
                    break;
            }
        }

    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    private void callFeedbackApi() {
        commonUtils.showProgressDialog();
        feedbackValues(userId, GlobalConstants.RECORD_NO, currentPage);
    }

    private void feedbackValues(String userId, String recordNo, int currentPage) {
        FeedbackServiceCall.getFeedBackCall(userId, recordNo, String.valueOf(currentPage), mContext, this);
    }

    private void bindRecyclerview() {
        if (!comingString.equals(IntentConstants.PURCHASED)) {
            mRecyclerview.setHasFixedSize(true);
            layoutManager = new GridLayoutManager(getApplicationContext(), 2);
            mRecyclerview.setLayoutManager(layoutManager);
            switch (comingString) {
                case GlobalConstants.MTOPSELLING:
                    ViewAllAdapter viewAllAdapter = new ViewAllAdapter(mContext, topSellings, 1);
                    mRecyclerview.setAdapter(viewAllAdapter);
                    break;
                case GlobalConstants.MDEALS:
                    ViewAllAdapter viewAllAdapter1 = new ViewAllAdapter(mContext, deals, comingString);
                    mRecyclerview.setAdapter(viewAllAdapter1);
                    break;
                case GlobalConstants.M_EDM:
                    edmAdapter = new EdmAdapter(mContext, edmImages, GlobalConstants.M_EDM);
                    mRecyclerview.setAdapter(edmAdapter);
                    break;
                default:
                    //No data
                    break;
            }

            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
            mRecyclerview.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
        } else if (comingString.equals(IntentConstants.PURCHASED)) {
            if (currentPage == 1) {
                mRecyclerview.setHasFixedSize(true);
                layoutManager = new GridLayoutManager(getApplicationContext(), 2);
                mRecyclerview.setLayoutManager(layoutManager);
                edmAdapter = new EdmAdapter(mContext, feedbackArrayList, 1);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                mRecyclerview.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                mRecyclerview.setAdapter(edmAdapter);
            } else {
                edmAdapter.feedbackupdate(feedbackArrayList);

            }

            edmAdapter.notifyDataSetChanged();

            mRecyclerview.addOnScrollListener(new ScrollListener(layoutManager) {
                @Override
                protected void loadMoreItems() {
                    isLoading = true;
                    currentPage += 1;
                    mRecyclerview.removeOnScrollListener(this);
                    mProgressBar.setVisibility(View.VISIBLE);
                    loadNextPage();
                }

                @Override
                public int getTotalPageCount() {
                    return mTotalPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            });

        }

    }

    /*Api Response*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        FeedbackJsonResponse feedbackJsonResponse = (FeedbackJsonResponse) successObject;
        if (feedbackJsonResponse != null) {
            switch (feedbackJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    feedbackArrayList = new ArrayList(feedbackJsonResponse.getData());
                    mTotalPage = Integer.parseInt(feedbackArrayList.get(0).getTotalPage());
                    bindRecyclerview();
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    if (currentPage == 1) {
                        noRecords(mContext, GlobalConstants.SERVER_ERR);
                    } else {
                        callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
                    }
                    break;
            }
        } else {
            if (currentPage == 1) {
                noRecords(mContext, GlobalConstants.SERVER_ERR);
            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
            }
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        mProgressBar.setVisibility(View.GONE);
        commonUtils.dismissProgressDialog();
        if (currentPage == 1) {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        } else {
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
        }
    }

    @Override
    public void onError() {
        mProgressBar.setVisibility(View.GONE);
        commonUtils.dismissProgressDialog();
        if (currentPage == 1) {
            LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
        } else {
            callSnackBar(GlobalConstants.INTERNET_ERROR);
        }
    }

    private void callSnackBar(String snackString) {
        Snackbar snackbar = Snackbar
                .make(mRelativeLayout, snackString, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    mProgressBar.setVisibility(View.VISIBLE);
                    feedbackValues(userId, GlobalConstants.RECORD_NO, currentPage);

                });
        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

    private void loadNextPage() {
        isLoading = false;
        if (currentPage <= mTotalPage) {
            feedbackValues(userId, GlobalConstants.RECORD_NO, currentPage);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            callFeedbackApi();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IntentConstants.EDM_FEEDBACK_INTENT && resultCode == Activity.RESULT_OK && data != null) {
            String result = data.getStringExtra(IntentConstants.EDM_FEEDBACK_RESULT);
            BaseActivity.shortToast(mContext, result);
        }
    }
}
