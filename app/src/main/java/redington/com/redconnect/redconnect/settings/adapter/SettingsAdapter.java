package redington.com.redconnect.redconnect.settings.adapter;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.ViewAllActivity;
import redington.com.redconnect.redconnect.login.activity.LoginActivity;
import redington.com.redconnect.redconnect.onboard.activity.TermsAndCondition;
import redington.com.redconnect.redconnect.preorder.activity.TotalReviews;
import redington.com.redconnect.redconnect.preorder.model.PreOrderBean;
import redington.com.redconnect.redconnect.profile.helper.ChangePasswordManager;
import redington.com.redconnect.redconnect.profile.model.ChangePasswordJsonResponse;
import redington.com.redconnect.redconnect.settings.dialog.CareBottomSheet;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

import static redington.com.redconnect.common.activity.BaseActivity.SP_CUSTOMER;
import static redington.com.redconnect.common.activity.BaseActivity.SP_CUSTOMERNAME;
import static redington.com.redconnect.common.activity.BaseActivity.SP_EMAIL_ID;
import static redington.com.redconnect.common.activity.BaseActivity.SP_LOGGED_IN;
import static redington.com.redconnect.common.activity.BaseActivity.SP_MAIL;
import static redington.com.redconnect.common.activity.BaseActivity.SP_PASSWORD;
import static redington.com.redconnect.common.activity.BaseActivity.SP_PH_NO;
import static redington.com.redconnect.common.activity.BaseActivity.SP_USER_REMEMBER;
import static redington.com.redconnect.common.activity.BaseActivity.billingaddressSeq;
import static redington.com.redconnect.common.activity.BaseActivity.cartDeliverySequence;
import static redington.com.redconnect.common.activity.BaseActivity.cashDiscount;
import static redington.com.redconnect.common.activity.BaseActivity.logd;
import static redington.com.redconnect.common.activity.BaseActivity.shortToast;
import static redington.com.redconnect.common.activity.BaseActivity.sp;

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsHolder> implements View.OnClickListener, UIListener {
    private Context mContext;
    private List<PreOrderBean> settingsList;
    private EditText oldPassword;
    private EditText newPassword;
    private ImageView imageShowOldPwd;
    private ImageView imaghideOldpwd;
    private ImageView imageshowNewpwd;
    private ImageView imagehideNewpwd;
    private String getOldPassword;
    private String getNewPassword;
    private String userID;
    private CommonUtils commonUtils;
    private Intent goToMarket;

    public SettingsAdapter(Context context, List<PreOrderBean> list) {
        this.mContext = context;
        this.settingsList = list;
        this.commonUtils = new CommonUtils(context);
    }

    @NonNull
    @Override
    public SettingsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_list, parent, false);
        return new SettingsAdapter.SettingsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsHolder holder, int position) {
        userID = GlobalConstants.GetSharedValues.getSpUserId();
        holder.mTextview.setText(settingsList.get(position).getName());
        holder.mTextviewVersion.setText(settingsList.get(position).getName1());

        Picasso.with(mContext).load(settingsList.get(position).getImages()).into(holder.mImageSettings);

        final int pos = holder.getAdapterPosition();
        holder.totalLayout.setOnClickListener(v -> {
            switch (pos) {
                case 0:/*Apple Care*/
                    //showDeviceId();
                    logd("Success");
                    break;
                case 1:/*Change Password*/
                    changePassword();
                    break;
                case 2: /*App Version*/
                    break;
                case 3:/*Rate our app*/
                    rateApp();
                    break;
                case 4:/*Report a Problem*/
                    Bundle bundle1 = new Bundle();
                    bundle1.putString(GlobalConstants.COMING_FROM, IntentConstants.PURCHASED);
                    bundle1.putString(GlobalConstants.TOOLBAR_TITLE, IntentConstants.PURCHASED);
                    LaunchIntentManager.routeToActivityStackBundle(mContext, ViewAllActivity.class, bundle1);
                    break;
                case 5:
                    /*Policies*/
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalConstants.TERMS_DATA, "Profile");
                    LaunchIntentManager.routeToActivityStackBundle(mContext, TermsAndCondition.class, bundle);
                    break;

                case 6:/*Customer Care*/
                    showBottomSheet();
                    break;

                case 7:/*Logout*/
                    logout();
                    break;

                default:
                    //No data
                    break;
            }
        });
    }

    private void showBottomSheet() {
        CareBottomSheet bottomSheetDialog = CareBottomSheet.getInstance();
        bottomSheetDialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), GlobalConstants.CUSTOM_BOTTOM_SHEET);
    }


    private void showDeviceId() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.show_device_id);

        EditText imeiNumber = dialog.findViewById(R.id.ed_imei_number);
        EditText deviceId = dialog.findViewById(R.id.ed_device_id);
        Button buttonSubmit = dialog.findViewById(R.id.btn_ok);

        buttonSubmit.setOnClickListener(v -> {
            String mImeiNumber = imeiNumber.getText().toString().trim();
            String mDeviceId = deviceId.getText().toString().trim();
            if (mImeiNumber.equals("") && !mDeviceId.equals("")) {
                shortToast(mContext, GlobalConstants.SUCCESS);
            } else if (mDeviceId.equals("") && !mImeiNumber.equals("")) {
                shortToast(mContext, GlobalConstants.SERVER_ERROR);
            } else if (mImeiNumber.equals("") && mDeviceId.equals("")) {
                shortToast(mContext, mContext.getString(R.string.invalid_id));
            } else if (!mImeiNumber.equals("") && !mDeviceId.equals("")) {
                shortToast(mContext, mContext.getString(R.string.imei_or_device));
            }
        });

        dialog.show();
    }

    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            mContext.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            shortToast(mContext, GlobalConstants.MARKET_APP);
        }
    }

    private void pdf() {
        Bundle bundle = new Bundle();
        bundle.putString(IntentConstants.PDF, IntentConstants.PDF_NAME);
        LaunchIntentManager.routeToActivityStackBundle(mContext, TotalReviews.class, bundle);
    }

    /*Change Password Popup */
    private void changePassword() {

        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_change_password_new);
        mDialog.setCanceledOnTouchOutside(false);

        oldPassword = mDialog.findViewById(R.id.edt_oldPassword);
        oldPassword.setPadding(15, 0, 0, 0);
        oldPassword.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        imageShowOldPwd = mDialog.findViewById(R.id.im_show_oldpwd);
        imaghideOldpwd = mDialog.findViewById(R.id.im_hide_oldpwd);
        newPassword = mDialog.findViewById(R.id.edt_newPassword);
        newPassword.setPadding(15, 0, 0, 0);
        newPassword.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        imageshowNewpwd = mDialog.findViewById(R.id.im_show_newpwd);
        imagehideNewpwd = mDialog.findViewById(R.id.im_hide_newpwd);
        Button mChangePassword = mDialog.findViewById(R.id.change_password);

        oldPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        newPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});

        imageShowOldPwd.setOnClickListener(this);
        imaghideOldpwd.setOnClickListener(this);
        imageshowNewpwd.setOnClickListener(this);
        imagehideNewpwd.setOnClickListener(this);

        mChangePassword.setOnClickListener(v -> {
            getOldPassword = oldPassword.getText().toString();
            getNewPassword = newPassword.getText().toString();

            if (getOldPassword.isEmpty() && getOldPassword.trim().equals("")) {
                BaseActivity.shortToast(mContext, GlobalConstants.OLD_PASS);
                oldPassword.requestFocus();

            } else if (getNewPassword.isEmpty() && getNewPassword.trim().equals("")) {
                BaseActivity.shortToast(mContext, GlobalConstants.NEW_PASS);
                newPassword.requestFocus();

            } else {
                changePasswordValues(userID, getOldPassword, getNewPassword);
            }
        });

        mDialog.show();

    }

    /*API Call*/
    private void changePasswordValues(String userid, String getOldPassword, String getNewPassword) {
        commonUtils.showProgressDialog();
        ChangePasswordManager.getChangePwdServiceCall(userid, getOldPassword, getNewPassword, mContext, this);
    }

    /*Logout popup*/
    private void logout() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(GlobalConstants.LOGOUT)
                .setMessage(GlobalConstants.WANTLOGOUT)
                .setPositiveButton(GlobalConstants.Y, (dialog, which) -> {

                    clearSharedValues();
                    if (sp.getString(SP_USER_REMEMBER, GlobalConstants.REMEMBER).equals(GlobalConstants.NO)) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean(SP_LOGGED_IN, false);
                        editor.putString(SP_EMAIL_ID, GlobalConstants.EMAIL);
                        editor.putString(SP_PASSWORD, GlobalConstants.PASS_WORD);
                        editor.putString(SP_CUSTOMER, GlobalConstants.CUSTOMER);
                        editor.putString(SP_CUSTOMERNAME, GlobalConstants.CUSTOMER_NAME);
                        editor.putString(SP_MAIL, GlobalConstants.MAIL);
                        editor.putString(SP_PH_NO, GlobalConstants.PHONE_NUMBER);
                        editor.clear();
                        editor.apply();
                        dialog.cancel();
                        setCartUpdate();
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                    } else {
                        setCartUpdate();
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean(SP_LOGGED_IN, false);
                        editor.apply();
                        dialog.cancel();
                        Intent in = new Intent(mContext, LoginActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(in);

                    }
                })
                .setNegativeButton(GlobalConstants.N, (dialog, which) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void clearSharedValues() {
        SharedPreferences.Editor editor1 = cartDeliverySequence.edit();
        SharedPreferences.Editor edit = billingaddressSeq.edit();
        SharedPreferences.Editor edit1 = cashDiscount.edit();
        edit.clear().apply();
        edit1.clear().apply();
        editor1.clear().apply();
    }

    @Override
    public int getItemCount() {
        return settingsList.size();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.im_show_oldpwd) {
            int oldPwdLength = oldPassword.getText().length();
            if (oldPwdLength > 0) {
                oldPassword.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                oldPassword.setSelection(oldPassword.getText().length());
                imageShowOldPwd.setVisibility(View.GONE);
                imaghideOldpwd.setVisibility(View.VISIBLE);
            }

        } else if (i == R.id.im_hide_oldpwd) {
            oldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
                    | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
            oldPassword.setSelection(oldPassword.getText().length());
            imageShowOldPwd.setVisibility(View.VISIBLE);
            imaghideOldpwd.setVisibility(View.GONE);

        } else if (i == R.id.im_show_newpwd) {
            int newPwdLength = newPassword.getText().length();
            if (newPwdLength > 0) {
                newPassword.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                newPassword.setSelection(newPassword.getText().length());
                imageshowNewpwd.setVisibility(View.GONE);
                imagehideNewpwd.setVisibility(View.VISIBLE);
            }


        } else if (i == R.id.im_hide_newpwd) {
            newPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
                    | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
            newPassword.setSelection(newPassword.getText().length());
            imageshowNewpwd.setVisibility(View.VISIBLE);
            imagehideNewpwd.setVisibility(View.GONE);

        }
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        ChangePasswordJsonResponse changePasswordJsonResponse = (ChangePasswordJsonResponse) successObject;
        if (changePasswordJsonResponse != null) {
            switch (changePasswordJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    BaseActivity.shortToast(mContext, GlobalConstants.CHANGE_PASS_RESPONSE);
                    SharedPreferences.Editor editor = BaseActivity.sp.edit();
                    editor.clear().apply();
                    clearSharedValues();
                    LaunchIntentManager.routeToActivity(mContext, LoginActivity.class);
                    break;
                case IntentConstants.FAILURE_URL:
                    BaseActivity.shortToast(mContext, changePasswordJsonResponse.getDescription());
                    break;
                default:
                    BaseActivity.shortToast(mContext, GlobalConstants.CONNECT_ERROR);
                    break;
            }
        } else {
            BaseActivity.shortToast(mContext, GlobalConstants.CONNECT_ERROR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(mContext, GlobalConstants.CONNECT_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    private void setCartUpdate() {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(GlobalConstants.SP_CART_COUNT, Context.MODE_PRIVATE).edit();
        editor.putInt(GlobalConstants.SP_CART_COUNT, 0);
        editor.apply();
    }

    class SettingsHolder extends RecyclerView.ViewHolder {
        private LinearLayout totalLayout;
        private TextView mTextview;
        private TextView mTextviewVersion;
        private ImageView mImageSettings;

        SettingsHolder(View itemView) {
            super(itemView);
            totalLayout = itemView.findViewById(R.id.LL_recycleView);
            mTextview = itemView.findViewById(R.id.text_settings);
            mTextviewVersion = itemView.findViewById(R.id.text_version);
            mImageSettings = itemView.findViewById(R.id.settings_image);

        }
    }

}
