package redington.com.redconnect.redconnect.preorder.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.adapter.PreOrderSubCategoryAdapter;
import redington.com.redconnect.redconnect.preorder.helper.PreorderCategoryServiceManager;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryData;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class ProductSubCategoryActivity extends BaseActivity implements UIListener {

    private CommonUtils commonUtils;
    private Context mContext;
    private String category;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preorder);

        mContext = ProductSubCategoryActivity.this;
        commonUtils = new CommonUtils(mContext);
        userId = GlobalConstants.GetSharedValues.getSpUserId();
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);

        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            category = bundle.getString(GlobalConstants.CATEGORY);
            String status = bundle.getString(GlobalConstants.STATUS);
            toolbar(status);
            bindActivity(GlobalConstants.API_DASHBOARD_MENULIST, category, userId);
        }

    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }


    private void bindActivity(String category, String brand, String userId) {
        commonUtils.showProgressDialog();
        PreorderCategoryServiceManager.getPreorderCatServiceCall(category, brand, userId, mContext, this);
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        PreorderCategoryJsonResponse preorderCategoryJsonResponse = (PreorderCategoryJsonResponse) successObject;
        if (preorderCategoryJsonResponse != null) {
            switch (preorderCategoryJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<PreorderCategoryData> arrayList = new ArrayList(preorderCategoryJsonResponse.getData().getCategory());
                    RecyclerView recyclerView = findViewById(R.id.recycle_view);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
                    recyclerView.setAdapter(new PreOrderSubCategoryAdapter(mContext, arrayList));
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
        finish();
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(this, InternetErrorCheck.class);

    }

    /*On Resume Method*/
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            bindActivity(GlobalConstants.API_DASHBOARD_MENULIST, category, userId);
        }
        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }

    }
}
