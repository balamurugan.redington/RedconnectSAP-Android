package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;

public class CityData {

    @SerializedName("CityDescription")
    private String mCityDescription;
    @SerializedName("CityMode")
    private String mCityMode;
    @SerializedName("State")
    private String mState;

    public String getCityDescription() {
        return mCityDescription;
    }

    public void setCityDescription(String cityDescription) {
        mCityDescription = cityDescription;
    }

    public String getCityMode() {
        return mCityMode;
    }

    public void setCityMode(String cityMode) {
        mCityMode = cityMode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

}
