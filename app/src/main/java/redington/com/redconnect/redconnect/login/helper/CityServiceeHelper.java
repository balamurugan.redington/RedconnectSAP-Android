package redington.com.redconnect.redconnect.login.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.login.model.CityJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityServiceeHelper {
    
    private CityServiceeHelper() {
        throw new IllegalStateException(IntentConstants.CUSTOMERPROFILE);
    }

    public static void getCityServiceCall(String mCity,
                                          final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getCityCall(getRequestBody(mCity))
                    .enqueue(new Callback<CityJsonResponse>() {
                        @Override
                        public void onResponse(Call<CityJsonResponse> call, Response<CityJsonResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<CityJsonResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });
        } else {
            listener.onError();
        }
    }


    private static RequestBody getRequestBody(String mCity) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.PRMT, mCity);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
