package redington.com.redconnect.redconnect.preorder.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.preorder.model.OtpJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpServiceManager {

    private OtpServiceManager() {
        throw new IllegalStateException(GlobalConstants.OTP);
    }

    public static void otpServiceCall(String refNum, String otpNum, String orderValue, String flag,
                                      final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).postOtpCall(getRequestBody(refNum, otpNum, orderValue, flag)).enqueue(new Callback<OtpJsonResponse>() {
                @Override
                public void onResponse(Call<OtpJsonResponse> call, Response<OtpJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<OtpJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(String refNum, String otpNum, String orderValue, String flag) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.REFNUMBER, refNum);
            jsonValues.put(JsonKeyConstants.OTP, otpNum);
            jsonValues.put(JsonKeyConstants.ORDERVALUE, orderValue);
            jsonValues.put(JsonKeyConstants.FLAG, flag);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }

}
