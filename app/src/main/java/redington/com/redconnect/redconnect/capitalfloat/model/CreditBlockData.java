
package redington.com.redconnect.redconnect.capitalfloat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class CreditBlockData implements Serializable {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("order_id")
    private Object mOrderId;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("tranche_id")
    private Object mTrancheId;

    public String getMessage() {
        return mMessage;
    }

    public Object getOrderId() {
        return mOrderId;
    }

    public String getStatus() {
        return mStatus;
    }

    public Object getTrancheId() {
        return mTrancheId;
    }

}
