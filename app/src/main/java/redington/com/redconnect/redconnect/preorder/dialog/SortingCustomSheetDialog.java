package redington.com.redconnect.redconnect.preorder.dialog;


import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.preorder.activity.PreOrderActivityList;
import redington.com.redconnect.redconnect.preorder.adapter.SortingAdapter;
import redington.com.redconnect.redconnect.preorder.model.SortingBean;
import redington.com.redconnect.util.RecyclerItemSelectListener;

public class SortingCustomSheetDialog extends BottomSheetDialogFragment {

    private int sortNo;
    SortingAdapter adapter;
    RecyclerView recyclerView;


    public static SortingCustomSheetDialog getInstance() {
        return new SortingCustomSheetDialog();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.sorting_bottom_sheet, viewGroup, false);

        sortNo = getArguments().getInt(GlobalConstants.SORT_NO);

        recyclerView = v.findViewById(R.id.sort_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager verticalLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(verticalLayout);
        ArrayList<SortingBean> sortingBeen = prepareData();
        adapter = new SortingAdapter(getActivity(), sortingBeen, sortNo);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(getActivity(),
                verticalLayout.getOrientation());
        recyclerView.addItemDecoration(mDividerItemDecoration);

        recyclerView.addOnItemTouchListener(new RecyclerItemSelectListener(getActivity(), recyclerView, new RecyclerItemSelectListener.TouchPosition() {
            @Override
            public void onItemClick(View view, int position) {
                sortCount(position);
            }

            @Override
            public void onLongItemClick(View view, int position) {
                //Not in use
            }
        }));

        return v;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources resources = getResources();

        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert getView() != null;
            View parent = (View) getView().getParent();
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) parent.getLayoutParams();
            layoutParams.setMargins(
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left), // 64dp
                    0,
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_right), // 64dp
                    0
            );
            parent.setLayoutParams(layoutParams);
        }
    }

    private ArrayList<SortingBean> prepareData() {
        String[] values = getResources().getStringArray(R.array.sorting);
        ArrayList<SortingBean> sortingBeans = new ArrayList<>();
        for (String value : values) {

            sortingBeans.add(new SortingBean(value));

        }
        return sortingBeans;
    }

    public void sortCount(int sortNo) {
        if (sortNo == 0) {
            this.sortNo = sortNo + 5;
        } else {
            this.sortNo = sortNo;
        }
        dismiss();
        ((PreOrderActivityList) getActivity()).reCallProductList(this.sortNo);
    }


}
