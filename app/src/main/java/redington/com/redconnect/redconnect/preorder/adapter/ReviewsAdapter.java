package redington.com.redconnect.redconnect.preorder.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsProductreview;
import redington.com.redconnect.redconnect.settings.activity.PdfActivity;
import redington.com.redconnect.redconnect.settings.model.Pdfdata;
import redington.com.redconnect.util.LaunchIntentManager;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewHolder> {

    private List<PreorderItemDetailsProductreview> mArrayList;
    private String mType = "";
    private List<Pdfdata> pdfdataList;
    private int type = 0;
    private int resLayout;
    private Context mContext;

    public ReviewsAdapter(Context context, List<PreorderItemDetailsProductreview> itemList, String type) {
        this.mArrayList = itemList;
        this.mType = type;
        this.mContext = context;
    }

    public ReviewsAdapter(Context context, List<Pdfdata> pdfData, int i) {
        this.pdfdataList = pdfData;
        this.type = i;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (type == 1 && mType.equals("")) {
            resLayout = R.layout.settings_list;
        } else if (type == 0 && mType.equals(IntentConstants.TOTALREVIEW)) {
            resLayout = R.layout.list_product_reviews_recycle;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(resLayout, parent, false);
        return new ReviewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ReviewHolder holder, int position) {
        if (type == 1 && mType.equals("")) {
            holder.mSettingsText.setText(pdfdataList.get(position).getFilename());

            Picasso.with(mContext).load(R.drawable.pdf)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mSettingsImage);
            final int pos = holder.getAdapterPosition();
            holder.mTotalLayout.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstants.PDF_URL, pdfdataList.get(pos).getActualURL());
                bundle.putString(IntentConstants.PDF_NAME, pdfdataList.get(pos).getFilename());
                LaunchIntentManager.routeToActivityStackBundle(mContext, PdfActivity.class, bundle);
            });
        } else if (type == 0 && mType.equals(IntentConstants.TOTALREVIEW)) {
            holder.mRatingCount.setText(mArrayList.get(position).getItemFeedback());
            holder.mReviewTitle.setText(mArrayList.get(position).getmUserid());
            holder.mReviewDesc.setText(mArrayList.get(position).getItemReview());
            holder.mReviewerName.setText(mArrayList.get(position).getmDate() + " , " + mArrayList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {
        if (type == 1 && mType.equals("")) {
            return pdfdataList.size();
        } else if (type == 0 && mType.equals(IntentConstants.TOTALREVIEW)) {
            return mArrayList.size();
        } else {
            return 0;
        }
    }

    class ReviewHolder extends RecyclerView.ViewHolder {
        private TextView mRatingCount;
        private TextView mReviewerName;
        private TextView mReviewDesc;
        private TextView mReviewTitle;
        private TextView mSettingsText;
        private LinearLayout mTotalLayout;
        private ImageView mSettingsImage;

        private ReviewHolder(View itemView) {
            super(itemView);

            mRatingCount = itemView.findViewById(R.id.ratingCount);
            mReviewTitle = itemView.findViewById(R.id.txt_reviewTitle);
            mReviewDesc = itemView.findViewById(R.id.txt_reviewDesc);
            mReviewerName = itemView.findViewById(R.id.tv_reviewerName);

            mTotalLayout = itemView.findViewById(R.id.LL_recycleView);
            mSettingsImage = itemView.findViewById(R.id.settings_image);
            mSettingsText = itemView.findViewById(R.id.text_settings);

        }
    }
}
