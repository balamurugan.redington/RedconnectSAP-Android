package redington.com.redconnect.redconnect.capitalfloat.helper;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.capitalfloat.model.FloatRegisterResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.CapitalFloatRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CapitalFloatRegisterManager {

    private CapitalFloatRegisterManager() {
        throw new IllegalStateException(IntentConstants.CAPITAL_FLOAT);
    }

    public static void registerCapitalFloat(List<String> registerList, final Context context, final NewListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = CapitalFloatRequest.getHeaderRequest(true, GlobalConstants.getCapitalFloatHeaders());
            RestClient.getInstance(headRequest).floatRegister(getRequestBody(registerList)).enqueue(new Callback<FloatRegisterResponse>() {
                @Override
                public void onResponse(Call<FloatRegisterResponse> call, Response<FloatRegisterResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<FloatRegisterResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });

        } else {
            listener.mError();
        }

    }

    private static RequestBody getRequestBody(List<String> registerList) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();

            JSONArray bankAccounts = new JSONArray();
            jsonObject.put(JsonKeyConstants.FLOAT_BANK_ACCOUNTS, bankAccounts);

            /*Business Details*/
            JSONArray businessDetails = new JSONArray();

            JSONObject businessDetailObject = new JSONObject();
            businessDetailObject.put(JsonKeyConstants.FLOAT_APP_CONTEXT, 10);
            businessDetailObject.put(JsonKeyConstants.FLOAT_INCORPORATION_DATE, GlobalConstants.NULL_DATA);
            businessDetailObject.put(JsonKeyConstants.FLOAT_INDUSTRY_TYPE, 1);
            businessDetailObject.put(JsonKeyConstants.FLOAT_MAIN_PRODUCT_CATEGORY, 1);
            businessDetailObject.put(JsonKeyConstants.FLOAT_NATURE_OF_BUSINESS, 0);
            businessDetailObject.put(JsonKeyConstants.FLOAT_OPERATE_AS, 0);
            businessDetailObject.put(JsonKeyConstants.FLOAT_OPERATE_START_DATE, GlobalConstants.NULL_DATA);
            businessDetailObject.put(JsonKeyConstants.FLOAT_REGISTERED_NAME, registerList.get(1));
            JSONArray serviceProvider = new JSONArray();
            businessDetailObject.put(JsonKeyConstants.FLOAT_SERVICES_PROVIDED, serviceProvider);

            JSONArray contactsList = new JSONArray();

            JSONObject contactObject = new JSONObject();
            contactObject.put(JsonKeyConstants.FLOAT_CONTEXT_CONTACT, 600);
            contactObject.put(JsonKeyConstants.FLOAT_CONTACT_INFO, registerList.get(3));
            contactObject.put(JsonKeyConstants.FLOAT_CONTACT_TYPE, 200);
            contactObject.put(JsonKeyConstants.FLOAT_CONTACT_ENTITY_TYPE, 100);
            contactsList.put(0, contactObject);

            JSONObject contactObject1 = new JSONObject();
            contactObject1.put(JsonKeyConstants.FLOAT_CONTEXT_CONTACT, 100);
            contactObject1.put(JsonKeyConstants.FLOAT_CONTACT_INFO, registerList.get(2));
            contactObject1.put(JsonKeyConstants.FLOAT_CONTACT_TYPE, 100);
            contactObject1.put(JsonKeyConstants.FLOAT_CONTACT_ENTITY_TYPE, 100);
            contactsList.put(1, contactObject1);

            businessDetailObject.put(JsonKeyConstants.FLOAT_CONTACTS_LIST, contactsList);

            JSONArray addressList = new JSONArray();

            JSONObject addressObject = new JSONObject();
            addressObject.put(JsonKeyConstants.FLOAT_LATITUDE, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_LONGITUDE, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_LANDMARK, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_ADDRESS, registerList.get(4));
            addressObject.put(JsonKeyConstants.FLOAT_CITY, registerList.get(5));
            addressObject.put(JsonKeyConstants.FLOAT_PINCODE, registerList.get(6));
            addressObject.put(JsonKeyConstants.FLOAT_STATE, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_OWNERSHIP_TYPE, 0);
            addressObject.put(JsonKeyConstants.FLOAT_OCCUPANCY_START_DATE, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_COUNTRY, 1);
            addressObject.put(JsonKeyConstants.FLOAT_POST_OFFICE, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_DISTRICT, GlobalConstants.NULL_DATA);
            addressObject.put(JsonKeyConstants.FLOAT_ADDRESS_TYPE, 2000);
            addressObject.put(JsonKeyConstants.FLOAT_IS_ACTIVE, 100);
            addressObject.put(JsonKeyConstants.FLOAT_IS_FI_NEED, 0);
            addressObject.put(JsonKeyConstants.FLOAT_ENTITY_TYPE, 100);
            addressObject.put(JsonKeyConstants.FLOAT_ADDRESS_OWNER, GlobalConstants.NULL_DATA);
            JSONArray contactList = new JSONArray();
            addressObject.put(JsonKeyConstants.FLOAT_CONTACT_LIST, contactList);
            addressList.put(0, addressObject);

            businessDetailObject.put(JsonKeyConstants.ADDRESS_LIST, addressList);

            businessDetails.put(0, businessDetailObject);

            jsonObject.put(JsonKeyConstants.FLOAT_BUSINESS_DETAILS, businessDetails);

            /*Business Financials*/
            JSONObject businessFinancials = new JSONObject();
            jsonObject.put(JsonKeyConstants.FLOAT_BUSINESS_FINANCIALS, businessFinancials);

            /*Eco System*/
            JSONArray ecosystemDetails = new JSONArray();
            jsonObject.put(JsonKeyConstants.FLOAT_ECOSYS_DETAILS, ecosystemDetails);

            /*Fund Use Details*/
            JSONArray funduseDetails = new JSONArray();
            jsonObject.put(JsonKeyConstants.FLOAT_FUND_USE_DETAILS, funduseDetails);

            /*Individual Details*/
            JSONArray individualDetails = new JSONArray();
            jsonObject.put(JsonKeyConstants.FLOAT_INDIVIDUAL_DETAILS, individualDetails);

            /*Meta Data*/
            JSONObject metadata = new JSONObject();
            metadata.put(JsonKeyConstants.FLOAT_APP_SOURCE_NAME, JsonKeyConstants.FLOAT_API_INTEGRATION);
            metadata.put(JsonKeyConstants.FLOAT_APP_LIFE_CYCLE_ID, 100);
            metadata.put(JsonKeyConstants.FLOAT_APP_SEGMENT, 100);
            metadata.put(JsonKeyConstants.FLOAT_APP_TYPE, 100);
            metadata.put(JsonKeyConstants.FLOAT_PRODUCT_TYPE, 0);
            metadata.put(JsonKeyConstants.FLOAT_VERSION, "1.0.0");
            metadata.put(JsonKeyConstants.FLOAT_IS_ARCIVED, 0);
            metadata.put(JsonKeyConstants.FLOAT_HUB_OR_SPOKE, 0);
            metadata.put(JsonKeyConstants.FLOAT_IN_PROCESS_STATUS, 0);
            metadata.put(JsonKeyConstants.FLOAT_IS_FS, 0);
            metadata.put(JsonKeyConstants.FLOAT_IS_PERFIOS, 0);
            metadata.put(JsonKeyConstants.FLOAT_SL_NO, 0);
            metadata.put(JsonKeyConstants.FLOAT_STATUS, 100);
            metadata.put(JsonKeyConstants.FLOAT_THIRD_PARTY_REF_ID, registerList.get(0));
            JSONObject sourceAttrJson = new JSONObject();
            metadata.put(JsonKeyConstants.FLOAT_SOURCE_ATTR_JSON, sourceAttrJson);
            JSONArray tagsArray = new JSONArray();
            metadata.put(JsonKeyConstants.FLOAT_TAGS, tagsArray);

            jsonObject.put(JsonKeyConstants.FLOAT_METADATA, metadata);

            jsonValues.put(JsonKeyConstants.APP_ID_DETAILS, jsonObject);

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());

        }
        return body;
    }


}
