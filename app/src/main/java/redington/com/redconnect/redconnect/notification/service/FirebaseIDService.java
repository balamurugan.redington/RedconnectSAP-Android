package redington.com.redconnect.redconnect.notification.service;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import redington.com.redconnect.common.activity.BaseActivity;

public class FirebaseIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        BaseActivity.logd(refreshedToken);


    }
}
