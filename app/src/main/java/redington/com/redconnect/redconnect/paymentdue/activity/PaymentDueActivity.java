package redington.com.redconnect.redconnect.paymentdue.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;

public class PaymentDueActivity extends BaseActivity implements UIListener {

    private CommonUtils commonUtils;
    private Context mContext;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_due_new);


        mContext = PaymentDueActivity.this;
        commonUtils = new CommonUtils(mContext);
        userId = GlobalConstants.GetSharedValues.getSpUserId();
    }
    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();

    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
    }
}
