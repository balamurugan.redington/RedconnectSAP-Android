package redington.com.redconnect.redconnect.payment.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.capitalfloat.helper.CreditBlockHelper;
import redington.com.redconnect.redconnect.capitalfloat.helper.OtpVerifyHelper;
import redington.com.redconnect.redconnect.capitalfloat.helper.ResentOtpHelper;
import redington.com.redconnect.redconnect.capitalfloat.model.CreditBlockResponse;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.CartUpdationManager;
import redington.com.redconnect.redconnect.payment.model.CartUpdationJsonResponse;
import redington.com.redconnect.redconnect.paymentcall.model.PaymentPojo;
import redington.com.redconnect.redconnect.preorder.helper.OtpServiceManager;
import redington.com.redconnect.redconnect.preorder.model.OtpJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.smsgateway.constants.TemplateConstants;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.EmptyCartActivity;
import redington.com.redconnect.util.LaunchIntentManager;

public class OtpActivity extends BaseActivity implements NewListener, CartListener, UIListener {

    private Context mContext;
    private CommonUtils commonUtils;
    private String orderValue;
    private String referenceNumber;

    private String stockLocation;
    private String paymentDays;
    private String deliverySequence;
    private ArrayList<CartData> cartArrayList;
    private ArrayList<SubPromoCode> subPromoCodes;
    private String currentDate;

    private boolean mSpotBoolean;
    private Double firstAmount;
    private EditText mOtp;
    private String apiString = "";
    private String responseString = "";
    private String appId;
    private CoordinatorLayout totalView;
    private String transactionId;

    //Edited Deepak
    SmsRetrieverClient smsRetrieverClient;
    BroadcastReceiver smsReceiver;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("OTP")) {
                final String message = intent.getStringExtra("message");
                if (message.contains(TemplateConstants.TEAM)) {
                    String ch = message.substring(0, 7);
                    mOtp.setText(ch.trim());
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actitvity_otp);

        mContext = OtpActivity.this;
        commonUtils = new CommonUtils(mContext);

        totalView = findViewById(R.id.drawer);

        initViews();

        transactionId = Long.toString(System.currentTimeMillis());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            orderValue = bundle.getString(GlobalConstants.PRICE);
            assert orderValue != null;
            orderValue = orderValue.replace(",", "");
            orderValue = orderValue.replace("₹ ", "");
            referenceNumber = bundle.getString(GlobalConstants.REFERENCE_NUM);

            currentDate = bundle.getString(IntentConstants.DATE);

            paymentDays = bundle.getString(IntentConstants.PAYMENTDAYS_CARD);
            deliverySequence = bundle.getString(IntentConstants.DELIVERYSEQUENCE_CARD);

            mSpotBoolean = bundle.getBoolean(IntentConstants.SPOTBOOLEAN);
            appId = bundle.getString(IntentConstants.APP_ID);

            subPromoCodes = (ArrayList<SubPromoCode>) bundle.getSerializable(IntentConstants.SPOTARRAY);
            cartArrayList = (ArrayList<CartData>) bundle.getSerializable(IntentConstants.CARTARRAY);

            if (!mSpotBoolean) {
                stockLocation = bundle.getString(IntentConstants.STOCKLOCATION);
            } else {
                stockLocation = subPromoCodes.get(0).getmSTOCKROOM();
            }

            if (cartArrayList != null && !cartArrayList.isEmpty()) {
                calculateAmount(cartArrayList, 1);
            }
        }

        if (referenceNumber.equals(GlobalConstants.NULL_DATA)) {
            creditBlockApi();
        }


    }

    private void creditBlockApi() {
        apiString = IntentConstants.CAPITAL_FLOAT;
        commonUtils.showProgressDialog();
        CreditBlockHelper.checkCreditBlock(appId, transactionId, GlobalConstants.NULL_DATA, orderValue,
                GlobalConstants.NULL_DATA, mContext, this);

    }

    private void initViews() {

        toolbar(IntentConstants.OTP);

        hideKeyboard();

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        mOtp = findViewById(R.id.ed_otp);

        Button mOtpSubmit = findViewById(R.id.otp_submit);
        TextView mResentOtp = findViewById(R.id.resent_otp);

        mOtpSubmit.setOnClickListener(v -> {
            if (referenceNumber.equals(GlobalConstants.NULL_DATA)) {
                if (mOtp.length() > 3) {
                    capitalFloatOtpCall();
                } else {
                    shortToast(mContext, GlobalConstants.OTP);
                }
            } else {
                if (mOtp.length() > 5) {
                    skipOrderApiCall(1);
                } else {
                    shortToast(mContext, GlobalConstants.OTP);
                }
            }

        });

        mResentOtp.setOnClickListener(v -> {
            mOtp.setText("");
            if (referenceNumber.equals(GlobalConstants.NULL_DATA)) {
                resentOtpCall();
            } else {
                skipOrderApiCall(2);
            }

        });
    }

    private void capitalFloatOtpCall() {
        commonUtils.showProgressDialog();
        responseString = IntentConstants.OTP;
        OtpVerifyHelper.otpVerifyBlock(appId, transactionId, GlobalConstants.NULL_DATA, orderValue,
                mOtp.getText().toString().trim(), mContext, this);
    }

    private void resentOtpCall() {
        commonUtils.showProgressDialog();
        responseString = IntentConstants.RESENT_OTP;
        ResentOtpHelper.resentOtp(appId, transactionId, mContext, this);
    }

    private void skipOrderApiCall(int flag) {
        commonUtils.showProgressDialog();
        OtpServiceManager.otpServiceCall(referenceNumber, mOtp.getText().toString().trim(), orderValue,
                String.valueOf(flag), mContext, this);
    }

    public void menuBack(View v) {

        showEligibilityPopup(v.getContext(), "");
    }

    @Override
    public void onBackPressed() {
        showEligibilityPopup(mContext, "");
    }

    private void calculateAmount(ArrayList<CartData> list, int value) {
        if (value == 2) {
            Double count = 0.0d;
            for (int i = 0; i < list.size(); i++) {
                count += list.get(i).getUnitPrice() * (double) list.get(i).getQuantity();
            }
            double secondAmount = count;

            if (firstAmount == secondAmount) {
                statusUpdate();
            } else {
                showEligibilityPopup(mContext, IntentConstants.CART);
            }
        } else if (value == 1) {
            Double count = 0.0d;
            for (int i = 0; i < list.size(); i++) {
                count += list.get(i).getUnitPrice() * (double) list.get(i).getQuantity();
            }
            firstAmount = count;
        }
    }

    /*Cart Check Validation*/
    private void showEligibilityPopup(final Context context, String cameFrom) {
        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);

        TextView prefTitle = mDialog.findViewById(R.id.pre_ref_title);
        TextView prefcont = mDialog.findViewById(R.id.pre_ref_cont);
        Button prefcontbtm = mDialog.findViewById(R.id.pre_ref_btm);
        prefcont.setVisibility(View.GONE);
        if (cameFrom.equals(IntentConstants.CART)) {
            prefTitle.setText(IntentConstants.AMOUNTMISMATCH);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setCancelable(false);
        } else {
            prefTitle.setText(GlobalConstants.CANCEL_ORDER);
        }

        if (!mSpotBoolean && cartArrayList != null && !cartArrayList.isEmpty()) {
            setCartUpdate(cartArrayList.size());
        }
        prefcontbtm.setOnClickListener(v -> {
            mDialog.dismiss();
            if (cameFrom.equals(IntentConstants.CART)) {
                setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
                LaunchIntentManager.routeToActivity(mContext, MyCartActivity.class);
            } else {
                LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
            }
        });

        mDialog.show();
    }

    /*Api Call*/
    private void payLater() {
        ArrayList<PaymentPojo> pojoArrayList = new ArrayList<>();
        PaymentPojo paymentPojo = new PaymentPojo();
        paymentPojo.setmDate(currentDate);
        if (referenceNumber.equals(GlobalConstants.NULL_DATA)) {
            paymentPojo.setmAmount(orderValue);
            paymentPojo.setmTransactionID(transactionId);
            paymentPojo.setmStatus(GlobalConstants.SUCCESS_VALIDATION);
            paymentPojo.setmAmountReceived(orderValue);
            paymentPojo.setmTransactionRecieved(transactionId);
        } else {
            paymentPojo.setmAmount(GlobalConstants.NULL_DATA);
            paymentPojo.setmTransactionID(GlobalConstants.NULL_DATA);
            paymentPojo.setmStatus(GlobalConstants.NULL_DATA);
            paymentPojo.setmAmountReceived(GlobalConstants.NULL_DATA);
            paymentPojo.setmTransactionRecieved(GlobalConstants.NULL_DATA);
        }


        paymentPojo.setmRemarks(GlobalConstants.NULL_DATA);
        paymentPojo.setmReferenceNum(GlobalConstants.NULL_DATA);
        paymentPojo.setmUserID(GlobalConstants.GetSharedValues.getSpUserId());
        paymentPojo.setmDeviceID(GlobalConstants.NULL_DATA);
        paymentPojo.setmUID(GlobalConstants.NULL_DATA);
        pojoArrayList.add(paymentPojo);

        newOrderGenerate(GlobalConstants.NULL_DATA, stockLocation, paymentDays, deliverySequence, pojoArrayList);

    }

    private void newOrderGenerate(String status, String mStockLocation, String mPaymentDays, String mDeliverySeq,
                                  ArrayList<PaymentPojo> paymentPojoArrayList) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add(GlobalConstants.GetSharedValues.getSpUserId());
        stringArrayList.add(status);
        stringArrayList.add(mStockLocation);
        stringArrayList.add(mPaymentDays);
        stringArrayList.add(mDeliverySeq);
        stringArrayList.add(GlobalConstants.GetSharedValues.getBillingSeq());
        stringArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        stringArrayList.add(referenceNumber);
        if (referenceNumber.equals(GlobalConstants.NULL_DATA)) {
            stringArrayList.add(IntentConstants.CAPITAL_FLOAT);
        } else {
            stringArrayList.add(GlobalConstants.HDFC);
        }

        moveToOrderGenerate(stringArrayList, paymentPojoArrayList, bankServiceCharges());
    }

    private void moveToOrderGenerate(ArrayList<String> stringArrayList,
                                     ArrayList<PaymentPojo> pojoArrayList, ArrayList<String> serviceCharges) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(GlobalConstants.LOCATION, stringArrayList);
        bundle.putSerializable(GlobalConstants.CART_ARRAY, cartArrayList);
        bundle.putSerializable(GlobalConstants.POJO_ARRAY, pojoArrayList);
        bundle.putSerializable(GlobalConstants.PROMOCODE_ARRAY, subPromoCodes);
        bundle.putString(GlobalConstants.FROM, GlobalConstants.NULL_DATA);
        bundle.putString(GlobalConstants.TXN_ID, referenceNumber);
        bundle.putSerializable(GlobalConstants.ORDER_BANK_INFO, serviceCharges);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, Ordergeneration.class, bundle);
    }

    private ArrayList<String> bankServiceCharges() {
        ArrayList<String> serviceCharges = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            serviceCharges.add("");
        }
        return serviceCharges;
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        if (apiString.equals(IntentConstants.CAPITAL_FLOAT)) {
            creditBlockResponse(successObject);
        } else {
            otpResponse(successObject);

        }
    }

    @Override
    public void onFailure(Object failureObject) {
        logd(IntentConstants.OTP);
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        logd(IntentConstants.OTP);
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    /*CreditBlockResponse*/
    private void creditBlockResponse(Object successObject) {
        CreditBlockResponse creditBlockResponse = (CreditBlockResponse) successObject;
        if (creditBlockResponse != null) {
            if (creditBlockResponse.getStatusCode().equals(IntentConstants.SUCCESS_URL)) {
                String message = creditBlockResponse.getData().getMessage();
                switch (responseString) {
                    case IntentConstants.OTP:
                        floatOtpResponse(message);
                        break;
                    case IntentConstants.RESENT_OTP:
                        resentOtpResponse(message);
                        break;
                    default:
                        creditBlockMessage(message, creditBlockResponse.getData().getStatus());
                        break;
                }

            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR);
            }
        } else {
            callSnackBar(GlobalConstants.SERVER_ERROR);
        }
    }

    private void floatOtpResponse(String responseMessage) {
        switch (responseMessage) {
            case GlobalConstants.SUCCESS:
                shortToast(mContext, IntentConstants.OTP_VALIDATE);
                payLater();
                break;
            case GlobalConstants.OTP_NOT_MATCH:
                shortToast(mContext, GlobalConstants.OTP_FAILURE);
                break;
            default:
                shortToast(mContext, GlobalConstants.OTP_FAILURE);
                break;
        }
    }

    private void resentOtpResponse(String message) {
        if (message.equals(GlobalConstants.OTP_SENT)) {
            shortToast(mContext, GlobalConstants.OTP_RE_GEN);
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    private void creditBlockMessage(String message, String mStatus) {
        if (mStatus.equals(GlobalConstants.ONE)) {
            shortToast(mContext, message);
        } else {
            finish();
            shortToast(mContext, message);
        }
    }

    /*Snack Bar*/
    private void callSnackBar(String error) {
        Snackbar snackbar = Snackbar
                .make(totalView, error, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                            switch (responseString) {
                                case IntentConstants.OTP:
                                    capitalFloatOtpCall();
                                    break;
                                case IntentConstants.RESENT_OTP:
                                    resentOtpCall();
                                    break;
                                default:
                                    creditBlockApi();
                                    break;

                            }

                        }
                );
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /*Otp response*/
    private void otpResponse(Object successObject) {
        OtpJsonResponse otpJsonResponse = (OtpJsonResponse) successObject;
        if (otpJsonResponse != null) {
            switch (otpJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String message = otpJsonResponse.getData().getMessage();
                    if (!message.equals(IntentConstants.VALIDOTP)) {
                        mOtp.setText("");
                    }

                    switch (message) {
                        case IntentConstants.INVALIDOTP:
                            shortToast(mContext, GlobalConstants.OTP_FAILURE);
                            break;
                        case IntentConstants.VALIDOTP:
                            shortToast(mContext, IntentConstants.OTP_VALIDATE);
                            if (mSpotBoolean) {
                                payLater();
                            } else {
                                cartApiCall();
                            }
                            break;
                        case IntentConstants.OTPREG:
                            shortToast(mContext, GlobalConstants.OTP_RE_GEN);
                            break;
                        case IntentConstants.OTPDATE:
                            shortToast(mContext, GlobalConstants.OTP_DATE);
                            break;
                        case IntentConstants.OTPTIME:
                            shortToast(mContext, GlobalConstants.OTP_TIME);
                            break;
                        default:
                            //No data
                            break;
                    }

                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, otpJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    private void cartApiCall() {

        ArrayList<String> cartArray = new ArrayList<>();
        cartArray.add(GlobalConstants.GetSharedValues.getSpUserId());
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.MODE_SELECT);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArray.add(GlobalConstants.GetSharedValues.getCashDiscount());
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArray, mContext, this);

    }

    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJsonResponse = (CartJsonResponse) successObject;
        if (cartJsonResponse != null) {
            switch (cartJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<CartData> list = new ArrayList<>();
                    setCartUpdate(list.size());
                    /*9-1-19*/
                    /*for (int i = 0; i < cartJsonResponse.getData().size(); i++) {
                        list.addAll(cartJsonResponse.getData().get(i).getCart());
                    }*/
                    list.addAll(cartJsonResponse.getData().get(0).getCart());
                    calculateAmount(list, 2);
                    break;
                case IntentConstants.FAILURE_URL:
                    LaunchIntentManager.routeToActivity(mContext, EmptyCartActivity.class);
                    break;
                default:
                    BaseActivity.shortToast(mContext, GlobalConstants.PAY_NOW_AGAIN);
                    break;
            }
        } else {
            BaseActivity.shortToast(mContext, GlobalConstants.PAY_NOW_AGAIN);
        }
    }

    @Override
    public void cartFailure(Object failureObject) {
        logd(String.valueOf(failureObject));
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void cartError() {
        logd(IntentConstants.CART);
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    @Override
    public void onPause() {
        super.onPause();
        //LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);
        unregisterReceiver(smsReceiver);
    }

    @Override
    public void onResume() {
        //LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, new IntentFilter("OTP"));
        setupSmsRetriver();
        super.onResume();
    }



    private void statusUpdate() {
        commonUtils.showProgressDialog();
        CartUpdationManager.cartUpdateCall(GlobalConstants.GetSharedValues.getSpUserId(), IntentConstants.Y, mContext, this);

    }


    /*cart Update response*/
    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartUpdationJsonResponse cartUpdationJsonResponse = (CartUpdationJsonResponse) successObject;
        if (cartUpdationJsonResponse != null) {
            switch (cartUpdationJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    payLater();
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    private void setupSmsRetriver()
    {
        smsReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction()))
                {
                    Bundle extras = intent.getExtras();
                    Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                    if(status!=null && status.getStatusCode()==CommonStatusCodes.SUCCESS) {
                        String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                        if(message!=null && message.length()>=10) {
                            String otp = message.substring(4,10).trim();
                            if(otp!=null && otp.length()==6 && CommonMethod.isNumber(otp))
                                mOtp.setText(otp);
                        }
                    }
                }
            }
        };
        registerReceiver(smsReceiver,new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED"));
        smsRetrieverClient = SmsRetriever.getClient(this);
        smsRetrieverClient.startSmsRetriever();
    }
}
