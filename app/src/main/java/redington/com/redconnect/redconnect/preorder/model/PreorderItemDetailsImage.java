
package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PreorderItemDetailsImage implements Serializable{

    @SerializedName("ImageURL")
    private String mImageURL;

    public String getImageURL() {
        return mImageURL;
    }

}
