package redington.com.redconnect.redconnect.login.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.AppSignatureHelper;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.login.dialog.ForgotPassCustomSheetDialog;
import redington.com.redconnect.redconnect.login.helper.LoginServiceManager;
import redington.com.redconnect.redconnect.login.model.LoginJsonResponse;
import redington.com.redconnect.restapiclient.listener.PermissionResultCallback;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;
import redington.com.redconnect.util.PermissionUtils;


public class LoginActivity extends BaseActivity implements View.OnClickListener, View.OnLongClickListener,UIListener,
        ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback
    {

    ArrayList<String> permissions = new ArrayList<>();
    private boolean doubleBackToExitPressedOnce = false;
    private EditText mEmailID;
    private EditText mPassword;
    private CheckBox mCheckBox;
    private String getPassword;
    private String remember;
    private CommonUtils commonUtils;
    private PermissionUtils mPermssionUtils;
    private RelativeLayout mScrollView;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.gradient_theme);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
            window.setNavigationBarColor(ContextCompat.getColor(this, android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_login);

        mContext = LoginActivity.this;
        commonUtils = new CommonUtils(mContext);

        hideKeyboard();
        TextView title = findViewById(R.id.txt_title);
        /*Font Styles*/
        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/andalus.ttf");
        title.setTypeface(typeface);

        TextView mLoginButton = findViewById(R.id.btn_login);
        TextView forgotButton = findViewById(R.id.forogt_button);

        mEmailID = findViewById(R.id.ed_uesr_id);

        mPassword = findViewById(R.id.ed_password);

        mCheckBox = findViewById(R.id.ch_remember);
        mScrollView = findViewById(R.id.login_scrollView);
        mEmailID.getText().clear();
        mPassword.getText().clear();
        mCheckBox.setChecked(false);

        /*mEmailID.setFilters(new InputFilter[]{filter});*/
        mPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        mEmailID.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8),
                setValidInoutFilter(BLOCK_USER_CHARACTER_SET)});

        float[] loginRadius = {0, 0, 0, 0, 0, 0, 0, 0};
        commonUtils.setTwoGradientLoginColor(mScrollView, R.color.new_gradient, R.color.new_gradient2, loginRadius);
        commonUtils.setTwoGradientLoginColor(mLoginButton, R.color.new_gradient, R.color.new_gradient2, loginRadius);

        mLoginButton.setOnClickListener(this);
        //uncomment to generate sms hashkey
        //mLoginButton.setOnLongClickListener(this);
        forgotButton.setOnClickListener(this);

        if (sp.getString(SP_USER_REMEMBER, GlobalConstants.REMEMBER).equalsIgnoreCase("Y")) {
            mEmailID.setText(sp.getString(SP_EMAIL_ID, GlobalConstants.EMAIL));
            mPassword.setText((sp.getString(SP_PASSWORD, GlobalConstants.PASS_WORD)));
            mCheckBox.setChecked(true);
        } else if (sp.getString(SP_USER_REMEMBER, GlobalConstants.REMEMBER).equalsIgnoreCase("")) {
            mEmailID.getText().clear();
            mPassword.getText().clear();
        } else {
            mEmailID.getText().clear();
            mPassword.getText().clear();
            logd(mContext.getClass().getSimpleName());
        }

        commonUtils = new CommonUtils(mContext);
        mPermssionUtils = new PermissionUtils(mContext);
        permissions.add(Manifest.permission.READ_PHONE_STATE);
    }

    @Override
    public void onClick(View view) {

        int i = view.getId();
        if (i == R.id.forogt_button) {
            ForgotPassCustomSheetDialog bottomSheetDialog = ForgotPassCustomSheetDialog.getInstance();
            bottomSheetDialog.show(getSupportFragmentManager(), GlobalConstants.CUSTOM_BOTTOM_SHEET);

        } else if (i == R.id.btn_login) {
            mPermssionUtils.checkPermission(permissions, GlobalConstants.DEVICE_ERROR, 1);
        }
    }

    @Override
    public boolean onLongClick(View v) {

        if(v.getId()==R.id.btn_login) {
            String hashText = "HashKey : "+new AppSignatureHelper(LoginActivity.this).getAppSignatures().get(0);
           sendNotification(hashText);
        }
        return false;
    }



        private void callLoginService()
    {
        TelephonyManager access = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        String getEmailID = mEmailID.getText().toString().trim();
        getPassword = mPassword.getText().toString().trim();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String getIMEIno = access.getDeviceId();
        String token = FirebaseInstanceId.getInstance().getToken();
        String version = "";

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            logd(e.getMessage());
        }

    if (mCheckBox.isChecked()) {
        remember = GlobalConstants.YES;
    } else {
        remember = GlobalConstants.NO;
    }

    if (getEmailID.length() == 0) {
        shortToast(mContext, GlobalConstants.EMAILVALI);
        mEmailID.requestFocus();
    } else if (getPassword.length() == 0) {
        shortToast(mContext, GlobalConstants.PASS_VALID);
        mPassword.requestFocus();
    } else if (TextUtils.isEmpty(token)) {
        shortToast(mContext, IntentConstants.FIREBASE);
    } else {
        loadLoginLists(getEmailID, getPassword, token, getIMEIno, version);
    }
    }

    private void loadLoginLists(String mUserid, String mPassword, String dviceId, String iMEIno, String version) {
        commonUtils.showProgressDialog();
        LoginServiceManager.getLoginServiceCall(mUserid, mPassword, dviceId, iMEIno, version, mContext, this);
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        shortToast(mContext, GlobalConstants.BACK);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> doubleBackToExitPressedOnce = false, IntentConstants.BACK_TIME_OUT);

    }

    public void mSignUp(View view) {
        LaunchIntentManager.routeToActivityStack(view.getContext(), Partnercreation.class);
    }


    /*Login Response*/
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();

        LoginJsonResponse loginJsonResponse = (LoginJsonResponse) successObject;
        if (loginJsonResponse != null) {
            switch (loginJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    SharedPreferences.Editor editor = BaseActivity.sp.edit();
                    editor.putString(SP_EMAIL_ID, loginJsonResponse.getData().getUserID());
                    editor.putString(SP_PASSWORD, getPassword);
                    editor.putString(SP_USER_REMEMBER, remember);
                    editor.putString(SP_CUSTOMER, loginJsonResponse.getData().getCustomer());
                    editor.putString(SP_CUSTOMERNAME, loginJsonResponse.getData().getCustomerName());
                    editor.putString(SP_MAIL, loginJsonResponse.getData().getMail());
                    editor.putString(SP_PH_NO, loginJsonResponse.getData().getPhoneNumber());
                    editor.putBoolean(SP_LOGGED_IN, true);
                    editor.apply();
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalConstants.FROM, GlobalConstants.LOGIN);
                    LaunchIntentManager.routeToActivityStackBundleStack(LoginActivity.this,
                            DashboardActivity.class, bundle);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, loginJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.CONNECT_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.CONNECT_ERROR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.CONNECT_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        Snackbar snackbar = Snackbar.make(mScrollView, GlobalConstants.INTERNET_CHECK, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

    /*Request Permission Result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        mPermssionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    /*Request Permission Results Method's*/

    @Override
    public void permissionGranted(int requestCode) {
        if (requestCode == 1) {
            callLoginService();
        }
    }

    @Override
    public void partialPermissionGranted(int requestCode, List<String> grantedPermissions) {
        //Not In use
    }

    @Override
    public void permissionDenied(int requestCode) {
        //Not In use
    }

    @Override
    public void neverAskAgain(int requestCode) {
        //Not In use
    }


}
