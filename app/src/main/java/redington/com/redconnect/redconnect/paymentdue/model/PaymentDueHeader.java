
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class PaymentDueHeader implements Serializable {

    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("TotalInvoiceValue")
    private String mTotalInvoiceValue;
    @SerializedName("TotalOverDue")
    private String mTotalOverDue;
    @SerializedName("WEEK")
    private String mWEEK;
    @SerializedName("Week1")
    private String mWeek1;
    @SerializedName("Week2")
    private String mWeek2;
    @SerializedName("Week3")
    private String mWeek3;
    @SerializedName("Week4")
    private String mWeek4;
    @SerializedName("WeekGreater4")
    private String mGreater4;

    public String getCustomerCode() {
        return mCustomerCode;
    }


    public String getCustomerName() {
        return mCustomerName;
    }


    public Double getTotalInvoiceValue() {
        return CommonMethod.getValidDouble(mTotalInvoiceValue);
    }


    public Double getTotalOverDue() {
        return CommonMethod.getValidDouble(mTotalOverDue);
    }


    public Double getWEEK() {
        return CommonMethod.getValidDouble(mWEEK);
    }


    public Double getWeek1() {
        return CommonMethod.getValidDouble(mWeek1);
    }


    public Double getWeek2() {
        return CommonMethod.getValidDouble(mWeek2);
    }


    public Double getWeek3() {
        return CommonMethod.getValidDouble(mWeek3);
    }


    public Double getWeek4() {
        return CommonMethod.getValidDouble(mWeek4);
    }


    public Double getmGreater4() {
        return CommonMethod.getValidDouble(mGreater4);
    }

}
