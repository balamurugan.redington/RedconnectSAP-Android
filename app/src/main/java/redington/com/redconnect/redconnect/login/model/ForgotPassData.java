package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ForgotPassData implements Serializable {

    @SerializedName("Message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }


}
