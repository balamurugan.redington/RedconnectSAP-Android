package redington.com.redconnect.redconnect.preorder.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemListData;


public class PreorderListAdapter extends RecyclerView.Adapter<PreorderListAdapter.PreorderListHolder> {


    private boolean mList;
    private List<PreorderItemListData> mArrayList;
    private Context context;
    private CommonMethod commonMethod;


    public PreorderListAdapter(Context context, List<PreorderItemListData> arrayList, boolean list) {
        this.mArrayList = arrayList;
        this.context = context;
        this.mList = list;
        commonMethod = new CommonMethod(context);
    }

    public void preOrderListPagination(List<PreorderItemListData> item, boolean list) {
        mArrayList.addAll(item);
        this.mList = list;

    }

    public void preOrderUpation(Boolean list) {
        this.mList = list;
    }

    @NonNull
    @Override
    public PreorderListAdapter.PreorderListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (mList) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_preorder_recycler_list, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.preorder_list_grid, viewGroup, false);

        }

        return new PreorderListAdapter.PreorderListHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PreorderListAdapter.PreorderListHolder holder, int position) {
        holder.mProductName.setText(mArrayList.get(position).getProductName());
        holder.mItemcode.setText(mArrayList.get(position).getItemCode());
        double discountPercent = Double.parseDouble(mArrayList.get(position).getmDiscountPercent());

        if (discountPercent == 0.0) {
            holder.mProductPrice.setVisibility(View.VISIBLE);
            holder.mDiscountLayout.setVisibility(View.GONE);
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE
                    + BaseActivity.isValidNumberFormat().format(Double.parseDouble(mArrayList.get(position).getmDiscountPrice())));
        } else {
            holder.mProductPrice.setVisibility(View.GONE);
            holder.mDiscountLayout.setVisibility(View.VISIBLE);
            holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(mArrayList.get(position).getmDiscountPrice())));
            holder.mActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(Double.parseDouble(mArrayList.get(position).getmActualPrice())));

            holder.mActualPrice.setPaintFlags(holder.mActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }


        if (!mArrayList.get(position).getImages().isEmpty()) {
            Glide.with(context).load(mArrayList.get(position).getImages().get(0).getImageURL())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .fitCenter()
                    .placeholder(R.drawable.redinton_image)
                    .error(R.drawable.noimagefound)
                    .override(120, 120)
                    .into(holder.mImageView);

        }

        holder.mStockCheck.setText(mArrayList.get(position).getAvailability());
        if (mArrayList.get(position).getAvailability().equals(GlobalConstants.STOCK_AVAILABLE)) {
            holder.mStockCheck.setTextColor(ContextCompat.getColor(context, R.color.category));
        } else if (mArrayList.get(position).getAvailability().equals(GlobalConstants.LOW_STOCK)) {
            holder.mStockCheck.setTextColor(ContextCompat.getColor(context, R.color.login_icon));
        }
        final int pos = holder.getAdapterPosition();
        holder.mLinearLayout.setOnClickListener(v -> {
            if (commonMethod.listAccess()) {
                Intent i = new Intent(context, ProductDetail.class);
                i.putExtra(GlobalConstants.PRODUCT_NAME, mArrayList.get(pos).getProductName());
                i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, mArrayList.get(pos).getItemCode());
                i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, mArrayList.get(pos).getVendorCode());
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class PreorderListHolder extends RecyclerView.ViewHolder {
        private final TextView mProductName;
        private final TextView mProductPrice;
        private final TextView mStockCheck;
        private final ImageView mImageView;
        private final LinearLayout mLinearLayout;

        private LinearLayout mDiscountLayout;
        private final TextView mActualPrice;
        private final TextView mDiscountPrice;
        private TextView mItemcode;


        PreorderListHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            mProductName = itemView.findViewById(R.id.productName);
            mProductPrice = itemView.findViewById(R.id.productPrice);
            mStockCheck = itemView.findViewById(R.id.stockCheck);
            mImageView = itemView.findViewById(R.id.imageView);
            mLinearLayout = itemView.findViewById(R.id.LL_recycleView);

            mDiscountLayout = itemView.findViewById(R.id.layout_dicount);
            mActualPrice = itemView.findViewById(R.id.text_actualprice);
            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mItemcode = itemView.findViewById(R.id.itemCode);

        }
    }


}

