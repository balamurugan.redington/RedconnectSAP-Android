package redington.com.redconnect.redconnect.dashboard.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.RecentPurchase;
import redington.com.redconnect.redconnect.dashboard.model.RecentView;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;

public class DashboardAdapter1 extends RecyclerView.Adapter<DashboardAdapter1.DashboardHolder1> {

    private Context mContext;
    private List<RecentView> mArrayRecentView;
    private List<RecentPurchase> recentPurchaseArrayList;
    private String mType = "";
    private int layoutRes;
    private static int[] backgroundColors = {R.color.md_purple_100,
            R.color.md_green_100, R.color.md_light_blue_100, R.color.md_red_100, R.color.md_amber_100, R.color.md_light_blue_100, R.color.md_cyan_100};


    public DashboardAdapter1(Context context, List<RecentView> recentViews, String type) {
        this.mContext = context;
        this.mArrayRecentView = recentViews;
        this.mType = type;
    }

    public DashboardAdapter1(Context context, List<RecentPurchase> recentPurchases) {
        this.recentPurchaseArrayList = recentPurchases;
        this.mContext = context;
    }


    @NonNull
    @Override
    public DashboardAdapter1.DashboardHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (mType.equals(GlobalConstants.MRECENTVIEWS) && mArrayRecentView.size() != 1) {
            layoutRes = R.layout.list_main_recylce;
        } else if (mType.equals("") && recentPurchaseArrayList.size() != 1) {
            layoutRes = R.layout.list_main_recylce;
        } else if (mType.equals(GlobalConstants.MRECENTVIEWS) && mArrayRecentView.size() == 1) {
            layoutRes = R.layout.list_main_recylce_full;
        } else if (mType.equals("") && recentPurchaseArrayList.size() == 1) {
            layoutRes = R.layout.list_main_recylce_full;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new DashboardHolder1(view);


    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DashboardHolder1 holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (mType.equals(GlobalConstants.MRECENTVIEWS)) {

            holder.mItemCode.setText(mArrayRecentView.get(position).getItemCode());
            holder.mProductName1.setText(mArrayRecentView.get(position).getProductName());

            if (GlobalConstants.LOW_STOCK.equals(mArrayRecentView.get(pos).getStock())) {
                holder.mStockCheck.setVisibility(View.VISIBLE);
                holder.mStockCheck.setText(mArrayRecentView.get(pos).getStock());
            } else {
                holder.mStockCheck.setVisibility(View.GONE);
            }

            double dispercent = Double.parseDouble(mArrayRecentView.get(position).getDiscountPercent());
            if (dispercent == 0.0) {

                //Deepak - Replace below method with common method (String param not supported)
                /*holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(mArrayRecentView.get(position).getDiscountPrice()));*/
                holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(mArrayRecentView.get(position).getDiscountPrice()));
            } else {


                holder.mProductPrice1.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);

                //Deepak - Replace with Common Method
                /*holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(mArrayRecentView.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(mArrayRecentView.get(position).getActualPrice()));*/
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(mArrayRecentView.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(mArrayRecentView.get(position).getActualPrice()));


                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            Glide.with(mContext).load(mArrayRecentView.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.imagePreOrder);


            int bgColor = ContextCompat.getColor(mContext, backgroundColors[position % 7]);
            holder.mRelTop.setBackgroundColor(bgColor);


            holder.mLinLayout.setOnClickListener(view -> setClickFunc(
                    mArrayRecentView.get(pos).getProductName(),
                    mArrayRecentView.get(pos).getItemCode(),
                    mArrayRecentView.get(pos).getVendorCode()));


        } else if (mType.equals("")) {
            holder.mItemCode.setText(recentPurchaseArrayList.get(position).getItemCode());
            holder.mProductName1.setText(recentPurchaseArrayList.get(position).getProductName());
            double dispercent = Double.parseDouble(recentPurchaseArrayList.get(position).getDiscountPercent());
            if (dispercent == 0.0)
            {

                /*holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(recentPurchaseArrayList.get(position).getDiscountPrice()));*/
                holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(recentPurchaseArrayList.get(position).getDiscountPrice()));
            } else {

                holder.mProductPrice1.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);

                /*holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(recentPurchaseArrayList.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(recentPurchaseArrayList.get(position).getActualPrice()));*/
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(recentPurchaseArrayList.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(recentPurchaseArrayList.get(position).getActualPrice()));
                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            if (GlobalConstants.LOW_STOCK.equals(recentPurchaseArrayList.get(pos).getStock())) {
                holder.mStockCheck.setVisibility(View.VISIBLE);
                holder.mStockCheck.setText(recentPurchaseArrayList.get(pos).getStock());
            } else {
                holder.mStockCheck.setVisibility(View.GONE);
            }

            Glide.with(mContext).load(recentPurchaseArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.imagePreOrder);

            int bgColor = ContextCompat.getColor(mContext, backgroundColors[position % 7]);
            holder.imagePreOrder.setBackgroundColor(bgColor);


            holder.mLinLayout.setOnClickListener(view -> setClickFunc(
                    recentPurchaseArrayList.get(pos).getProductName(),
                    recentPurchaseArrayList.get(pos).getItemCode(),
                    recentPurchaseArrayList.get(pos).getVendorCode()));
        }
    }

    private void setClickFunc(String productName, String itemCode, String vendorCode) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, productName);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        switch (mType) {
            case GlobalConstants.MRECENTVIEWS:
                return mArrayRecentView.size();
            case "":
                return recentPurchaseArrayList.size();
            default:
                return 0;
        }

    }


    class DashboardHolder1 extends RecyclerView.ViewHolder {


        private TextView mProductName1;
        private TextView mProductPrice1;
        private ImageView imagePreOrder;
        private LinearLayout mLinLayout;
        private TextView mItemCode;
        private TextView mDiscountPrice;
        private TextView mTopActualPrice;
        private TextView mStockCheck;
        private LinearLayout mTopDiscountLayout;
        private RelativeLayout mRelTop;


        DashboardHolder1(View view) {
            super(view);

            mContext = view.getContext();

            mProductName1 = itemView.findViewById(R.id.txt_productName);
            mProductPrice1 = itemView.findViewById(R.id.txt_productDes);
            imagePreOrder = itemView.findViewById(R.id.imagePreOrder);
            mLinLayout = itemView.findViewById(R.id.LL_recycleView);
            mItemCode = itemView.findViewById(R.id.text_itemcode);

            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mTopActualPrice = itemView.findViewById(R.id.text_actualprice);
            mTopDiscountLayout = itemView.findViewById(R.id.discountLayout);

            mRelTop = itemView.findViewById(R.id.rel_top);

            mStockCheck = itemView.findViewById(R.id.text_stock);


        }
    }

}
