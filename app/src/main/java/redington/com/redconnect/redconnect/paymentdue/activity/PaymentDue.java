package redington.com.redconnect.redconnect.paymentdue.activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.paymentdue.adapter.PaymentDueAdapter;
import redington.com.redconnect.redconnect.paymentdue.helper.PaymentDueManager;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.redconnect.shipmenttracking.model.ShipmentTrackingBean;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class PaymentDue extends BaseActivity implements UIListener {
    private Context mContext;
    private CommonUtils commonUtils;
    private String[] weekValues = {"", "", "", "", ""};
    private TextView mOverallAmount;
    private TextView mInvoiceAmount;
    private LinearLayout totalLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_due_new);

        mContext = PaymentDue.this;
        commonUtils = new CommonUtils(mContext);
        toolbar(IntentConstants.PAYMENTDUE);

        setInitViews();
    }

    private void setInitViews() {
        totalLayout = findViewById(R.id.total_layout);
        ImageView mOverdueImage = findViewById(R.id.overdue_image);
        mOverdueImage.setColorFilter(Color.WHITE);

        totalLayout.setOnClickListener(v -> LaunchIntentManager.routeToActivityStack(mContext, PaymentCommitmentDue.class));
        mOverallAmount = findViewById(R.id.text_overall_amount);
        mInvoiceAmount = findViewById(R.id.text_invoice_amount);

        callPaymentApi();
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    public void rightNavigation(View v) {
        setRightNavigation(v.getContext());
    }

    private void callPaymentApi() {
        String userId = GlobalConstants.GetSharedValues.getSpUserId();
        commonUtils.showProgressDialog();
        PaymentDueManager.getPaymentDueServiceCall(userId, GlobalConstants.NULL_DATA, mContext, this);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();

        PaymentDueResponse paymentDueResponse = (PaymentDueResponse) successObject;
        if (paymentDueResponse != null) {
            switch (paymentDueResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    totalLayout.setVisibility(View.VISIBLE);
                    weekValues[0] = String.valueOf(paymentDueResponse.getData().getHeader().get(0).getWeek1());
                    weekValues[1] = String.valueOf(paymentDueResponse.getData().getHeader().get(0).getWeek2());
                    weekValues[2] = String.valueOf(paymentDueResponse.getData().getHeader().get(0).getWeek3());
                    weekValues[3] = String.valueOf(paymentDueResponse.getData().getHeader().get(0).getWeek4());
                    weekValues[4] = String.valueOf(paymentDueResponse.getData().getHeader().get(0).getmGreater4());
                    mOverallAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalOverDue()));
                    mInvoiceAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalInvoiceValue()));
                    bindRecyclerViews();
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);

    }

    private void bindRecyclerViews() {
        RecyclerView paymentRecyclerView = findViewById(R.id.recycle_view_payment);
        paymentRecyclerView.setHasFixedSize(true);
        ArrayList<ShipmentTrackingBean> trackingBeans = prepareData();
        paymentRecyclerView.setAdapter(new PaymentDueAdapter(mContext, trackingBeans));
        paymentRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

    }

    private ArrayList<ShipmentTrackingBean> prepareData() {
        String[] weekNames = {"First Week", "Second Week", "Third Week", "Fourth Week", "Greater than 1 month"};
        ArrayList<ShipmentTrackingBean> preOrderBeans = new ArrayList<>();
        for (int i = 0; i < weekNames.length; i++) {
            preOrderBeans.add(new ShipmentTrackingBean(weekNames[i], weekValues[i]));
        }
        return preOrderBeans;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean internetCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (internetCheck) {
            internetCheckFalse();
            callPaymentApi();
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }
    }
}
