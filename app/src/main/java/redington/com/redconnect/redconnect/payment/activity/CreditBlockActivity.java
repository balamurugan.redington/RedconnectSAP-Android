package redington.com.redconnect.redconnect.payment.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.preorder.helper.CustomerEligibilityManager;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class CreditBlockActivity extends BaseActivity implements NewListener {
    private Context mContext;
    private CommonUtils commonUtils;
    private String userID;
    private String orderValue;
    private String stockLocation;
    private String paymentDays;
    private String deliverySequence;
    private ArrayList<CartData> cartArrayList;
    private String otpRefNumber = "";
    private boolean mSpotBoolean;
    private ArrayList<SubPromoCode> subPromoCodes;
    private String currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_block);

        mContext = CreditBlockActivity.this;
        commonUtils = new CommonUtils(mContext);

        Date date = new Date();
        currentDate = dateFormat(date);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            orderValue = bundle.getString(GlobalConstants.PRICE);
            assert orderValue != null;
            orderValue = orderValue.replace(",", "");
            orderValue = orderValue.replace("₹ ", "");

            paymentDays = bundle.getString(IntentConstants.PAYMENTDAYS_CARD);
            deliverySequence = bundle.getString(IntentConstants.DELIVERYSEQUENCE_CARD);

            mSpotBoolean = bundle.getBoolean(IntentConstants.SPOTBOOLEAN);
            subPromoCodes = (ArrayList<SubPromoCode>) bundle.getSerializable(IntentConstants.SPOTARRAY);
            cartArrayList = (ArrayList<CartData>) bundle.getSerializable(IntentConstants.CARTARRAY);

            if (!mSpotBoolean) {
                stockLocation = bundle.getString(IntentConstants.STOCKLOCATION);
            } else {
                stockLocation = subPromoCodes.get(0).getmSTOCKROOM();
            }

        }

        intiViews();


    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    private void intiViews() {
        toolbar(IntentConstants.PAYMENTOPTIONS);
        userID = GlobalConstants.GetSharedValues.getSpUserId();

        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        CardView creditBlock = findViewById(R.id.credit_limit_header);
        float[] tenRadius = {10f, 10f, 10f, 10f, 5f, 5f, 5f, 5f};
        commonUtils.setGradientColor(creditBlock, R.color.colorAccent_1, tenRadius);


        creditBlock.setOnClickListener(v -> checkEligibility());
    }


    private void checkEligibility() {
        commonUtils.showProgressDialog();
        CustomerEligibilityManager.getEligibilityCall(userID, Double.valueOf(orderValue), GlobalConstants.ONE, mContext, this);

    }

    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        eligibilityData(successObject);
    }

    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void eligibilityData(Object successObject) {
        CustomerEligibilityJsonResponse customerEligibilityJsonResponse = (CustomerEligibilityJsonResponse) successObject;
        if (customerEligibilityJsonResponse != null) {
            switch (customerEligibilityJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String status = customerEligibilityJsonResponse.getData().getStatus();
                    String mReason = customerEligibilityJsonResponse.getData().getReason();
                    otpRefNumber = customerEligibilityJsonResponse.getData().getReferenceNumber();
                    switch (status) {
                        case GlobalConstants.STATUS_R:
                            showEligibilityPopup(mContext, mReason);
                            break;
                        case GlobalConstants.STATUS_A:
                            showOtpScreen(GlobalConstants.NULL_DATA);
                            break;
                        default:
                            shortToast(mContext, GlobalConstants.SERVER_ERROR);
                            break;
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, customerEligibilityJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    private void showOtpScreen(String appId) {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.PRICE, orderValue);
        bundle.putString(GlobalConstants.REFERENCE_NUM, otpRefNumber);
        bundle.putString(IntentConstants.DATE, currentDate);
        bundle.putString(IntentConstants.REMARKS, GlobalConstants.SPACE);
        bundle.putString(IntentConstants.STOCKLOCATION, stockLocation);
        bundle.putString(IntentConstants.PAYMENTDAYS_CARD, paymentDays);
        bundle.putString(IntentConstants.DELIVERYSEQUENCE_CARD, deliverySequence);
        bundle.putBoolean(IntentConstants.SPOTBOOLEAN, mSpotBoolean);
        bundle.putString(IntentConstants.APP_ID, appId);
        bundle.putSerializable(IntentConstants.SPOTARRAY, subPromoCodes);
        bundle.putSerializable(IntentConstants.CARTARRAY, cartArrayList);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, OtpActivity.class, bundle);
    }

    @SuppressLint("NewApi")
    private void showEligibilityPopup(Context context, final String reason) {

        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);

        TextView prefTitle = mDialog.findViewById(R.id.pre_ref_title);
        TextView prefcont = mDialog.findViewById(R.id.pre_ref_cont);
        Button prefcontbtm = mDialog.findViewById(R.id.pre_ref_btm);
        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }

        if (!mSpotBoolean) {
            setCartUpdate(cartArrayList.size());
        }
        prefTitle.setVisibility(View.GONE);
        prefcont.setText(reason);


        prefcontbtm.setOnClickListener(v -> {
            mDialog.dismiss();
            if (reason.equals(IntentConstants.AMOUNTMISMATCH)) {
                setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
                LaunchIntentManager.routeToActivity(mContext, MyCartActivity.class);
            } else {
                LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
            }

        });

        mDialog.show();
    }


}
