package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.common.methods.CommonMethod;

public class PreorderItemDetailsData implements Serializable {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("AvailableQuantity")
    private String mAvailableQuantity;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("STOCK")
    private String mStock;
    @SerializedName("UNITPRICE")
    private String mUnitPrice;
    @SerializedName("DISCOUNTPRICE")
    private String mDiscountPrice;
    @SerializedName("DISCOUNTPERC")
    private String mDiscountPercent;
    @SerializedName("Images")
    private List<PreorderItemDetailsImage> mImages;
    @SerializedName("Productreview")
    private List<PreorderItemDetailsProductreview> mProductreview;
    @SerializedName("Specification")
    private List<PreorderItemDetailsSpecification> mSpecification;

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmAvailableQuantity() {
        return mAvailableQuantity;
    }

    public void setmAvailableQuantity(String mAvailableQuantity) {
        this.mAvailableQuantity = mAvailableQuantity;
    }

    public List<PreorderItemDetailsImage> getmImages() {
        return mImages;
    }

    public void setmImages(List<PreorderItemDetailsImage> mImages) {
        this.mImages = mImages;
    }

    public List<PreorderItemDetailsProductreview> getmProductreview() {
        return mProductreview;
    }

    public void setmProductreview(List<PreorderItemDetailsProductreview> mProductreview) {
        this.mProductreview = mProductreview;
    }

    public List<PreorderItemDetailsSpecification> getmSpecification() {
        return mSpecification;
    }

    public void setmSpecification(List<PreorderItemDetailsSpecification> mSpecification) {
        this.mSpecification = mSpecification;
    }

    public String getStock() {
        return mStock;
    }

    public void setStock(String mStock) {
        this.mStock = mStock;
    }

    public Double getUnitPrice() {
        return CommonMethod.getValidDouble(mUnitPrice);
    }

    public void setUnitPrice(Double mUnitPrice) {
        this.mUnitPrice = String.valueOf(mUnitPrice);
    }

    public Double getDiscountPrice() {
        return CommonMethod.getValidDouble(mDiscountPrice);
    }

    public void setDiscountPrice(Double mDiscountPrice) {
        this.mDiscountPrice = String.valueOf(mDiscountPrice);
    }


    public Double getDiscountPercent() {
        return CommonMethod.getValidDouble(mDiscountPercent);
    }

    public void setDiscountPercent(Double mDiscountPercent) {
        this.mDiscountPercent = String.valueOf(mDiscountPercent);
    }
}
