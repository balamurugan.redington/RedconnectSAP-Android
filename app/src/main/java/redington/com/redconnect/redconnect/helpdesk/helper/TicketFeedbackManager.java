package redington.com.redconnect.redconnect.helpdesk.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.helpdesk.model.TicketFeedbackResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketFeedbackManager {

    private TicketFeedbackManager() {
        throw new IllegalStateException(IntentConstants.HELPDESKSTATUS);
    }

    public static void getTicketFeedbackCall(String userID, String ticketNum, String rating, String feedback, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getTicketFeedback(getRequestBody(userID, ticketNum, rating, feedback)).enqueue(new Callback<TicketFeedbackResponse>() {
                @Override
                public void onResponse(Call<TicketFeedbackResponse> call, Response<TicketFeedbackResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<TicketFeedbackResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(String userid, String ticketNum, String rating, String feedback) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JHP_USERID, userid);
            jsonValues.put(JsonKeyConstants.JHP_TICKET_NUMBER, ticketNum);
            jsonValues.put(JsonKeyConstants.JHP_RATING, rating);
            jsonValues.put(JsonKeyConstants.JHP_FEEDBACK, feedback);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
