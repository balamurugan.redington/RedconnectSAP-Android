package redington.com.redconnect.redconnect.helpdesk.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.helpdesk.model.TicketHistoryJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketHistoryManager {

    private TicketHistoryManager() {
        throw new IllegalStateException(IntentConstants.TICKETHISTORY);
    }

    public static void getTicketHistoryServiceCall(String userId, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getTicketHistoryList(userId).enqueue(new Callback<TicketHistoryJsonResponse>() {
                @Override
                public void onResponse(Call<TicketHistoryJsonResponse> call, Response<TicketHistoryJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<TicketHistoryJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

}
