package redington.com.redconnect.redconnect.paymentdue.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueDetail;


public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    private final List<PaymentDueDetail> itemList;

    public InvoiceAdapter(List<PaymentDueDetail> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public InvoiceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_overdue_invoice_recycle, parent, false);

        return new InvoiceAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (position % 2 == 0) {
            holder.totalLayout.setBackgroundResource(R.color.white_100);
        } else {
            holder.totalLayout.setBackgroundResource(R.color.black_light_200);
        }

        holder.mInvoiceNum.setText(itemList.get(position).getInvoiceNumber());
        holder.mInvoiceDate.setText(itemList.get(position).getInvoiceDate());
        holder.mInvoiceValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + Double.toString(itemList.get(position).getInvoiceValue()));
        holder.mInvoiceBalDue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + Double.toString(itemList.get(position).getDueAmount()));
        holder.mInvoiceOldDays.setText(String.valueOf(itemList.get(position).getODDays()));

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        final LinearLayout totalLayout;
        final TextView mInvoiceNum;
        final TextView mInvoiceValue;
        final TextView mInvoiceDate;
        final TextView mInvoiceBalDue;
        final TextView mInvoiceOldDays;

        MyViewHolder(View view) {
            super(view);
            mInvoiceNum = view.findViewById(R.id.txt_invoice_no);
            mInvoiceDate = view.findViewById(R.id.txt_invoice_date);
            mInvoiceValue = view.findViewById(R.id.txt_invoice_amount);
            mInvoiceBalDue = view.findViewById(R.id.txt_invoice_bal_due);
            mInvoiceOldDays = view.findViewById(R.id.txt_invoice_od_days);

            totalLayout = view.findViewById(R.id.LL_recycleView);


        }
    }
}

