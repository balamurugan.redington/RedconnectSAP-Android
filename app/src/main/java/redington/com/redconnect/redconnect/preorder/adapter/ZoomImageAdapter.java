package redington.com.redconnect.redconnect.preorder.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetailZoomer;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsImage;

public class ZoomImageAdapter extends RecyclerView.Adapter<ZoomImageAdapter.ZoomHolder> {

    private final List<PreorderItemDetailsImage> mArrayList;
    private Context mContext;

    public ZoomImageAdapter(Context context, List<PreorderItemDetailsImage> itemList) {
        this.mArrayList = itemList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ZoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_zoom_image_recylce, parent, false);
        return new ZoomImageAdapter.ZoomHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ZoomHolder holder, int position) {
        Glide.with(mContext)
                .load(mArrayList.get(position).getImageURL())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .fitCenter()
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .into(holder.mImage);


        holder.mImage.setOnClickListener(view -> ((ProductDetailZoomer) mContext).setImage(mArrayList.get(holder.getAdapterPosition()).getImageURL()));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class ZoomHolder extends RecyclerView.ViewHolder {
        private final ImageView mImage;

        ZoomHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();

            mImage = itemView.findViewById(R.id.imagePreOrder);


        }
    }
}
