package redington.com.redconnect.redconnect.capitalfloat.helper;

import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.capitalfloat.model.CheckJbaIdReponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.CapitalFloatRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckJbaIdManager {
    private CheckJbaIdManager() {
        throw new IllegalStateException(IntentConstants.CAPITAL_FLOAT);
    }

    public static void checkJbaIdManager(String userId, final Context context, final UIListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = CapitalFloatRequest.getHeaderRequest(true, GlobalConstants.getCapitalFloatHeaders());
            RestClient.getInstance(headRequest).checkJbaId(userId).enqueue(new Callback<CheckJbaIdReponse>() {
                @Override
                public void onResponse(Call<CheckJbaIdReponse> call, Response<CheckJbaIdReponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<CheckJbaIdReponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });

        } else {
            listener.onError();
        }


    }
}
