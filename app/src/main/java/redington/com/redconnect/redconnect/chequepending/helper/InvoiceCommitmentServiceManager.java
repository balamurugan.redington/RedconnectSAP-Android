package redington.com.redconnect.redconnect.chequepending.helper;


import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.preorder.model.EncryptedInvoiceResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceCommitmentServiceManager {

    private InvoiceCommitmentServiceManager() {
        throw new IllegalStateException(IntentConstants.CHEQUEPENDING);
    }

    public static void getInvoiceCommitmentServiceCall(List<String> stringList,
                                                       ArrayList<String> serviceCharge,
                                                       final Context context,
                                                       final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getInvoiceCommitmentServiceCall(getRequestBody(stringList,serviceCharge))
                    .enqueue(new Callback<EncryptedInvoiceResponse>() {
                        @Override
                        public void onResponse(Call<EncryptedInvoiceResponse> call, Response<EncryptedInvoiceResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<EncryptedInvoiceResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(List<String> stringList,
                                              ArrayList<String> serviceCharge) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JIC_USERID, stringList.get(0));
            jsonValues.put(JsonKeyConstants.JIC_DATE, stringList.get(1));
            jsonValues.put(JsonKeyConstants.JIC_AMOUNT, stringList.get(2));
            jsonValues.put(JsonKeyConstants.JIC_TRANSACTION_ID, stringList.get(3));
            jsonValues.put(JsonKeyConstants.JIC_STATUS, stringList.get(4));
            jsonValues.put(JsonKeyConstants.JIC_REMARKS, stringList.get(5));
            jsonValues.put(JsonKeyConstants.JIC_AMOUNT_RECEIVED, stringList.get(6));
            jsonValues.put(JsonKeyConstants.JIC_TRANSACTION_RECEIVED, stringList.get(7));
            jsonValues.put(JsonKeyConstants.JIC_REFERENCE_NO, stringList.get(8));
            jsonValues.put(JsonKeyConstants.JIC_DEVICE_ID, stringList.get(9));
            jsonValues.put(JsonKeyConstants.JIC_UID, stringList.get(10));

            jsonValues.put(JsonKeyConstants.JIC_ACTUAL_AMOUNT, serviceCharge.get(0));
            jsonValues.put(JsonKeyConstants.JIC_SERVICE_CHARGE, serviceCharge.get(1));
            jsonValues.put(JsonKeyConstants.JIC_GST, serviceCharge.get(2));
            jsonValues.put(JsonKeyConstants.JIC_BANK_NAME, serviceCharge.get(3));
            jsonValues.put(JsonKeyConstants.JIC_TYPE_OF_TRANSACTION, serviceCharge.get(4));
            jsonValues.put(JsonKeyConstants.JIC_TYPE_OF_CARD, serviceCharge.get(5));

            String mEncryptedString = BaseActivity.mEncrypt(String.valueOf(jsonValues));

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JsonKeyConstants.JIC_DATA, mEncryptedString);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
