package redington.com.redconnect.redconnect.chequepending.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChequePendingServiceManager {

    private ChequePendingServiceManager() {
        throw new IllegalStateException(IntentConstants.CHEQUEPENDING);
    }

    public static void getChequePendingServiceCall(String userId, String pageNum, String recordNum,
                                                   final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getChequesList(userId, pageNum, recordNum)
                    .enqueue(new Callback<ChequePendingJsonResponse>() {
                        @Override
                        public void onResponse(Call<ChequePendingJsonResponse> call, Response<ChequePendingJsonResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<ChequePendingJsonResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });
        } else {
            listener.onError();
        }
    }


}
