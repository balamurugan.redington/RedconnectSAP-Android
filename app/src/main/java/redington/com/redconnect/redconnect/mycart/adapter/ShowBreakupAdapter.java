package redington.com.redconnect.redconnect.mycart.adapter;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.mycart.model.ShowBreakup;

public class ShowBreakupAdapter extends RecyclerView.Adapter<ShowBreakupAdapter.BreakupHolder> {


    private List<ShowBreakup> mArrayList;

    public ShowBreakupAdapter(List<ShowBreakup> breakups) {

        this.mArrayList = breakups;
    }

    @NonNull
    @Override
    public ShowBreakupAdapter.BreakupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_show_breakup, parent, false);
        return new ShowBreakupAdapter.BreakupHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ShowBreakupAdapter.BreakupHolder holder, int position) {

        holder.mLocation.setText(mArrayList.get(position).getSTOCKROOMDESC());
        holder.mQuantity.setText(String.valueOf(mArrayList.get(position).getUTILISEDQTY()));

        double igst = Double.parseDouble(mArrayList.get(position).getIGSTVALUE());
        double cgst = Double.parseDouble(mArrayList.get(position).getCGSTVALUE());
        double sgst = Double.parseDouble(mArrayList.get(position).getSGSTVALUE());

        double discountPercent = Double.parseDouble(mArrayList.get(position).getCDCDiscount());
        if (discountPercent == 0.00) {
            holder.layoutCdc.setVisibility(View.GONE);
            holder.layoutunitPrice.setVisibility(View.GONE);
        } else {
            holder.layoutCdc.setVisibility(View.VISIBLE);
            holder.layoutunitPrice.setVisibility(View.VISIBLE);
            holder.mDiscountPercent.setText(mArrayList.get(position).getCDCDiscount() +
                    GlobalConstants.PERCENT + GlobalConstants.DISCOUNT);
            holder.mDiscountAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    mArrayList.get(position).getCDCDiscountPRICE());

            double price = Double.parseDouble(mArrayList.get(position).getmUnitPrice());
            holder.mPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    BaseActivity.isValidNumberFormat().format(price));
        }

        if (igst != 0.00) {
            holder.laypotIGST.setVisibility(View.VISIBLE);
        } else {
            holder.laypotIGST.setVisibility(View.GONE);
        }

        if (cgst != 0.00) {
            holder.laypotCGST.setVisibility(View.VISIBLE);
        } else {
            holder.laypotCGST.setVisibility(View.GONE);
        }

        if (sgst != 0.00) {
            holder.laypotSGST.setVisibility(View.VISIBLE);
        } else {
            holder.laypotSGST.setVisibility(View.GONE);
        }

        holder.mIGSTTitle.setText(IntentConstants.IGST + mArrayList.get(position).getIGST() + IntentConstants.PERCENT);
        holder.mIGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(igst));

        holder.mCGSTTitle.setText(IntentConstants.CGST + mArrayList.get(position).getCGST() + IntentConstants.PERCENT);
        holder.mCGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(cgst));

        holder.mSGSTTitle.setText(IntentConstants.SGST + mArrayList.get(position).getSGST() + IntentConstants.PERCENT);
        holder.mSGST.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(sgst));

        holder.mTotalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getTOTAL()));
        holder.mUnitPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getTOTALPRICE()));

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class BreakupHolder extends RecyclerView.ViewHolder {

        private final TextView mLocation;
        private final TextView mQuantity;
        private final TextView mIGST;
        private final TextView mCGST;
        private final TextView mSGST;
        private final TextView mIGSTTitle;
        private final TextView mCGSTTitle;
        private final TextView mSGSTTitle;
        private final TextView mTotalAmount;
        private final TextView mUnitPrice;
        private final TextView mDiscountPercent;
        private final TextView mDiscountAmount;
        private final TextView mPrice;
        private LinearLayout laypotIGST;
        private LinearLayout laypotCGST;
        private LinearLayout laypotSGST;
        private LinearLayout layoutCdc;
        private LinearLayout layoutunitPrice;

        BreakupHolder(View itemView) {
            super(itemView);

            mLocation = itemView.findViewById(R.id.location);
            mQuantity = itemView.findViewById(R.id.Qty);
            mUnitPrice = itemView.findViewById(R.id.text_price);

            mIGST = itemView.findViewById(R.id.text_igst);
            mCGST = itemView.findViewById(R.id.text_cgst);
            mSGST = itemView.findViewById(R.id.text_sgst);
            mIGSTTitle = itemView.findViewById(R.id.igst_title);
            mCGSTTitle = itemView.findViewById(R.id.cgst_title);
            mSGSTTitle = itemView.findViewById(R.id.sgst_title);
            mTotalAmount = itemView.findViewById(R.id.totalPrice);
            mDiscountPercent = itemView.findViewById(R.id.discountPercent);
            mDiscountAmount = itemView.findViewById(R.id.discountAmount);
            mPrice = itemView.findViewById(R.id.unit_price);

            laypotIGST = itemView.findViewById(R.id.layout_igst);
            laypotCGST = itemView.findViewById(R.id.layout_cgst);
            laypotSGST = itemView.findViewById(R.id.layout_sgst);
            layoutCdc = itemView.findViewById(R.id.cdcDiscount);
            layoutunitPrice = itemView.findViewById(R.id.unitPriceLayout);
        }
    }
}
