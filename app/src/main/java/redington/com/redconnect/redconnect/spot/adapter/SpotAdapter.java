package redington.com.redconnect.redconnect.spot.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.FestivalOffer;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;
import redington.com.redconnect.redconnect.spot.dialog.ViewBreakupSheet;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;

public class SpotAdapter extends RecyclerView.Adapter<SpotAdapter.SpotHolder> {

    private Context mContext;
    private List<SubPromoCode> mArrayList;
    private List<FestivalOffer> festivalOffers;
    private String mType = "";
    private int cameFrom = 0;

    public SpotAdapter(Context context, List<SubPromoCode> itemList, String type) {
        this.mContext = context;
        this.mArrayList = itemList;
        this.mType = type;

    }

    public SpotAdapter(Context context, List<FestivalOffer> festivalOffers, int type) {
        this.mContext = context;
        this.festivalOffers = festivalOffers;
        this.cameFrom = type;

    }

    @NonNull
    @Override
    public SpotAdapter.SpotHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spot_recycle_list, parent, false);
        return new SpotAdapter.SpotHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final SpotAdapter.SpotHolder holder, int position) {

        final int pos = holder.getAdapterPosition();
        if (mType.equals(IntentConstants.SPOT) && cameFrom == 0) {
            holder.mViewBreakup.setVisibility(View.VISIBLE);
            holder.mProductName.setText(mArrayList.get(position).getmProductName());
            double offer = mArrayList.get(position).getDiscountPercent();
            if (offer == 0.0) {
                holder.mFrameLayout.setVisibility(View.GONE);
                holder.mLinearLayout.setVisibility(View.GONE);
                holder.mPrice.setVisibility(View.VISIBLE);
                holder.mPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        String.valueOf(BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getUnitPrice())));

            } else {
                holder.mPrice.setVisibility(View.GONE);
                holder.mFlatOffer.setText(String.valueOf(mArrayList.get(position).getDiscountPercent()) + " Off");
                holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        String.valueOf(BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getUnitPrice())));
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        String.valueOf(BaseActivity.isValidNumberFormat().format(mArrayList.get(position).getActualPrice())));
                holder.mDiscountPrice.setPaintFlags(holder.mDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            holder.mQuantity.setText(String.format("Quantity : %s", String.valueOf(mArrayList.get(position).getQuantity())));
            Glide.with(mContext).load(mArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .fitCenter()
                    .into(holder.mImage);

            holder.mViewBreakup.setOnClickListener(v -> showBottomSheet(pos));

        } else if (mType.equals("") && cameFrom == 1) {

            spotValues(position, holder);

            holder.mTotalLayout.setOnClickListener(view -> setClickFunc(festivalOffers.get(pos).getmProductName(),
                    String.valueOf(festivalOffers.get(pos).getmDiscountPrice()),
                    festivalOffers.get(pos).getmItemCode(),
                    festivalOffers.get(pos).getmVendorCode(),
                    GlobalConstants.STOCK_AVAILABLE,
                    String.valueOf(festivalOffers.get(pos).getmActualPrice()),
                    festivalOffers.get(pos).getmDiscountPercent()));
        }

    }

    @SuppressLint("SetTextI18n")
    private void spotValues(final int position, final SpotHolder holder) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.getDefault());
        String serverDate = festivalOffers.get(position).getmServerDate();
        holder.mTimerText.setVisibility(View.VISIBLE);
        holder.mProductName.setText(festivalOffers.get(position).getmProductName());
        holder.mQuantity.setText(festivalOffers.get(position).getmItemCode());
        double offer = Double.parseDouble(festivalOffers.get(position).getmDiscountPercent());
        if (offer == 0.0) {
            holder.mFrameLayout.setVisibility(View.GONE);
            holder.mLinearLayout.setVisibility(View.GONE);
            holder.mProductPrice.setVisibility(View.VISIBLE);
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(festivalOffers.get(position).getmDiscountPrice())));
        } else {
            holder.mPrice.setVisibility(View.GONE);
            holder.mFlatOffer.setText(String.valueOf(festivalOffers.get(position).getmDiscountPercent()) + " Off");
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(festivalOffers.get(position).getmDiscountPrice())));
            holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(festivalOffers.get(position).getmActualPrice())));
            holder.mDiscountPrice.setPaintFlags(holder.mDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        Glide.with(mContext).load(festivalOffers.get(position).getmImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.redinton_image)
                .into(holder.mImage);

        try {
            Date date1 = simpleDateFormat.parse(serverDate + " " + festivalOffers.get(position).getmServerTime());
            Date date2 = simpleDateFormat.parse(festivalOffers.get(position).getmEndingDate() + " " + festivalOffers.get(position).getmEndTime());

            String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
            long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

            long endTime = BaseActivity.dateFormatMinusTwo(festivalOffers.get(position).getmEndingDate(), festivalOffers.get(position).getmEndTime());
            long startTime = BaseActivity.longConversion(serverDate, festivalOffers.get(position).getmServerTime());
            if (startTime >= endTime) {
                new CountDownTimer(diffInMillisec, 1000) {
                    @Override
                    public void onTick(long millis) {
                        @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millis),
                                TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                        holder.mTimerText.setText(hms);
                    }

                    @Override
                    public void onFinish() {
                        // Not in use
                    }
                }.start();
            } else {
                holder.mTimerText.setText(diffrenceDate);
            }

        } catch (ParseException e) {
            BaseActivity.logd(e.getMessage());
        }


    }

    private void showBottomSheet(int pos) {
        ViewBreakupSheet viewBreakupSheet = ViewBreakupSheet.getInstance();
        Bundle bundle = new Bundle();
        bundle.putString(IntentConstants.CGST, mArrayList.get(pos).getmCGSTPercent());
        bundle.putString(IntentConstants.SGST, mArrayList.get(pos).getmSGSTPercent());
        bundle.putString(IntentConstants.IGST, mArrayList.get(pos).getmIGSTPercent());
        bundle.putString(IntentConstants.CASH_DISCOUNT, String.valueOf(mArrayList.get(pos).getCASHDISCOUNT()));
        bundle.putString(IntentConstants.CGSTVALUE, mArrayList.get(pos).getmCGSTVALUE());
        bundle.putString(IntentConstants.SGSTVALUE, mArrayList.get(pos).getmSGSTVALUE());
        bundle.putString(IntentConstants.IGSTVALUE, mArrayList.get(pos).getmIGSTVALUE());
        bundle.putString(IntentConstants.CASH_DISCOUNT_PRICE, String.valueOf(mArrayList.get(pos).getCASHDISCOUNTPRICE()));
        bundle.putString(IntentConstants.SPOTTOTTAL, String.valueOf(mArrayList.get(pos).getmTOTAL()));
        bundle.putString(IntentConstants.SPOTUNITPRICE, String.valueOf(mArrayList.get(pos).getUnitPrice()));
        bundle.putString(IntentConstants.SPOTACTUALPRICE, String.valueOf(mArrayList.get(pos).getActualPrice()));
        bundle.putString(IntentConstants.SPOTSTOCKROOM, String.valueOf(mArrayList.get(pos).getmSTOCKROOMDESC()));
        viewBreakupSheet.setArguments(bundle);
        viewBreakupSheet.show(((FragmentActivity) mContext).getSupportFragmentManager(), "Bottom Sheet");
    }

    private void setClickFunc(String productName, String discountPrice, String itemCode, String vendorCode, String stockAvaliable,
                              String actualPrice, String discountPercent) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, productName);
        i.putExtra(GlobalConstants.PRODUCT_PRICE, discountPrice);
        i.putExtra(GlobalConstants.STOCK_CHECK, stockAvaliable);
        i.putExtra(GlobalConstants.PRODUCT_CATEGORY, GlobalConstants.NULL_DATA);
        i.putExtra(GlobalConstants.PRODUCT_BRAND, GlobalConstants.NULL_DATA);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);
        i.putExtra(GlobalConstants.PROCDUCT_ACTUAL_PRICE, actualPrice);
        i.putExtra(GlobalConstants.PROCDUCT_DIS_PERCENT, discountPercent);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        if (mType.equals(IntentConstants.SPOT) && cameFrom == 0) {
            return mArrayList.size();
        } else if (mType.equals("") && cameFrom == 1) {
            return festivalOffers.size();
        } else {
            return 0;
        }
    }

    class SpotHolder extends RecyclerView.ViewHolder {
        private final TextView mProductName;
        private final TextView mProductPrice;

        private final TextView mFlatOffer;
        private final TextView mDiscountPrice;
        private final TextView mQuantity;
        private final FrameLayout mFrameLayout;
        private final ImageView mImage;
        private final LinearLayout mLinearLayout;
        private final TextView mPrice;
        private TextView mTimerText;
        private CardView mTotalLayout;
        private TextView mViewBreakup;


        SpotHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();

            mProductName = itemView.findViewById(R.id.text_productname);
            mProductPrice = itemView.findViewById(R.id.text_productprice);

            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mFlatOffer = itemView.findViewById(R.id.text_flatOffer);
            mQuantity = itemView.findViewById(R.id.text_quantity);
            mFrameLayout = itemView.findViewById(R.id.offerLayout);
            mLinearLayout = itemView.findViewById(R.id.discountLayout);
            mTotalLayout = itemView.findViewById(R.id.totallayout);
            mPrice = itemView.findViewById(R.id.productPrice);
            mImage = itemView.findViewById(R.id.image);
            mTimerText = itemView.findViewById(R.id.timer_text);

            mViewBreakup = itemView.findViewById(R.id.text_breakup);


        }
    }

}
