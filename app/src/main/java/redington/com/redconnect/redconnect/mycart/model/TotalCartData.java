
package redington.com.redconnect.redconnect.mycart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class TotalCartData implements Serializable {

    @SerializedName("Cart")
    private List<CartData> mCart;

    public List<CartData> getCart() {
        return mCart;
    }

    public void setCart(List<CartData> cart) {
        mCart = cart;
    }

}
