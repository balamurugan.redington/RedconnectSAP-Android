package redington.com.redconnect.redconnect.preorder.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsSpecification;


public class PreOrderSpecAdapter extends RecyclerView.Adapter<PreOrderSpecAdapter.SpecHolder> {

    private final List<PreorderItemDetailsSpecification> mArrayList;


    public PreOrderSpecAdapter(List<PreorderItemDetailsSpecification> mArrayList) {
        this.mArrayList = mArrayList;

    }

    @NonNull
    @Override
    public PreOrderSpecAdapter.SpecHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_preorderdetail_spec, parent, false);
        return new PreOrderSpecAdapter.SpecHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderSpecAdapter.SpecHolder holder, int position) {
        holder.specHeader.setText(mArrayList.get(position).getHeader());
        holder.specDetails.setText(mArrayList.get(position).getHeaderValue());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class SpecHolder extends RecyclerView.ViewHolder {

        private final TextView specHeader;
        private final TextView specDetails;


        SpecHolder(View itemView) {
            super(itemView);

            specHeader = itemView.findViewById(R.id.spec_header);
            specDetails = itemView.findViewById(R.id.spec_details);
            

        }
    }
}
