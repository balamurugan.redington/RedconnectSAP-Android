package redington.com.redconnect.redconnect.login.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.helper.CityServiceeHelper;
import redington.com.redconnect.redconnect.login.model.CityData;
import redington.com.redconnect.redconnect.login.model.CityJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

public class Partnercreation3 extends BaseActivity implements UIListener {

    private EditText mResCity;
    private EditText mResState;
    private EditText mResCity1;
    private EditText mResState1;
    private Context mContext;
    private CommonUtils commonUtils;
    private ScrollView mScrollview;
    private ArrayAdapter<String> listAdapter;
    private String cityString;
    private ArrayList<String> secondArray;
    private EditText mRedAdd1;
    private EditText mResAdd2;
    private EditText mResAdd3;
    private EditText mRedAdd4;
    private EditText mResAdd5;
    private EditText mResAdd6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partner_fragment3);

        mContext = Partnercreation3.this;
        commonUtils = new CommonUtils(mContext);

        toolbar(IntentConstants.CUSTOMERPROFILE);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        mScrollview = findViewById(R.id.scrollView);

        mRedAdd1 = findViewById(R.id.part_resaddress1);
        mResAdd2 = findViewById(R.id.part_resaddress2);
        mResAdd3 = findViewById(R.id.part_resaddress3);
        mResCity = findViewById(R.id.part_rescity);
        mResState = findViewById(R.id.part_resstate);
        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            secondArray = bundle.getStringArrayList(IntentConstants.SECONDARRAY);
        }

        mRedAdd4 = findViewById(R.id.part_resaddress2_1);
        mResAdd5 = findViewById(R.id.part_resaddress2_2);
        mResAdd6 = findViewById(R.id.part_resaddress2_3);
        mResCity1 = findViewById(R.id.part_rescity2);
        mResState1 = findViewById(R.id.part_resstate2);

        mResCity.setOnClickListener(v -> {
            cityString = GlobalConstants.M_RES_CITY1;
            showPopup(mContext);
        });

        mResCity1.setOnClickListener(v -> {
            cityString = GlobalConstants.M_RES_CITY2;
            showPopup(mContext);
        });

        findViewById(R.id.part_buttonPrevious1).setOnClickListener(v -> finish());
        findViewById(R.id.part_buttonNext1).setOnClickListener(v -> {
            if (mRedAdd1.getText().toString().trim().isEmpty()) {
                shortToast(mContext, plsEnter(R.string.resAdd1));
                mRedAdd1.setFocusableInTouchMode(true);
                mRedAdd1.requestFocus();
            } else if (mResAdd2.getText().toString().trim().isEmpty()) {
                shortToast(mContext, plsEnter(R.string.resAdd2));
                mResAdd2.setFocusableInTouchMode(true);
                mResAdd2.requestFocus();
            } else if (mResAdd3.getText().toString().trim().isEmpty()) {
                shortToast(mContext, plsEnter(R.string.resAdd3));
                mResAdd3.setFocusableInTouchMode(true);
                mResAdd3.requestFocus();
            } else if (mResCity.getText().toString().trim().isEmpty()) {
                shortToast(mContext, plsEnter(R.string.resCity1));
                mResCity.setFocusableInTouchMode(true);
                mResCity.requestFocus();
            } else {

                onClickPart();

            }
        });

    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }


    @SuppressLint("NewApi")
    private void showPopup(final Context mContext) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_quanty);
        mDialog.setCanceledOnTouchOutside(false);

        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        TextView mTextView = mDialog.findViewById(R.id.text_title);
        mTextView.setText(GlobalConstants.ENTER_CITY);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }
        final EditText editable = mDialog.findViewById(R.id.ed_myquantity);
        editable.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editable.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});

        Button button = mDialog.findViewById(R.id.pre_ref_btm);
        button.setOnClickListener(v -> {
            final String ed = editable.getText().toString();
            if (!ed.isEmpty() && ed.length() > 2) {
                callCityApi(ed);
                mDialog.dismiss();
            } else {
                shortToast(mContext, GlobalConstants.VALID_CITY);
            }
        });

        mDialog.show();
    }

    private String plsEnter(int id) {
        return GlobalConstants.ENTER_TEXT + getString(id);
    }

    private void onClickPart() {
        Bundle bundle1 = new Bundle();
        ArrayList<String> thirdArrayList = new ArrayList<>(secondArray);
        thirdArrayList.add(mRedAdd1.getText().toString().trim());
        thirdArrayList.add(mResAdd2.getText().toString().trim());
        thirdArrayList.add(mResAdd3.getText().toString().trim());
        thirdArrayList.add(mResCity.getText().toString().trim());
        thirdArrayList.add(mResState.getText().toString().trim());
        thirdArrayList.add(mRedAdd4.getText().toString().trim());
        thirdArrayList.add(mResAdd5.getText().toString().trim());
        thirdArrayList.add(mResAdd6.getText().toString().trim());
        thirdArrayList.add(mResCity1.getText().toString().trim());
        thirdArrayList.add(mResState1.getText().toString().trim());
        bundle1.putStringArrayList(IntentConstants.THIRDARRAY, thirdArrayList);
        LaunchIntentManager.routeToActivityStackBundle(mContext, Partnercreation4.class, bundle1);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    private void callCityApi(String cityName) {
        commonUtils.showProgressDialog();
        CityServiceeHelper.getCityServiceCall(cityName, mContext, this);
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CityJsonResponse cityJsonResponse = (CityJsonResponse) successObject;
        if (cityJsonResponse != null) {
            switch (cityJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    List<CityData> cityData = cityJsonResponse.getData();
                    List<String> listArr = new ArrayList<>();
                    List<String> listArrStste = new ArrayList<>();
                    for (int i = 0; i < cityData.size(); i++) {
                        listArr.add(cityData.get(i).getCityMode() + " - " + cityData.get(i).getCityDescription());
                        listArrStste.add(cityData.get(i).getState());
                    }

                    showDialogWithLV(mContext, listArr, listArrStste, cityString);
                    break;
                case IntentConstants.FAILURE_URL:
                    snackBar(GlobalConstants.NO_DATA);
                    clearTextValues(cityString);
                    break;
                default:
                    snackBar(GlobalConstants.SERVER_ERROR_BAR);
                    clearTextValues(cityString);
                    break;
            }
        } else {
            snackBar(GlobalConstants.SERVER_ERROR_BAR);
            clearTextValues(cityString);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.SERVER_ERROR_BAR);
        clearTextValues(cityString);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.INTERNET_ERROR);
    }

    private void clearTextValues(String cameString) {
        if (cameString.equals(GlobalConstants.M_RES_CITY1)) {
            mResCity.setText("");
            mResState.setText("");
        } else if (cameString.equals(GlobalConstants.M_RES_CITY2)) {
            mResCity1.setText("");
            mResState1.setText("");
        }

    }

    /*City Response*/
    @SuppressLint("NewApi")
    public void showDialogWithLV(Context con, final List<String> list, final List<String> state, final String title) {

        final Dialog mDialog = new Dialog(con, R.style.ActivityDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.spinner_dialog_despatch_mode);
        mDialog.setCanceledOnTouchOutside(true);
        ListView mListView = mDialog.findViewById(R.id.recipe_list_view);
        TextView textTitle = mDialog.findViewById(R.id.recipe_list_title);
        textTitle.setText(GlobalConstants.SELECT_CITY);
        mListView.setDividerHeight(1);

        LinearLayout mDialogmain = mDialog.findViewById(R.id.dialogMain);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(ContextCompat.getColor(con, R.color.colorPrimary));
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mDialogmain.setBackground(gradientDrawable);
        }
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            if (title.equals(GlobalConstants.M_RES_CITY1)) {
                String[] s1 = list.get(position).split("-");
                mResCity.setText(s1[1]);
                mResState.setText(state.get(position));
            } else if (title.equals(GlobalConstants.M_RES_CITY2)) {
                String[] s2 = list.get(position).split("-");
                mResCity1.setText(s2[1]);
                mResState1.setText(state.get(position));
            }
            mDialog.dismiss();
        });

        listAdapter = new ArrayAdapter<String>(mContext, R.layout.mytext, list) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                listAdapter.notifyDataSetChanged();
                return view;
            }
        };
        mListView.setAdapter(listAdapter);

        mDialog.show();
    }

    /*Show Snack Bar*/
    private void snackBar(String newString) {
        Snackbar snackbar = Snackbar.make(mScrollview, newString, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
