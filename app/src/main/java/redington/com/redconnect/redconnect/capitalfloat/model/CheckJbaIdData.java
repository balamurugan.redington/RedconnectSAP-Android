
package redington.com.redconnect.redconnect.capitalfloat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class CheckJbaIdData implements Serializable {

    @SerializedName("AppId")
    private String mAppId;
    @SerializedName("AvlCredit")
    private String mAvlCredit;
    @SerializedName("JBAID")
    private String mJBAID;
    @SerializedName("UserSts")
    private String mUserSts;

    public String getAppId() {
        return mAppId;
    }

    public String getAvlCredit() {
        return mAvlCredit;
    }

    public String getJBAID() {
        return mJBAID;
    }

    public String getUserSts() {
        return mUserSts;
    }


}
