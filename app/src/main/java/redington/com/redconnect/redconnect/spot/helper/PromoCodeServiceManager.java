package redington.com.redconnect.redconnect.spot.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromoCodeServiceManager {

    private PromoCodeServiceManager() {
        throw new IllegalStateException(IntentConstants.SPOT);
    }

    public static void promoCodeServicecall(String custCode, String delSeq, String promoCode, String saleOrdNo, String flag, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getPromoCode(getRequestBody(custCode, delSeq, promoCode, saleOrdNo, flag)).enqueue(new Callback<PromoCodeJsonResponse>() {
                @Override
                public void onResponse(Call<PromoCodeJsonResponse> call, Response<PromoCodeJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<PromoCodeJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }


    /*String to Request Body*/
    private static RequestBody getRequestBody(String custCode, String delSeq, String promoCode, String saleOrdNo, String flag) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CUSTOMERCODE, custCode);
            if (delSeq.equals(IntentConstants.CARTDELSEQ)) {
                jsonValues.put(JsonKeyConstants.JOG_DEL_SEQ, custCode);
            } else {
                jsonValues.put(JsonKeyConstants.JOG_DEL_SEQ, delSeq);
            }
            jsonValues.put(JsonKeyConstants.JPROMO_CODE, promoCode);
            jsonValues.put(JsonKeyConstants.JPROMO_SALE_ORD_NO, saleOrdNo);
            jsonValues.put(JsonKeyConstants.JPROMO_FLAG, flag);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
