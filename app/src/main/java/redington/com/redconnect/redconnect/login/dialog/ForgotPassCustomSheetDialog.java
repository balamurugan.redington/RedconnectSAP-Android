package redington.com.redconnect.redconnect.login.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.Objects;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.activity.LoginActivity;
import redington.com.redconnect.redconnect.login.helper.ForgotPassServiceManager;
import redington.com.redconnect.redconnect.login.model.ForgotPassJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.KeyboardUtil;


public class ForgotPassCustomSheetDialog extends BottomSheetDialogFragment implements UIListener {

    private String getForgottEdt;
    private CommonUtils commonUtils;
    private Context mContext;
    private CoordinatorLayout layout;


    public static ForgotPassCustomSheetDialog getInstance() {

        return new ForgotPassCustomSheetDialog();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.forgotpass_bottom_sheet, viewGroup, false);

        //mContext = v.getContext();
        mContext = getActivity();
        commonUtils = new CommonUtils(getActivity());
        layout = v.findViewById(R.id.topLayout);

        new KeyboardUtil(Objects.requireNonNull(getActivity()), v);

        Button btm = v.findViewById(R.id.btn_forgotpass);
        final EditText mEditText = v.findViewById(R.id.ed_forgotpass);
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8),
                BaseActivity.setValidInoutFilter(BaseActivity.BLOCK_USER_CHARACTER_SET)});

        btm.setOnClickListener(v1 -> {
            getForgottEdt = mEditText.getText().toString();
            if (getForgottEdt.length() > 5 && getForgottEdt.length() < 9) {
                loadData(getForgottEdt);
            } else {
                /*BaseActivity.shortToast(mContext, GlobalConstants.CUST_CODE_VAL);*/
                BaseActivity.shortToast(mContext, layout, GlobalConstants.CUST_CODE_VAL);
            }
        });

        return v;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources resources = getResources();

        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            assert getView() != null;
            View parent = (View) getView().getParent();
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) parent.getLayoutParams();
            layoutParams.setMargins(
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left), // 64dp
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left),
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_right), // 64dp
                    resources.getDimensionPixelSize(R.dimen.bottom_sheet_margin_left)
            );
            parent.setLayoutParams(layoutParams);
        }
    }

    private void loadData(String custCode) {
        commonUtils.showProgressDialog();
        ForgotPassServiceManager.getForgotPassService(custCode, mContext, this);
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        ForgotPassJsonResponse forgotPassJsonResponse = (ForgotPassJsonResponse) successObject;

        switch (forgotPassJsonResponse.getStatusCode()) {
            case IntentConstants.SUCCESS_URL:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCancelable(false);
                alertDialog.setMessage(GlobalConstants.FORGOT_PASS);
                alertDialog.setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    dismiss();
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
                Button button = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
                break;
            case IntentConstants.FAILURE_URL:
                /*BaseActivity.shortToast(mContext, GlobalConstants.NOT_VALID_CUST);*/
                BaseActivity.shortToast(mContext, layout, GlobalConstants.NOT_VALID_CUST);
                break;
            default:
                /*BaseActivity.shortToast(mContext, GlobalConstants.CONNECT_ERROR);*/
                BaseActivity.shortToast(mContext, layout, GlobalConstants.CONNECT_ERROR);
                break;
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        /*BaseActivity.shortToast(mContext, GlobalConstants.CONNECT_ERROR);*/
        BaseActivity.shortToast(mContext, layout, GlobalConstants.CONNECT_ERROR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        dismiss();
        ((LoginActivity) mContext).onError();
    }
}

