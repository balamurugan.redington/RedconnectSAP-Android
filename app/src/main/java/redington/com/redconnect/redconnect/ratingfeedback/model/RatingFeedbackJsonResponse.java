package redington.com.redconnect.redconnect.ratingfeedback.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RatingFeedbackJsonResponse implements Serializable {

    @SerializedName("Data")
    private RatingFeedbackData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public RatingFeedbackData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }

    public String getStatusCode() {
        return mStatusCode;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }


}
