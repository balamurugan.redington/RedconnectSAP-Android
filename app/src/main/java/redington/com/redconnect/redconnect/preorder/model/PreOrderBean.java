package redington.com.redconnect.redconnect.preorder.model;


public class PreOrderBean {
    private final String name;
    private final int images;
    private final String name1;


    public PreOrderBean(String name, String name1, int images) {
        this.name = name;
        this.images = images;
        this.name1 = name1;

    }

    public String getName() {
        return name;
    }

    public int getImages() {
        return images;
    }

    public String getName1() {
        return name1;
    }


}
