
package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdatedCartData implements Serializable {

    @SerializedName("Status")
    private String mStatus;

    public String getStatus() {
        return mStatus;
    }


}
