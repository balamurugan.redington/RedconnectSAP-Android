package redington.com.redconnect.redconnect.login.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.util.LaunchIntentManager;

public class PartnerCreation2 extends BaseActivity {
    private Context mContext;
    private int mSpinenrPositionType;


    private ArrayList<String> firstArray;

    private EditText mContactName;
    private EditText mContactMail;
    private EditText mPhoneNum;
    private EditText mFaxNum;
    private EditText mPanNum;
    private EditText mGst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partner_fragment2);
        mContext = PartnerCreation2.this;


        toolbar(IntentConstants.CUSTOMERPROFILE);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            firstArray = bundle.getStringArrayList(IntentConstants.FIRSTARRAY);
        }

        String[] spinnerType = getResources().getStringArray(R.array.spinnerType);

        mContactName = findViewById(R.id.part_contactName);
        mContactMail = findViewById(R.id.part_contactmail);
        mPhoneNum = findViewById(R.id.part_contactphone);
        mFaxNum = findViewById(R.id.part_contactfax);
        mPanNum = findViewById(R.id.part_contactpan);

        mGst = findViewById(R.id.part_contactgst);
        Spinner mTypeSpinner = findViewById(R.id.part_spinner_type);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, spinnerType);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeSpinner.setAdapter(arrayAdapter);


        mTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSpinenrPositionType = i + 1;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //No data
            }
        });

        findViewById(R.id.part_buttonPrevious).setOnClickListener(v -> finish());


        mPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //No data
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mPhoneNum.getText().toString().matches("^0")) {
                    mPhoneNum.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //No data
            }
        });

        mGst.setFilters(new InputFilter[]{filter});
        mPanNum.setFilters(new InputFilter[]{filter});
        mPanNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //No data
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mPanNum.getText().toString().matches("^0")) {
                    mPanNum.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //No data
            }
        });


        findViewById(R.id.part_buttonNext).setOnClickListener(v -> buttonNext());

    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    private void buttonNext() {
        if (mContactName.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.contactName));
            mContactName.setFocusableInTouchMode(true);
            mContactName.requestFocus();
        } else if (mContactMail.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.contactMail));
            mContactMail.setFocusableInTouchMode(true);
            mContactMail.requestFocus();
        } else if (!mContactMail.getText().toString().contains("@") && !mContactMail.getText().toString().contains(".")) {
            shortToast(mContext, plsEnter(R.string.validMail));
            mContactMail.setFocusableInTouchMode(true);
            mContactMail.requestFocus();
        } else if (mPhoneNum.length() < 10 || mPhoneNum.length() > 11) {
            shortToast(mContext, plsEnter(R.string.validNum));
            mPhoneNum.setFocusableInTouchMode(true);
            mPhoneNum.requestFocus();
        } else if (mPhoneNum.getText().toString().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.phNum));
            mPhoneNum.setFocusableInTouchMode(true);
            mPhoneNum.requestFocus();
        } else if (!mFaxNum.getText().toString().trim().equals("") && (mFaxNum.length() < 10 || mFaxNum.length() > 11)) {
            shortToast(mContext, plsEnter(R.string.faxNum));
            mFaxNum.setFocusableInTouchMode(true);
            mFaxNum.requestFocus();
        } else if (mPanNum.getText().toString().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.pan));
            mPanNum.setFocusableInTouchMode(true);
            mPanNum.requestFocus();
        } else if (mPanNum.getText().toString().length() != 10) {
            shortToast(mContext, plsEnter(R.string.validPan));
            mPanNum.setFocusableInTouchMode(true);
            mPanNum.requestFocus();
        } else if (mGst.getText().toString().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.gst));
            mGst.setFocusableInTouchMode(true);
            mGst.requestFocus();
        } else if (mGst.getText().toString().length() != 15) {
            shortToast(mContext, plsEnter(R.string.validGst));
            mGst.setFocusableInTouchMode(true);
            mGst.requestFocus();
        } else {

            ArrayList<String> secondArraylist = new ArrayList<>(firstArray);
            secondArraylist.add(mContactName.getText().toString().trim());
            secondArraylist.add(mContactMail.getText().toString().trim());
            secondArraylist.add(mPhoneNum.getText().toString().trim());
            secondArraylist.add(mFaxNum.getText().toString().trim());
            secondArraylist.add(mPanNum.getText().toString().trim());
            secondArraylist.add(mGst.getText().toString().trim());
            secondArraylist.add(String.valueOf(mSpinenrPositionType).trim());
            Bundle bundle1 = new Bundle();
            bundle1.putStringArrayList(IntentConstants.SECONDARRAY, secondArraylist);
            LaunchIntentManager.routeToActivityStackBundle(mContext, Partnercreation3.class, bundle1);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        }
    }

    private String plsEnter(int id) {
        return GlobalConstants.ENTER_TEXT + getString(id);
    }

}
