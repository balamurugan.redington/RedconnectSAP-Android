package redington.com.redconnect.redconnect.dashboard.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.EDMJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EdmServiceManager {
    
    private EdmServiceManager() {
        throw new IllegalStateException(IntentConstants.EDM);
    }

    public static void edmServiceCall(String userId, String requestNuumber, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getEDMList(userId, requestNuumber).enqueue(new Callback<EDMJsonResponse>() {
                @Override
                public void onResponse(Call<EDMJsonResponse> call, Response<EDMJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<EDMJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }
}
