
package redington.com.redconnect.redconnect.ratingfeedback.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import redington.com.redconnect.common.methods.CommonMethod;


public class FeedbackData implements Serializable {

    @SerializedName("Category")
    private String mCategory;
    @SerializedName("Images")
    private List<FeedbackImage> mImages;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDescription")
    private String mItemDescription;
    @SerializedName("PageNo")
    private String mPageNo;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("RecordNo")
    private String mRecordNo;
    @SerializedName("TotalPage")
    private String mTotalPage;
    @SerializedName("UnitPrice")
    private String mUnitPrice;
    @SerializedName("VendorCode")
    private String mVendorCode;

    public String getCategory() {
        return mCategory;
    }


    public List<FeedbackImage> getImages() {
        return mImages;
    }


    public String getItemCode() {
        return mItemCode;
    }


    public String getItemDescription() {
        return mItemDescription;
    }


    public String getPageNo() {
        return mPageNo;
    }


    public String getProductName() {
        return mProductName;
    }


    public String getRecordNo() {
        return mRecordNo;
    }


    public String getTotalPage() {
        return CommonMethod.getValidIntString(mTotalPage);
    }


    public String getUnitPrice() {
        return CommonMethod.getValidDoubleString(mUnitPrice);
    }


    public String getVendorCode() {
        return mVendorCode;
    }


}
