package redington.com.redconnect.redconnect.login.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.newrelic.agent.android.NewRelic;

import io.fabric.sdk.android.Fabric;
import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.onboard.activity.ViewPagerActivity;
import redington.com.redconnect.util.LaunchIntentManager;

public class SplashScreenActivity extends BaseActivity {
    private final Handler handler = new Handler();
    private final Runnable run = () -> {

        boolean b = sp1.getBoolean(SP_FIRST_TIME, false);
        if (b) {
            boolean loggedIn = sp.getBoolean(SP_LOGGED_IN, false);
            if (loggedIn) {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.FROM, GlobalConstants.LOGIN);
                LaunchIntentManager.routeToActivityStackBundle(SplashScreenActivity.this, DashboardActivity.class, bundle);

                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
            } else {
                LaunchIntentManager.routeToActivityStack(SplashScreenActivity.this, LoginActivity.class);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        } else {
            LaunchIntentManager.routeToActivityStack(SplashScreenActivity.this, ViewPagerActivity.class);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }

    };
    private Context mContext;
    private TextView mAppName;
    private TextView txtPoweredby;
    private TextView txtRedington;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash_screen);

        NewRelic.withApplicationToken(
                "AA783073f2e4494f58947840601a4b4a88bf50bf31"
        ).start(this.getApplication());
        mContext = SplashScreenActivity.this;

        mAppName = findViewById(R.id.txt_appname);
        txtPoweredby = findViewById(R.id.txt_poweredby);
        txtRedington = findViewById(R.id.txt_redington);



        /*Font Styles*/

        Typeface typeface = Typeface.createFromAsset(getAssets(), "font/andalus.ttf");
        mAppName.setTypeface(typeface);

        Typeface asset = Typeface.createFromAsset(getAssets(), "font/Long Haired Freaky People.otf");
        txtRedington.setTypeface(asset);


        startAnimation();


    }

    private void startAnimation() {
        ImageView mAppLogo = findViewById(R.id.app_logo);
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.rotate_animation);
        mAppName.startAnimation(animation);
        mAppLogo.startAnimation(animation);

        Animation loadAnimation = AnimationUtils.loadAnimation(mContext, R.anim.from_side);
        txtPoweredby.startAnimation(loadAnimation);
        txtRedington.startAnimation(loadAnimation);

        handler.postDelayed(run, IntentConstants.SPLASH_TIME_OUT);

    }


}
