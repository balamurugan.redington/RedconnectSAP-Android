package redington.com.redconnect.redconnect.settings.helper;


import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.settings.model.Pdfjsonresponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PdfServicemanager {

    private PdfServicemanager() {
        throw new IllegalStateException(IntentConstants.PDF);
    }

    public static void getPdf(String value, final Context context, final UIListener listener) {

        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject headRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headRequest).getPdfList(value).enqueue(new Callback<Pdfjsonresponse>() {
                @Override
                public void onResponse(Call<Pdfjsonresponse> call, Response<Pdfjsonresponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<Pdfjsonresponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });

        } else {
            listener.onError();
        }


    }
}
