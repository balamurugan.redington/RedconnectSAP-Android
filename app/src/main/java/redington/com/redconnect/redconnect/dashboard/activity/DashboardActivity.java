package redington.com.redconnect.redconnect.dashboard.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import me.crosswall.lib.coverflow.CoverFlow;
import me.crosswall.lib.coverflow.core.PagerContainer;
import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.adapter.DashboardAdapter;
import redington.com.redconnect.redconnect.dashboard.adapter.DashboardAdapter1;
import redington.com.redconnect.redconnect.dashboard.adapter.DashboardAdapter2;
import redington.com.redconnect.redconnect.dashboard.adapter.MainPreOrderCatAdapter;
import redington.com.redconnect.redconnect.dashboard.adapter.ViewPagerAdapter;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.dashboard.helper.DashboardServiceManager;
import redington.com.redconnect.redconnect.dashboard.helper.EdmServiceManager;
import redington.com.redconnect.redconnect.dashboard.model.DashboardBean;
import redington.com.redconnect.redconnect.dashboard.model.DashboardJsonresponse;
import redington.com.redconnect.redconnect.dashboard.model.Deal;
import redington.com.redconnect.redconnect.dashboard.model.EDM;
import redington.com.redconnect.redconnect.dashboard.model.EDMData;
import redington.com.redconnect.redconnect.dashboard.model.EDMJsonResponse;
import redington.com.redconnect.redconnect.dashboard.model.Emagazine;
import redington.com.redconnect.redconnect.dashboard.model.FestivalOffer;
import redington.com.redconnect.redconnect.dashboard.model.RecentPurchase;
import redington.com.redconnect.redconnect.dashboard.model.RecentView;
import redington.com.redconnect.redconnect.dashboard.model.TopSelling;
import redington.com.redconnect.redconnect.dashboard.model.WishListEnquiry;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.ReOrderManager;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.EncryptedOrderResponse;
import redington.com.redconnect.redconnect.payment.model.OrderPlacedResponse;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;
import redington.com.redconnect.redconnect.preorder.helper.PreorderCategoryServiceManager;
import redington.com.redconnect.redconnect.preorder.model.InvoiceCommitJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryData;
import redington.com.redconnect.redconnect.preorder.model.PreorderCategoryJsonResponse;
import redington.com.redconnect.redconnect.profile.helper.UserProfileServiceManager;
import redington.com.redconnect.redconnect.profile.model.ProfileJsonResponse;
import redington.com.redconnect.redconnect.search.activity.SearchActivity;
import redington.com.redconnect.redconnect.spot.activity.SPOTActivity;
import redington.com.redconnect.reddb.dao.RedDAO;
import redington.com.redconnect.reddb.model.DashboardMenuPojo;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.GridSpaceItemDecoration;
import redington.com.redconnect.util.LaunchIntentManager;

public class DashboardActivity extends BaseActivity implements UIListener, CartListener, NewListener {

    private static int[] backgroundColors = {R.color.md_green_100, R.color.md_deep_orange_100, R.color.md_light_blue_100,
            R.color.md_deep_purple_100, R.color.md_pink_100, R.color.md_cyan_100, R.color.md_red_100};
    float[] zeroRadius = {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f};
    private Context mContext;
    private CommonUtils mCommonUtils;
    private String mUserID;
    private TextView mTextTitleFirst;
    private TextView mTextTitleFestival;
    private TextView mTextViewAllFirst;
    private TextView mTextTitleSecond;
    private TextView mTextViewAllSecond;
    private TextView mTextTitleFirst1;
    private TextView mTextViewAllFirst1;
    private TextView mTextViewAllFestival;
    private TextView mTextTitleSecond1;
    private TextView mTextViewAllSecond1;
    private ArrayList<EDM> mDashBoardImages;
    private LinearLayout mCartLayout;
    private CoordinatorLayout mRelLayout;
    private FrameLayout mDealsFramelayout;
    private FrameLayout mTopSellingFramelayout;
    private FrameLayout mRecentPurchaseFramelayout;
    private FrameLayout mRecentViewFramelayout;
    private FrameLayout mWishlistFramelayout;
    private FrameLayout mFestivalOffersFramelayout;
    private FrameLayout mMagazineLayout;
    private TextView mTextTitleSecond2;
    private TextView mTextViewAllSecond2;
    private boolean doubleBackToExitPressedOnce = false;
    private ArrayList<WishListEnquiry> wishListEnquiryList;
    private LinearLayout mLayoutPager;
    private String mFlag = "";
    private String apiString = "";
    private String requestNumber;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean swipeboolean = false;
    private String balanceBf = "";
    private TextView mTextViewMore;
    private String retryData = "";
    private String decryptedOrderString;
    private String decryptedString;
    private boolean checkingPendingOrder = false;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_Dashboard);
        setContentView(R.layout.activity_dashboard);

        mContext = DashboardActivity.this;
        mCommonUtils = new CommonUtils(mContext);
        mUserID = GlobalConstants.GetSharedValues.getSpUserId();


        toolbar();

        setInitViews();

        Bundle getBundle = getIntent().getExtras();
        if (getBundle != null) {
            String from = getBundle.getString(GlobalConstants.FROM);
            assert from != null;
            if (from.equals(GlobalConstants.LOGIN)) {

                mCallApi();
            }
        } else {
            RedDAO redDAO = new RedDAO(mContext);
            int categoryCount = redDAO.getCategoryCount();
            if (categoryCount > 0) {
                List<PreorderCategoryData> arrayListMenu = redDAO.getDashboardCategory();
                bindDashboard(arrayListMenu);
                getDashboardValues();
                callRetryAPI();
            } else {
                mCallApi();

            }
        }


        EditText searchBtn = findViewById(R.id.search_dashboad_btm);

        Drawable mDrawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_search);
        mDrawable.mutate().setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        searchBtn.setCompoundDrawablesWithIntrinsicBounds(mDrawable, null, null, null);
        searchBtn.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString(GlobalConstants.FROM, IntentConstants.DASHBOARD);
            bundle.putString(IntentConstants.TAG, mFlag);
            LaunchIntentManager.routeToActivityStackBundle(mContext, SearchActivity.class, bundle);
        });

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(() -> new Handler().postDelayed(() -> {
            swipeRefreshLayout.setRefreshing(true);
            swipeboolean = true;
            mCallApi();
        }, 1000L));

    }

    @Override
    protected void onResume() {
        super.onResume();

        dbValidation();
        getCartUpdate();

    }

    private void setInitViews() {

        mCartLayout = findViewById(R.id.LL_cartText);
        mCartLayout.setVisibility(View.GONE);
        mRelLayout = findViewById(R.id.drawer);

        mTextViewMore = findViewById(R.id.txt_menu_list_dash);
        mTextViewMore.setText(GlobalConstants.VIEW_MORE);
        mTextViewMore.setVisibility(View.GONE);

        mLayoutPager = findViewById(R.id.layout_viewpager);

        mTextTitleFirst = findViewById(R.id.text_titleFirst);
        mTextTitleFestival = findViewById(R.id.text_titleFestival);
        mTextViewAllFirst = findViewById(R.id.viewALLFirst);
        mTextTitleSecond = findViewById(R.id.text_titleSecond);
        mTextViewAllSecond = findViewById(R.id.viewAllList);

        mTextTitleFirst1 = findViewById(R.id.text_titleFirst1);
        mTextViewAllFirst1 = findViewById(R.id.viewALLFirst1);
        mTextViewAllFestival = findViewById(R.id.viewALLFestival);

        mTextTitleSecond1 = findViewById(R.id.text_titleSecond1);
        mTextViewAllSecond1 = findViewById(R.id.viewAllList1);

        mTextTitleSecond2 = findViewById(R.id.text_titleSecond2);
        mTextViewAllSecond2 = findViewById(R.id.viewAllList2);

        mDealsFramelayout = findViewById(R.id.frameLayout1);
        mTopSellingFramelayout = findViewById(R.id.frameLayout2);
        mRecentPurchaseFramelayout = findViewById(R.id.frameLayout3);
        mRecentViewFramelayout = findViewById(R.id.frameLayout4);
        mWishlistFramelayout = findViewById(R.id.frameLayout5);
        mFestivalOffersFramelayout = findViewById(R.id.frameLayout6);
        mMagazineLayout = findViewById(R.id.magazine_layout);

        dbValidation();
    }

    /*Toolbar*/
    private void toolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /*DB Validation*/
    @SuppressLint("NewApi")
    private void dbValidation() {
        RelativeLayout mNotificationLayout = findViewById(R.id.notificationLayout);
        if (balanceBf.equals(IntentConstants.Y)) {
            ImageView mNotifyImage = findViewById(R.id.notifyImage);
            LinearLayout mNotifyText = findViewById(R.id.LL_notifyText);
            TextView mTextNotifycount = findViewById(R.id.notifyCount);
            mNotifyText.setVisibility(View.VISIBLE);
            mNotifyImage.setImageResource(R.mipmap.ic_notify);

            final Animation animation = new AlphaAnimation(1.0f, (float) 0);
            animation.setDuration(500L);

            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            mNotificationLayout.startAnimation(animation);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mNotifyImage.setImageTintList(ContextCompat.getColorStateList(mContext, R.color.white_100));
            } else {
                mNotifyImage.setColorFilter(ContextCompat.getColor(mContext, R.color.white_100));
            }
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion < Build.VERSION_CODES.M) {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setShape(GradientDrawable.OVAL);
                gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                mTextNotifycount.setBackground(gradientDrawable);
            }
        }
        setCartViews();
    }


    public void menuNavi(View view) {
        setRightNavigation(view.getContext());

    }

    private void mCallApi() {
        apiString = IntentConstants.PRE_ORDER;
        if (!swipeboolean) {
            mCommonUtils.showProgressDialog();
        }
        PreorderCategoryServiceManager.getPreorderCatServiceCall(GlobalConstants.API_DASHBOARD_MENULIST,
                GlobalConstants.NULL_DATA, mUserID, mContext, this);
    }

    /*Api response Preorder*/
    @Override
    public void onSuccess(Object successObject) {
        mCommonUtils.dismissProgressDialog();
        if (apiString.equals(IntentConstants.PRE_ORDER)) {

            callDashboardApi();

            PreorderCategoryJsonResponse preorderCategoryJsonResponse = (PreorderCategoryJsonResponse) successObject;
            if (preorderCategoryJsonResponse != null) {
                switch (preorderCategoryJsonResponse.getStatusCode()) {
                    case IntentConstants.SUCCESS_URL:
                        RedDAO redDAO = new RedDAO(mContext);
                        redDAO.deleteCategory(mUserID);
                        ArrayList<PreorderCategoryData> arrayListPreorder = new ArrayList(preorderCategoryJsonResponse.getData().getCategory());
                        insertCategory(arrayListPreorder);
                        break;
                    case IntentConstants.FAILURE_URL:
                        callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PREORDER);
                        break;
                    default:
                        callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PREORDER);
                        break;
                }
            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PREORDER);
            }
        } else if (apiString.equals(IntentConstants.EDM)) {
            EDMJsonResponse edmJsonResponse = (EDMJsonResponse) successObject;
            if (edmJsonResponse != null) {
                switch (edmJsonResponse.getStatusCode()) {
                    case IntentConstants.SUCCESS_URL:
                        ArrayList<EDMData> arrayList = new ArrayList(edmJsonResponse.getData());
                        Bundle bundle = new Bundle();
                        bundle.putString(GlobalConstants.COMING_FROM, GlobalConstants.M_EDM);
                        bundle.putString(GlobalConstants.TOOLBAR_TITLE, GlobalConstants.M_EDM);
                        bundle.putSerializable(GlobalConstants.RECENTEDM, arrayList);
                        LaunchIntentManager.routeToActivityStackBundle(mContext, ViewAllActivity.class, bundle);
                        break;
                    case IntentConstants.FAILURE_URL:
                        shortToast(mContext, GlobalConstants.NO_DATA);
                        break;
                    default:
                        callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_EDM);
                        break;
                }
            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_EDM);
            }
        }
    }

    private void insertCategory(ArrayList<PreorderCategoryData> arrayListPreorder) {
        for (int i = 0; i < arrayListPreorder.size(); i++) {
            RedDAO redDAO = new RedDAO(mContext);
            DashboardMenuPojo dashboardMenuPojo = new DashboardMenuPojo();
            dashboardMenuPojo.setUserId(mUserID);
            dashboardMenuPojo.setCategory(arrayListPreorder.get(i).getCategory());
            dashboardMenuPojo.setDescription(arrayListPreorder.get(i).getDescription());
            dashboardMenuPojo.setBrand(arrayListPreorder.get(i).getBRND68());
            dashboardMenuPojo.setImage1(arrayListPreorder.get(i).getmImage1());
            dashboardMenuPojo.setFlag(arrayListPreorder.get(i).getFlag());
            redDAO.insertDashboardMenu(dashboardMenuPojo);
        }

        RedDAO dao = new RedDAO(mContext);
        List<PreorderCategoryData> arrayListMenu = dao.getDashboardCategory();
        bindDashboard(arrayListMenu);
    }

    @Override
    public void onFailure(Object failureObject) {
        mCommonUtils.dismissProgressDialog();
        if (apiString.equals(IntentConstants.PRE_ORDER)) {
            callDashboardApi();
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PREORDER);
        } else if (apiString.equals(IntentConstants.EDM)) {
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_EDM);
        }
    }

    @Override
    public void onError() {
        mCommonUtils.dismissProgressDialog();
        if (apiString.equals(IntentConstants.PRE_ORDER)) {
            callSnackBar(GlobalConstants.INTERNET_ERROR, GlobalConstants.M_PREORDER);
        } else if (apiString.equals(IntentConstants.EDM)) {
            callSnackBar(GlobalConstants.INTERNET_ERROR, GlobalConstants.M_EDM);
        }
    }


    private void bindDashboard(List<PreorderCategoryData> arrayListPreorder) {
        final RecyclerView recyclerViewPreOrder = findViewById(R.id.recycle_menu_dashboard);
        recyclerViewPreOrder.setHasFixedSize(true);
        recyclerViewPreOrder.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(mContext, 4);
        recyclerViewPreOrder.setLayoutManager(layoutManager);
        recyclerViewPreOrder.setNestedScrollingEnabled(false);
        mFlag = arrayListPreorder.get(0).getFlag();
        if (arrayListPreorder.size() > 4) {
            mTextViewMore.setVisibility(View.VISIBLE);
            MainPreOrderCatAdapter adapter = new MainPreOrderCatAdapter(mContext, arrayListPreorder.subList(0, 4));
            recyclerViewPreOrder.setAdapter(adapter);
        } else {
            mTextViewMore.setVisibility(View.GONE);
            MainPreOrderCatAdapter adapter = new MainPreOrderCatAdapter(mContext, arrayListPreorder);
            recyclerViewPreOrder.setAdapter(adapter);

        }

        mTextViewMore.setOnClickListener(view -> {
            if (mTextViewMore.getText().toString().equals(GlobalConstants.VIEW_MORE)) {
                mTextViewMore.setText(GlobalConstants.VIEW_LESS);
                MainPreOrderCatAdapter adapter = new MainPreOrderCatAdapter(mContext, arrayListPreorder);
                recyclerViewPreOrder.setAdapter(adapter);
            } else {
                mTextViewMore.setText(GlobalConstants.VIEW_MORE);
                MainPreOrderCatAdapter adapter = new MainPreOrderCatAdapter(mContext, arrayListPreorder.subList(0, 4));
                recyclerViewPreOrder.setAdapter(adapter);
            }
        });
    }

    /*Get Cart Count*/
    private void getCartCount() {
        ArrayList<String> cartArrayList = new ArrayList<>();
        cartArrayList.add(mUserID);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.MODE_SELECT);
        cartArrayList.add(GlobalConstants.NULL_DATA);
        cartArrayList.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        cartApiCall(cartArrayList);
    }

    private void cartApiCall(ArrayList<String> cartArrayList) {
        if (!swipeboolean) {
            mCommonUtils.showProgressDialog();
        }
        CartServiceManager.getCartServiceCall(cartArrayList, mContext, this);
    }


    /*Cart Response*/
    @SuppressLint({"NewApi", "SetTextI18n"})
    @Override
    public void cartSuccess(Object successObject) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeboolean = false;
            swipeRefreshLayout.setRefreshing(false);
        } else {
            mCommonUtils.dismissProgressDialog();
        }
        callProfileApi(mUserID);

        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*9-1-19*/
                    /*int getDataSize = cartJson.getData().size();*/
                    int getDataSize = cartJson.getData().get(0).getCart().size();
                    mCartLayout.setVisibility(View.VISIBLE);
                    TextView txtCount = findViewById(R.id.txtCount);
                    txtCount.setText(Integer.toString(getDataSize));
                    int currentAPIVersion = Build.VERSION.SDK_INT;
                    if (currentAPIVersion < Build.VERSION_CODES.M) {
                        GradientDrawable gradientDrawable = new GradientDrawable();
                        gradientDrawable.setShape(GradientDrawable.OVAL);
                        gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                        txtCount.setBackground(gradientDrawable);
                    }
                    setCartUpdate(getDataSize);
                    break;
                case IntentConstants.FAILURE_URL:
                    mCartLayout.setVisibility(View.GONE);
                    setCartUpdate(0);
                    break;
                default:
                    mCartLayout.setVisibility(View.GONE);
                    BaseActivity.shortToastChanges(mContext, GlobalConstants.SERVER_ERROR_BAR);
                    /*RedConnectSAPChanges
                    callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_CART);*/
                    break;
            }
        } else {
            mCartLayout.setVisibility(View.GONE);
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_CART);
        }

    }

    @Override
    public void cartFailure(Object failureObject) {
        mCommonUtils.dismissProgressDialog();
        mCartLayout.setVisibility(View.GONE);
        callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_CART);

        callProfileApi(mUserID);

    }


    @Override
    public void cartError() {
        mCommonUtils.dismissProgressDialog();
        mCartLayout.setVisibility(View.GONE);
        callSnackBar(GlobalConstants.INTERNET_ERROR, GlobalConstants.M_CART);
    }


    private void setCartViews() {
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        final ImageView mCartImage = findViewById(R.id.imageviewCart);
        mCartImage.setImageResource(R.mipmap.ic_cart);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mCartImage.setImageTintList(ContextCompat.getColorStateList(mContext, R.color.white_100));
        } else {
            mCartImage.setColorFilter(Color.WHITE);
        }

        imageCart.setOnClickListener(v -> {
            if (!checkingPendingOrder) {
                shortToast(mContext, IntentConstants.CARTPAYMENT);
            } else {
                LaunchIntentManager.routeToActivityStack(mContext, MyCartActivity.class);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });
    }


    /*Dashboard Api*/
    private void callDashboardApi() {
        apiString = IntentConstants.DASHBOARD;
        if (!swipeboolean) {
            mCommonUtils.showProgressDialog();
        }
        DashboardServiceManager.getDashboardServiceCall(mUserID, mContext, this);
    }

    /*Dashboard Response*/
    @Override
    public void mSuccessObject(Object successObject) {
        switch (apiString) {
            case IntentConstants.DASHBOARD:

                mCommonUtils.dismissProgressDialog();
                getCartCount();

                DashboardJsonresponse dashboardJsonresponse = (DashboardJsonresponse) successObject;
                if (dashboardJsonresponse != null) {
                    dashboardResponse(dashboardJsonresponse);
                } else {
                    callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_DASHBOARD);
                }

                break;
            case IntentConstants.CUSTOMERPROFILE:

                mCommonUtils.dismissProgressDialog();

                ProfileJsonResponse profileJsonResponse = (ProfileJsonResponse) successObject;
                if (profileJsonResponse != null) {
                    profileResponse(profileJsonResponse);
                } else {
                    callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PROFILE);
                }
                break;
            case GlobalConstants.RETRY:
                mCommonUtils.dismissProgressDialog();
                RetryResponse retryJsonResponse = (RetryResponse) successObject;
                if (retryJsonResponse != null) {
                    retryResponse(retryJsonResponse);
                } else {
                    callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_RETRY);
                }
                break;
            case GlobalConstants.RE_ORDER:
                mCommonUtils.dismissProgressDialog();
                orderGenerationResponse(successObject);
                break;
            default:
                //Not in use
                break;
        }

    }


    @Override
    public void mFailureObject(Object failureObject) {
        mCommonUtils.dismissProgressDialog();
        switch (apiString) {
            case IntentConstants.DASHBOARD:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_DASHBOARD);
                getCartCount();
                break;
            case IntentConstants.CUSTOMERPROFILE:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PROFILE);
                break;
            case GlobalConstants.RETRY:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_RETRY);
                break;
            default:
                //Not in use
                break;
        }
    }

    @Override
    public void mError() {
        mCommonUtils.dismissProgressDialog();
        switch (apiString) {
            case IntentConstants.DASHBOARD:
                callSnackBar(GlobalConstants.INTERNET_ERROR, GlobalConstants.M_DASHBOARD);
                break;
            case IntentConstants.CUSTOMERPROFILE:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PROFILE);
                break;
            case GlobalConstants.RETRY:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_RETRY);
                break;
            default:
                //Not in use
                break;
        }

    }

    private void retryResponse(RetryResponse retryResponse) {
        checkingPendingOrder = false;
        switch (retryResponse.getStatusCode()) {
            case IntentConstants.SUCCESS_URL:
                retryData = retryResponse.getData().getMode();
                String penidngTransactionId = retryResponse.getData().getPendOrd().get(0).getTransactionID();
                String penidngReferenceId = retryResponse.getData().getPendOrd().get(0).getReferenceNo();
                if (!penidngTransactionId.isEmpty() || !penidngReferenceId.isEmpty()) {
                    showPopup(GlobalConstants.DASHBOARD_RETRY);
                } else {
                    checkingPendingOrder = true;
                }
                break;
            case IntentConstants.FAILURE_URL:
                checkingPendingOrder = true;
                break;
            default:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_RETRY);
                break;
        }
    }


    /*Profile Json Response*/
    private void profileResponse(ProfileJsonResponse profileJsonResponse) {
        switch (profileJsonResponse.getStatusCode()) {
            case IntentConstants.SUCCESS_URL:
                balanceBf = profileJsonResponse.getData().get(0).getmBALBF();

                /*callRetryAPI();*/

                dbValidation();
                break;
            case IntentConstants.FAILURE_URL:
                shortToast(mContext, GlobalConstants.NO_DATA);
                break;
            default:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_PROFILE);
                break;
        }
        callRetryAPI();
    }

    private void callRetryAPI() {
        apiString = GlobalConstants.RETRY;
        mCommonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(mUserID, mContext, this);
    }

    /*Dashboard Response*/
    private void dashboardResponse(DashboardJsonresponse dashboardJsonresponse) {

        switch (dashboardJsonresponse.getStatusCode()) {
            case IntentConstants.SUCCESS_URL:

                RedDAO dao = new RedDAO(mContext);
                dao.deleteDahboard(mUserID);

                String dashBoardValue = dashboardJsonresponse.toString();
                RedDAO redDAO = new RedDAO(mContext);
                DashboardBean dashboardBean = new DashboardBean();
                dashboardBean.setUserId(mUserID);
                dashboardBean.setDashboardData(dashBoardValue);
                redDAO.insertDashboardData(dashboardBean);

                getDashboardValues();

                break;
            case IntentConstants.FAILURE_URL:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_DASHBOARD);
                break;
            default:
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR, GlobalConstants.M_DASHBOARD);
                break;
        }
    }

    private void getDashboardValues() {

        RedDAO redDAO1 = new RedDAO(mContext);
        int count = redDAO1.getDashCount();
        if (count > 0) {
            List<DashboardBean> dbData = redDAO1.getDashboard();
            String dashData = dbData.get(0).getDashboardData();

            ArrayList<DashboardJsonresponse> arrayList = new ArrayList<>();
            DashboardJsonresponse jsonResponse = DashboardJsonresponse.fromString(dashData);
            arrayList.add(jsonResponse);

            int edmSize = jsonResponse.getData().getEDM().size();

            if (edmSize > 0) {
                initPager(jsonResponse.getData().getEDM());
            } else {
                mLayoutPager.setVisibility(View.GONE);
            }

            deal(jsonResponse.getData().getDeals());
            topSelling(jsonResponse.getData().getmTopSelling());
            recentpurchase(jsonResponse.getData().getRecentPurchase());
            recentView(jsonResponse.getData().getRecentView());
            pager(jsonResponse.getData().getWishListEnquiry());
            festivaloffer(jsonResponse.getData().getmFestivalOffer());
            magazine(jsonResponse.getData().getEmagazine());


        } else {
            callDashboardApi();
        }


    }

    private void magazine(List<Emagazine> emagazine) {
        if (!emagazine.isEmpty()) {
            mMagazineLayout.setVisibility(View.VISIBLE);
            ViewPager viewPager = findViewById(R.id.magazine_image);
            viewPager.setAdapter(new ViewPagerAdapter(mContext, emagazine));
        } else {
            mMagazineLayout.setVisibility(View.GONE);
        }
    }

    /*Init Pager*/
    @SuppressLint("ClickableViewAccessibility")
    private void initPager(List<EDM> edm) {

        mDashBoardImages = new ArrayList(edm);
        PagerContainer container;
        container = findViewById(R.id.pager_container);
        ViewPager pager = container.getViewPager();
        pager.setAdapter(new MyPagerAdapter());
        pager.setClipChildren(false);
        pager.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    swipeRefreshLayout.setEnabled(false);
                    break;
                case MotionEvent.ACTION_UP:
                    //No data
                    break;
                case MotionEvent.ACTION_CANCEL:
                    swipeRefreshLayout.setEnabled(true);
                    break;

                default:
                    //No data
                    break;
            }
            return false;
        });

        boolean showTransformer = getIntent().getBooleanExtra("showTransformer", false);
        if (showTransformer) {
            new CoverFlow.Builder()
                    .with(pager)
                    .scale(0.3f)
                    .pagerMargin((float) getResources().getDimensionPixelSize(R.dimen.pager_margin))
                    .spaceSize(0.0f)
                    .build();
        } else {
            pager.setPageMargin(30);
        }

    }


    /*Deal*/
    private void deal(final List<Deal> deals) {
        int recentSize = deals.size();
        if (recentSize != 0) {
            RecyclerView recyclerView = findViewById(R.id.recycler_view_grid);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            mDealsFramelayout.setVisibility(View.VISIBLE);
            mTextTitleFirst.setText(deals.get(0).getHeaderName());
            recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
            if (deals.size() > 3) {
                DashboardAdapter dashboardAdapter2 = new DashboardAdapter(mContext, deals.subList(0, 4), GlobalConstants.MDEALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            } else if (deals.size() == 2 || deals.size() == 3) {
                DashboardAdapter dashboardAdapter2 = new DashboardAdapter(mContext, deals.subList(0, 2), GlobalConstants.MDEALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            } else if (deals.size() == 1) {
                mTextViewAllFirst.setVisibility(View.GONE);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                DashboardAdapter adapter = new DashboardAdapter(mContext, deals, GlobalConstants.MDEALS);
                recyclerView.setAdapter(adapter);
            } else {
                mTextViewAllFirst.setVisibility(View.GONE);
                DashboardAdapter dashboardAdapter2 = new DashboardAdapter(mContext, deals, GlobalConstants.MDEALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            }
            mTextViewAllFirst.setOnClickListener(view -> {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.COMING_FROM, GlobalConstants.MDEALS);
                bundle.putString(GlobalConstants.TOOLBAR_TITLE, deals.get(0).getHeaderName());
                bundle.putSerializable(GlobalConstants.RECENT_DEALS, (Serializable) deals);
                LaunchIntentManager.routeToActivityStackBundle(mContext, ViewAllActivity.class, bundle);
            });

        } else {
            mDealsFramelayout.setVisibility(View.GONE);
        }
    }

    /*Top Selling*/
    private void topSelling(final List<TopSelling> topSellings) {
        int recentSize = topSellings.size();
        if (recentSize != 0) {
            RecyclerView recyclerViewTopSellings = findViewById(R.id.recycler_view_horizontal);
            recyclerViewTopSellings.setHasFixedSize(true);
            recyclerViewTopSellings.setNestedScrollingEnabled(false);
            mTopSellingFramelayout.setVisibility(View.VISIBLE);
            mTextTitleSecond.setText(topSellings.get(0).getHeaderName());
            if (recentSize > 3) {
                DashboardAdapter dashboardAdapter2 = new DashboardAdapter(mContext, topSellings.subList(0, 4), 1);
                recyclerViewTopSellings.setAdapter(dashboardAdapter2);
                mTextViewAllSecond.setVisibility(View.VISIBLE);
            } else {
                DashboardAdapter dashboardAdapter2 = new DashboardAdapter(mContext, topSellings, 1);
                recyclerViewTopSellings.setAdapter(dashboardAdapter2);
                mTextViewAllSecond.setVisibility(View.GONE);
            }
            LinearLayoutManager verticalLayout = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            recyclerViewTopSellings.setLayoutManager(verticalLayout);

            mTextViewAllSecond.setOnClickListener(view -> {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.COMING_FROM, GlobalConstants.MTOPSELLING);
                bundle.putString(GlobalConstants.TOOLBAR_TITLE, topSellings.get(0).getHeaderName());
                bundle.putSerializable(GlobalConstants.RECENT_TOP_SELLINGS, (Serializable) topSellings);
                LaunchIntentManager.routeToActivityStackBundle(mContext, ViewAllActivity.class, bundle);
            });

        } else {
            mTopSellingFramelayout.setVisibility(View.GONE);
        }
    }

    /*Recent purchase*/
    @SuppressLint("NewApi")
    private void recentpurchase(List<RecentPurchase> recentPurchase) {
        int recentSize = recentPurchase.size();
        if (recentSize != 0) {
            mRecentPurchaseFramelayout.setVisibility(View.VISIBLE);
            RecyclerView mRecyclerViewHorizontal1 = findViewById(R.id.recycler_view_horizontal1);
            mRecyclerViewHorizontal1.setHasFixedSize(true);
            mRecyclerViewHorizontal1.setNestedScrollingEnabled(true);
            mRecyclerViewHorizontal1.setPadding(15, 15, 5, 15);
            mTextTitleSecond1.setText(recentPurchase.get(0).getHeaderName());
            mTextViewAllSecond1.setVisibility(View.GONE);
            ArrayList<RecentPurchase> recentPurchaseArrayList = new ArrayList<>(recentPurchase);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            mRecyclerViewHorizontal1.setLayoutManager(linearLayoutManager);
            DashboardAdapter1 dashboardAdapter1 = new DashboardAdapter1(mContext, recentPurchaseArrayList);
            mRecyclerViewHorizontal1.setAdapter(dashboardAdapter1);


        } else {
            mRecentPurchaseFramelayout.setVisibility(View.GONE);
        }
    }


    /*Recent Views*/
    private void recentView(final List<RecentView> recentView) {
        int size = recentView.size();
        if (size != 0) {
            mTextTitleFirst1.setText(recentView.get(0).getHeaderName());
            mRecentViewFramelayout.setVisibility(View.VISIBLE);
            RecyclerView recyclerView1 = findViewById(R.id.recycler_view_grid1);
            mCommonUtils.setTwoGradientColor(mRecentViewFramelayout, R.color.md_cyan_200, R.color.md_cyan_50, zeroRadius);
            recyclerView1.setHasFixedSize(true);
            recyclerView1.setPadding(15, 15, 5, 15);
            mTextViewAllFirst1.setVisibility(View.GONE);
            recyclerView1.setNestedScrollingEnabled(true);
            recyclerView1.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            DashboardAdapter1 dashboardAdapter1 = new DashboardAdapter1(mContext, recentView, GlobalConstants.MRECENTVIEWS);
            recyclerView1.setAdapter(dashboardAdapter1);
        } else {
            mRecentViewFramelayout.setVisibility(View.GONE);
        }

    }

    /*Festival Offers*/
    private void festivaloffer(final List<FestivalOffer> festivalOffers) {
        int recentSize = festivalOffers.size();
        if (recentSize != 0) {
            RecyclerView recyclerView = findViewById(R.id.recycler_view_grid_festival);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            mFestivalOffersFramelayout.setVisibility(View.VISIBLE);
            mCommonUtils.setTwoGradientColor(mFestivalOffersFramelayout, R.color.md_teal_200, R.color.md_teal_50, zeroRadius);
            mTextTitleFestival.setText(festivalOffers.get(0).getmHeaderName());
            recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
            if (festivalOffers.size() > 3) {
                DashboardAdapter2 dashboardAdapter2 = new DashboardAdapter2(mContext, festivalOffers.subList(0, 4), GlobalConstants.MFESTIVALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            } else if (festivalOffers.size() == 2 || festivalOffers.size() == 3) {
                DashboardAdapter2 dashboardAdapter2 = new DashboardAdapter2(mContext, festivalOffers.subList(0, 2), GlobalConstants.MFESTIVALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            } else if (festivalOffers.size() == 1) {
                mTextViewAllFestival.setVisibility(View.GONE);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                DashboardAdapter2 adapter = new DashboardAdapter2(mContext, festivalOffers, GlobalConstants.MFESTIVALS);
                recyclerView.setAdapter(adapter);
            } else {
                mTextViewAllFestival.setVisibility(View.GONE);
                DashboardAdapter2 dashboardAdapter2 = new DashboardAdapter2(mContext, festivalOffers, GlobalConstants.MFESTIVALS);
                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_layout_margin);
                recyclerView.addItemDecoration(new GridSpaceItemDecoration(2, spacingInPixels, true, 0));
                recyclerView.setAdapter(dashboardAdapter2);
            }
            mTextViewAllFestival.setOnClickListener(view -> {
                Bundle bundle = new Bundle();
                bundle.putString(GlobalConstants.COMING_FROM, GlobalConstants.MFESTIVALS);
                bundle.putString(GlobalConstants.TOOLBAR_TITLE, festivalOffers.get(0).getmHeaderName());
                bundle.putSerializable(GlobalConstants.RECENTFESTIVALS, (Serializable) festivalOffers);
                LaunchIntentManager.routeToActivityStackBundle(mContext, SPOTActivity.class, bundle);
            });

        } else {
            mFestivalOffersFramelayout.setVisibility(View.GONE);
        }
    }


    /*Snack Bar*/
    private void callSnackBar(String mStrValue, final String mCame) {
        if (swipeboolean) {
            swipeboolean = false;
            swipeRefreshLayout.setRefreshing(false);
        }
        Snackbar snackbar = Snackbar
                .make(mRelLayout, mStrValue, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    switch (mCame) {
                        case GlobalConstants.M_PREORDER:
                            mCallApi();
                            break;
                        case GlobalConstants.M_CART:
                            getCartCount();
                            break;
                        case GlobalConstants.M_DASHBOARD:
                            callDashboardApi();
                            break;
                        case GlobalConstants.M_EDM:
                            callEdmApi(requestNumber);
                            break;
                        case GlobalConstants.M_PROFILE:
                            callProfileApi(mUserID);
                            break;
                        case GlobalConstants.M_RETRY:
                            callRetryAPI();
                            break;
                        default:
                            //Not in use
                            break;
                    }
                });
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    /*On Back Pressed*/
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        shortToast(mContext, GlobalConstants.BACK);
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, (long) IntentConstants.BACK_TIME_OUT);
    }

    /*Wish List*/
    private void pager(final List<WishListEnquiry> wishListEnquiry) {
        int wishSize = wishListEnquiry.size();
        if (wishSize != 0) {
            mWishlistFramelayout.setVisibility(View.VISIBLE);
            mTextTitleSecond2.setText(wishListEnquiry.get(0).getHeaderName());
            mTextViewAllSecond2.setVisibility(View.GONE);

            wishListEnquiryList = new ArrayList<>(wishListEnquiry);

            PagerContainer pagerContainer = findViewById(R.id.pager_container_wishlist);
            ViewPager viewPager = pagerContainer.getViewPager();
            viewPager.setAdapter(new WishlistAdapter());
            viewPager.setClipChildren(false);
            new CoverFlow.Builder()
                    .with(viewPager)
                    .scale(0.3f)
                    .pagerMargin((float) getResources().getDimensionPixelSize(R.dimen.pager_margin))
                    .spaceSize(0.0f)
                    .build();

        } else {
            mWishlistFramelayout.setVisibility(View.GONE);
        }
    }

    private void callEdmApi(String requestNumber) {
        apiString = IntentConstants.EDM;
        mCommonUtils.showProgressDialog();
        EdmServiceManager.edmServiceCall(mUserID, requestNumber, mContext, this);
    }

    private void callProfileApi(String mUserID) {
        apiString = IntentConstants.CUSTOMERPROFILE;
        mCommonUtils.showProgressDialog();
        UserProfileServiceManager.getUserProfileServiceManager(mUserID, mContext, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences.Editor editor = cartDeliverySequence.edit();
        if (GlobalConstants.GetSharedValues.getCartDelSeq().equals(GlobalConstants.CART_DEL_SEQ)) {
            editor.putString(CARTDELSEQ, IntentConstants.CARTDELSEQ);
        } else {
            editor.putString(CARTDELSEQ, GlobalConstants.GetSharedValues.getCartDelSeq());
        }
        editor.apply();

        SharedPreferences.Editor edit = billingaddressSeq.edit();
        if (GlobalConstants.GetSharedValues.getBillingSeq().equals(GlobalConstants.BILLING_SEQ)) {
            edit.putString(BILLINGDELSEQ, IntentConstants.CARTDELSEQ);
        } else {
            edit.putString(BILLINGDELSEQ, GlobalConstants.GetSharedValues.getBillingSeq());
        }
        edit.apply();

        SharedPreferences.Editor editor1 = cashDiscount.edit();
        if (GlobalConstants.GetSharedValues.getCashDiscount().equals(GlobalConstants.CASH_DISCOUNT)) {
            editor1.putString(CASH_DISCOUNT, GlobalConstants.NO);
        } else {
            editor1.putString(CASH_DISCOUNT, GlobalConstants.GetSharedValues.getCashDiscount());
        }

        editor1.apply();

    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup(String responseString) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        switch (responseString) {
            case GlobalConstants.CUST_CARE:
                alertContent.setText(responseString);
                alertContent.setTextSize(11f);
                break;
            case GlobalConstants.DASHBOARD_RETRY:
                alertContent.setText(responseString);
                break;
            default:
                alertContent.setText(mContext.getString(R.string.order_no) + GlobalConstants.SPACE + responseString);
                break;
        }

        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            if (responseString.equals(GlobalConstants.DASHBOARD_RETRY)) {
                callReOrderApi(mUserID);
            }

        });


        mDialog.show();
    }

    private void callReOrderApi(String mUserID) {
        apiString = GlobalConstants.RE_ORDER;
        mCommonUtils.showProgressDialog();
        ReOrderManager.getReorderCall(mUserID, mContext, this);
    }

    /*Order Generation Response*/
    private void orderGenerationResponse(Object successObject) {
        if (retryData.equals(GlobalConstants.ORDER_GENERATION)) {
            ordePlacedData(successObject);
        } else if (retryData.equals(GlobalConstants.INVOICE_COMMITMENT)) {
            invoiceResponse(successObject);
        }
    }

    /*Place Order Response*/
    public void ordePlacedData(Object successObject) {
        EncryptedOrderResponse encryptedOrderResponse = (EncryptedOrderResponse) successObject;
        if (encryptedOrderResponse != null) {
            try {
                decryptedOrderString = mDecypt(encryptedOrderResponse.getData());
            } catch (Exception e) {
                BaseActivity.logd(e.getMessage());
            }
            ArrayList<OrderPlacedResponse> orderPlacedJsonResponse = new ArrayList<>();

            OrderPlacedResponse jsonResponse = OrderPlacedResponse.fromString(decryptedOrderString);
            orderPlacedJsonResponse.add(jsonResponse);

            switch (orderPlacedJsonResponse.get(0).getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String mSkipOrderNum = orderPlacedJsonResponse.get(0).getData().getUID();
                    String referenceOrder = orderPlacedJsonResponse.get(0).getData().getmReferenceNumber();
                    String successNumber = (!mSkipOrderNum.isEmpty()) ? mSkipOrderNum : referenceOrder;
                    showPopup(successNumber);
                    break;
                case IntentConstants.FAILURE_URL:
                    /*showPopup(GlobalConstants.CUST_CARE);*/ /*Kamesh*/
                    break;
                default:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
            }
        } else {
            showPopup(GlobalConstants.CUST_CARE);
        }
    }

    /*Invoice response*/
    private void invoiceResponse(Object successObject) {
        EncryptedOrderResponse encryptedInvoiceResponse = (EncryptedOrderResponse) successObject;
        if (encryptedInvoiceResponse != null) {

            try {
                decryptedString = BaseActivity.mDecypt(encryptedInvoiceResponse.getData());
            } catch (Exception e) {
                logd(e.getMessage());
            }

            ArrayList<InvoiceCommitJsonResponse> invoiceCommitJsonResponse = new ArrayList<>();
            InvoiceCommitJsonResponse jsonResponse = InvoiceCommitJsonResponse.fromString(decryptedString);
            invoiceCommitJsonResponse.add(jsonResponse);

            switch (invoiceCommitJsonResponse.get(0).getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String referenceNumber = invoiceCommitJsonResponse.get(0).getData().getReferenceNumber();
                    showPopup(referenceNumber);
                    break;
                case IntentConstants.FAILURE_URL:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
                default:
                    showPopup(GlobalConstants.CUST_CARE);
                    break;
            }
        } else {
            showPopup(GlobalConstants.CUST_CARE);
        }
    }


    /* EDM Pager adapter*/
    private class MyPagerAdapter extends PagerAdapter {

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View view = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.viewpager_slide_show, null);
            ImageView imageView = view.findViewById(R.id.image_cover);
            CardView cardView = view.findViewById(R.id.cardview);
            Glide.with(mContext)
                    .load(mDashBoardImages.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.image_loader)
                    .into(imageView);

            container.addView(view);
            cardView.setOnClickListener(v -> {
                requestNumber = mDashBoardImages.get(position).getRequestNumber();
                callEdmApi(requestNumber);
            });
            return view;

        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mDashBoardImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }
    }

    /*WishList Adapter*/
    private class WishlistAdapter extends PagerAdapter {

        @NonNull
        @SuppressLint("SetTextI18n")
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View view = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.list_main_recylce_full, null);
            ImageView imageView = view.findViewById(R.id.imagePreOrder);

            Glide.with(mContext)
                    .load(wishListEnquiryList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.image_loader)
                    .error(R.drawable.image_error)
                    .into(imageView);
            final LinearLayout linearLayout = view.findViewById(R.id.LL_recycleView);
            TextView productName = view.findViewById(R.id.txt_productName);
            TextView price = view.findViewById(R.id.txt_productDes);
            TextView itemCode = view.findViewById(R.id.text_itemcode);
            TextView mStockCheck = view.findViewById(R.id.text_stock);

            if (GlobalConstants.LOW_STOCK.equals(wishListEnquiryList.get(position).getStock())) {
                mStockCheck.setVisibility(View.VISIBLE);
                mStockCheck.setText(wishListEnquiryList.get(position).getStock());
            } else {
                mStockCheck.setVisibility(View.GONE);
            }

            TextView mDiscountPrice = view.findViewById(R.id.text_discountprice);
            TextView mTopActualPrice = view.findViewById(R.id.text_actualprice);
            LinearLayout mTopDiscountLayout = view.findViewById(R.id.discountLayout);
            productName.setText(wishListEnquiryList.get(position).getProductName());
            double disPercent = Double.parseDouble(wishListEnquiryList.get(position).getDiscountPercent());
            if (disPercent == 0.0) {
                price.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(wishListEnquiryList.get(position).getDiscountPrice()));
                /*isValidNumberFormat().format(wishListEnquiryList.get(position).getDiscountPrice()));*/
            } else {
                price.setVisibility(View.GONE);
                mTopDiscountLayout.setVisibility(View.VISIBLE);
                mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(wishListEnquiryList.get(position).getDiscountPrice()));
                /*BaseActivity.isValidNumberFormat().format(wishListEnquiryList.get(position).getDiscountPrice()));*/
                mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        CommonMethod.formatLargeDouble(wishListEnquiryList.get(position).getActualPrice()));
                /*BaseActivity.isValidNumberFormat().format(wishListEnquiryList.get(position).getActualPrice()));*/
                mTopActualPrice.setPaintFlags(mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }
            itemCode.setText(wishListEnquiryList.get(position).getItemCode());

            RelativeLayout relativeLayout = view.findViewById(R.id.rel_top);
            LinearLayout layout = view.findViewById(R.id.layout_text);
            layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.ed_grey));

            int bgColor = ContextCompat.getColor(mContext, backgroundColors[position % 7]);
            relativeLayout.setBackgroundColor(bgColor);

            linearLayout.setOnClickListener(view1 -> {
                Intent i = new Intent(mContext, ProductDetail.class);
                i.putExtra(GlobalConstants.PRODUCT_NAME, wishListEnquiryList.get(position).getProductName());
                i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, wishListEnquiryList.get(position).getItemCode());
                i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, wishListEnquiryList.get(position).getVendorCode());
                mContext.startActivity(i);
            });
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return wishListEnquiryList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }
    }

}
