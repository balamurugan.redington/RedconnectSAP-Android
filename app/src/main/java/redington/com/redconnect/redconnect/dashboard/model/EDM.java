
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class EDM implements Serializable {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("HeaderName")
    private String mHeaderName;
    @SerializedName("Heading")
    private String mHeading;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("Link")
    private String mLink;
    @SerializedName("RequestNumber")
    private String mRequestNumber;

    public String getDescription() {
        return mDescription;
    }

    public String getHeaderName() {
        return mHeaderName;
    }

    public String getHeading() {
        return mHeading;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public String getLink() {
        return mLink;
    }

    public String getRequestNumber() {
        return mRequestNumber;
    }


}
