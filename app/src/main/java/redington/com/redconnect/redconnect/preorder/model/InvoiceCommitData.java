package redington.com.redconnect.redconnect.preorder.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InvoiceCommitData implements Serializable {

    @SerializedName("Status")
    private String mStatus;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("ReferenceNumber")
    private String mReferenceNumber;


    public String getMessage() {
        return mMessage;
    }

    public String getReferenceNumber() {
        return mReferenceNumber;
    }

    public String getmStatus() {
        return mStatus;
    }
}
