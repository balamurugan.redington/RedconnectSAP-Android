package redington.com.redconnect.redconnect.dashboard.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.redconnect.dashboard.dialog.ImageDialog;
import redington.com.redconnect.redconnect.dashboard.model.Emagazine;
import redington.com.redconnect.util.LaunchIntentManager;

public class ViewPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Emagazine> emagazinesList;

    public ViewPagerAdapter(Context context, List<Emagazine> mImages) {
        this.mContext = context;
        this.emagazinesList = mImages;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return emagazinesList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        View imageLayout = LayoutInflater.from(mContext).inflate(R.layout.view_pager_detail, view, false);

        ImageView imageView = imageLayout.findViewById(R.id.img_slide);
        view.addView(imageLayout, 0);

        imageView.setScaleType(ImageView.ScaleType.FIT_XY);


        Glide.with(mContext)
                .load(emagazinesList.get(position).getCoverURL())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .placeholder(R.drawable.redinton_image)
                .error(R.drawable.noimagefound)
                .into(imageView);

        imageView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString(GlobalConstants.IMAGE, emagazinesList.get(position).getMagazineURL());
            bundle.putString(GlobalConstants.IMAGE_URL, emagazinesList.get(position).getPdfURL());
            LaunchIntentManager.routeToActivityStackBundle(mContext, ImageDialog.class, bundle);
        });
        return imageLayout;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //No data
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
