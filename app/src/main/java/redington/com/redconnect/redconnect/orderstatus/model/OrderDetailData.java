package redington.com.redconnect.redconnect.orderstatus.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;

public class OrderDetailData implements Serializable {
    @SerializedName("BillingAddress7")
    private String mBillingAddress7;
    @SerializedName("BillingAddress6")
    private String mBillingAddress6;
    @SerializedName("BillingAddress5")
    private String mBillingAddress5;
    @SerializedName("BillingAddress4")
    private String mBillingAddress4;
    @SerializedName("BillingAddress3")
    private String mBillingAddress3;
    @SerializedName("BillingAddress2")
    private String mBillingAddress2;
    @SerializedName("BillingAddress1")
    private String mBillingAddress1;


    @SerializedName("DeliveryAddress7")
    private String mDeliveryAddress7;
    @SerializedName("DeliveryAddress6")
    private String mDeliveryAddress6;
    @SerializedName("DeliveryAddress5")
    private String mDeliveryAddress5;
    @SerializedName("DeliveryAddress4")
    private String mDeliveryAddress4;
    @SerializedName("DeliveryAddress3")
    private String mDeliveryAddress3;
    @SerializedName("DeliveryAddress2")
    private String mDeliveryAddress2;
    @SerializedName("DeliveryAddress1")
    private String mDeliveryAddress1;

    @SerializedName("Product_Name")
    private String mProductName;
    @SerializedName("Invoice_Date")
    private String mInvoiceDate;
    @SerializedName("Invoice_Number")
    private String mInvoiceNumber;
    @SerializedName("Item_Code")
    private String mItemCode;
    @SerializedName("item_Description")
    private String mItemDescription;
    @SerializedName("Item_VendorCode")
    private String mItemVendorCode;
    @SerializedName("Name")
    private String mName;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("Payment_Days")
    private String mPaymentDays;
    @SerializedName("payment_Method")
    private String mPaymentMethod;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("SO_Date")
    private String mSODate;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("Total_Linevalue")
    private String mTotalLinevalue;
    @SerializedName("Unit_Price")
    private String mUnitPrice;
    @SerializedName("User_ID")
    private String mUserID;

    public String getmBillingAddress7() {
        return mBillingAddress7;
    }

    public String getmBillingAddress6() {
        return mBillingAddress6;
    }

    public String getmBillingAddress5() {
        return mBillingAddress5;
    }

    public String getmBillingAddress4() {
        return mBillingAddress4;
    }

    public String getmBillingAddress3() {
        return mBillingAddress3;
    }

    public String getmBillingAddress2() {
        return mBillingAddress2;
    }

    public String getmBillingAddress1() {
        return mBillingAddress1;
    }

    public String getmDeliveryAddress7() {
        return mDeliveryAddress7;
    }

    public String getmDeliveryAddress6() {
        return mDeliveryAddress6;
    }

    public String getmDeliveryAddress5() {
        return mDeliveryAddress5;
    }

    public String getmDeliveryAddress4() {
        return mDeliveryAddress4;
    }

    public String getmDeliveryAddress3() {
        return mDeliveryAddress3;
    }

    public String getmDeliveryAddress2() {
        return mDeliveryAddress2;
    }

    public String getmDeliveryAddress1() {
        return mDeliveryAddress1;
    }

    public String getmProductName() {
        return mProductName;
    }

    public String getInvoiceDate() {
        return mInvoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        mInvoiceDate = invoiceDate;
    }

    public String getInvoiceNumber() {
        return mInvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        mInvoiceNumber = invoiceNumber;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public void setItemCode(String itemCode) {
        mItemCode = itemCode;
    }

    public String getItemDescription() {
        return mItemDescription;
    }

    public void setItemDescription(String itemDescription) {
        mItemDescription = itemDescription;
    }

    public String getItemVendorCode() {
        return mItemVendorCode;
    }

    public void setItemVendorCode(String itemVendorCode) {
        mItemVendorCode = itemVendorCode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        mOrderNumber = orderNumber;
    }

    public Double getPaymentDays() {
        return CommonMethod.getValidDouble(mPaymentDays);
    }

    public void setPaymentDays(Double paymentDays) {
        mPaymentDays = String.valueOf(paymentDays);
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public int getQuantity() {
        return CommonMethod.getValidInt(mQuantity);
    }

    public void setQuantity(int quantity) {
        mQuantity = String.valueOf(quantity);
    }

    public String getSODate() {
        return mSODate;
    }

    public void setSODate(String soDate) {
        mSODate = soDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Double getTotalLinevalue() {
        return CommonMethod.getValidDouble(mTotalLinevalue);
    }

    public void setTotalLinevalue(Double totalLinevalue) {
        mTotalLinevalue = String.valueOf(totalLinevalue);
    }

    public Double getUnitPrice() {
        return CommonMethod.getValidDouble(mUnitPrice);
    }

    public void setUnitPrice(Double unitPrice) {
        mUnitPrice = String.valueOf(unitPrice);
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }


}
