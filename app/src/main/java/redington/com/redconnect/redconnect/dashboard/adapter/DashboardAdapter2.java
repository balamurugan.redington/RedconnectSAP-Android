package redington.com.redconnect.redconnect.dashboard.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.FestivalOffer;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;


public class DashboardAdapter2 extends RecyclerView.Adapter<DashboardAdapter2.OfferHolder> {
    private Context mContext;
    private List<FestivalOffer> festivalOffers;
    private String mType;
    private int layoutRes;

    public DashboardAdapter2(Context context, List<FestivalOffer> festivalOffers, String type) {
        this.mContext = context;
        this.festivalOffers = festivalOffers;
        this.mType = type;

    }

    @NonNull
    @Override
    public OfferHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mType.equals(GlobalConstants.MFESTIVALS) && festivalOffers.size() != 1) {
            layoutRes = R.layout.spot_recycle_list;
        } else if (mType.equals(GlobalConstants.MFESTIVALS) && festivalOffers.size() == 1) {
            layoutRes = R.layout.list_main_recylce_full;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new OfferHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final OfferHolder holder, final int position) {
        final int pos = holder.getAdapterPosition();
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yy hh:mm:ss", Locale.getDefault());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.getDefault());
        String serverDate = festivalOffers.get(position).getmServerDate();

        if (mType.equals(GlobalConstants.MFESTIVALS)) {
            if (festivalOffers.size() == 1) {
                holder.mItemCode.setText(festivalOffers.get(position).getmItemCode());
                holder.mProductName1.setText(festivalOffers.get(position).getmProductName());

                double disPercent = Double.parseDouble(festivalOffers.get(position).getmDiscountPercent());

                if (disPercent == 0.0) {
                    holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            CommonMethod.formatLargeDouble(festivalOffers.get(position).getmDiscountPrice()));
                } else {
                    holder.mProductPrice1.setVisibility(View.GONE);
                    holder.mTopDiscountLayout.setVisibility(View.VISIBLE);
                    holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            CommonMethod.formatLargeDouble(festivalOffers.get(position).getmDiscountPrice()));

                    holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            CommonMethod.formatLargeDouble(festivalOffers.get(position).getmActualPrice()));

                    holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
                Glide.with(mContext).load(festivalOffers.get(position).getmImageURL())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.redinton_image)
                        .into(holder.imagePreOrder);


                try {
                    Date date1 = simpleDateFormat1.parse(serverDate + " " + festivalOffers.get(position).getmServerTime());
                    Date date2 = simpleDateFormat.parse(festivalOffers.get(position).getmEndingDate() + " " + festivalOffers.get(position).getmEndTime());

                    String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
                    long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

                    long endTime = BaseActivity.dateFormatMinusTwo(festivalOffers.get(position).getmEndingDate(), festivalOffers.get(position).getmEndTime());
                    long startTime = BaseActivity.longConversion(serverDate, festivalOffers.get(position).getmServerTime());
                    if (startTime >= endTime) {
                        new CountDownTimer(diffInMillisec, 1000) {
                            @Override
                            public void onTick(long millis) {
                                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                        TimeUnit.MILLISECONDS.toHours(millis),
                                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                                holder.mTimerText.setText(hms);
                            }

                            @Override
                            public void onFinish() {
                                //Not in use
                            }
                        }.start();
                    } else {
                        holder.mTimerText.setText(diffrenceDate);
                    }


                } catch (ParseException e) {
                    BaseActivity.logd(e.getMessage());
                }


                holder.mLinLayout.setOnClickListener(view -> setClickFunc(
                        festivalOffers.get(pos).getmProductName(),
                        festivalOffers.get(pos).getmItemCode(),
                        festivalOffers.get(pos).getmVendorCode()));

            } else {

                defaultFestivalOffers(holder, position, serverDate, simpleDateFormat);


            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void defaultFestivalOffers(final OfferHolder holder, final int position, String serverDate, SimpleDateFormat simpleDateFormat) {
        holder.mTimerText.setVisibility(View.VISIBLE);
        holder.mProductName.setText(festivalOffers.get(position).getmProductName());
        holder.mQuantity.setText(festivalOffers.get(position).getmItemCode());
        double offer = Double.parseDouble(festivalOffers.get(position).getmDiscountPercent());
        if (offer == 0.0) {
            holder.mFrameLayout.setVisibility(View.GONE);
            holder.mLinearLayout.setVisibility(View.GONE);
            holder.mProductPrice.setVisibility(View.VISIBLE);
            holder.mProductPrice.setText(IntentConstants.RUPEES +
                    String.valueOf(BaseActivity.isValidNumberFormat()
                            .format(festivalOffers.get(position).getmDiscountPrice())));
        } else {
            holder.mPrice.setVisibility(View.GONE);
            holder.mFlatOffer.setText(String.valueOf(festivalOffers.get(position).getmDiscountPercent()) + " Off");
            holder.mProductPrice.setText(IntentConstants.RUPEES +
                    CommonMethod.formatLargeDouble(festivalOffers.get(position).getmDiscountPrice()));
            holder.mDiscountPrice.setText(IntentConstants.RUPEES +
                    CommonMethod.formatLargeDouble(festivalOffers.get(position).getmActualPrice()));
            holder.mDiscountPrice.setPaintFlags(holder.mDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        Glide.with(mContext).load(festivalOffers.get(position).getmImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.redinton_image)
                .into(holder.mImage);

        try {
            Date date1 = simpleDateFormat.parse(serverDate + " " + festivalOffers.get(position).getmServerTime());
            Date date2 = simpleDateFormat.parse(festivalOffers.get(position).getmEndingDate() + " " + festivalOffers.get(position).getmEndTime());

            String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
            long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

            long endTime = BaseActivity.dateFormatMinusTwo(festivalOffers.get(position).getmEndingDate(), festivalOffers.get(position).getmEndTime());
            long startTime = BaseActivity.longConversion(serverDate, festivalOffers.get(position).getmServerTime());
            if (startTime >= endTime) {
                new CountDownTimer(diffInMillisec, 1000) {
                    @Override
                    public void onTick(long millis) {
                        @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millis),
                                TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                        holder.mTimerText.setText(hms);
                    }

                    @Override
                    public void onFinish() {
                        //No data
                    }
                }.start();
            } else {
                holder.mTimerText.setText(diffrenceDate);
            }

        } catch (ParseException e) {
            BaseActivity.logd(e.getMessage());
        }


        holder.mLinearLayout.setOnClickListener(view -> setClickFunc(
                festivalOffers.get(position).getmProductName(),
                festivalOffers.get(position).getmItemCode(),
                festivalOffers.get(position).getmVendorCode()));
    }

    /*Click Function*/
    private void setClickFunc(String productName, String itemCode, String vendorCode) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, productName);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);

        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        if (mType.equals(GlobalConstants.MFESTIVALS)) {
            return festivalOffers.size();
        } else {
            return 0;
        }

    }

    class OfferHolder extends RecyclerView.ViewHolder {

        private TextView mProductName1;
        private TextView mProductPrice1;
        private ImageView imagePreOrder;
        private LinearLayout mLinLayout;
        private CardView mLinearLayout;
        private TextView mProductName;
        private TextView mProductPrice;
        private TextView mDiscountPrice;
        private TextView mFlatOffer;
        private TextView mQuantity;
        private TextView mPrice;

        private FrameLayout mFrameLayout;
        private ImageView mImage;
        private TextView mItemCode;
        private TextView mTimerText;

        private TextView mTopActualPrice;
        private LinearLayout mTopDiscountLayout;

        private OfferHolder(View view) {
            super(view);


            mContext = view.getContext();

            mProductName1 = itemView.findViewById(R.id.txt_productName);
            mProductPrice1 = itemView.findViewById(R.id.txt_productDes);
            mTimerText = itemView.findViewById(R.id.timer_text);

            imagePreOrder = itemView.findViewById(R.id.imagePreOrder);

            mLinLayout = itemView.findViewById(R.id.LL_recycleView);

            mProductName = itemView.findViewById(R.id.text_productname);

            mProductPrice = itemView.findViewById(R.id.text_productprice);
            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mFlatOffer = itemView.findViewById(R.id.text_flatOffer);
            mQuantity = itemView.findViewById(R.id.text_quantity);
            mFrameLayout = itemView.findViewById(R.id.offerLayout);
            mLinearLayout = itemView.findViewById(R.id.totallayout);
            mPrice = itemView.findViewById(R.id.productPrice);
            mImage = itemView.findViewById(R.id.image);

            mItemCode = itemView.findViewById(R.id.text_itemcode);

            mTopActualPrice = itemView.findViewById(R.id.text_actualprice);
            mTopDiscountLayout = itemView.findViewById(R.id.discountLayout);

        }
    }

}
