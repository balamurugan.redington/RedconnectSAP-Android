package redington.com.redconnect.redconnect.chequepending.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.payment.activity.PaymentScreen;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;


public class PaymentCommitment extends BaseActivity implements NewListener {


    private TextView textOverdueAmt;
    private String expValue = "";
    private int totalLength;
    private EditText editExpectedValue;
    private EditText editRemarks;
    private Context mContext;
    private CommonUtils commonUtils;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_payment_commitment);

        hideKeyboard();

        mContext = PaymentCommitment.this;
        commonUtils = new CommonUtils(mContext);


        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        ImageView imageView = findViewById(R.id.image_merge);
        imageView.setImageResource(R.drawable.cheque);
        TextView txtbizdesc = findViewById(R.id.txt_biz_desc);
        TextView txtinvoiceValue = findViewById(R.id.tv_invoiceValue);
        textOverdueAmt = findViewById(R.id.tv_overdueAmt);
        editExpectedValue = findViewById(R.id.ed_expectedValue);
        editRemarks = findViewById(R.id.ed_remarks);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            txtbizdesc.setText(bundle.getString(GlobalConstants.BIZ_DESC));
            txtinvoiceValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + bundle.getString(GlobalConstants.INV_VALUE));
            textOverdueAmt.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + bundle.getString(GlobalConstants.BALANCE_DUEAMOUNT));
        }

        if (textOverdueAmt.getText().toString().contains(".")) {
            Double aDouble = Double.parseDouble(textOverdueAmt.getText().toString().replace("₹ ", ""));
            String toString = Double.toString(aDouble);
            String fromString = toString.substring(0, toString.indexOf('.'));
            totalLength = fromString.length();
        } else {
            totalLength = textOverdueAmt.length();
        }

        editExpectedValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {
                expValue = editExpectedValue.getText().toString();
                if (expValue.isEmpty()) return;
                String str2 = mValidateDecimal(expValue, totalLength);

                if (!str2.equals(expValue)) {
                    editExpectedValue.setText(str2);
                    int position = editExpectedValue.getText().length();
                    editExpectedValue.setSelection(position);
                }
            }
        });

        toolbar(IntentConstants.PAYMENTCOMMIT);

    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    /*Button Payment Commitment*/
    public void mPayCommit(View view) {
        calLPaymentScreen(view.getContext());
    }

    private void calLPaymentScreen(Context context) {
        String overdueAmount = textOverdueAmt.getText().toString().replace("₹ ", "");
        Double mExpectedValue = doubleValidate(editExpectedValue.getText().toString().trim());
        if (expValue.isEmpty()) {
            shortToast(mContext, GlobalConstants.PLEASE_ENTER_THE + GlobalConstants.EXP_VALUE);
        } else if (mExpectedValue != 0.0d) {
            if (Double.parseDouble(expValue) <= Double.parseDouble(overdueAmount)) {
                char[] chars = expValue.toCharArray();
                char lastCharacter = chars[chars.length - 1];
                String s = String.valueOf(lastCharacter);
                if (s.contains(".")) {
                    shortToast(mContext, GlobalConstants.VALID_AMOUNT);
                } else {
                    callRetryAPI(GlobalConstants.GetSharedValues.getSpUserId(), context);
                }

            } else {
                shortToast(mContext, GlobalConstants.EXPECTED_VALUE_VA_2);
            }
        } else {
            shortToast(mContext, GlobalConstants.EXPECTED_ZERO_VALUE);
        }
    }

    private void callRetryAPI(String userId, Context context) {
        commonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(userId, context, this);
    }

    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String transactionId = retryResponse.getData().getPendOrd().get(0).getTransactionID();
                    /*Kamesh*/
                    String referenceId = retryResponse.getData().getPendOrd().get(0).getReferenceNo();
                    if (!transactionId.isEmpty() || !referenceId.isEmpty()) {
                        showPopup();
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString(IntentConstants.REMARKS, editRemarks.getText().toString());
                        bundle.putString(IntentConstants.AMOUNT, expValue);
                        bundle.putString(IntentConstants.TAG, IntentConstants.CHEQUEPENDING);
                        LaunchIntentManager.routeToActivityStackBundle(mContext, PaymentScreen.class, bundle);
                    }
                    /*Kamesh*/
                    break;
                case IntentConstants.FAILURE_URL:
                    Bundle bundle = new Bundle();
                    bundle.putString(IntentConstants.REMARKS, editRemarks.getText().toString());
                    bundle.putString(IntentConstants.AMOUNT, expValue);
                    bundle.putString(IntentConstants.TAG, IntentConstants.CHEQUEPENDING);
                    LaunchIntentManager.routeToActivityStackBundle(mContext, PaymentScreen.class, bundle);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }


    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup() {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        alertContent.setText(GlobalConstants.DASHBOARD_RETRY);


        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });


        mDialog.show();
    }
}





