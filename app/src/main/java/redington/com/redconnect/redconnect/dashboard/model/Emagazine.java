
package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Emagazine implements Serializable {

    @SerializedName("CoverURL")
    private String mCoverURL;
    @SerializedName("magazineURL")
    private String mMagazineURL;
    @SerializedName("pdfURL")
    private String mPdfURL;

    public String getCoverURL() {
        return mCoverURL;
    }

    public String getMagazineURL() {
        return mMagazineURL;
    }

    public String getPdfURL() {
        return mPdfURL;
    }


}
