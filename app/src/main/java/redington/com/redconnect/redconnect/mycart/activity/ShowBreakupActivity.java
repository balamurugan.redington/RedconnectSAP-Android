package redington.com.redconnect.redconnect.mycart.activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.mycart.adapter.ShowBreakupAdapter;
import redington.com.redconnect.redconnect.mycart.model.ShowBreakup;

public class ShowBreakupActivity extends BaseActivity {
    private Context mContext;
    private TextView mTotalAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breakup);

        mContext = ShowBreakupActivity.this;
        TextView mProductName = findViewById(R.id.product_name);
        TextView mItemcode = findViewById(R.id.itemcode);
        TextView mVendorCode = findViewById(R.id.txt_vendor);
        TextView mTotalQuantity = findViewById(R.id.txt_quantity);
        mTotalAmount = findViewById(R.id.totalAmount);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mProductName.setText(bundle.getString(IntentConstants.PRODUCT_NAME));
            mItemcode.setText(bundle.getString(IntentConstants.ITEMCODE));
            mVendorCode.setText(bundle.getString(IntentConstants.VENDORCODE));
            mTotalQuantity.setText(bundle.getString(IntentConstants.TOTALQUANTITY));
            ArrayList<ShowBreakup> breakups = (ArrayList<ShowBreakup>) bundle.getSerializable(IntentConstants.BREAKUPARRAY);
            bindRecyclerView(breakups);
        }

        initViews();
    }

    private void bindRecyclerView(ArrayList<ShowBreakup> breakups) {
        RecyclerView recyclerView = findViewById(R.id.breakup_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new ShowBreakupAdapter(breakups));
        NestedScrollView nestedScrollView = findViewById(R.id.nestedScroll);
        nestedScrollView.getParent().requestChildFocus(nestedScrollView, nestedScrollView);
        calculateTotAmt(breakups);
    }

    /*Amount Cal's*/
    @SuppressLint("SetTextI18n")
    private void calculateTotAmt(ArrayList<ShowBreakup> breakups) {
        Double amount = 0.00;
        int sizTot = breakups.size();
        for (int i = 0; i < sizTot; i++) {
            amount += breakups.get(i).getTOTAL();
        }

        mTotalAmount.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                BaseActivity.isValidNumberFormat().format(amount));
    }

    private void initViews() {
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        toolbar(IntentConstants.SHOWBREAKUP);
    }


    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }
}
