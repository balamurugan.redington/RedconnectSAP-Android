package redington.com.redconnect.redconnect.voiceofcustomer.model;

import com.google.gson.annotations.SerializedName;

public class VocNumber {
    @SerializedName("VoiceOfCustomerNumber")
    private String voiceNumber;

    public String getVoiceNumber() {
        return voiceNumber;
    }

    public void setVoiceNumber(String voiceCode) {
        this.voiceNumber = voiceNumber;
    }
}
