package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class LoginData implements Serializable {

    @SerializedName("Customer")
    private String mCustomer;
    @SerializedName("Customer_Name")
    private String mCustomerName;
    @SerializedName("Mail")
    private String mMail;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("Phone_Number")
    private String mPhoneNumber;
    @SerializedName("UserID")
    private String mUserID;

    public String getCustomer() {
        return mCustomer;
    }


    public String getCustomerName() {
        return mCustomerName;
    }


    public String getMail() {
        return mMail;
    }


    public String getMessage() {
        return mMessage;
    }


    public String getPhoneNumber() {
        return mPhoneNumber;
    }


    public String getUserID() {
        return mUserID;
    }


}
