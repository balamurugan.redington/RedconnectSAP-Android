package redington.com.redconnect.redconnect.profile.helper;


import android.content.Context;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.profile.model.ChangePasswordJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordManager {

    private ChangePasswordManager() {
        throw new IllegalStateException(IntentConstants.CHANGE_PASS);
    }

    public static void getChangePwdServiceCall(String userid, String oldPassword, String newPassword, final Context context, final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getChangePassword(getRequestBody(userid, oldPassword, newPassword)).enqueue(new Callback<ChangePasswordJsonResponse>() {
                @Override
                public void onResponse(Call<ChangePasswordJsonResponse> call, Response<ChangePasswordJsonResponse> response) {
                    listener.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<ChangePasswordJsonResponse> call, Throwable t) {
                    listener.onFailure(t.toString());
                }
            });
        } else {
            listener.onError();
        }
    }

    /*String to Request Body*/
    private static RequestBody getRequestBody(String userid, String oldPassword, String newPassword) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.JCUSERID, userid);
            jsonValues.put(JsonKeyConstants.JCOLD_PASSKEY, oldPassword);
            jsonValues.put(JsonKeyConstants.JCNEW_PASSKEY, newPassword);
            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }


}
