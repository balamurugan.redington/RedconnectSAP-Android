package redington.com.redconnect.redconnect.dashboard.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import redington.com.redconnect.common.methods.CommonMethod;


public class FestivalOffer implements Serializable {
    @SerializedName("Actual_Price")
    private String mActualPrice;
    @SerializedName("CustomerCode")
    private String mCustomerCode;
    @SerializedName("Discount")
    private String mDiscount;
    @SerializedName("Discount_Price")
    private String mDiscountPrice;
    @SerializedName("ImageURL")
    private String mImageURL;
    @SerializedName("ItemCode")
    private String mItemCode;
    @SerializedName("ItemDesc")
    private String mItemDesc;
    @SerializedName("ProductName")
    private String mProductName;
    @SerializedName("VendorCode")
    private String mVendorCode;
    @SerializedName("HeaderName")
    private String mHeaderName;
    @SerializedName("StartingDate")
    private String mStartingDate;
    @SerializedName("EndingDate")
    private String mEndingDate;
    @SerializedName("ServerTime")
    private String mServerTime;
    @SerializedName("EndTime")
    private String mEndTime;
    @SerializedName("ServerDate")
    private String mServerDate;
    @SerializedName("Discount_Percent")
    private String mDiscountPercent;


    public String getmDiscountPercent() {return CommonMethod.getValidDoubleString(mDiscountPercent); }


    public String getmServerDate() {
        return mServerDate;
    }


    public String getmActualPrice() {
        return CommonMethod.getValidDoubleString(mActualPrice);
    }


    public String getmCustomerCode() {
        return mCustomerCode;
    }


    public String getmDiscount() {
        return CommonMethod.getValidDoubleString(mDiscount);
    }


    public String getmDiscountPrice() {
        return CommonMethod.getValidDoubleString(mDiscountPrice);
    }


    public String getmImageURL() {
        return mImageURL;
    }


    public String getmItemCode() {
        return mItemCode;
    }


    public String getmItemDesc() {
        return mItemDesc;
    }


    public String getmProductName() {
        return mProductName;
    }


    public String getmVendorCode() {
        return mVendorCode;
    }


    public String getmHeaderName() {
        return mHeaderName;
    }


    public String getmStartingDate() {
        return mStartingDate;
    }


    public String getmEndingDate() {
        return mEndingDate;
    }


    public String getmServerTime() {
        return mServerTime;
    }


    public String getmEndTime() {
        return mEndTime;
    }

}
