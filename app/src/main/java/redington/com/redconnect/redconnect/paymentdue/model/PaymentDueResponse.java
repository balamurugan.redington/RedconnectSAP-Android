
package redington.com.redconnect.redconnect.paymentdue.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class PaymentDueResponse implements Serializable {

    @SerializedName("Data")
    private PaymentData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public PaymentData getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
