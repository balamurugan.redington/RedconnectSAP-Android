package redington.com.redconnect.redconnect.chequepending.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.chequepending.adapter.ChequeAdapter;
import redington.com.redconnect.redconnect.chequepending.adapter.ScrollListener;
import redington.com.redconnect.redconnect.chequepending.helper.ChequePendingServiceManager;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingData;
import redington.com.redconnect.redconnect.chequepending.model.ChequePendingJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class ChequePendingActivity extends BaseActivity implements UIListener {

    private LinearLayout linearLayout;
    private CommonUtils commonUtils;
    private Context context;
    private String userID;
    private int currentPage = 1;
    private RecyclerView myChequeRecyclerview;
    private boolean isLoading = false;
    private FrameLayout mProgressBar;
    private ChequeAdapter chequeAdapter;
    private LinearLayoutManager verticalLayout;
    private int mTotalPage;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_cheque_pending);

        context = ChequePendingActivity.this;

        RelativeLayout myImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        linearLayout = findViewById(R.id.LL_totallayout);
        myChequeRecyclerview = findViewById(R.id.cheques_recyclerView);
        linearLayout.setVisibility(View.GONE);
        imageButton.setVisibility(View.VISIBLE);
        myImageCart.setVisibility(View.GONE);
        relativeLayout = findViewById(R.id.coordinatorLayout);
        commonUtils = new CommonUtils(context);

        ImageView imageView = findViewById(R.id.image_merge);
        imageView.setImageResource(R.drawable.cheque);

        mProgressBar = findViewById(R.id.main_progress);
        mProgressBar.setVisibility(View.GONE);

        toolbar(IntentConstants.CHEQUEPENDING);

        chequePendingApi();

    }

    private void chequePendingApi() {
        commonUtils.showProgressDialog();
        userID = GlobalConstants.GetSharedValues.getSpUserId();
        getChequesValues(userID, currentPage, GlobalConstants.RECORD_NO);
    }

    private void getChequesValues(String userID, int pageNum, String recordNum) {
        ChequePendingServiceManager.getChequePendingServiceCall(userID, Integer.toString(pageNum),
                recordNum, context, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            chequePendingApi();
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }

    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    public void rightNavigation(View view) {
        setRightNavigation(view.getContext());
    }


    /*Response  From  API*/

    @Override
    public void onSuccess(Object successObject) {
        mProgressBar.setVisibility(View.GONE);
        commonUtils.dismissProgressDialog();
        ChequePendingJsonResponse chequePendingJsonResponse = (ChequePendingJsonResponse) successObject;
        if (chequePendingJsonResponse != null) {
            switch (chequePendingJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    ArrayList<ChequePendingData> arrayList = new ArrayList(chequePendingJsonResponse.getData());
                    mTotalPage = arrayList.get(0).getmTotalPage();
                    linearLayout.setVisibility(View.VISIBLE);
                    if (currentPage == 1) {
                        myChequeRecyclerview.setHasFixedSize(true);
                        verticalLayout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        myChequeRecyclerview.setLayoutManager(verticalLayout);
                        chequeAdapter = new ChequeAdapter(this, arrayList);
                        myChequeRecyclerview.setAdapter(chequeAdapter);
                    } else {
                        chequeAdapter.chequePendingServiceUpdate(arrayList);
                    }
                    chequeAdapter.notifyDataSetChanged();

                    myChequeRecyclerview.addOnScrollListener(new ScrollListener(verticalLayout) {
                        @Override
                        protected void loadMoreItems() {
                            isLoading = true;
                            currentPage += 1;
                            myChequeRecyclerview.removeOnScrollListener(this);
                            mProgressBar.setVisibility(View.VISIBLE);
                            loadNextPage();
                        }

                        @Override
                        public int getTotalPageCount() {
                            return mTotalPage;
                        }

                        @Override
                        public boolean isLoading() {
                            return isLoading;
                        }
                    });


                    break;
                case IntentConstants.FAILURE_URL:
                    linearLayout.setVisibility(View.GONE);
                    noRecords(context, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    linearLayout.setVisibility(View.GONE);
                    if (currentPage == 1) {
                        noRecords(context, GlobalConstants.SERVER_ERR);
                    } else {
                        callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
                    }
                    break;
            }

        } else {

            if (currentPage == 1) {
                linearLayout.setVisibility(View.GONE);
                noRecords(context, GlobalConstants.SERVER_ERR);
            } else {
                callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
            }
        }
    }


    @Override
    public void onFailure(Object failureObject) {
        mProgressBar.setVisibility(View.GONE);
        commonUtils.dismissProgressDialog();
        if (currentPage == 1) {
            noRecords(context, GlobalConstants.SERVER_ERR);
        } else {
            callSnackBar(GlobalConstants.SERVER_ERROR_BAR);
        }
    }


    @Override
    public void onError() {
        mProgressBar.setVisibility(View.GONE);
        commonUtils.dismissProgressDialog();
        if (currentPage == 1) {
            LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
        } else {
            callSnackBar(GlobalConstants.INTERNET_ERROR);
        }
    }

    /*Load Next Page for Pagination*/
    private void loadNextPage() {
        isLoading = false;
        if (currentPage <= mTotalPage) {
            getChequesValues(userID, currentPage, GlobalConstants.RECORD_NO);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /*Server error & Internet Error SnackBar*/
    private void callSnackBar(String snackString) {
        Snackbar snackbar = Snackbar
                .make(relativeLayout, snackString, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> {
                    mProgressBar.setVisibility(View.VISIBLE);
                    getChequesValues(userID, currentPage, GlobalConstants.RECORD_NO);

                });
        snackbar.setActionTextColor(Color.YELLOW);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }
}
