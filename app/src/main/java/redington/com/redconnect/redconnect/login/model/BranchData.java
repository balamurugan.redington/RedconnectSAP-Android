
package redington.com.redconnect.redconnect.login.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class BranchData implements Serializable {

    @SerializedName("Branch")
    private String mBranch;
    @SerializedName("BranchDesc")
    private String mBranchDesc;

    public String getBranch() {
        return mBranch;
    }

    public String getBranchDesc() {
        return mBranchDesc;
    }


}
