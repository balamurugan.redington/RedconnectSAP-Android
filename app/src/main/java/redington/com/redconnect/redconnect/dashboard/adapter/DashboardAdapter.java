package redington.com.redconnect.redconnect.dashboard.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.Deal;
import redington.com.redconnect.redconnect.dashboard.model.TopSelling;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardHolder> {

    private Context mContext;
    private List<TopSelling> topSellingArrayList;
    private List<Deal> dealArrayList;
    private String mType = "";
    private int layoutRes;
    private int type = 0;


    public DashboardAdapter(Context context, List<TopSelling> topSellings, int type) {
        this.mContext = context;
        this.topSellingArrayList = topSellings;
        this.type = type;
    }

    public DashboardAdapter(Context context, List<Deal> deals, String type) {
        this.mContext = context;
        this.dealArrayList = deals;
        this.mType = type;
    }


    @NonNull
    @Override
    public DashboardAdapter.DashboardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mType.equals(GlobalConstants.MDEALS) && dealArrayList.size() != 1) {
            layoutRes = R.layout.spot_recycle_list;
        } else if (mType.equals("")) {
            layoutRes = R.layout.list_dashboard_new;
        } else if (mType.equals(GlobalConstants.MDEALS) && dealArrayList.size() == 1) {
            layoutRes = R.layout.list_main_recylce_full;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new DashboardAdapter.DashboardHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final DashboardHolder holder, int position)
    {
        final int pos = holder.getAdapterPosition();

        if (mType.equals("") && type == 1)
        {
            double discountpercent = Double.parseDouble(topSellingArrayList.get(position).getDiscountPercent());

            if (discountpercent == 0.0) {
                holder.mProductPriceViewAll.setVisibility(View.VISIBLE);
                holder.mProductPriceViewAll.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getDiscountPrice()));
            } else {
                holder.mProductPriceViewAll.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getDiscountPrice()));

                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getActualPrice()));
                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            holder.mProductName.setText(topSellingArrayList.get(position).getProductName());
            holder.mQuantity.setText(topSellingArrayList.get(position).getItemCode());
            Glide.with(mContext).load(topSellingArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mImage);
            holder.mTotaLayout.setOnClickListener(view -> setClickFunc(
                    topSellingArrayList.get(pos).getProductName(),
                    topSellingArrayList.get(pos).getItemCode(),
                    topSellingArrayList.get(pos).getVendorCode()));


        } else if (mType.equals(GlobalConstants.MDEALS) && type == 0) {
            dealsValues(holder, pos);

        }
    }

    /*Deals Values*/
    @SuppressLint("SetTextI18n")
    private void dealsValues(final DashboardHolder holder, final int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.getDefault());
        holder.mTimerText.setVisibility(View.VISIBLE);
        String serverDate = dealArrayList.get(position).getmServerDate();

        if (dealArrayList.size() == 1) {
            holder.mItemCode.setText(dealArrayList.get(position).getItemCode());
            holder.mProductName1.setText(dealArrayList.get(position).getProductName());

            double disPercent = Double.parseDouble(dealArrayList.get(position).getmDiscountPercent());
            if (disPercent == 0.0) {
                holder.mProductPrice1.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getDiscountPrice()));
            } else {
                holder.mProductPrice1.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getActualPrice()));

                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }


            Glide.with(mContext).load(dealArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.imagePreOrder);

            try {
                Date date1 = simpleDateFormat.parse(serverDate + " " + dealArrayList.get(position).getServerTime());
                Date date2 = simpleDateFormat.parse(dealArrayList.get(position).getEndingDate() + " " + dealArrayList.get(position).getEndTime());

                String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
                long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

                long endTime = BaseActivity.dateFormatMinusTwo(dealArrayList.get(position).getEndingDate(), dealArrayList.get(position).getEndTime());
                long startTime = BaseActivity.longConversion(serverDate, dealArrayList.get(position).getServerTime());
                if (startTime >= endTime) {
                    new CountDownTimer(diffInMillisec, 1000) {
                        @Override
                        public void onTick(long millis) {
                            @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                    TimeUnit.MILLISECONDS.toHours(millis),
                                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                            holder.mTimerText.setText(hms);
                        }

                        @Override
                        public void onFinish() {
                            //Not in use
                        }
                    }.start();
                } else {
                    holder.mTimerText.setText(diffrenceDate);
                }

            } catch (ParseException e) {
                BaseActivity.logd(e.getMessage());
            }

            holder.mLinLayout.setOnClickListener(view -> setClickFunc(
                    dealArrayList.get(position).getProductName(),
                    dealArrayList.get(position).getItemCode(),
                    dealArrayList.get(position).getVendorCode()));

        } else {
            defaultDealValues(holder, position, simpleDateFormat, serverDate);

        }
    }

    /*More no. of deals*/
    @SuppressLint("SetTextI18n")
    private void defaultDealValues(final DashboardHolder holder, final int position, SimpleDateFormat simpleDateFormat, String serverDate) {
        holder.mProductName.setText(dealArrayList.get(position).getProductName());
        holder.mQuantity.setText(dealArrayList.get(position).getItemCode());
        double offer = Double.parseDouble(dealArrayList.get(position).getmDiscountPercent());
        if (offer == 0.0) {
            holder.mFrameLayout.setVisibility(View.GONE);
            holder.mLinearLayout.setVisibility(View.GONE);
            holder.mProductPrice.setVisibility(View.VISIBLE);
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getDiscountPrice())));
        } else {
            holder.mPrice.setVisibility(View.GONE);
            holder.mFlatOffer.setText(String.valueOf(dealArrayList.get(position).getmDiscountPercent()) + " Off");
            holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                    String.valueOf(BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getDiscountPrice())));
            holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +

                    String.valueOf(BaseActivity.isValidNumberFormat().format(dealArrayList.get(position).getActualPrice())));

            holder.mDiscountPrice.setPaintFlags(holder.mDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        Glide.with(mContext).load(dealArrayList.get(position).getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.redinton_image)
                .into(holder.mImage);


        try {
            Date date1 = simpleDateFormat.parse(serverDate + " " + dealArrayList.get(position).getServerTime());
            Date date2 = simpleDateFormat.parse(dealArrayList.get(position).getEndingDate() + " " + dealArrayList.get(position).getEndTime());

            String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
            long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

            long endTime = BaseActivity.dateFormatMinusTwo(dealArrayList.get(position).getEndingDate(), dealArrayList.get(position).getEndTime());
            long startTime = BaseActivity.longConversion(serverDate, dealArrayList.get(position).getServerTime());
            if (startTime >= endTime) {
                new CountDownTimer(diffInMillisec, 1000) {
                    @Override
                    public void onTick(long millis) {
                        @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millis),
                                TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                        holder.mTimerText.setText(hms);
                    }

                    @Override
                    public void onFinish() {
                        //Not in use
                    }
                }.start();
            } else {
                holder.mTimerText.setText(diffrenceDate);
            }

        } catch (ParseException e) {
            BaseActivity.logd(e.getMessage());
        }

        holder.mLinearLayout.setOnClickListener(view -> setClickFunc(
                dealArrayList.get(position).getProductName(),
                dealArrayList.get(position).getItemCode(),
                dealArrayList.get(position).getVendorCode()));
    }

    /*Intent*/
    private void setClickFunc(String itemDesc, String itemCode, String vendorCode) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, itemDesc);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);
        mContext.startActivity(i);

    }

    @Override
    public int getItemCount() {
        if (mType.equals(GlobalConstants.MDEALS) && type == 0) {
            return dealArrayList.size();
        } else if (mType.equals("") && type == 1) {
            return topSellingArrayList.size();
        } else {
            return 0;
        }


    }

    class DashboardHolder extends RecyclerView.ViewHolder {


        private TextView mProductName1;
        private TextView mProductPrice1;
        private ImageView imagePreOrder;
        private LinearLayout mLinLayout;
        private LinearLayout mTotaLayout;
        private CardView mLinearLayout;
        private TextView mProductName;
        private TextView mProductPrice;
        private TextView mDiscountPrice;
        private TextView mFlatOffer;
        private TextView mQuantity;
        private TextView mPrice;
        private TextView mProductPriceViewAll;
        private FrameLayout mFrameLayout;
        private ImageView mImage;
        private TextView mItemCode;
        private TextView mTimerText;
        private TextView mTopActualPrice;
        private LinearLayout mTopDiscountLayout;

        DashboardHolder(View view) {
            super(view);

            mContext = view.getContext();

            mProductName1 = itemView.findViewById(R.id.txt_productName);
            mProductPrice1 = itemView.findViewById(R.id.txt_productDes);
            mTimerText = itemView.findViewById(R.id.timer_text);

            imagePreOrder = itemView.findViewById(R.id.imagePreOrder);

            mLinLayout = itemView.findViewById(R.id.LL_recycleView);

            mProductName = itemView.findViewById(R.id.text_productname);
            mProductPriceViewAll = itemView.findViewById(R.id.text_price);
            mProductPrice = itemView.findViewById(R.id.text_productprice);
            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mFlatOffer = itemView.findViewById(R.id.text_flatOffer);
            mQuantity = itemView.findViewById(R.id.text_quantity);
            mFrameLayout = itemView.findViewById(R.id.offerLayout);
            mLinearLayout = itemView.findViewById(R.id.totallayout);
            mPrice = itemView.findViewById(R.id.productPrice);
            mImage = itemView.findViewById(R.id.image);
            mTotaLayout = itemView.findViewById(R.id.linearLayout);

            mItemCode = itemView.findViewById(R.id.text_itemcode);

            mTopActualPrice = itemView.findViewById(R.id.text_actualprice);
            mTopDiscountLayout = itemView.findViewById(R.id.discountLayout);

        }
    }


}

