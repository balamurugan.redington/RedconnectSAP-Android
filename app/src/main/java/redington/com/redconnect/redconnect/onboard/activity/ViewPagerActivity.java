package redington.com.redconnect.redconnect.onboard.activity;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.util.LaunchIntentManager;

public class ViewPagerActivity extends AhoyOnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String mSMSPermssions = this.getResources().getString(R.string.sms) + " \n " + this.getResources().getString(R.string.sms_per);
        String mRecordAudio = this.getResources().getString(R.string.audio) + " \n " + this.getResources().getString(R.string.microPhone);
        String mCamera = this.getResources().getString(R.string.cam) + " \n " + this.getResources().getString(R.string.camera);
        String mStorage = this.getResources().getString(R.string.storage) + " \n " + this.getResources().getString(R.string.storage_per);

        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("Shopping Cart", this.getResources().getString(R.string.shopping_cart), R.drawable.backpack);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("SPOT", this.getResources().getString(R.string.spot), R.drawable.chalk);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("Payment Dues", "Customer can view and Pay the Cheque Pending and Payment Dues..", R.drawable.chat);
        AhoyOnboarderCard ahoyOnboarderCard4 = new AhoyOnboarderCard(this.getResources().getString(R.string.per_enabled),
                mSMSPermssions + GlobalConstants.NEW_LINE + mRecordAudio + GlobalConstants.NEW_LINE + mCamera + GlobalConstants.NEW_LINE + mStorage);


        ahoyOnboarderCard1.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard2.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard3.setBackgroundColor(R.color.black_transparent);
        ahoyOnboarderCard4.setBackgroundColor(R.color.black_transparent);


        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);
        pages.add(ahoyOnboarderCard4);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.grey_200);
            if (page.getTitle().equals("REDington Connect is using following permissions.")) {
                page.setDescriptionTextSize(12.0F);
                page.setTitleTextSize(15.0f);
            }
        }

        setFinishButtonTitle();
        showNavigationControls();
        setGradientBackground();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }


        setOnboardPages(pages);
    }

    @Override
    public void onFinishButtonPressed() {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.TERMS_DATA, "View Pager");
        LaunchIntentManager.routeToActivityStackBundleStack(ViewPagerActivity.this, TermsAndCondition.class, bundle);

    }
}
