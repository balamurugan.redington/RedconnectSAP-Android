package redington.com.redconnect.redconnect.dashboard.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.model.Deal;
import redington.com.redconnect.redconnect.dashboard.model.TopSelling;
import redington.com.redconnect.redconnect.preorder.activity.ProductDetail;

public class ViewAllAdapter extends RecyclerView.Adapter<ViewAllAdapter.ViewAllHolder> {


    private Context mContext;
    private List<TopSelling> topSellingArrayList;
    private List<Deal> dealList;
    private String mType = "";
    private int type = 0;

    public ViewAllAdapter(Context context, List<TopSelling> topSellings, int type) {
        this.mContext = context;
        this.topSellingArrayList = topSellings;
        this.type = type;
    }

    public ViewAllAdapter(Context mContext, List<Deal> deals, String type) {
        this.mContext = mContext;
        this.dealList = deals;
        this.mType = type;
    }

    @NonNull
    @Override
    public ViewAllHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_all_recycle_list, parent, false);
        return new ViewAllAdapter.ViewAllHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewAllHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.getDefault());

        if (mType.equals("") && type == 1) {
            holder.mProductName.setText(topSellingArrayList.get(position).getProductName());
            double disPercent = Double.parseDouble(topSellingArrayList.get(position).getDiscountPercent());
            if (disPercent == 0.0) {
                holder.mOfferLayout.setVisibility(View.GONE);
                holder.mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getDiscountPrice()));
            } else {
                holder.mOfferLayout.setVisibility(View.VISIBLE);
                holder.mTextOffer.setText(String.valueOf(topSellingArrayList.get(position).getDiscountPercent()) + " Off");
                holder.mProductPrice.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getDiscountPrice()));

                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(topSellingArrayList.get(position).getActualPrice()));

                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            holder.mProductQuantity.setText(topSellingArrayList.get(position).getItemCode());
            Glide.with(mContext).load(topSellingArrayList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mImage);
            holder.cardView.setOnClickListener(view -> setClickFunc(
                    topSellingArrayList.get(pos).getProductName()
                    , topSellingArrayList.get(pos).getItemCode(),
                    topSellingArrayList.get(pos).getVendorCode()));
        } else if (mType.equals(GlobalConstants.MDEALS) && type == 0) {
            String serverDate = dealList.get(position).getmServerDate();
            holder.mTimerText.setVisibility(View.VISIBLE);
            holder.mProductName.setText(dealList.get(position).getProductName());
            double disPercent = Double.parseDouble(dealList.get(position).getmDiscountPercent());

            if (disPercent == 0.0) {
                holder.mOfferLayout.setVisibility(View.GONE);
                holder.mProductPrice.setText(IntentConstants.RUPEES +
                        String.valueOf(BaseActivity.isValidNumberFormat().format(dealList.get(position).getDiscountPrice())));
            } else {
                holder.mOfferLayout.setVisibility(View.VISIBLE);
                holder.mTextOffer.setText(String.valueOf(dealList.get(position).getmDiscountPercent()) + " Off");
                holder.mProductPrice.setVisibility(View.GONE);
                holder.mTopDiscountLayout.setVisibility(View.VISIBLE);
                holder.mDiscountPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(dealList.get(position).getDiscountPrice()));
                holder.mTopActualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                        BaseActivity.isValidNumberFormat().format(dealList.get(position).getActualPrice()));
                holder.mTopActualPrice.setPaintFlags(holder.mTopActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            holder.mProductQuantity.setText(dealList.get(position).getItemCode());
            Glide.with(mContext).load(dealList.get(position).getImageURL())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.redinton_image)
                    .into(holder.mImage);
            holder.cardView.setOnClickListener(view -> setClickFunc(
                    dealList.get(pos).getProductName(),
                    dealList.get(pos).getItemCode()
                    , dealList.get(pos).getVendorCode()));


            try {
                Date date1 = simpleDateFormat.parse(serverDate + " " + dealList.get(position).getServerTime());
                Date date2 = simpleDateFormat.parse(dealList.get(position).getEndingDate() + " " + dealList.get(position).getEndTime());

                String diffrenceDate = BaseActivity.calculateDifference(date1, date2);
                long diffInMillisec = BaseActivity.longDiffDate(date1, date2);

                long endTime = BaseActivity.dateFormatMinusTwo(dealList.get(position).getEndingDate(), dealList.get(position).getEndTime());
                long startTime = BaseActivity.longConversion(serverDate, dealList.get(position).getServerTime());
                if (startTime >= endTime) {
                    new CountDownTimer(diffInMillisec, 1000) {
                        @Override
                        public void onTick(long millis) {
                            @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                                    TimeUnit.MILLISECONDS.toHours(millis),
                                    TimeUnit.MILLISECONDS.toMinutes(millis) -
                                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                            holder.mTimerText.setText(hms);
                        }

                        @Override
                        public void onFinish() {
                            //Not in use
                        }
                    }.start();
                } else {
                    holder.mTimerText.setText(diffrenceDate);
                }
            } catch (ParseException e) {
                BaseActivity.logd(e.getMessage());
            }

        }

    }

    private void setClickFunc(String productName, String itemCode, String vendorCode) {
        Intent i = new Intent(mContext, ProductDetail.class);
        i.putExtra(GlobalConstants.PRODUCT_NAME, productName);
        i.putExtra(GlobalConstants.PRODUCT_ITEM_CODE, itemCode);
        i.putExtra(GlobalConstants.PRODUCT_VENDOR_CODE, vendorCode);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        if (mType.equals("") && type == 1) {
            return topSellingArrayList.size();
        } else if (mType.equals(GlobalConstants.MDEALS) && type == 0) {
            return dealList.size();
        } else {
            return 0;
        }

    }


    class ViewAllHolder extends RecyclerView.ViewHolder {
        private TextView mProductName;
        private TextView mProductPrice;
        private TextView mProductQuantity;
        private ImageView mImage;
        private CardView cardView;


        private TextView mTimerText;

        private TextView mDiscountPrice;
        private TextView mTopActualPrice;
        private LinearLayout mTopDiscountLayout;
        private FrameLayout mOfferLayout;
        private TextView mTextOffer;

        ViewAllHolder(View view) {
            super(view);

            mContext = view.getContext();

            mProductName = itemView.findViewById(R.id.text_productname);
            mProductPrice = itemView.findViewById(R.id.text_price);
            mProductQuantity = itemView.findViewById(R.id.text_quantity);
            mImage = itemView.findViewById(R.id.image);

            cardView = itemView.findViewById(R.id.cardView);

            mTimerText = itemView.findViewById(R.id.timer_text);

            mDiscountPrice = itemView.findViewById(R.id.text_discountprice);
            mTopActualPrice = itemView.findViewById(R.id.text_actualprice);
            mTopDiscountLayout = itemView.findViewById(R.id.discountLayout);

            mOfferLayout = itemView.findViewById(R.id.offerLayout);
            mTextOffer = itemView.findViewById(R.id.text_flatOffer);


        }
    }

}
