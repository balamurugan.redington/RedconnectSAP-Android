
package redington.com.redconnect.redconnect.payment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartUpdationJsonResponse implements Serializable {

    @SerializedName("Data")
    private UpdatedCartData mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public UpdatedCartData getData() {
        return mData;
    }
    
    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


}
