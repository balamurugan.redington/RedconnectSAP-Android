package redington.com.redconnect.redconnect.helpdesk.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TicketSubmitData implements Serializable {

    @SerializedName("TicketNumber")
    private String mTicketNumber;
    @SerializedName("Message")
    private String mMessage;

    public String getTicketNumber() {
        return mTicketNumber;
    }

    public String getMessage() {
        return mMessage;
    }
}
