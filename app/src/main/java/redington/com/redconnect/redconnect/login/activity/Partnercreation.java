package redington.com.redconnect.redconnect.login.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.login.helper.BranchHelper;
import redington.com.redconnect.redconnect.login.helper.CityServiceeHelper;
import redington.com.redconnect.redconnect.login.model.BranchData;
import redington.com.redconnect.redconnect.login.model.BranchResponse;
import redington.com.redconnect.redconnect.login.model.CityData;
import redington.com.redconnect.redconnect.login.model.CityJsonResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.LaunchIntentManager;

public class Partnercreation extends BaseActivity implements UIListener, NewListener {

    private Context mContext;
    private CommonUtils commonUtils;
    private ScrollView mScrollview;
    private ArrayAdapter<String> listAdapter;
    private EditText mCity;
    private EditText state;
    private EditText mAddress1;
    private EditText mAddress2;
    private EditText mAddress3;
    private EditText mPostalCode;
    private EditText mCompanyName;
    private TextView mTextBranch;
    private Spinner mBranchSpinner;
    private String branchCode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partner_fragment1);
        mContext = Partnercreation.this;
        commonUtils = new CommonUtils(mContext);

        toolbar(IntentConstants.CUSTOMERPROFILE);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageCart.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);
        setInitViews();
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    private void setInitViews() {
        mScrollview = findViewById(R.id.scrollView);
        mCompanyName = findViewById(R.id.part_comopanyName);
        mAddress1 = findViewById(R.id.part_address1);
        mAddress2 = findViewById(R.id.part_address2);
        mAddress3 = findViewById(R.id.part_address3);
        mCity = findViewById(R.id.part_city);
        state = findViewById(R.id.part_state);
        mPostalCode = findViewById(R.id.part_postalCode);
        mTextBranch = findViewById(R.id.part_branch);
        mBranchSpinner = findViewById(R.id.part_spinner_branch);


        findViewById(R.id.part_city).setOnClickListener(v -> showPopup(mContext));

        findViewById(R.id.part_button).setOnClickListener((View v) -> onClickPart());
    }

    private void onClickPart() {
        if (mCompanyName.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.company_name));
            mCompanyName.setFocusableInTouchMode(true);
            mCompanyName.requestFocus();
        } else if (mAddress1.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.address));
            mAddress1.setFocusableInTouchMode(true);
            mAddress1.requestFocus();
        } else if (mCity.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.city));
            mCity.setFocusableInTouchMode(true);
            mCity.requestFocus();
        } else if (mPostalCode.getText().toString().trim().isEmpty()) {
            shortToast(mContext, plsEnter(R.string.postalCode));
            mPostalCode.setFocusableInTouchMode(true);
        } else if (mPostalCode.getText().length() < 6) {
            shortToast(mContext, plsEnter(R.string.validPostalCode));
            mPostalCode.setFocusableInTouchMode(true);
        } else {
            Bundle bundle = new Bundle();
            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add(mCompanyName.getText().toString().trim());
            stringArrayList.add(mAddress1.getText().toString().trim());
            stringArrayList.add(mAddress2.getText().toString().trim());
            stringArrayList.add(mAddress3.getText().toString().trim());
            stringArrayList.add(mCity.getText().toString().trim());
            stringArrayList.add(state.getText().toString().trim());
            stringArrayList.add(branchCode.trim());
            stringArrayList.add(mPostalCode.getText().toString().trim());
            bundle.putStringArrayList(IntentConstants.FIRSTARRAY, stringArrayList);
            LaunchIntentManager.routeToActivityStackBundle(mContext, PartnerCreation2.class, bundle);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    private String plsEnter(int id) {
        return GlobalConstants.ENTER_TEXT + getString(id);
    }


    @SuppressLint("NewApi")
    private void showPopup(final Context mContext) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_quanty);
        mDialog.setCanceledOnTouchOutside(false);

        LinearLayout mTotalLayout = mDialog.findViewById(R.id.LL_totallayout);
        GradientDrawable gradientDrawable = new GradientDrawable();
        TextView mTextView = mDialog.findViewById(R.id.text_title);
        mTextView.setText(GlobalConstants.ENTER_CITY);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mTotalLayout.setBackground(gradientDrawable);
        }
        final EditText editable = mDialog.findViewById(R.id.ed_myquantity);
        editable.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editable.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        Button button = mDialog.findViewById(R.id.pre_ref_btm);
        button.setOnClickListener(v -> {
            final String ed = editable.getText().toString();
            if (!ed.isEmpty() && ed.length() > 2) {
                callCityApi(ed);
                mDialog.dismiss();
            } else {
                shortToast(mContext, GlobalConstants.VALID_CITY);
            }
        });

        mDialog.show();
    }

    private void callCityApi(String cityName) {
        commonUtils.showProgressDialog();
        CityServiceeHelper.getCityServiceCall(cityName, mContext, this);
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CityJsonResponse cityJsonResponse = (CityJsonResponse) successObject;
        if (cityJsonResponse != null) {
            switch (cityJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    List<CityData> cityData = cityJsonResponse.getData();
                    List<String> listArr = new ArrayList<>();
                    List<String> listArrStste = new ArrayList<>();
                    for (int i = 0; i < cityData.size(); i++) {
                        listArr.add(cityData.get(i).getCityMode() + " - " + cityData.get(i).getCityDescription());
                        listArrStste.add(cityData.get(i).getState());
                    }

                    showDialogWithLV(mContext, listArr, listArrStste);
                    break;
                case IntentConstants.FAILURE_URL:
                    snackBar(GlobalConstants.NO_DATA);
                    clearTextValues();
                    break;
                default:
                    snackBar(GlobalConstants.SERVER_ERROR_BAR);
                    clearTextValues();
                    break;
            }
        } else {
            snackBar(GlobalConstants.SERVER_ERROR_BAR);
            clearTextValues();
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.SERVER_ERROR_BAR);
        clearTextValues();
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        snackBar(GlobalConstants.INTERNET_ERROR);
    }

    private void clearTextValues() {
        mCity.setText("");
        state.setText("");
    }

    /*City Response*/
    @SuppressLint("NewApi")
    public void showDialogWithLV(Context con, final List<String> list, final List<String> stateList) {

        final Dialog mDialog = new Dialog(con, R.style.ActivityDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.spinner_dialog_despatch_mode);
        mDialog.setCanceledOnTouchOutside(true);
        ListView mListView = mDialog.findViewById(R.id.recipe_list_view);
        TextView textTitle = mDialog.findViewById(R.id.recipe_list_title);
        LinearLayout mDialogmain = mDialog.findViewById(R.id.dialogMain);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(ContextCompat.getColor(con, R.color.colorPrimary));
            gradientDrawable.setCornerRadius(IntentConstants.CORNER_RADIUS_3);
            mDialogmain.setBackground(gradientDrawable);
        }
        textTitle.setText(GlobalConstants.SELECT_CITY);
        mListView.setDividerHeight(1);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            String[] s = list.get(position).split("-");
            mCity.setText(s[1]);
            state.setText(stateList.get(position));
            mDialog.dismiss();
            callBranchApi();
        });


        listAdapter = new ArrayAdapter<String>(mContext, R.layout.mytext, list) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                listAdapter.notifyDataSetChanged();
                return view;
            }
        };
        mListView.setAdapter(listAdapter);

        mDialog.show();
    }


    /*Show Snack Bar*/
    private void snackBar(String newString) {
        Snackbar snackbar = Snackbar.make(mScrollview, newString, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void callBranchApi() {
        commonUtils.showProgressDialog();
        String selectedState = state.getText().toString();
        mTextBranch.setVisibility(View.GONE);
        mBranchSpinner.setVisibility(View.GONE);
        if (!selectedState.equals(GlobalConstants.NULL_DATA)) {
            BranchHelper.getBranch(selectedState, mContext, this);
        }


    }


    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        BranchResponse branchResponse = (BranchResponse) successObject;
        if (branchResponse != null) {
            switch (branchResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    List<BranchData> branchData = branchResponse.getData();
                    mBranchSpinner.setVisibility(View.VISIBLE);
                    mTextBranch.setVisibility(View.VISIBLE);
                    List<String> branchArray = new ArrayList<>();
                    for (int i = 0; i < branchData.size(); i++) {
                        branchArray.add(branchData.get(i).getBranch() + " - " + branchData.get(i).getBranchDesc());

                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, branchArray);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mBranchSpinner.setAdapter(arrayAdapter);


                    mBranchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String[] branch = mBranchSpinner.getSelectedItem().toString().split(" - ");
                            branchCode = branch[0];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            //No data
                        }
                    });
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, GlobalConstants.NO_BRANCH);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }
}
