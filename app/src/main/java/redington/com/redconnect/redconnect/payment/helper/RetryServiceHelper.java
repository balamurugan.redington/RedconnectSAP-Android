package redington.com.redconnect.redconnect.payment.helper;

import android.content.Context;

import org.json.JSONObject;

import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetryServiceHelper {

    private RetryServiceHelper() {
        throw new IllegalStateException(IntentConstants.RETRY);
    }

    public static void retryCall(String userId, final Context context, final NewListener listener) {
        if (CommonUtils.isNetworkAvailable(context)) {
            JSONObject loginHeader = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(loginHeader).getRetryReq(userId).enqueue(new Callback<RetryResponse>() {
                @Override
                public void onResponse(Call<RetryResponse> call, Response<RetryResponse> response) {
                    listener.mSuccessObject(response.body());
                }

                @Override
                public void onFailure(Call<RetryResponse> call, Throwable t) {
                    listener.mFailureObject(t.toString());
                }
            });
        } else {
            listener.mError();
        }
    }
}
