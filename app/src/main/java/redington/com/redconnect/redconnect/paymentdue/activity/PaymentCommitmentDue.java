package redington.com.redconnect.redconnect.paymentdue.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.payment.activity.PaymentScreen;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.paymentdue.helper.PaymentDueManager;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueDetail;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueHeader;
import redington.com.redconnect.redconnect.paymentdue.model.PaymentDueResponse;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;


public class PaymentCommitmentDue extends BaseActivity implements UIListener, NewListener {

    private CommonUtils commonUtils;
    private TextView tvtotalinvoice;
    private TextView tvtotaloverdue;
    private TextView mCustName;
    private EditText expectedValue;
    private EditText remarksEdit;
    private ArrayList<PaymentDueDetail> arrayList;
    private Context context;
    private int totalLength;
    private String totalDue;
    private String expValue = "";
    private String uiTypes = "";
    private String userID;

    private String week;
    private Bundle bundle;


    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_paymentdue_commitment);
        context = PaymentCommitmentDue.this;

        hideKeyboard();

        commonUtils = new CommonUtils(context);

        userID = GlobalConstants.GetSharedValues.getSpUserId();
        LinearLayout mWeekLayout = findViewById(R.id.weeklayout);

        TextView mWeekTitle = findViewById(R.id.titleWeek);
        TextView mWeekValue = findViewById(R.id.txt_week);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            mWeekLayout.setVisibility(View.VISIBLE);
            week = bundle.getString(IntentConstants.WEEK);
            String weelValue = bundle.getString(IntentConstants.WEEKVALUE);
            double value = Double.parseDouble(weelValue);
            String weekName = bundle.getString(IntentConstants.WEEKNAME);
            mWeekTitle.setText(weekName);
            mWeekValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + isValidNumberFormat().format(value));

            callPaymentApi(week);
        } else {
            mWeekLayout.setVisibility(View.GONE);
            callPaymentApi(GlobalConstants.NULL_DATA);
        }

        LinearLayout mMergeLayout = findViewById(R.id.mergeLayout);
        mMergeLayout.setGravity(Gravity.BOTTOM);
        mMergeLayout.setPadding(0, 0, 0, 20);

        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.GONE);

        expectedValue = findViewById(R.id.ed_expectedValue);
        remarksEdit = findViewById(R.id.ed_remarks);

        expectedValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //No data
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //No data
            }

            @Override
            public void afterTextChanged(Editable s) {

                expValue = expectedValue.getText().toString();
                if (expValue.isEmpty()) return;
                String str2 = mValidateDecimal(expValue, totalLength);
                if (!str2.equals(expValue)) {
                    expectedValue.setText(str2);
                    int position = expectedValue.getText().length();
                    expectedValue.setSelection(position);
                }
            }
        });


        TextView txtviewsummary = findViewById(R.id.txt_view_summary);

        tvtotalinvoice = findViewById(R.id.tv_totalinvoice);
        tvtotaloverdue = findViewById(R.id.tv_totaloverdue);
        mCustName = findViewById(R.id.txt_biz_desc);


        GradientDrawable gd = new GradientDrawable();
        gd.setCornerRadius(2.0f);
        gd.setStroke(1, ContextCompat.getColor(this, R.color.new_black_light));
        txtviewsummary.setBackground(gd);


        toolbar(IntentConstants.PAYMENTCOMMIT);

        txtviewsummary.setOnClickListener(view -> {
            if (!arrayList.isEmpty()) {
                Bundle bundle1 = new Bundle();
                bundle1.putString(GlobalConstants.WEEK_VALUE, week);
                LaunchIntentManager.routeToActivityStackBundle(this, InvoiceListActivity.class, bundle1);
            } else {
                noRecords(context, GlobalConstants.NO_RECORDS);
            }
        });


    }

    /*Call API*/
    private void callPaymentApi(String weekValue) {
        uiTypes = GlobalConstants.ADD_TYP_INV_DUE;
        commonUtils.showProgressDialog();
        PaymentDueManager.getPaymentDueServiceCall(userID, weekValue, context, this);
    }

    /*On Click Function*/
    public void mPayCommit(View view) {
        calLPaymentScreen(view.getContext());

    }

    /*CalCulation And Validation Part*/
    private void calLPaymentScreen(Context context) {
        expValue = expectedValue.getText().toString();
        Double mExpectedValue = doubleValidate(expectedValue.getText().toString().trim());
        if (expValue.isEmpty()) {
            shortToast(context, GlobalConstants.EDT_EXPECTED_VALUE);
        } else if (mExpectedValue != 0.0d) {
            if (Double.parseDouble(expValue) <= Double.parseDouble(totalDue)) {
                char[] chars = expValue.toCharArray();
                char lastCharacter = chars[chars.length - 1];
                String s = String.valueOf(lastCharacter);
                if (s.contains(".")) {
                    shortToast(context, GlobalConstants.VALID_AMOUNT);
                } else {

                    callRetryAPI(userID, context);

                }

            } else {
                shortToast(context, GlobalConstants.EXPECTED_VALUE_VAL);
            }
        } else {
            shortToast(context, GlobalConstants.EXPECTED_ZERO_VALUE);
        }
    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);
        if (errorCheck) {
            internetCheckFalse();
            if (bundle != null) {
                callPaymentApi(week);
            } else {
                callPaymentApi(GlobalConstants.NULL_DATA);
            }
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(context);
        }
    }

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        if (uiTypes.equals(GlobalConstants.ADD_TYP_INV_DUE)) {
            paymentApi(successObject);
        }
    }

    @SuppressLint("SetTextI18n")
    private void paymentApi(Object successObject) {
        PaymentDueResponse paymentDueResponse = (PaymentDueResponse) successObject;
        if (paymentDueResponse != null) {
            switch (paymentDueResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    arrayList = new ArrayList<>(paymentDueResponse.getData().getDetail());
                    tvtotalinvoice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalInvoiceValue()));
                    tvtotaloverdue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                            isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalOverDue()));
                    mCustName.setText(paymentDueResponse.getData().getHeader().get(0).getCustomerName());
                    dotRemovedFunction();
                    break;
                case IntentConstants.FAILURE_URL:
                    arrayList = new ArrayList<>(paymentDueResponse.getData().getDetail());
                    ArrayList<PaymentDueHeader> paymentDueHeaders = new ArrayList<>(paymentDueResponse.getData().getHeader());
                    if (!paymentDueHeaders.isEmpty() && arrayList.isEmpty()) {
                        tvtotalinvoice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                                isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalInvoiceValue()));
                        tvtotaloverdue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE +
                                isValidNumberFormat().format(paymentDueResponse.getData().getHeader().get(0).getTotalOverDue()));
                        mCustName.setText(paymentDueResponse.getData().getHeader().get(0).getCustomerName());
                        dotRemovedFunction();
                    } else if (paymentDueHeaders.isEmpty() && arrayList.isEmpty()) {
                        noRecords(context, GlobalConstants.NO_RECORDS);
                    }

                    break;
                default:
                    noRecords(context, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        if (uiTypes.equals(GlobalConstants.ADD_TYP_INV_DUE)) {
            noRecords(context, GlobalConstants.SERVER_ERR);
        }

    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        if (uiTypes.equals(GlobalConstants.ADD_TYP_INV_DUE)) {
            LaunchIntentManager.routeToActivityStack(context, InternetErrorCheck.class);
        }
    }

    /*Dot Removed*/
    private void dotRemovedFunction() {
        totalDue = tvtotaloverdue.getText().toString().replace("₹ ", "");
        totalDue = totalDue.replace(",", "");
        if (tvtotaloverdue.getText().toString().contains(".")) {
            Double aDouble = Double.parseDouble(totalDue);
            String toString = Double.toString(aDouble);
            String fromString = toString.substring(0, toString.indexOf('.'));
            totalLength = fromString.length();
        } else {
            totalLength = totalDue.length();
        }

    }

    private void callRetryAPI(String userId, Context context) {
        commonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(userId, context, this);
    }

    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        RetryResponse retryResponse = (RetryResponse) successObject;
        if (retryResponse != null) {
            switch (retryResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String transactionId = retryResponse.getData().getPendOrd().get(0).getTransactionID();
                    /*Kamesh*/
                    String referenceId = retryResponse.getData().getPendOrd().get(0).getReferenceNo();
                    if (!transactionId.isEmpty() || !referenceId.isEmpty()) {
                        showPopup();
                    } else {
                        Bundle bundle2 = new Bundle();
                        bundle2.putString(IntentConstants.REMARKS, remarksEdit.getText().toString());
                        bundle2.putString(IntentConstants.AMOUNT, expValue);
                        bundle2.putString(IntentConstants.TAG, IntentConstants.PAYMENTDUE);
                        LaunchIntentManager.routeToActivityStackBundle(context, PaymentScreen.class, bundle2);
                    }
                    /*Kamesh*/
                    break;
                case IntentConstants.FAILURE_URL:
                    Bundle bundle2 = new Bundle();
                    bundle2.putString(IntentConstants.REMARKS, remarksEdit.getText().toString());
                    bundle2.putString(IntentConstants.AMOUNT, expValue);
                    bundle2.putString(IntentConstants.TAG, IntentConstants.PAYMENTDUE);
                    LaunchIntentManager.routeToActivityStackBundle(context, PaymentScreen.class, bundle2);
                    break;
                default:
                    shortToast(context, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(context, GlobalConstants.SERVER_ERROR);
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(context, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        shortToast(context, GlobalConstants.INTERNET_CHECK);
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void showPopup() {
        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(context.getString(R.string.alert));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        layout.setVisibility(View.GONE);
        textOk.setVisibility(View.VISIBLE);
        alertContent.setText("Previous order is pending...!");


        textOk.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(context, DashboardActivity.class);
        });


        mDialog.show();
    }
}
