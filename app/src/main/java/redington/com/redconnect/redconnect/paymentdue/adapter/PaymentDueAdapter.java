package redington.com.redconnect.redconnect.paymentdue.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.paymentdue.activity.PaymentCommitmentDue;
import redington.com.redconnect.redconnect.shipmenttracking.model.ShipmentTrackingBean;
import redington.com.redconnect.util.LaunchIntentManager;

public class PaymentDueAdapter extends RecyclerView.Adapter<PaymentDueAdapter.PaymentHolder> {
    private Context mContext;
    private List<ShipmentTrackingBean> trackingBeanList;
    private CommonMethod commonMethod;

    public PaymentDueAdapter(Context context, List<ShipmentTrackingBean> shipmentTrackingBeans) {
        this.mContext = context;
        this.trackingBeanList = shipmentTrackingBeans;
        commonMethod = new CommonMethod(context);
    }

    @NonNull
    @Override
    public PaymentDueAdapter.PaymentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_payment_due, parent, false);
        return new PaymentDueAdapter.PaymentHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PaymentDueAdapter.PaymentHolder holder, int position) {
        holder.mTextWeekName.setText(trackingBeanList.get(position).getOrderNum());
        double v = Double.parseDouble(trackingBeanList.get(position).getInvoiceNum());
        holder.mTextWeekValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + BaseActivity.isValidNumberFormat().format(v));
        final int pos = holder.getAdapterPosition();
        holder.mTotalLayout.setOnClickListener(v1 -> {
            if (commonMethod.listAccess()) {
                Bundle bundle = new Bundle();
                bundle.putString(IntentConstants.WEEK, String.valueOf(pos + 1));
                bundle.putString(IntentConstants.WEEKVALUE, trackingBeanList.get(pos).getInvoiceNum());
                bundle.putString(IntentConstants.WEEKNAME, trackingBeanList.get(pos).getOrderNum());
                LaunchIntentManager.routeToActivityStackBundle(mContext, PaymentCommitmentDue.class, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trackingBeanList.size();
    }

    class PaymentHolder extends RecyclerView.ViewHolder {
        private TextView mTextWeekName;
        private TextView mTextWeekValue;
        private LinearLayout mTotalLayout;

        private PaymentHolder(final View view) {
            super(view);
            mContext = view.getContext();

            mTotalLayout = view.findViewById(R.id.totallayout);
            mTextWeekName = view.findViewById(R.id.text_week_name);
            mTextWeekValue = view.findViewById(R.id.text_week_value);
        }
    }
}
