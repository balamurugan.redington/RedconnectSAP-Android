package redington.com.redconnect.redconnect.login.helper;


import android.content.Context;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.login.model.PartnerRegistrationJsonResponse;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.restapiclient.service.HeaderRequest;
import redington.com.redconnect.restapiclient.service.RestClient;
import redington.com.redconnect.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PartnerRegisterService {

    private PartnerRegisterService() {
        throw new IllegalStateException(IntentConstants.CUSTOMERPROFILE);
    }

    public static void partnerRegisterServiceCall(List<String> overallValues,
                                                  String[] radioValues,
                                                  Context mContext,
                                                  final UIListener listener) {
        if (CommonUtils.isNetworkAvailable(mContext)) {
            JSONObject headerRequest = HeaderRequest.getHeaderRequest(true, GlobalConstants.getJsonHeaders());
            RestClient.getInstance(headerRequest).getPartnerRegistration(getRequestBody(overallValues, radioValues))
                    .enqueue(new Callback<PartnerRegistrationJsonResponse>() {
                        @Override
                        public void onResponse(Call<PartnerRegistrationJsonResponse> call, Response<PartnerRegistrationJsonResponse> response) {
                            listener.onSuccess(response.body());
                        }

                        @Override
                        public void onFailure(Call<PartnerRegistrationJsonResponse> call, Throwable t) {
                            listener.onFailure(t.toString());
                        }
                    });
        } else {
            listener.onError();
        }
    }

    private static RequestBody getRequestBody(List<String> overall, String[] radioValues) {

        RequestBody body = null;
        JSONObject jsonValues = new JSONObject();
        try {
            jsonValues.put(JsonKeyConstants.CUSTOMER_NAME, overall.get(0));
            jsonValues.put(JsonKeyConstants.CUSTOMER_ADDR1, overall.get(1));
            jsonValues.put(JsonKeyConstants.CUSTOMER_ADDR2, overall.get(2));
            jsonValues.put(JsonKeyConstants.CUSTOMER_ADDR3, overall.get(3));
            jsonValues.put(JsonKeyConstants.CUSTOMER_CITY, overall.get(4));
            jsonValues.put(JsonKeyConstants.CUSTOMER_STATE, overall.get(5));
            jsonValues.put(JsonKeyConstants.PART_BRANCH, overall.get(6));
            jsonValues.put(JsonKeyConstants.PINCODE, overall.get(7));
            jsonValues.put(JsonKeyConstants.CONTACT_PERSON, overall.get(8));
            jsonValues.put(JsonKeyConstants.EMAIL_ID, overall.get(9));
            jsonValues.put(JsonKeyConstants.PHONE_NO, overall.get(10));
            jsonValues.put(JsonKeyConstants.FAX_NO1, overall.get(11));
            jsonValues.put(JsonKeyConstants.PANNO, overall.get(12));
            jsonValues.put(JsonKeyConstants.LSTNUMBER, overall.get(13));
            jsonValues.put(JsonKeyConstants.TYPEOF_CONCERN, overall.get(14));

            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR1_LN1, overall.get(15));
            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR1_LN2, overall.get(16));
            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR1_LN3, overall.get(17));
            jsonValues.put(JsonKeyConstants.RESIDENCE_CITY1, overall.get(18));
            jsonValues.put(JsonKeyConstants.RESIDENCE_STATE1, overall.get(19));
            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR2_LN1, overall.get(20));
            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR2_LN2, overall.get(21));
            jsonValues.put(JsonKeyConstants.RESIDENCE_ADDR2_LN3, overall.get(22));
            jsonValues.put(JsonKeyConstants.RESIDENCE_CITY2, overall.get(23));
            jsonValues.put(JsonKeyConstants.PART_RESIDENCE_STATE2, overall.get(24));
            jsonValues.put(JsonKeyConstants.PART_STATUS_FLAG, overall.get(25));
            jsonValues.put(JsonKeyConstants.PART_REQ_USER, overall.get(26));
            jsonValues.put(JsonKeyConstants.PART_FORM_REMARKS, overall.get(27));
            jsonValues.put(JsonKeyConstants.PART_CREDITLIMIT, GlobalConstants.NULL_DATA);
            jsonValues.put(JsonKeyConstants.PART_CUST_PROFILE, GlobalConstants.NULL_DATA);
            jsonValues.put(JsonKeyConstants.PART_CUST_DIV, GlobalConstants.NULL_DATA);
            for (int i = 0; i < radioValues.length; i++) {
                String key = JsonKeyConstants.PART_FORM + Integer.toString(i + 1) + JsonKeyConstants.PART_AVAILABLE;
                jsonValues.put(key, radioValues[i]);
            }

            body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonValues.toString());
        } catch (Exception e) {
            BaseActivity.logd(e.getMessage());
        }
        return body;
    }
}
