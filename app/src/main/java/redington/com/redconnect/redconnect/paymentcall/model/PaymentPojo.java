package redington.com.redconnect.redconnect.paymentcall.model;

import java.io.Serializable;

public class PaymentPojo implements Serializable {
    private String mDate;
    private String mAmount;
    private String mTransactionID;
    private String mStatus;
    private String mRemarks;
    private String mAmountReceived;
    private String mTransactionRecieved;

    private String mReferenceNum;
    private String mUserID;
    private String mDeviceID;
    private String mUID;


    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmAmount() {
        return mAmount;
    }

    public void setmAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public String getmTransactionID() {
        return mTransactionID;
    }

    public void setmTransactionID(String mTransactionID) {
        this.mTransactionID = mTransactionID;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmRemarks() {
        return mRemarks;
    }

    public void setmRemarks(String mRemarks) {
        this.mRemarks = mRemarks;
    }

    public String getmAmountReceived() {
        return mAmountReceived;
    }

    public void setmAmountReceived(String mAmountReceived) {
        this.mAmountReceived = mAmountReceived;
    }

    public String getmTransactionRecieved() {
        return mTransactionRecieved;
    }

    public void setmTransactionRecieved(String mTransactionRecieved) {
        this.mTransactionRecieved = mTransactionRecieved;
    }

    public String getmReferenceNum() {
        return mReferenceNum;
    }

    public void setmReferenceNum(String mReferenceNum) {
        this.mReferenceNum = mReferenceNum;
    }

    public String getmUserID() {
        return mUserID;
    }

    public void setmUserID(String mUserID) {
        this.mUserID = mUserID;
    }

    public String getmDeviceID() {
        return mDeviceID;
    }

    public void setmDeviceID(String mDeviceID) {
        this.mDeviceID = mDeviceID;
    }

    public String getmUID() {
        return mUID;
    }

    public void setmUID(String mUID) {
        this.mUID = mUID;
    }
}
