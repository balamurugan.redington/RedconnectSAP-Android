package redington.com.redconnect.redconnect.search.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SearchData implements Serializable {

    @SerializedName("Biz_Code")
    private String mBizCode;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Item_Code")
    private String mItemCode;
    @SerializedName("Minor_Code")
    private String mMinorCode;
    @SerializedName("Vendor_Code")
    private String mVendorCode;

    public String getBizCode() {
        return mBizCode;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public String getMinorCode() {
        return mMinorCode;
    }

    public String getVendorCode() {
        return mVendorCode;
    }


}
