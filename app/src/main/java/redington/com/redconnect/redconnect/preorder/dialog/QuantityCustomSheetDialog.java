package redington.com.redconnect.redconnect.preorder.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Objects;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.common.methods.CommonMethod;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.preorder.listener.MyDialogCloseListener;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.KeyboardUtil;


public class QuantityCustomSheetDialog extends BottomSheetDialogFragment implements CartListener {


    private int quantity;
    private CommonUtils commonUtils;
    private Context mContext;
    private String getQuantity;
    private String itemCode;
    private int cartSize;

    private boolean apiCall = false;

    public static QuantityCustomSheetDialog getInstance() {

        return new QuantityCustomSheetDialog();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.quantity_bottom_sheet, viewGroup);

        new KeyboardUtil(Objects.requireNonNull(getActivity()), v);

        //mContext = v.getContext();
        mContext = getActivity();

        final String itemDesc = getArguments().getString(GlobalConstants.PRODUCT_NAME);
        itemCode = getArguments().getString(GlobalConstants.ITEM_CODE);
        final String vendorCode = getArguments().getString(GlobalConstants.VENDOR_CODE);
        String price = getArguments().getString(GlobalConstants.PRODUCT_PRICE);
        assert price != null;
        final double unitPrice = Double.parseDouble(price.trim());


        commonUtils = new CommonUtils(getActivity());
        final Button quantityButton = v.findViewById(R.id.btn_quantity);
        final EditText mEditText = v.findViewById(R.id.ed_quantity);


        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // No Data
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (mEditText.getText().toString().matches("^0")) {
                    mEditText.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //No data
            }
        });
        quantityButton.setOnClickListener(v1 -> {
            getQuantity = mEditText.getText().toString();

            switch (getQuantity) {
                case "0":
                    BaseActivity.shortToast(getActivity(), GlobalConstants.ZERO_QTY);
                    break;
                case "":
                    BaseActivity.shortToast(getActivity(), GlobalConstants.EMPTY_QTY);
                    break;
                default:
                    String userID = GlobalConstants.GetSharedValues.getSpUserId();
                    quantity = Integer.parseInt(getQuantity);
                    insertCart(userID, itemCode, itemDesc, vendorCode, quantity, unitPrice);
                    break;
            }
        });

        return v;

    }


    /*Insert Cart API*/
    private void insertCart(String userID, String itemCode, String itemDesc, String vendorCode, int getQuantity, Double unitPrice) {
        ArrayList<String> cartArray = new ArrayList<>();
        cartArray.add(userID);
        cartArray.add(itemCode);
        cartArray.add(itemDesc);
        cartArray.add(vendorCode);
        cartArray.add(String.valueOf(getQuantity));
        cartArray.add(String.valueOf(unitPrice));
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.MODE_INSERT);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArray.add(GlobalConstants.GetSharedValues.getCashDiscount());
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArray, mContext, this);
    }


    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    dismiss();
                    apiCall = true;
                    /*9-1-19*/
                    /*ArrayList<CartData> cartArrayList = new ArrayList<>();
                    for (int i = 0; i < cartJson.getData().size(); i++) {
                        cartArrayList.addAll(cartJson.getData().get(i).getCart());
                    }*/
                    ArrayList<CartData> cartArrayList = new ArrayList<>(cartJson.getData().get(0).getCart());
                    cartSize = cartArrayList.size();
                    qtyValidation(cartArrayList);

                    break;
                case IntentConstants.FAILURE_URL:
                    BaseActivity.shortToast(mContext, GlobalConstants.CART_NOT_ADDED);
                    break;
                default:
                    BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    private void qtyValidation(ArrayList<CartData> cartData) {
        if (!TextUtils.isEmpty(itemCode)) {
            boolean itemCodeCheck = true;
            for (int i = 0; i < cartData.size(); i++) {
                if (cartData.get(i).getItemCode().contains(itemCode)) {
                    int avaliableQty = cartData.get(i).getmAvailableQuantity();
                    int userQty = cartData.get(i).getQuantity();
                    /*itemCodeCheck = avaliableQty >= userQty;*/
                    itemCodeCheck = (CommonMethod.getValidInt(getQuantity) <= avaliableQty)
                            && (CommonMethod.getValidInt(getQuantity) == userQty);
                    break;
                }
            }
            if (!itemCodeCheck) {
                BaseActivity.longToast(mContext, mContext.getString(R.string.qunatityText));
            }
        }
    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        BaseActivity.shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Activity activity = getActivity();
        if (activity instanceof MyDialogCloseListener) {
            ((MyDialogCloseListener) activity).handleDialogClose(dialog);
            if (apiCall) {
                ((MyDialogCloseListener) activity).cartSize(cartSize);
            }
        }
    }


}

