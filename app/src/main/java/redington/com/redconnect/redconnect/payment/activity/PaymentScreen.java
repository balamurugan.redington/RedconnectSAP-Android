package redington.com.redconnect.redconnect.payment.activity;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Interfaces.ValueAddedServiceApiListener;
import com.payu.india.Model.CardStatus;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentDetails;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.Payu.PayuUtils;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.india.Tasks.ValueAddedServiceTask;
import com.payu.payuui.widget.MonthYearPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.constants.JsonKeyConstants;
import redington.com.redconnect.redconnect.capitalfloat.helper.CheckJbaIdManager;
import redington.com.redconnect.redconnect.capitalfloat.model.CheckJbaIdReponse;
import redington.com.redconnect.redconnect.dashboard.activity.DashboardActivity;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.CartUpdationManager;
import redington.com.redconnect.redconnect.payment.helper.ServiceTaxHelper;
import redington.com.redconnect.redconnect.payment.model.CartUpdationJsonResponse;
import redington.com.redconnect.redconnect.payment.model.ServiceTaxJsonResponse;
import redington.com.redconnect.redconnect.payment.model.ServiceTaxdata;
import redington.com.redconnect.redconnect.paymentcall.model.PaymentPojo;
import redington.com.redconnect.redconnect.preorder.helper.CustomerEligibilityManager;
import redington.com.redconnect.redconnect.preorder.model.CustomerEligibilityJsonResponse;
import redington.com.redconnect.redconnect.settings.activity.SettingsActivity;
import redington.com.redconnect.redconnect.spot.helper.PromoCodeServiceManager;
import redington.com.redconnect.redconnect.spot.model.PromoCodeData;
import redington.com.redconnect.redconnect.spot.model.PromoCodeJsonResponse;
import redington.com.redconnect.redconnect.spot.model.SubPromoCode;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.EmptyCartActivity;
import redington.com.redconnect.util.LaunchIntentManager;

public class PaymentScreen extends BaseActivity implements PaymentRelatedDetailsListener, View.OnClickListener
        , ValueAddedServiceApiListener, UIListener, NewListener, CartListener {
    protected PayuHashes mPayuHashes;
    private Context mContext;
    private CommonUtils commonUtils;
    private String currentDate;
    private String userId;
    private Button payNowButton;
    private EditText nameOnCardEditText;
    private EditText cardNumberEditText;
    private EditText cardCvvEditText;
    private EditText cardExpiryMonthEditText;
    private EditText cardExpiryYearEditText;
    private ImageView cardImage;
    private ImageView cvvImage;
    private TextView amountText;
    private ImageView mImageOpen1;
    private ImageView mImageClose1;
    private ImageView mImageOpen1Debit;
    private ImageView mImageClose1Debit;
    private ImageView mImageOpen2;
    private ImageView mImageClose2;
    private PostData mPostData;
    private PayuResponse mPayuResponse;
    private PayuResponse valueAddedResponse;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private TextView bankDownText;
    private Spinner spinnerNetbanking;
    private ArrayList<PaymentDetails> netBankingList;
    private HashMap<String, Integer> valueAddedValues;
    private String bankCode;
    private String mBankcode;
    private PayuUtils payuUtils;
    private String cvv;
    private DatePickerDialog.OnDateSetListener datePickerListener;
    private String issuer;
    private HashMap<String, CardStatus> valueAddedHashMap;
    private TextView issuingBankDown;
    private TextView issuingBankDownDebit;
    private LinearLayout mLinearLayout;
    private PayUChecksum checksum;

    private boolean isCreditcard = false;
    private boolean isDebitCard = false;
    private boolean isNetbanking = false;

    private boolean isCvvValid = false;
    private boolean isExpiryYearValid = false;
    private boolean isExpiryMonthValid = false;
    private boolean isCardNumberValid = false;
    private boolean isNameOnCard = false;

    private CardView mContentCreditCard;
    private CardView mContentDebitCard;
    private CardView mContentNetBankCard;
    private CardView mContentCreditLending;

    private String merchantKey;
    private String userCreditials;
    private String transactionId;

    private String comingFrom;
    private String mStockLocation;
    private String mPaymentDays;
    private String mDeliverySeq;
    private boolean mSpotBoolean;
    private ArrayList<SubPromoCode> subPromoCodes;
    private ArrayList<CartData> cartArrayList;
    private String mRemarks;
    private String overallAmount;
    private String status;
    private boolean debitCardVisibility = false;
    private boolean creditCardVisibility = false;
    private boolean netBankingVisibilty = false;
    private TextView serviceTaxTitle;
    private TextView serviceTaxValue;
    private TextView gstTitle;
    private TextView gstValue;
    private TextView cardAmount;
    private ArrayList<ServiceTaxdata> serviceTaxdata;
    private TextView amountGst;
    private String totalAmount = "";
    private Double firstAmount;
    private String cartStatus = "";
    private Double firstSpotAmount;
    private String spotNumber;
    private ArrayList<SubPromoCode> subPromoCodeArrayList;
    private String apiString = "";
    private boolean cartUpdateBoolean = false;
    private String capitalFloat = "";
    private String otpRefNumber = "";
    /*Kamesh*/
    private int servicePosition = -1;
    /*Kamesh*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_screen);

        mContext = PaymentScreen.this;
        commonUtils = new CommonUtils(mContext);

        Payu.setInstance(mContext);

        Date date = new Date();
        currentDate = dateFormat(date);
        userId = GlobalConstants.GetSharedValues.getSpUserId();
        amountText = findViewById(R.id.textview_amount);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            comingFrom = bundle.getString(IntentConstants.TAG);
            assert comingFrom != null;
            if (comingFrom.equals(IntentConstants.PLACEORDER)) {
                mRemarks = bundle.getString(IntentConstants.REMARKS);

                mPaymentDays = bundle.getString(IntentConstants.PAYMENTDAYS_CARD);
                mDeliverySeq = bundle.getString(IntentConstants.DELIVERYSEQUENCE_CARD);
                mSpotBoolean = bundle.getBoolean(IntentConstants.SPOTBOOLEAN);
                subPromoCodes = (ArrayList<SubPromoCode>) bundle.getSerializable(IntentConstants.SPOTARRAY);
                cartArrayList = (ArrayList<CartData>) bundle.getSerializable(IntentConstants.CARTARRAY);
                if (mSpotBoolean) {
                    mStockLocation = subPromoCodes.get(0).getmSTOCKROOM();
                    spotNumber = subPromoCodes.get(0).getPromoCode();
                    calculateSpotAmount(subPromoCodes, 1);
                } else {
                    mStockLocation = bundle.getString(IntentConstants.STOCKLOCATION);
                }

                if (cartArrayList != null && !cartArrayList.isEmpty()) {
                    calculateAmount(cartArrayList, 1);
                }
            } else if (comingFrom.equals(IntentConstants.CHEQUEPENDING) || comingFrom.equals(IntentConstants.PAYMENTDUE)) {
                mRemarks = bundle.getString(IntentConstants.REMARKS);
            }
            overallAmount = bundle.getString(IntentConstants.AMOUNT);
            amountText.setText(overallAmount);
            toolbar(IntentConstants.PAYMENTOPTIONS);

        }

        initViews();
    }

    public void menuBack(View view) {
        showEligibilityPopup(view.getContext(), IntentConstants.BACK);
    }


    private void initViews() {
        RelativeLayout mImageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);

        imageButton.setVisibility(View.GONE);
        mImageCart.setVisibility(View.GONE);

        CardView mCreditHeader = findViewById(R.id.credit_header);
        CardView mDebitHeader = findViewById(R.id.debit_header);
        CardView mNetHeader = findViewById(R.id.net_header);
        CardView mRedingtonCreditHeader = findViewById(R.id.credit_limit_header);
        CardView mAgencyHeader = findViewById(R.id.agency_header);


        float[] nullRadius = {8, 8, 8, 8, 0, 0, 0, 0};
        float[] tenRadius = {10f, 10f, 10f, 10f, 5f, 5f, 5f, 5f};

        commonUtils.setGradientColor(mCreditHeader, R.color.colorAccent_1, tenRadius);
        commonUtils.setGradientColor(mDebitHeader, R.color.colorAccent_1, tenRadius);
        commonUtils.setGradientColor(mNetHeader, R.color.colorAccent_1, nullRadius);
        commonUtils.setGradientColor(mRedingtonCreditHeader, R.color.colorAccent_1, nullRadius);
        commonUtils.setGradientColor(mAgencyHeader, R.color.colorAccent_1, nullRadius);

        mCreditHeader.setOnClickListener(this);
        mDebitHeader.setOnClickListener(this);
        mNetHeader.setOnClickListener(this);
        mAgencyHeader.setOnClickListener(this);

        if (comingFrom.equals(IntentConstants.PLACEORDER)) {
            mRedingtonCreditHeader.setVisibility(View.VISIBLE);
            mAgencyHeader.setVisibility(View.VISIBLE);
        } else {
            mRedingtonCreditHeader.setVisibility(View.GONE);
            mAgencyHeader.setVisibility(View.GONE);
        }

        mRedingtonCreditHeader.setOnClickListener(v -> checkCustomerEligibilty());
        /*Kamesh*/
        /*findViewById(R.id.capitalfloat).setOnClickListener(v -> callCapitalFloat());*/
        findViewById(R.id.capitalfloat).setOnClickListener(v -> shortToast(mContext, GlobalConstants.COMING_SOON));
        /*Kamesh*/
        findViewById(R.id.indifi).setOnClickListener(v -> shortToast(mContext, GlobalConstants.COMING_SOON));

        setInitViews();

    }

    private void callCapitalFloat() {
        commonUtils.showProgressDialog();
        capitalFloat = IntentConstants.CAPITAL_FLOAT;
        CheckJbaIdManager.checkJbaIdManager(userId, mContext, this);
    }

    private void checkCustomerEligibilty() {
        apiString = IntentConstants.OTP;
        commonUtils.showProgressDialog();
        CustomerEligibilityManager.getEligibilityCall(userId, Double.valueOf(overallAmount), GlobalConstants.ONE, mContext, this);
    }


    private void setInitViews() {

        payNowButton = findViewById(R.id.button_pay_now);
        payNowButton.setEnabled(false);
        payNowButton.setOnClickListener(v -> {

            if (CommonUtils.isNetworkAvailable(mContext)) {
                mPaymentParams.setHash(mPayuHashes.getPaymentHash());
                if (isCreditcard || isDebitCard) {
                    checkData();
                    if (mPayuHashes != null) {
                        mPaymentParams.setHash(mPayuHashes.getPaymentHash());
                        makePaymentByCard();
                    }
                } else if (isNetbanking) {
                    makePaymentByNB();
                }

                callPayment();

            } else {
                shortToast(mContext, GlobalConstants.INTERNET_CHECK);
            }
        });

        imageView();
    }

    /*Online Payment Call*/
    private void callPayment() {
        if (comingFrom.equals(IntentConstants.PLACEORDER)) {
            if (cartArrayList != null && !cartArrayList.isEmpty()) {
                cartApiCall();
            } else if (subPromoCodes != null && !subPromoCodes.isEmpty()) {
                capitalFloat = IntentConstants.SPOT;
                spotApiCall();
            }
        } else {
            callOnlinePaymentAPI();
        }
    }

    private void spotApiCall() {
        commonUtils.showProgressDialog();
        PromoCodeServiceManager.promoCodeServicecall(userId,
                GlobalConstants.GetSharedValues.getCartDelSeq(),
                "", "", "1", mContext, this);
    }

    private void imageView() {

        mImageOpen1 = findViewById(R.id.img_open1);
        mImageClose1 = findViewById(R.id.img_close1);
        mImageOpen1Debit = findViewById(R.id.img_open1_1);
        mImageClose1Debit = findViewById(R.id.img_close1_1);
        mImageOpen2 = findViewById(R.id.img_open2);
        mImageClose2 = findViewById(R.id.img_close2);


        mContentCreditCard = findViewById(R.id.card_creditCard);
        mContentDebitCard = findViewById(R.id.card_debitCard);
        mContentNetBankCard = findViewById(R.id.card_netBank);
        mContentCreditLending = findViewById(R.id.creditLendingAgencies);

        serviceCallApi();

    }

    /*Check Data*/
    private void checkData() {
        if (!cardExpiryYearEditText.getText().toString().equals("") && !cardExpiryMonthEditText.getText().toString().equals("")) {
            isExpiryYearValid = true;
            isExpiryMonthValid = true;
        }
        if (!cardCvvEditText.getText().toString().equals("") && !cardNumberEditText.getText().toString().equals("")
                && payuUtils.validateCvv(cardNumberEditText.getText().toString().replace(" ", ""), cvv)) {
            isCvvValid = true;
        }

        cardValidation();
    }

    @SuppressLint("SetTextI18n")
    private void cardValidation() {

        if (!(payuUtils.validateCardNumber(cardNumberEditText.getText().toString().replace(" ", ""))) && cardNumberEditText.length() > 0) {
            cardImage.setImageResource(R.drawable.error_icon);
            isCardNumberValid = false;
            amountText.setText(GlobalConstants.AMOUNT + ": ₹ " + overallAmount);

        } else
            isCardNumberValid = payuUtils.validateCardNumber(cardNumberEditText.getText().toString().replace(" ", "")) && cardNumberEditText.length() > 0;
        uiValidation();
    }

    @SuppressLint("NewApi")
    private void uiValidation() {
        if (Objects.equals(issuer, "SMAE")) {
            isCvvValid = true;
            isExpiryMonthValid = true;
            isExpiryYearValid = true;
            isNameOnCard = true;
        }

        if (isCardNumberValid && isCvvValid && isExpiryYearValid && isExpiryMonthValid && isNameOnCard) {
            payNowButton.setEnabled(true);

        } else {
            payNowButton.setEnabled(false);
        }
    }

    private void makePaymentByCard() {

        String cardNum = cardNumberEditText.getText().toString().replace(" ", "");
        mPaymentParams.setCardNumber(cardNum);
        mPaymentParams.setNameOnCard(nameOnCardEditText.getText().toString());
        mPaymentParams.setExpiryMonth(cardExpiryMonthEditText.getText().toString());
        mPaymentParams.setExpiryYear(cardExpiryYearEditText.getText().toString());
        mPaymentParams.setCvv(cardCvvEditText.getText().toString());
        try {
            mPostData = new PaymentPostParams(mPaymentParams, PayuConstants.CC).getPaymentPostParams();
        } catch (Exception e) {
            logd(e.getMessage());
        }
    }

    private void makePaymentByNB() {

        if (mPayuResponse != null)
            netBankingList = mPayuResponse.getNetBanks();

        if (netBankingList != null && netBankingList.get(spinnerNetbanking.getSelectedItemPosition()) != null)
            bankCode = netBankingList.get(spinnerNetbanking.getSelectedItemPosition()).getBankCode();
        mPaymentParams.setBankCode(bankCode);

        try {
            mPostData = new PaymentPostParams(mPaymentParams, PayuConstants.NB).getPaymentPostParams();
        } catch (Exception e) {
            logd(e.getMessage());
        }
    }

    /*API Call*/
    private void callOnlinePaymentAPI() {
        if (mPostData != null && mPostData.getCode() == PayuErrors.NO_ERROR) {
            payuConfig.setData(mPostData.getResult());

            Intent intent = new Intent(PaymentScreen.this, com.payu.payuui.activity.PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        } else {
            if (mPostData != null)
                shortToast(getApplicationContext(), mPostData.getResult());

        }
    }

    @Override
    public void onBackPressed() {
        showEligibilityPopup(mContext, IntentConstants.BACK);
    }

    @SuppressLint("SetTextI18n")
    private void cardValues(int getid, String type) {
        final View viewCard = findViewById(getid);
        if (!type.equals(IntentConstants.NETBANKING)) {
            nameOnCardEditText = viewCard.findViewById(R.id.edit_text_name_on_card);
            cardNumberEditText = viewCard.findViewById(R.id.edit_text_card_number);
            cardCvvEditText = viewCard.findViewById(R.id.edit_text_card_cvv);
            cardExpiryMonthEditText = viewCard.findViewById(R.id.edit_text_expiry_month);
            cardExpiryYearEditText = viewCard.findViewById(R.id.edit_text_expiry_year);
            cardImage = viewCard.findViewById(R.id.image_card_type);
            cvvImage = viewCard.findViewById(R.id.image_cvv);
            mLinearLayout = viewCard.findViewById(R.id.layout_expiry_date);
            issuingBankDown = findViewById(R.id.text_view_issuing_bank_down_error);
            issuingBankDownDebit = findViewById(R.id.text_view_issuing_bank_down_error_debit);


            cardExpiryMonthEditText.setOnClickListener(v -> {
                MonthYearPickerDialog newFragment = new MonthYearPickerDialog();
                newFragment.show(getSupportFragmentManager(), "DatePicker");
                newFragment.setListener(datePickerListener);
            });

            cardExpiryYearEditText.setOnClickListener(arg0 -> {

                MonthYearPickerDialog newFragment = new MonthYearPickerDialog();
                newFragment.show(getSupportFragmentManager(), "DatePicker");
                newFragment.setListener(datePickerListener);

            });

            datePickerListener = (view, selectedDay, selectedMonth, selectedYear) -> {
                cardExpiryYearEditText.setText("" + selectedYear);
                cardExpiryMonthEditText.setText("" + selectedMonth);


                if (!cardExpiryMonthEditText.getText().toString().equals("") && !cardExpiryYearEditText.getText().toString().equals("")) {
                    isExpiryYearValid = true;
                    isExpiryMonthValid = true;
                }
                if (selectedYear == Calendar.YEAR && selectedMonth < Calendar.MONTH) {
                    isExpiryMonthValid = false;
                }

                uiValidation();
            };


            payuUtils = new PayuUtils();

            cardNumberEditText.addTextChangedListener(new TextWatcher() {
                int image;
                int cardLength;


                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //No data
                }

                @SuppressLint("NewApi")
                @Override
                public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                    if (charSequence.length() > 6) { // to confirm rupay card we need min 6 digit.
                        if (null == issuer) {
                            issuer = payuUtils.getIssuer(charSequence.toString().replace(" ", ""));
                        }
                        if (issuer != null && issuer.length() > 1) {
                            image = getIssuerImage(issuer);
                            cardImage.setImageResource(image);

                            if (Objects.equals(issuer, "AMEX"))
                                cardCvvEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                            else
                                cardCvvEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});

                            if (Objects.equals(issuer, "SMAE") || Objects.equals(issuer, "MAES")) {
                                cardNumberEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(23)});
                                cardLength = 23;
                            } else if (Objects.equals(issuer, "AMEX")) {
                                cardNumberEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(18)});
                                cardLength = 18;
                            } else if (Objects.equals(issuer, "DINR")) {
                                cardNumberEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(17)});
                                cardLength = 17;
                            } else {
                                cardNumberEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(19)});
                                cardLength = 20;
                            }
                        }
                    } else {
                        issuer = null;
                        mLinearLayout.setVisibility(View.VISIBLE);
                        cardImage.setImageResource(R.drawable.icon_card);
                        cardCvvEditText.getText().clear();
                    }

                    if (charSequence.length() == 7) {

                        if (null != valueAddedHashMap) {
                            if (valueAddedHashMap.get(charSequence.toString().replace(" ", "")) != null) {
                                int statusCode = valueAddedHashMap.get(charSequence.toString().replace(" ", "")).getStatusCode();

                                if (statusCode == 0) {
                                    if (creditCardVisibility) {
                                        issuingBankDown.setVisibility(View.VISIBLE);
                                        issuingBankDown.setText(valueAddedHashMap.get(charSequence.toString().replace(" ", "")).getBankName() + GlobalConstants.TEMP_DOWN);
                                    } else if (debitCardVisibility) {
                                        issuingBankDownDebit.setVisibility(View.VISIBLE);
                                        issuingBankDownDebit.setText(valueAddedHashMap.get(charSequence.toString().replace(" ", "")).getBankName() + GlobalConstants.TEMP_DOWN);
                                    }
                                } else {
                                    if (creditCardVisibility) {
                                        issuingBankDown.setVisibility(View.GONE);
                                    } else if (debitCardVisibility) {
                                        issuingBankDownDebit.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                if (creditCardVisibility) {
                                    issuingBankDown.setVisibility(View.GONE);
                                } else if (debitCardVisibility) {
                                    issuingBankDownDebit.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else if (charSequence.length() < 7) {
                        if (creditCardVisibility) {
                            issuingBankDown.setVisibility(View.GONE);
                        } else if (debitCardVisibility) {
                            issuingBankDownDebit.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().replace(" ", "").length() > cardLength - (cardLength / 5) && s.toString().replace(" ", "").length() >= 6) {
                        s.delete(cardLength - (cardLength / 5), s.length());
                    }

                    // Remove all spacing char
                    int pos = 0;
                    while (true) {
                        if (pos >= s.length()) break;
                        if ((int) ' ' == (int) s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                            s.delete(pos, pos + 1);
                        } else {
                            pos++;
                        }
                    }

                    // Insert char where needed.
                    pos = 4;
                    while (true) {
                        if (pos >= s.length()) break;
                        final char c = s.charAt(pos);
                        // Only if its a digit where there should be a space we insert a space
                        if ("0123456789".indexOf(c) >= 0) {
                            s.insert(pos, "" + ' ');
                        }
                        pos += 5;
                    }

                    if (cardNumberEditText.getSelectionStart() > 0 && (int) s.charAt(cardNumberEditText.getSelectionStart() - 1) == ' ') {
                        cardNumberEditText.setSelection(cardNumberEditText.getSelectionStart() - 1);
                    }

                    if (s.length() >= cardLength - 1) {
                        cardValidation();
                    } else {
                        isCardNumberValid = false;
                        payNowButton.setEnabled(false);
                    }
                }
            });


            cardNumberEditText.setOnFocusChangeListener((view, b) -> {
                if (!b) {
                    cardValidation();
                }

            });

            cardCvvEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    charSequence.toString();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    cvv = charSequence.toString();
                    if (payuUtils.validateCvv(cardNumberEditText.getText().toString().replace(" ", ""), cvv)) {
                        cvvImage.setAlpha((float) 1);
                        isCvvValid = true;
                        uiValidation();
                    } else {
                        cvvImage.setAlpha(0.5f);
                        isCvvValid = false;
                        uiValidation();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //Not in use
                }

            });

            nameOnCardEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //Not in use
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    isNameOnCard = s.length() > 0;
                    uiValidation();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    //Not in use
                }
            });
        }


        setValues(viewCard, type);


    }

    @SuppressLint("SetTextI18n")
    private void setValues(View viewCard, String type) {
        TextView taxTitle = viewCard.findViewById(R.id.tax_title);
        if (creditCardVisibility) {
            taxTitle.setText(this.getString(R.string.taxText) + IntentConstants.CREDITCARD_PAYMNENT);
        } else if (debitCardVisibility) {
            taxTitle.setText(this.getString(R.string.taxText) + IntentConstants.DEBITCARD_PAYMNENT);
        } else if (netBankingVisibilty) {
            taxTitle.setText(this.getString(R.string.taxText) + IntentConstants.NETBANKING_PAYMNENT);
        }
        amountGst = viewCard.findViewById(R.id.amountGst);
        serviceTaxTitle = viewCard.findViewById(R.id.text_servicetax);
        serviceTaxValue = viewCard.findViewById(R.id.service_tax);
        gstTitle = viewCard.findViewById(R.id.textgst);
        gstValue = viewCard.findViewById(R.id.gst);
        cardAmount = viewCard.findViewById(R.id.totalAmount);
        int position = -1;
        for (int i = 0; i < serviceTaxdata.size(); i++) {
            if (type.equals(serviceTaxdata.get(i).getType())) {
                position = i;
                servicePosition = i;
            }
        }
        if (position != -1) {
            gstData(position);

        }

        hashKeyGeneration();
    }

    private int getIssuerImage(String issuer) {

        switch (issuer) {
            case PayuConstants.VISA:
                return R.drawable.logo_visa;
            case PayuConstants.LASER:
                return R.drawable.laser;
            case PayuConstants.DISCOVER:
                return R.drawable.discover;
            case PayuConstants.MAES:
                return R.drawable.mas_icon;
            case PayuConstants.MAST:
                return R.drawable.mc_icon;
            case PayuConstants.AMEX:
                return R.drawable.amex;
            case PayuConstants.DINR:
                return R.drawable.diner;
            case PayuConstants.JCB:
                return R.drawable.jcb;
            case PayuConstants.SMAE:
                return R.drawable.maestro;
            case PayuConstants.RUPAY:
                return R.drawable.rupay;
            default:
                // Not in use
                break;
        }
        return 0;
    }


    private void netBankingView(PayuResponse mPayuResponse, PayuResponse valueResponse) {
        findViewById(R.id.image_button_axis).setOnClickListener(this);
        findViewById(R.id.image_button_hdfc).setOnClickListener(this);
        findViewById(R.id.image_button_citi).setOnClickListener(this);
        findViewById(R.id.image_button_sbi).setOnClickListener(this);
        findViewById(R.id.image_button_icici).setOnClickListener(this);

        bankDownText = findViewById(R.id.text_view_bank_down_error);
        spinnerNetbanking = findViewById(R.id.spinner);
        mPostData = new PostData();

        if (netBankingVisibilty) {
            payNowButton.setEnabled(true);
        }

        netBankingList = mPayuResponse.getNetBanks();
        valueAddedValues = valueResponse.getNetBankingDownStatus();

        netBankList();

    }

    private void netBankList() {
        if (netBankingList != null) {
            List<String> spinnerArray = new ArrayList<>();
            for (int i = 0; i < netBankingList.size(); i++) {
                spinnerArray.add(netBankingList.get(i).getBankName());
                String s = netBankingList.get(i).getBankCode();

                checkCase(s);
            }

            ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
            mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerNetbanking.setAdapter(mAdapter);
            spinnerNetbanking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
                    mBankcode = netBankingList.get(index).getBankCode();
                    if (valueAddedValues != null && valueAddedValues.get(netBankingList.get(index).getBankCode()) != null && getApplicationContext() != null) {

                        int statusCode = valueAddedValues.get(mBankcode);
                        if (statusCode == 0) {
                            bankDownText.setVisibility(View.VISIBLE);
                            bankDownText.setText(netBankingList.get(index).getBankName() + GlobalConstants.TEMP_DOWN);
                        } else {
                            bankDownText.setVisibility(View.GONE);
                        }

                    } else {
                        bankDownText.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    //
                }
            });

        } else {
            shortToast(getApplicationContext(), "Could not get netbanking list data.");
        }
    }

    private void checkCase(String s) {
        switch (s) {
            case "AXIB":
                findViewById(R.id.layout_axis).setVisibility(View.VISIBLE);
                break;
            case "HDFB":
                findViewById(R.id.layout_hdfc).setVisibility(View.VISIBLE);
                break;
            case "SBIB":
                findViewById(R.id.layout_sbi).setVisibility(View.VISIBLE);
                break;
            case "ICIB":
                findViewById(R.id.layout_icici).setVisibility(View.VISIBLE);
                break;
            default:
                //Not in use
                break;
        }
    }


    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;

        if (valueAddedResponse != null)
            setupResponse(mPayuResponse, valueAddedResponse);

        MerchantWebService valueAddedWebService = new MerchantWebService();
        valueAddedWebService.setKey(mPaymentParams.getKey());
        valueAddedWebService.setCommand(PayuConstants.VAS_FOR_MOBILE_SDK);
        valueAddedWebService.setHash(mPayuHashes.getVasForMobileSdkHash());
        valueAddedWebService.setVar1(PayuConstants.DEFAULT);
        valueAddedWebService.setVar2(PayuConstants.DEFAULT);
        valueAddedWebService.setVar3(PayuConstants.DEFAULT);

        if ((mPostData = new MerchantWebServicePostParams(valueAddedWebService).getMerchantWebServicePostParams()) != null && mPostData.getCode() == PayuErrors.NO_ERROR) {
            payuConfig.setData(mPostData.getResult());
            ValueAddedServiceTask valueAddedServiceTask = new ValueAddedServiceTask(this);
            valueAddedServiceTask.execute(payuConfig);
        } else {
            if (mPostData != null)
                shortToast(this, mPostData.getResult());
        }
    }


    @Override
    public void onValueAddedServiceApiResponse(PayuResponse payuResponse) {
        valueAddedResponse = payuResponse;

        if (mPayuResponse != null) {
            if (mPayuResponse.isCreditCardAvailable() && mPayuResponse.isDebitCardAvailable()) {
                payNowButton.setEnabled(false);
            } else {
                payNowButton.setEnabled(true);
            }

            setupResponse(mPayuResponse, valueAddedResponse);
        }
    }

    private void setupResponse(PayuResponse mPayuResponse, PayuResponse valueAddedResponse) {
        if (mPayuResponse.isResponseAvailable() && mPayuResponse.getResponseStatus().getCode() == PayuErrors.NO_ERROR) {

            netBankingView(mPayuResponse, valueAddedResponse);

        } else {
            shortToast(this, "Something went wrong : " + mPayuResponse.getResponseStatus().getResult());
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.image_button_axis) {
            String mBankCode = "AXIB";
            makePaymentNetBank(mBankCode);

        } else if (i == R.id.image_button_hdfc) {
            String mBankCode;
            mBankCode = "HDFB";
            makePaymentNetBank(mBankCode);

        } else if (i == R.id.image_button_citi) {
            String mBankCode;
            mBankCode = "";
            makePaymentNetBank(mBankCode);
        } else if (i == R.id.image_button_sbi) {
            String mBankCode;
            mBankCode = "SBIB";
            makePaymentNetBank(mBankCode);

        } else if (i == R.id.image_button_icici) {
            String mBankCode;
            mBankCode = "ICIB";
            makePaymentNetBank(mBankCode);

        } else if (i == R.id.credit_header) {

            creditCardClick();

        } else if (i == R.id.debit_header) {

            debitCardClick();

        } else if (i == R.id.net_header) {

            netbankingClick();

        } else if (i == R.id.agency_header) {
            payNowButton.setEnabled(false);
            lendingPartnersClick();

        }
    }


    /*Net Banking Click*/
    private void netbankingClick() {

        if (mContentNetBankCard.getVisibility() == View.GONE) {

            creditCardVisibility = false;
            debitCardVisibility = false;

            mContentNetBankCard.setVisibility(View.VISIBLE);
            mImageOpen2.setVisibility(View.GONE);
            mImageClose2.setVisibility(View.VISIBLE);


            mImageOpen1.setVisibility(View.VISIBLE);
            mImageClose1.setVisibility(View.GONE);

            mImageOpen1Debit.setVisibility(View.VISIBLE);
            mImageClose1Debit.setVisibility(View.GONE);


            mContentDebitCard.setVisibility(View.GONE);
            mContentCreditCard.setVisibility(View.GONE);
            mContentCreditLending.setVisibility(View.GONE);

            isNetbanking = true;
            isCreditcard = false;
            isDebitCard = false;

            netBankingVisibilty = true;

            cardValues(R.id.content_netbank, IntentConstants.NETBANKING);


        } else if (mContentNetBankCard.getVisibility() == View.VISIBLE) {
            mContentNetBankCard.setVisibility(View.GONE);

            imageDisable();


            isNetbanking = false;
            payNowButton.setEnabled(false);
            netBankingVisibilty = false;
        }


    }

    /*Debit Card Button Click*/
    private void debitCardClick() {
        if (mContentDebitCard.getVisibility() == View.GONE) {
            creditCardVisibility = false;
            debitCardVisibility = true;
            netBankingVisibilty = false;

            mContentDebitCard.setVisibility(View.VISIBLE);
            mImageOpen1Debit.setVisibility(View.GONE);
            mImageClose1Debit.setVisibility(View.VISIBLE);
            mImageOpen1.setVisibility(View.VISIBLE);
            mImageClose1.setVisibility(View.GONE);
            mImageOpen2.setVisibility(View.VISIBLE);
            mImageClose2.setVisibility(View.GONE);


            mContentNetBankCard.setVisibility(View.GONE);
            mContentCreditCard.setVisibility(View.GONE);
            mContentCreditLending.setVisibility(View.GONE);

            isDebitCard = true;
            isCreditcard = false;
            isNetbanking = false;

            cardValues(R.id.content_debit, IntentConstants.DEBITCARD);
            clearData();
        } else if (mContentDebitCard.getVisibility() == View.VISIBLE) {
            mContentDebitCard.setVisibility(View.GONE);

            imageDisable();

            isDebitCard = false;

            debitCardVisibility = false;

        }
    }

    /*Credit Card Button Click*/
    private void creditCardClick() {

        if (mContentCreditCard.getVisibility() == View.GONE) {
            //Hide Other views
            netBankingVisibilty = false;
            debitCardVisibility = false;
            creditCardVisibility = true;

            //
            mContentCreditCard.setVisibility(View.VISIBLE);
            mImageOpen1.setVisibility(View.GONE);
            mImageClose1.setVisibility(View.VISIBLE);

            mImageOpen1Debit.setVisibility(View.VISIBLE);
            mImageClose1Debit.setVisibility(View.GONE);

            mImageOpen2.setVisibility(View.VISIBLE);
            mImageClose2.setVisibility(View.GONE);

            mContentNetBankCard.setVisibility(View.GONE);
            mContentDebitCard.setVisibility(View.GONE);
            mContentCreditLending.setVisibility(View.GONE);

            isCreditcard = true;
            isDebitCard = false;
            isNetbanking = false;

            cardValues(R.id.content_credit, IntentConstants.CREDITCARD);
            clearData();
        } else if (mContentCreditCard.getVisibility() == View.VISIBLE) {

            mContentCreditCard.setVisibility(View.GONE);


            isCreditcard = false;
            creditCardVisibility = false;

            imageDisable();
        }
    }

    /*Credit Lending Partners*/
    private void lendingPartnersClick() {
        if (mContentCreditLending.getVisibility() == View.GONE) {
            mContentCreditLending.setVisibility(View.VISIBLE);

            mContentCreditCard.setVisibility(View.GONE);
            mContentDebitCard.setVisibility(View.GONE);
            mContentNetBankCard.setVisibility(View.GONE);

            netBankingVisibilty = false;
            debitCardVisibility = false;
            creditCardVisibility = false;

            isNetbanking = false;
            isCreditcard = false;
            isDebitCard = false;
            imageDisable();
        } else {
            mContentCreditLending.setVisibility(View.GONE);
            imageDisable();
        }
    }

    private void imageDisable() {
        mImageOpen1.setVisibility(View.VISIBLE);
        mImageClose1.setVisibility(View.GONE);

        mImageOpen1Debit.setVisibility(View.VISIBLE);
        mImageClose1Debit.setVisibility(View.GONE);

        mImageOpen2.setVisibility(View.VISIBLE);
        mImageClose2.setVisibility(View.GONE);
    }


    private void clearData() {

        cardNumberEditText.setText("");
        isCardNumberValid = false;
        cardExpiryMonthEditText.setText("");
        cardExpiryYearEditText.setText("");
        isExpiryMonthValid = false;
        isExpiryYearValid = false;
        cardCvvEditText.setText("");
        isCvvValid = false;
        nameOnCardEditText.setText("");
        isNameOnCard = false;
        uiValidation();

    }


    /*Make payment by Net Banking*/
    private void makePaymentNetBank(String mBankCode) {
        mPaymentParams.setHash(mPayuHashes.getPaymentHash());
        mPaymentParams.setBankCode(mBankCode);
        try {
            mPostData = new PaymentPostParams(mPaymentParams, PayuConstants.NB).getPaymentPostParams();

            callOnlinePaymentAPI();

        } catch (Exception e) {
            logd(e.getMessage());
        }
    }

    private void hashKeyGeneration() {

        merchantKey = GlobalConstants.MERCHANT_KEY;

        userCreditials = merchantKey + ":" + GlobalConstants.GetSharedValues.getSpMail();
        transactionId = Long.toString(System.currentTimeMillis());
        PaymentParams params = new PaymentParams();
        params.setKey(merchantKey);
        if (creditCardVisibility) {
            totalAmount = cardAmount.getText().toString().replace(IntentConstants.PAYMENTAMOUNT, "");
            totalAmount = totalAmount.replace("₹ ", "");
            params.setAmount(totalAmount);
        }
        if (debitCardVisibility) {
            totalAmount = cardAmount.getText().toString().replace(IntentConstants.PAYMENTAMOUNT, "");
            totalAmount = totalAmount.replace("₹ ", "");
            params.setAmount(totalAmount);
        }
        if (netBankingVisibilty) {
            totalAmount = cardAmount.getText().toString().replace(IntentConstants.PAYMENTAMOUNT, "");
            totalAmount = totalAmount.replace("₹ ", "");
            params.setAmount(totalAmount);
        }

        params.setProductInfo(GlobalConstants.APP_NAME);
        params.setFirstName(GlobalConstants.GetSharedValues.getSpUserName());
        params.setEmail(GlobalConstants.GetSharedValues.getSpMail());
        params.setTxnId(transactionId);
        params.setSurl(GlobalConstants.URL);
        params.setFurl(GlobalConstants.URL);
        params.setUdf1(GlobalConstants.UDF1);
        params.setUdf2(GlobalConstants.UDF2);
        params.setUdf3(GlobalConstants.UDF3);
        params.setUdf4(GlobalConstants.UDF4);
        params.setUdf5(GlobalConstants.UDF5);
        params.setUserCredentials(userCreditials);

        payuConfig = new PayuConfig();

        /*-- Payment Environment Change (Test / Production) --*/
        payuConfig.setEnvironment(PayuConstants.STAGING_ENV);
        //payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);


        generateHashFromSDK(params, GlobalConstants.SALTKEY);


    }

    private void generateHashFromSDK(PaymentParams mPaymentParams, String salt) {
        PayuHashes payuHashes = new PayuHashes();
        PostData postData = new PostData();
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setAmount(mPaymentParams.getAmount());
        checksum.setKey(mPaymentParams.getKey());
        checksum.setTxnid(mPaymentParams.getTxnId());
        checksum.setEmail(mPaymentParams.getEmail());
        checksum.setSalt(salt);
        checksum.setProductinfo(mPaymentParams.getProductInfo());
        checksum.setFirstname(mPaymentParams.getFirstName());
        checksum.setUdf1(mPaymentParams.getUdf1());
        checksum.setUdf2(mPaymentParams.getUdf2());
        checksum.setUdf3(mPaymentParams.getUdf3());
        checksum.setUdf4(mPaymentParams.getUdf4());
        checksum.setUdf5(mPaymentParams.getUdf5());

        postData = checksum.getHash();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setPaymentHash(postData.getResult());
        }

        // checksum for payment related details
        // var1 should be either user credentials or default
        String var1 = mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials();
        String key = mPaymentParams.getKey();

        if ((postData = calculateHash(key, PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // Assign post data first then CHECK for SUCCESS
            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(postData.getResult());
        //vas
        if ((postData = calculateHash(key, PayuConstants.VAS_FOR_MOBILE_SDK, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setVasForMobileSdkHash(postData.getResult());

        // getIbibocodes
        if ((postData = calculateHash(key, PayuConstants.GET_MERCHANT_IBIBO_CODES, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setMerchantIbiboCodesHash(postData.getResult());

        if (!var1.contentEquals(PayuConstants.DEFAULT)) {

            // get user card
            if ((postData = calculateHash(key, PayuConstants.GET_USER_CARDS, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setStoredCardsHash(postData.getResult());
            // SAVE user card
            if ((postData = calculateHash(key, PayuConstants.SAVE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setSaveCardHash(postData.getResult());
            // delete user card
            if ((postData = calculateHash(key, PayuConstants.DELETE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setDeleteCardHash(postData.getResult());
            // edit user card
            if ((postData = calculateHash(key, PayuConstants.EDIT_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setEditCardHash(postData.getResult());
        }

        if (mPaymentParams.getOfferKey() != null) {
            postData = calculateHash(key, PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey(), salt);
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                payuHashes.setCheckOfferStatusHash(postData.getResult());
            }
        }

        if (mPaymentParams.getOfferKey() != null && (postData = calculateHash(key, PayuConstants.CHECK_OFFER_STATUS, mPaymentParams.getOfferKey(), salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setCheckOfferStatusHash(postData.getResult());
        }

        launchSdkUI(payuHashes, mPaymentParams);
    }

    /*Calculate Hash*/
    private PostData calculateHash(String key, String command, String var1, String salt) {
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setKey(key);
        checksum.setCommand(command);
        checksum.setVar1(var1);
        checksum.setSalt(salt);
        return checksum.getHash();
    }

    private void launchSdkUI(PayuHashes payuHashes, PaymentParams paymentParams) {
        mPayuHashes = payuHashes;
        mPaymentParams = paymentParams;
        fetchMerchantHashes();

    }

    @SuppressLint("StaticFieldLeak")
    private void fetchMerchantHashes() {
        // now make the api call.
        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCreditials;
        new AsyncTask<Void, Void, HashMap<String, String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                runOnUiThread(() -> commonUtils.showProgressDialog());

            }

            @Override
            protected HashMap<String, String> doInBackground(Void... params) {
                try {

                    URL url = new URL("https://payu.herokuapp.com/get_merchant_hashes");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                    InputStream responseInputStream = conn.getInputStream();
                    StringBuilder responseStringBuffer = new StringBuilder();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }

                    JSONObject response = new JSONObject(responseStringBuffer.toString());

                    HashMap<String, String> cardTokens = new HashMap<>();
                    JSONArray oneClickCardsArray = response.getJSONArray("data");
                    int arrayLength;
                    if ((arrayLength = oneClickCardsArray.length()) >= 1) {
                        for (int i = 0; i < arrayLength; i++) {
                            cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
                        }
                        return cardTokens;
                    }

                } catch (JSONException | MalformedURLException e) {
                    logd(e.getMessage());
                } catch (ProtocolException | UnsupportedEncodingException e) {
                    logd(e.getMessage());
                } catch (IOException e) {
                    logd(e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(final HashMap<String, String> oneClickTokens) {
                super.onPostExecute(oneClickTokens);

                runOnUiThread(() -> {
                    commonUtils.dismissProgressDialog();
                    callPaymentMethod();
                });


            }
        }.execute();
    }


    private void callPaymentMethod() {

        if (mPaymentParams != null && mPayuHashes != null && payuConfig != null) {

            MerchantWebService merchantWebService = new MerchantWebService();
            merchantWebService.setKey(mPaymentParams.getKey());
            merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
            merchantWebService.setVar1(mPaymentParams.getUserCredentials() == null ? "default" : mPaymentParams.getUserCredentials());
            merchantWebService.setHash(mPayuHashes.getPaymentRelatedDetailsForMobileSdkHash());

            PostData data = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
            if (data.getCode() == PayuErrors.NO_ERROR) {

                payuConfig.setData(data.getResult());

                GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(this);
                paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);


            } else {
                shortToast(mContext, data.getResult());

            }

        } else {
            shortToast(mContext, getString(R.string.payment_cancel));
        }


    }

    /*Get output From PayuBiz*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {

            setResult(resultCode, data);

            if (data != null) {
                switch (comingFrom) {
                    case IntentConstants.CHEQUEPENDING:
                        chequePending(data);
                        break;
                    case IntentConstants.PAYMENTDUE:
                        paymentDue(data);
                        break;
                    case IntentConstants.PLACEORDER:
                        placeOrder(data);
                        break;
                    default:
                        //No data
                        break;
                }
            } else {
                switch (comingFrom) {
                    case IntentConstants.CHEQUEPENDING:
                        status = GlobalConstants.CANCEL;
                        newChequePending(currentDate, totalAmount, transactionId, status, mRemarks,
                                "0", "0", bankServiceCharges(null));
                        break;
                    case IntentConstants.PAYMENTDUE:
                        status = GlobalConstants.CANCEL;
                        newPaymentDue(currentDate, totalAmount, transactionId, status,
                                mRemarks, "0", "0",  bankServiceCharges(null));

                        break;
                    case IntentConstants.PLACEORDER:

                        ArrayList<PaymentPojo> pojoArrayList = new ArrayList<>();
                        PaymentPojo paymentPojo = new PaymentPojo();
                        paymentPojo.setmReferenceNum(GlobalConstants.NULL_DATA);
                        paymentPojo.setmUserID(userId);
                        paymentPojo.setmDeviceID(GlobalConstants.NULL_DATA);
                        paymentPojo.setmUID(GlobalConstants.NULL_DATA);
                        paymentPojo.setmDate(currentDate);
                        paymentPojo.setmAmount(totalAmount);
                        paymentPojo.setmTransactionID(transactionId);
                        status = GlobalConstants.CANCEL;
                        paymentPojo.setmStatus(status);
                        paymentPojo.setmRemarks(mRemarks);
                        paymentPojo.setmAmountReceived("0");
                        paymentPojo.setmTransactionRecieved("0");
                        pojoArrayList.add(paymentPojo);

                        newOrderGenerate(GlobalConstants.CANCEL, mStockLocation, mPaymentDays,
                                mDeliverySeq, pojoArrayList, bankServiceCharges(null));


                        break;
                    default:
                        //No data
                        break;
                }
            }
        }
    }

    private ArrayList<String> bankServiceCharges(Intent data){
        ArrayList<String> serviceCharges =new ArrayList<>();
        serviceCharges.add(overallAmount);
        serviceCharges.add(String.valueOf(serviceTaxdata.get(servicePosition).getServiceCharge()));
        serviceCharges.add(String.valueOf(serviceTaxdata.get(servicePosition).getGST()));
        if(data != null) {
            String payuJson = data.getStringExtra(IntentConstants.PAYU_RESPONSE);
            try {
                JSONObject jsonObject = new JSONObject(payuJson);
                serviceCharges.add(jsonObject.getString(JsonKeyConstants.PAY_U_ISSUING_BANK));
                serviceCharges.add(jsonObject.getString(JsonKeyConstants.PAY_U_MODE));
                serviceCharges.add(jsonObject.getString(JsonKeyConstants.PAY_U_CARD_TYPE));
            } catch (JSONException e) {
                logd(e.getMessage());
            }

        }else {
            serviceCharges.add("");
            serviceCharges.add("");
            serviceCharges.add("");
        }
        return serviceCharges;
    }

    /*Order placed*/
    private void placeOrder(Intent data) {
        String payuJson = data.getStringExtra(IntentConstants.PAYU_RESPONSE);
        try {
            JSONObject jsonObject = new JSONObject(payuJson);
            status = jsonObject.getString(JsonKeyConstants.PAY_U_STATUS);
            ArrayList<PaymentPojo> arrayList = new ArrayList<>();
            PaymentPojo paymentPojo = new PaymentPojo();
            paymentPojo.setmReferenceNum(GlobalConstants.NULL_DATA);
            paymentPojo.setmUserID(userId);
            paymentPojo.setmDeviceID(GlobalConstants.NULL_DATA);
            paymentPojo.setmUID(GlobalConstants.NULL_DATA);
            paymentPojo.setmDate(jsonObject.getString(JsonKeyConstants.PAY_U_ADDEDON));
            paymentPojo.setmAmount(totalAmount);
            paymentPojo.setmTransactionID(transactionId);
            paymentPojo.setmStatus(jsonObject.getString(JsonKeyConstants.PAY_U_STATUS));
            paymentPojo.setmRemarks(mRemarks);
            paymentPojo.setmAmountReceived(jsonObject.getString(JsonKeyConstants.PAY_U_AMOUNT));
            paymentPojo.setmTransactionRecieved(jsonObject.getString(JsonKeyConstants.PAY_U_TXNID));
            arrayList.add(paymentPojo);

            newOrderGenerate(status, mStockLocation, mPaymentDays, mDeliverySeq, arrayList, bankServiceCharges(data));


        } catch (JSONException e) {
            logd(e.getMessage());
        }

    }

    private void newOrderGenerate(String status, String mStockLocation, String mPaymentDays, String mDeliverySeq,
                                  ArrayList<PaymentPojo> paymentPojoArrayList, ArrayList<String> serviceCharges) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add(userId);
        stringArrayList.add(status);
        stringArrayList.add(mStockLocation);
        stringArrayList.add(mPaymentDays);
        stringArrayList.add(mDeliverySeq);
        stringArrayList.add(GlobalConstants.GetSharedValues.getBillingSeq());
        stringArrayList.add(GlobalConstants.GetSharedValues.getCashDiscount());
        stringArrayList.add(GlobalConstants.NULL_DATA);
        stringArrayList.add(GlobalConstants.HDFC);

        moveToOrderGenerate(stringArrayList, paymentPojoArrayList, serviceCharges);
    }

    private void moveToOrderGenerate(ArrayList<String> stringArrayList,
                                     ArrayList<PaymentPojo> pojoArrayList, ArrayList<String> serviceCharges) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(GlobalConstants.LOCATION, stringArrayList);
        bundle.putSerializable(GlobalConstants.CART_ARRAY, cartArrayList);
        bundle.putSerializable(GlobalConstants.POJO_ARRAY, pojoArrayList);
        bundle.putSerializable(GlobalConstants.PROMOCODE_ARRAY, subPromoCodes);
        bundle.putString(GlobalConstants.FROM, IntentConstants.PLACEORDER);
        bundle.putString(GlobalConstants.TXN_ID, transactionId);
        bundle.putString(GlobalConstants.STATUS, status);
        bundle.putSerializable(GlobalConstants.ORDER_BANK_INFO, serviceCharges);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, Ordergeneration.class, bundle);
    }


    /*Payment Due Response*/
    private void paymentDue(Intent data) {
        String payuJson = data.getStringExtra(IntentConstants.PAYU_RESPONSE);
        try {
            JSONObject jsonObject = new JSONObject(payuJson);
            String date = jsonObject.getString(JsonKeyConstants.PAY_U_ADDEDON);
            status = jsonObject.getString(JsonKeyConstants.PAY_U_STATUS);
            String amountReceived = jsonObject.getString(JsonKeyConstants.PAY_U_AMOUNT);
            String transactionReceived = jsonObject.getString(JsonKeyConstants.PAY_U_TXNID);

            newPaymentDue(date, totalAmount, transactionId, status, mRemarks, amountReceived,
                    transactionReceived,  bankServiceCharges(data));

        } catch (JSONException e) {
            logd(e.getMessage());
        }

    }

    private void newPaymentDue(String date, String totalAmount, String transactionId, String status, String remarks,
                               String amountReceived, String transactionReceived, ArrayList<String> serviceCharges) {
        ArrayList<String> paymentDueList = new ArrayList<>();
        paymentDueList.add(userId);
        paymentDueList.add(date);
        paymentDueList.add(totalAmount);
        paymentDueList.add(transactionId);
        paymentDueList.add(status);
        paymentDueList.add(remarks);
        paymentDueList.add(amountReceived);
        paymentDueList.add(transactionReceived);
        paymentDueList.add(GlobalConstants.NULL_DATA);
        paymentDueList.add(GlobalConstants.NULL_DATA);
        paymentDueList.add(GlobalConstants.NULL_DATA);

        Bundle bundle = new Bundle();
        bundle.putSerializable(IntentConstants.PAYMENTDUE, paymentDueList);
        bundle.putString(GlobalConstants.FROM, IntentConstants.PAYMENTDUE);
        bundle.putString(GlobalConstants.TXN_ID, transactionId);
        bundle.putString(GlobalConstants.STATUS, status);
        bundle.putSerializable(GlobalConstants.ORDER_BANK_INFO, serviceCharges);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, Ordergeneration.class, bundle);
    }


    /*Cheque Pending Data*/
    private void chequePending(Intent data) {

        String payuJson = data.getStringExtra(IntentConstants.PAYU_RESPONSE);
        try {
            JSONObject jsonObject = new JSONObject(payuJson);
            String date = jsonObject.getString(JsonKeyConstants.PAY_U_ADDEDON);
            String tranId = transactionId;
            status = jsonObject.getString(JsonKeyConstants.PAY_U_STATUS);
            String amountReceived = jsonObject.getString(JsonKeyConstants.PAY_U_AMOUNT);
            String transactionReceived = jsonObject.getString(JsonKeyConstants.PAY_U_TXNID);

            newChequePending(date, totalAmount, tranId, status, mRemarks, amountReceived,
                    transactionReceived, bankServiceCharges(data));

        } catch (JSONException e) {
            logd(e.getMessage());
        }
    }

    private void newChequePending(String date, String totalAmount, String tranId, String status, String mRemarks,
                                  String amountReceived, String transactionReceived, ArrayList<String> serviceCharges) {

        ArrayList<String> chequeList = new ArrayList<>();
        chequeList.add(userId);
        chequeList.add(date);
        chequeList.add(totalAmount);
        chequeList.add(tranId);
        chequeList.add(status);
        chequeList.add(mRemarks);
        chequeList.add(amountReceived);
        chequeList.add(transactionReceived);
        chequeList.add(GlobalConstants.NULL_DATA);
        chequeList.add(GlobalConstants.NULL_DATA);
        chequeList.add(GlobalConstants.NULL_DATA);

        Bundle bundle = new Bundle();
        bundle.putSerializable(IntentConstants.CHEQUEPENDING, chequeList);
        bundle.putString(GlobalConstants.FROM, IntentConstants.CHEQUEPENDING);
        bundle.putString(GlobalConstants.TXN_ID, transactionId);
        bundle.putString(GlobalConstants.STATUS, status);
        bundle.putSerializable(GlobalConstants.ORDER_BANK_INFO, serviceCharges);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, Ordergeneration.class, bundle);

    }

    /*Spot API response*/

    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        if (capitalFloat.equals(IntentConstants.SPOT)) {
            spotResponse(successObject);
        } else if (capitalFloat.equals(IntentConstants.CAPITAL_FLOAT)) {
            jbaIdResponse(successObject);
        }
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }

    @Override
    public void onError() {
        logd(mContext.getClass().getSimpleName());
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    /*Spot Response*/
    private void spotResponse(Object successObject) {
        PromoCodeJsonResponse promoCodeJsonResonse = (PromoCodeJsonResponse) successObject;
        if (promoCodeJsonResonse != null) {
            switch (promoCodeJsonResonse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    boolean itemCodeCheck = false;
                    ArrayList<PromoCodeData> promoCodeData = new ArrayList<>(promoCodeJsonResonse.getData());
                    for (int i = 0; i < promoCodeJsonResonse.getData().size(); i++) {
                        if (spotNumber.equals(promoCodeData.get(i).getPromoCode())) {
                            subPromoCodeArrayList = new ArrayList<>(promoCodeData.get(i).getSubPromoCode());
                            itemCodeCheck = true;
                            break;
                        }
                    }
                    if (itemCodeCheck) {
                        calculateSpotAmount(subPromoCodeArrayList, 2);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, GlobalConstants.NO_DATA);
                    LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    /*JBA ID Response*/
    private void jbaIdResponse(Object successObject) {
        CheckJbaIdReponse checkJbaIdReponse = (CheckJbaIdReponse) successObject;
        if (checkJbaIdReponse != null) {
            switch (checkJbaIdReponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    switch (checkJbaIdReponse.getData().getUserSts()) {
                        case IntentConstants.TRUE:
                            showOtpScreen(checkJbaIdReponse.getData().getAppId());
                            break;
                        case IntentConstants.FALSE:
                            showPopup(IntentConstants.NOT_ACTIVE);
                            break;
                        case IntentConstants.NOT_AVL:
                            showPopup(IntentConstants.NEW_USER);
                            break;
                        default:
                            shortToast(mContext, GlobalConstants.SERVER_ERROR);
                            break;
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, GlobalConstants.NO_DATA);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }

    private void showOtpScreen(String appId) {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.PRICE, overallAmount);
        bundle.putString(GlobalConstants.REFERENCE_NUM, otpRefNumber);
        bundle.putString(IntentConstants.DATE, currentDate);
        bundle.putString(IntentConstants.REMARKS, GlobalConstants.SPACE);
        bundle.putString(IntentConstants.STOCKLOCATION, mStockLocation);
        bundle.putString(IntentConstants.PAYMENTDAYS_CARD, mPaymentDays);
        bundle.putString(IntentConstants.DELIVERYSEQUENCE_CARD, mDeliverySeq);
        bundle.putBoolean(IntentConstants.SPOTBOOLEAN, mSpotBoolean);
        bundle.putString(IntentConstants.APP_ID, appId);
        bundle.putSerializable(IntentConstants.SPOTARRAY, subPromoCodes);
        bundle.putSerializable(IntentConstants.CARTARRAY, cartArrayList);
        LaunchIntentManager.routeToActivityStackBundleStack(mContext, OtpActivity.class, bundle);
    }

    @SuppressLint("NewApi")
    private void showPopup(String errorOrNot) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.alert_popup_new);

        ImageView alertImage = mDialog.findViewById(R.id.alertImage);
        TextView alertTitle = mDialog.findViewById(R.id.titleText);
        TextView alertContent = mDialog.findViewById(R.id.textContent);

        LinearLayout layout = mDialog.findViewById(R.id.layout);
        TextView textOk = mDialog.findViewById(R.id.text_ok);
        alertImage.setImageResource(R.drawable.bell);
        alertTitle.setText(mContext.getString(R.string.alert));
        if (errorOrNot.equals(IntentConstants.NOT_ACTIVE)) {
            layout.setVisibility(View.GONE);
            textOk.setVisibility(View.VISIBLE);
        } else {
            layout.setVisibility(View.VISIBLE);
            textOk.setVisibility(View.GONE);
        }

        TextView success = mDialog.findViewById(R.id.text_success);
        TextView failure = mDialog.findViewById(R.id.text_failure);

        success.setOnClickListener(v -> LaunchIntentManager.routeToActivityStack(mContext, SettingsActivity.class));

        failure.setOnClickListener(v -> mDialog.dismiss());

        alertContent.setText(errorOrNot);

        textOk.setOnClickListener(v -> mDialog.dismiss());
        mDialog.show();
    }

    private void serviceCallApi() {
        apiString = IntentConstants.SERVICE_CHARGE;
        commonUtils.showProgressDialog();
        ServiceTaxHelper.serviceTaxCall(overallAmount, mContext, this);
    }

    /*Service Tax Response*/
    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        switch (apiString) {
            case IntentConstants.SERVICE_CHARGE:
                ServiceTaxJsonResponse serviceTaxJsonResponse = (ServiceTaxJsonResponse) successObject;
                if (serviceTaxJsonResponse != null) {
                    switch (serviceTaxJsonResponse.getStatusCode()) {
                        case IntentConstants.SUCCESS_URL:
                            serviceTaxdata = new ArrayList<>(serviceTaxJsonResponse.getData());
                            break;
                        case IntentConstants.FAILURE_URL:
                            noRecords(mContext, GlobalConstants.SERVER_ERR);
                            break;
                        default:
                            noRecords(mContext, GlobalConstants.SERVER_ERR);
                            break;
                    }
                } else {
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                }

                break;
            case IntentConstants.CARTCHECK:
                cartUpdation(successObject);
                break;
            case IntentConstants.OTP:
                eligibilityData(successObject);
                break;
            default:
                //Not in use
                break;
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        commonUtils.dismissProgressDialog();
        switch (apiString) {
            case IntentConstants.SERVICE_CHARGE:
                noRecords(mContext, GlobalConstants.SERVER_ERR);
                break;
            case IntentConstants.CARTCHECK:
                shortToast(mContext, GlobalConstants.SERVER_ERROR);
                break;
            case IntentConstants.OTP:
                shortToast(mContext, GlobalConstants.SERVER_ERROR);
                break;
            default:
                //Not in use
                break;
        }
    }

    @Override
    public void mError() {
        commonUtils.dismissProgressDialog();
        switch (apiString) {
            case IntentConstants.SERVICE_CHARGE:
                /*LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);*/
                shortToast(mContext, GlobalConstants.INTERNET_CHECK);
                break;
            case IntentConstants.CARTCHECK:
                shortToast(mContext, GlobalConstants.INTERNET_CHECK);
                break;
            case IntentConstants.OTP:
                shortToast(mContext, GlobalConstants.INTERNET_CHECK);
                break;
            default:
                //Not in use
                break;
        }
    }

    private void eligibilityData(Object successObject) {
        CustomerEligibilityJsonResponse customerEligibilityJsonResponse = (CustomerEligibilityJsonResponse) successObject;
        if (customerEligibilityJsonResponse != null) {
            switch (customerEligibilityJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String mStatus = customerEligibilityJsonResponse.getData().getStatus();
                    String mReason = customerEligibilityJsonResponse.getData().getReason();
                    otpRefNumber = customerEligibilityJsonResponse.getData().getReferenceNumber();
                    switch (mStatus) {
                        case GlobalConstants.STATUS_R:
                            showEligibilityPopup(mContext, mReason);
                            break;
                        case GlobalConstants.STATUS_A:
                            showOtpScreen(GlobalConstants.NULL_DATA);
                            break;
                        default:
                            shortToast(mContext, GlobalConstants.SERVER_ERROR);
                            break;
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, customerEligibilityJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }
    }


    /*Cart status update response*/
    private void cartUpdation(Object successObject) {
        CartUpdationJsonResponse cartUpdationJsonResponse = (CartUpdationJsonResponse) successObject;
        if (cartUpdationJsonResponse != null) {
            switch (cartUpdationJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    if (cartUpdateBoolean) {
                        cartUpdateBoolean = false;
                        callOnlinePaymentAPI();
                    } else {
                        logd(GlobalConstants.UPDATED);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    shortToast(mContext, cartUpdationJsonResponse.getDescription());
                    break;
                default:
                    shortToast(mContext, GlobalConstants.SERVER_ERROR);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.SERVER_ERROR);
        }

    }

    private void cartApiCall() {

        ArrayList<String> cartArray = new ArrayList<>();
        cartArray.add(userId);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.MODE_SELECT);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArray.add(GlobalConstants.GetSharedValues.getCashDiscount());
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArray, mContext, this);

    }

    @SuppressLint("SetTextI18n")
    private void gstData(int position) {
        DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(false);
        df.setMaximumIntegerDigits(20);
        df.setMaximumFractionDigits(2);
        amountGst.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + df.format(serviceTaxdata.get(position).getValue()));
        serviceTaxTitle.setText(IntentConstants.SERVICE_CHARGE + serviceTaxdata.get(position).getPercentage() + IntentConstants.PERCENT);
        serviceTaxValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + df.format(serviceTaxdata.get(position).getServiceCharge()));
        gstTitle.setText(IntentConstants.GST);
        gstValue.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + df.format(serviceTaxdata.get(position).getGST()));
        cardAmount.setText(IntentConstants.PAYMENTAMOUNT + IntentConstants.RUPEES + GlobalConstants.SPACE + df.format(serviceTaxdata.get(position).getTotal()));

    }

    /*Cart response*/
    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJsonResponse = (CartJsonResponse) successObject;
        if (cartJsonResponse != null) {
            switch (cartJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*9-1-19*/
                    /*ArrayList<CartData> list = new ArrayList<>();
                    for (int i = 0; i < cartJsonResponse.getData().size(); i++) {
                        list.addAll(cartJsonResponse.getData().get(i).getCart());
                    }*/
                    ArrayList<CartData> list = new ArrayList<>(cartJsonResponse.getData().get(0).getCart());
                    cartStatus = list.get(0).getStatus();
                    calculateAmount(list, 2);
                    setCartUpdate(list.size());
                    break;
                case IntentConstants.FAILURE_URL:
                    setCartUpdate(0);
                    LaunchIntentManager.routeToActivity(mContext, EmptyCartActivity.class);
                    break;
                default:
                    shortToast(mContext, GlobalConstants.PAY_NOW_AGAIN);
                    break;
            }
        } else {
            shortToast(mContext, GlobalConstants.PAY_NOW_AGAIN);
        }
    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.PAY_NOW_AGAIN);
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.INTERNET_CHECK);
    }

    private void calculateAmount(ArrayList<CartData> list, int value) {
        if (value == 2) {
            Double count = 0.0d;
            for (int i = 0; i < list.size(); i++) {
                count += list.get(i).getUnitPrice() * (double) list.get(i).getQuantity();
            }
            double secondAmount = count;
            if (firstAmount == secondAmount) {
                if (cartStatus.equals(IntentConstants.Y)) {
                    shortToast(mContext, IntentConstants.CARTPAYMENT);
                } else {

                    cartUpdate(IntentConstants.Y);
                }
            } else {
                showEligibilityPopup(mContext, IntentConstants.CART);
            }
        } else if (value == 1) {
            Double count = 0.0d;
            for (int i = 0; i < list.size(); i++) {
                count += list.get(i).getUnitPrice() * (double) list.get(i).getQuantity();
            }
            firstAmount = count;
        }
    }

    private void calculateSpotAmount(ArrayList<SubPromoCode> subPromoCodes, int value) {

        if (value == 2) {
            Double count = 0.0d;
            for (int i = 0; i < subPromoCodes.size(); i++) {
                count += subPromoCodes.get(i).getmTOTAL();
            }
            double secondAmount = count;
            if (firstSpotAmount == secondAmount) {
                callOnlinePaymentAPI();
            } else {
                SharedPreferences preferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
                preferences.edit().clear().apply();
                showEligibilityPopup(mContext, IntentConstants.SPOT);
            }
        } else if (value == 1) {
            Double count = 0.0d;
            for (int i = 0; i < subPromoCodes.size(); i++) {
                count += subPromoCodes.get(i).getmTOTAL();
            }
            firstSpotAmount = count;
        }
    }


    private void showEligibilityPopup(Context context, String comeFrom) {

        final Dialog mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_alert);

        TextView prefTitle = mDialog.findViewById(R.id.pre_ref_title);
        TextView prefcont = mDialog.findViewById(R.id.pre_ref_cont);
        Button prefcontbtm = mDialog.findViewById(R.id.pre_ref_btm);
        SharedPreferences preferences = getSharedPreferences(GlobalConstants.CART_DETAILS, MODE_PRIVATE);
        preferences.edit().clear().apply();
        setCashDiscount(GlobalConstants.NO);
        if (!mSpotBoolean && cartArrayList != null && !cartArrayList.isEmpty()) {
            setCartUpdate(cartArrayList.size());
        }

        /*Kamesh*/
        Button refCancleBtm = mDialog.findViewById(R.id.pre_ref_btm_no);
        refCancleBtm.setVisibility(View.VISIBLE);
        prefcontbtm.setText(GlobalConstants.Y);
        refCancleBtm.setOnClickListener(v -> mDialog.dismiss());
        /*Kamesh*/

        switch (comeFrom) {
            case IntentConstants.CART:
                mDialog.setCancelable(false);
                mDialog.setCanceledOnTouchOutside(false);
                prefcont.setText(IntentConstants.AMOUNTMISMATCH);
                setCartDelSeqUpdate(IntentConstants.CARTDELSEQ);
                break;
            case IntentConstants.SPOT:
                mDialog.setCancelable(false);
                mDialog.setCanceledOnTouchOutside(false);
                prefcont.setText(IntentConstants.AMOUNTMISMATCHSPOT);
                break;
            case IntentConstants.BACK:
                prefcont.setText(GlobalConstants.CANCEL_ORDER);
                break;
            default:
                prefcont.setText(comeFrom);
                break;
        }

        prefcontbtm.setOnClickListener(v -> {
            mDialog.dismiss();
            LaunchIntentManager.routeToActivity(mContext, DashboardActivity.class);
        });

        mDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            if (apiString.equals(IntentConstants.SERVICE_CHARGE)) {
                serviceCallApi();
            }
        }

    }


    private void cartUpdate(String status) {
        apiString = IntentConstants.CARTCHECK;
        cartUpdateBoolean = true;
        commonUtils.showProgressDialog();
        CartUpdationManager.cartUpdateCall(userId, status, mContext, this);
    }


}
