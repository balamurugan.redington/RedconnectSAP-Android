package redington.com.redconnect.redconnect.preorder.activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import redington.com.redconnect.R;
import redington.com.redconnect.common.activity.BaseActivity;
import redington.com.redconnect.constants.GlobalConstants;
import redington.com.redconnect.constants.IntentConstants;
import redington.com.redconnect.redconnect.dashboard.helper.CartServiceManager;
import redington.com.redconnect.redconnect.mycart.activity.MyCartActivity;
import redington.com.redconnect.redconnect.mycart.model.CartData;
import redington.com.redconnect.redconnect.mycart.model.CartJsonResponse;
import redington.com.redconnect.redconnect.payment.helper.RetryServiceHelper;
import redington.com.redconnect.redconnect.payment.model.RetryResponse;
import redington.com.redconnect.redconnect.preorder.adapter.PreOrderSpecAdapter;
import redington.com.redconnect.redconnect.preorder.adapter.ProductViewPagerAdapter;
import redington.com.redconnect.redconnect.preorder.dialog.QuantityCustomSheetDialog;
import redington.com.redconnect.redconnect.preorder.helper.PreorderDetailsServiceManager;
import redington.com.redconnect.redconnect.preorder.listener.MyDialogCloseListener;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsData;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsImage;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsJsonResponse;
import redington.com.redconnect.redconnect.preorder.model.PreorderItemDetailsSpecification;
import redington.com.redconnect.reddb.dao.RedDAO;
import redington.com.redconnect.restapiclient.listener.CartListener;
import redington.com.redconnect.restapiclient.listener.NewListener;
import redington.com.redconnect.restapiclient.listener.UIListener;
import redington.com.redconnect.util.CommonUtils;
import redington.com.redconnect.util.InternetErrorCheck;
import redington.com.redconnect.util.LaunchIntentManager;

public class ProductDetail extends BaseActivity implements UIListener, CartListener, MyDialogCloseListener, NewListener {

    /*Kamesh (Note) ->  Added NewListener for CheckingWebOrderEntry (Retry) API*/

    private Context mContext;
    private CommonUtils commonUtils;
    private int getDataSize;

    private String productName;
    private String productItemcode;
    private String productVendorcode;
    private LinearLayout mCartLayout;
    private ArrayList<CartData> cartArrayList = new ArrayList<>();
    private TextView txtCount;
    private RelativeLayout mLayout;
    private Button addCartButton;
    private String userID;
    private boolean cartCheck = false;
    private String status;
    private Handler handler = new Handler();
    private TextView mProductname;
    private TextView textDescDetails;
    private Double discountPrice;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);

        mContext = ProductDetail.this;
        commonUtils = new CommonUtils(mContext);

        ScrollView mScrollview = findViewById(R.id.review_scrollview);
        mScrollview.smoothScrollTo(0, 0);

        productName = getIntent().getStringExtra(GlobalConstants.PRODUCT_NAME);
        productItemcode = getIntent().getStringExtra(GlobalConstants.PRODUCT_ITEM_CODE);
        productVendorcode = getIntent().getStringExtra(GlobalConstants.PRODUCT_VENDOR_CODE);


        toolbar(IntentConstants.PRODUCTDETAIL);
        userID = GlobalConstants.GetSharedValues.getSpUserId();


        setInstance();

    }

    @SuppressLint("SetTextI18n")
    private void setInstance() {

        TextView itemCode = findViewById(R.id.itemcode);
        mProductname = findViewById(R.id.product_name);
        addCartButton = findViewById(R.id.btn_addCart);

        ImageView mWishlist = findViewById(R.id.wishlist);
        mWishlist.setOnClickListener(v -> shortToast(mContext, "Product Details"));

        itemCode.setText(productItemcode);
        mProductname.setText(productName);

        mCartLayout = findViewById(R.id.LL_cartText);
        txtCount = findViewById(R.id.txtCount);
        mLayout = findViewById(R.id.RL_layout);
        RelativeLayout imageCart = findViewById(R.id.image_cart);
        ImageButton imageButton = findViewById(R.id.imageButton);
        imageButton.setVisibility(View.GONE);
        imageCart.setVisibility(View.VISIBLE);
        ImageView mCartImage = findViewById(R.id.imageviewCart);

        mCartImage.setImageResource(R.mipmap.ic_cart);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mCartImage.setImageTintList(ContextCompat.getColorStateList(mContext, R.color.new_black));
        } else {
            mCartImage.setColorFilter(ContextCompat.getColor(mContext, R.color.new_black));
        }

        imageCart.setOnClickListener(v -> {
            callRetryAPI();  /*Kamesh*/
        });

        addCartButton.setOnClickListener(v -> addToCartButton());

        callAPI();

    }

    /*Kamesh*/
    private void callRetryAPI() {
        commonUtils.showProgressDialog();
        RetryServiceHelper.retryCall(userID, mContext, this);
    }


    private void cartNavigation() {
        RedDAO redDAO = new RedDAO(mContext);
        int cartCnt = redDAO.getCartOrderCount();
        RedDAO redDAO2 = new RedDAO(mContext);
        int dbCount = redDAO2.getChequesPendingCount();
        RedDAO dao = new RedDAO(mContext);
        int count = dao.getPaymentDueCount();
        if (cartCnt < 1 && dbCount < 1 && count < 1) {
            LaunchIntentManager.routeToActivity(mContext, MyCartActivity.class);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        } else {
            shortToast(mContext, GlobalConstants.CART_PRESENT);
            /*LaunchIntentManager.routeToActivityStack(mContext, PaymentScreenActivity.class);*/
        }
    }
    /*Kamesh*/

    private void addToCartButton() {
        String btnStr = addCartButton.getText().toString();
        if (btnStr.equals(GlobalConstants.ADD_TO_CART)) {

            if (status.equals(IntentConstants.Y)) {
                shortToast(mContext, IntentConstants.CARTPAYMENT);
            } else if (discountPrice == 0.0d) {
                shortToast(mContext, GlobalConstants.PRODUCT_PRICE_NA);
            } else {
                checkItemCode();
            }

        } else if (btnStr.equals(GlobalConstants.GO_TO_CART)) {
            LaunchIntentManager.routeToActivity(this, MyCartActivity.class);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }

    }


    /*Item Code Validation*/
    private void checkItemCode() {
        boolean itemCodeCheck = false;
        if (getDataSize == 0) {
            callBottomSheet();
        } else {
            for (int i = 0; i < cartArrayList.size(); i++) {
                if (cartArrayList.get(i).getItemCode().contains(productItemcode)) {
                    shortToast(mContext, GlobalConstants.ALREADY_IN_CART);
                    itemCodeCheck = true;
                    break;
                }
            }
            if (!itemCodeCheck) {
                callBottomSheet();
            }
        }
    }

    /*Call Bottom Sheet*/
    private void callBottomSheet() {
        QuantityCustomSheetDialog bottomSheetDialog = QuantityCustomSheetDialog.getInstance();
        Bundle bundle = new Bundle();
        bundle.putString(GlobalConstants.PRODUCT_NAME, productName);
        bundle.putString(GlobalConstants.ITEM_CODE, productItemcode);
        bundle.putString(GlobalConstants.VENDOR_CODE, productVendorcode);
        bundle.putString(GlobalConstants.PRODUCT_PRICE, String.valueOf(discountPrice));
        bottomSheetDialog.setArguments(bundle);
        bottomSheetDialog.show(getSupportFragmentManager(), GlobalConstants.CUSTOM_BOTTOM_SHEET);

    }


    private void callAPI() {
        ArrayList<String> detailList = new ArrayList<>();
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(productItemcode);
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(GlobalConstants.NULL_DATA);
        detailList.add(userID);
        callProductApi(detailList);
    }

    private void callProductApi(ArrayList<String> detailList) {
        commonUtils.showProgressDialog();
        PreorderDetailsServiceManager.getPreorderDetailsServiceManager(detailList, mContext, this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(GlobalConstants.SP_INTERNET_CHECK, MODE_PRIVATE);
        Boolean errorCheck = prefs.getBoolean(GlobalConstants.SP_INTERNET_CHECK_BOOLEAN, false);

        if (errorCheck) {
            internetCheckFalse();
            if (cartCheck) {
                getCartCount();
                cartCheck = false;
            } else {
                callAPI();
            }
        }

        SharedPreferences sharedPreferences = getSharedPreferences(GlobalConstants.SP_BACK_CHECK, MODE_PRIVATE);
        Boolean aBoolean = sharedPreferences.getBoolean(GlobalConstants.SP_BACK_BUTTON, false);
        if (aBoolean) {
            backButtonFalse();
            setMenuBack(mContext);
        }

        getCartUpdate();


    }

    public void menuBack(View view) {
        setMenuBack(view.getContext());
    }

    @Override
    public void cartSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        CartJsonResponse cartJson = (CartJsonResponse) successObject;
        if (cartJson != null) {
            switch (cartJson.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    /*9-1-19*/
                    /*for (int i = 0; i < cartJson.getData().get(0).getCart().size(); i++) {
                        cartArrayList.addAll(cartJson.getData().get(0).getCart());
                    }*/
                    cartArrayList.addAll(cartJson.getData().get(0).getCart());
                    setCartValues(cartJson.getData().get(0).getCart().size());
                    break;
                case IntentConstants.FAILURE_URL:
                    setCartUpdate(0);
                    mCartLayout.setVisibility(View.GONE);
                    break;
                default:
                    mCartLayout.setVisibility(View.GONE);
                    callCartSnackBar();
                    break;
            }
        } else {
            mCartLayout.setVisibility(View.GONE);
            callCartSnackBar();
        }
    }

    @Override
    public void cartFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        mCartLayout.setVisibility(View.GONE);
        callCartSnackBar();
    }

    @Override
    public void cartError() {
        commonUtils.dismissProgressDialog();
        cartCheck = true;
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    @SuppressLint({"NewApi", "SetTextI18n"})
    private void setCartValues(int size) {
        getDataSize = size;
        mCartLayout.setVisibility(View.VISIBLE);
        txtCount.setText(Integer.toString(getDataSize));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(GradientDrawable.OVAL);
            gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.white_100));
            txtCount.setBackground(gradientDrawable);
        }
        setCartUpdate(getDataSize);
    }

    /*Cart Snack Bar*/
    private void callCartSnackBar() {
        Snackbar snackbar = Snackbar
                .make(mLayout, GlobalConstants.SERVER_ERROR_BAR, Snackbar.LENGTH_INDEFINITE)
                .setAction(GlobalConstants.RETRY, view -> getCartCount());
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }


    private void getCartCount() {
        ArrayList<String> cartArray = new ArrayList<>();
        cartArray.add(userID);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.MODE_SELECT);
        cartArray.add(GlobalConstants.NULL_DATA);
        cartArray.add(GlobalConstants.GetSharedValues.getCartDelSeq());
        cartArray.add(GlobalConstants.GetSharedValues.getCashDiscount());
        cartApiCall(cartArray);
    }

    private void cartApiCall(ArrayList<String> cartArray) {
        commonUtils.showProgressDialog();
        CartServiceManager.getCartServiceCall(cartArray, mContext, this);
    }


    @Override
    public void onSuccess(Object successObject) {
        commonUtils.dismissProgressDialog();
        mDetailedAPI(successObject);
    }

    @Override
    public void onFailure(Object failureObject) {
        commonUtils.dismissProgressDialog();
        noRecords(mContext, GlobalConstants.SERVER_ERR);
    }

    @Override
    public void onError() {
        commonUtils.dismissProgressDialog();
        LaunchIntentManager.routeToActivityStack(mContext, InternetErrorCheck.class);
    }

    private void mDetailedAPI(Object successObject) {
        PreorderItemDetailsJsonResponse preorderDetails = (PreorderItemDetailsJsonResponse) successObject;
        if (preorderDetails != null) {
            switch (preorderDetails.getmStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    showItemDetails(preorderDetails);
                    getCartCount();
                    break;
                case IntentConstants.FAILURE_URL:
                    noRecords(mContext, GlobalConstants.NO_RECORDS);
                    break;
                default:
                    noRecords(mContext, GlobalConstants.SERVER_ERR);
                    break;
            }
        } else {
            noRecords(mContext, GlobalConstants.SERVER_ERR);
        }
    }

    @SuppressLint("SetTextI18n")
    private void showItemDetails(PreorderItemDetailsJsonResponse preorderDetails) {

        bindRecyclerview(preorderDetails.getmData().getmSpecification());
        viewPager(preorderDetails.getmData());
        status = preorderDetails.getmData().getStatus();
        textDescDetails = findViewById(R.id.textDescDetails);

        String descr = preorderDetails.getmData().getmDescription();
        String stockCheck = preorderDetails.getmData().getStock();
        double actualPrice = preorderDetails.getmData().getUnitPrice();
        discountPrice = preorderDetails.getmData().getDiscountPrice();
        double disPercent = preorderDetails.getmData().getDiscountPercent();

        TextView mAcualPrice = findViewById(R.id.actualPrice);
        if (disPercent == 0) {
            mAcualPrice.setVisibility(View.GONE);
        } else {
            mAcualPrice.setVisibility(View.VISIBLE);
            mAcualPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + isValidNumberFormat().format(actualPrice));
            mAcualPrice.setPaintFlags(mAcualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        assert descr != null;
        if (!descr.equals("")) {
            if (descr.contains(":")) {
                String[] title = descr.split(":");

                if (title[1].contains("|")) {
                    String[] details = title[1].split(Pattern.quote("|"));
                    descDetails(details);
                }
            } else if (descr.contains("|")) {
                String[] details = descr.split(Pattern.quote("|"));
                descDetails(details);
            }
        } else {
            textDescDetails.setText(GlobalConstants.NO_DESCRIPTION);

        }

        setResponseValue(stockCheck);

    }

    @SuppressLint("SetTextI18n")
    private void setResponseValue(String stock) {

        RelativeLayout footer = findViewById(R.id.footer);
        if (stock.equals(GlobalConstants.LOW_STOCK)) {
            addCartButton.setEnabled(false);
            footer.setAlpha(0.5f);
            addCartButton.setText(GlobalConstants.STOCK_EXHAUST);
        }


        if (discountPrice == 0.0d) {
            addCartButton.setEnabled(false);
            footer.setAlpha(0.5f);
        }

        TextView totalprice = findViewById(R.id.total_price);
        TextView mProductPrice = findViewById(R.id.txt_productPrice);
        mProductPrice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + isValidNumberFormat().format(discountPrice));
        totalprice.setText(IntentConstants.RUPEES + GlobalConstants.SPACE + isValidNumberFormat().format(discountPrice));
    }

    private void descDetails(String[] details) {
        StringBuilder dataD = new StringBuilder();
        for (String data : details) {
            dataD.append("\n ").append(getResources().getString(R.string.bulletPoint)).append(data);
        }
        textDescDetails.setText(dataD.toString());

    }

    private void viewPager(PreorderItemDetailsData preorderData) {

        ArrayList<PreorderItemDetailsImage> arrayList = new ArrayList(preorderData.getmImages());
        ViewPager viewpager = findViewById(R.id.pager_imageview);
        viewpager.setAdapter(new ProductViewPagerAdapter(mContext, arrayList));
        CirclePageIndicator pageIndicator = findViewById(R.id.circle_imageview);
        pageIndicator.setViewPager(viewpager);
        final float density = getResources().getDisplayMetrics().density;
        pageIndicator.setBackgroundColor(Color.WHITE);
        pageIndicator.setRadius(6.0F * density);
        pageIndicator.setPageColor(Color.TRANSPARENT);
        pageIndicator.setFillColor(ContextCompat.getColor(getApplicationContext(), R.color.failure_color));
        pageIndicator.setBackgroundColor(Color.TRANSPARENT);
        pageIndicator.setStrokeColor(ContextCompat.getColor(getApplicationContext(), R.color.new_black_light));
        pageIndicator.setRadius(7.0f);
        final int[] noOfPages = {arrayList.size()};
        if (preorderData.getmSpecification().get(0).getHeader().equals(GlobalConstants.PRODUCT_NAME_DETAILS)) {
            mProductname.setText(preorderData.getmSpecification().get(0).getHeaderValue());
        }


        pageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                noOfPages[0] = position;
            }

            @Override
            public void onPageSelected(int position) {
                //No data
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //No data
            }
        });
    }

    private void bindRecyclerview(List<PreorderItemDetailsSpecification> preorderItemDetailsSpecifications) {
        RecyclerView specsList = findViewById(R.id.highlightsList);
        specsList.setHasFixedSize(true);
        specsList.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        specsList.setLayoutManager(layoutManager);
        PreOrderSpecAdapter adapter = new PreOrderSpecAdapter(preorderItemDetailsSpecifications);
        specsList.setAdapter(adapter);
    }


    @Override
    public void handleDialogClose(DialogInterface dialog) {
        logd(GlobalConstants.DIALOG_DISMISS);
    }

    @Override
    public void cartSize(int count) {
        setCartValues(count);
        flipAnimator(addCartButton);
    }

    private void flipAnimator(Button button) {
        final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(button, "rotationX", 180.0f, 360.0f);
        objectAnimator.setDuration(1000L);
        objectAnimator.start();
        addCartButton.setText("");
        shortToast(mContext, GlobalConstants.CART_SUCCESS);
        handler.postDelayed(() -> {
            objectAnimator.cancel();
            addCartButton.setText(GlobalConstants.GO_TO_CART);
        }, 1000L);

    }

    /*Kamesh*/
    @Override
    public void mSuccessObject(Object successObject) {
        commonUtils.dismissProgressDialog();
        RetryResponse retryJsonResponse = (RetryResponse) successObject;
        if (retryJsonResponse != null) {
            switch (retryJsonResponse.getStatusCode()) {
                case IntentConstants.SUCCESS_URL:
                    String pendingOrder = retryJsonResponse.getData().getPendOrd().get(0).getTransactionID();
                    String referenceOrder = retryJsonResponse.getData().getPendOrd().get(0).getReferenceNo();
                    if (pendingOrder.isEmpty() && referenceOrder.isEmpty()) {
                        cartNavigation();
                    } else {
                        shortToast(mContext, IntentConstants.CARTPAYMENT);
                    }
                    break;
                case IntentConstants.FAILURE_URL:
                    cartNavigation();
                    break;
                default:
                    errorDialogPage();
                    break;
            }

        } else {
            errorDialogPage();
        }
    }

    @Override
    public void mFailureObject(Object failureObject) {
        errorDialogPage();
    }

    @Override
    public void mError() {
        errorDialogPage();
    }


    private void errorDialogPage() {
        commonUtils.dismissProgressDialog();
        shortToast(mContext, GlobalConstants.SERVER_ERROR);
    }
    /*Kamesh*/
}
