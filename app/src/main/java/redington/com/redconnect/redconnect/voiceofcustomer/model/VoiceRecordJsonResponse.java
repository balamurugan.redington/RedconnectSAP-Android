package redington.com.redconnect.redconnect.voiceofcustomer.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VoiceRecordJsonResponse implements Serializable {

    @SerializedName("Data")
    private List<Object> mData;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Status_Code")
    private String mStatusCode;
    @SerializedName("Status_Message")
    private String mStatusMessage;

    public List<Object> getData() {
        return mData;
    }


    public String getDescription() {
        return mDescription;
    }


    public String getStatusCode() {
        return mStatusCode;
    }


    public String getStatusMessage() {
        return mStatusMessage;
    }


    public void setmData(List<Object> mData) {
        this.mData = mData;
    }
}
