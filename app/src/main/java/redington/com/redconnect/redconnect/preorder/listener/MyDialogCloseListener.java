package redington.com.redconnect.redconnect.preorder.listener;


import android.content.DialogInterface;

public interface MyDialogCloseListener {
    void handleDialogClose(DialogInterface dialog);

    void cartSize(int count);
}
